<?php


namespace app\models;


use core\base\Model;
use RedBeanPHP\RedException\SQL;

/**
 * @property int user_id
 * @property string message
 * @property string time
 */
class Chat extends Model
{

	protected static string $table = "adminchat";
	protected static string $parentClass = User::class;

	protected array $attributes = [
		'user_id' => '',
		'message' => '',
		'time' => ''
	];

	protected array $rules = [
		'required' => [
			['user_id'],
			['message']
		]
	];

	/**
	 * @throws SQL
	 */
	public function sendMessage(string $message = ""): bool
	{
		$data = array(
			"user_id" => $_SESSION['user']['id'],
			"message" => $message,
			"time" => getTimestamp()
		);
		if ($this->loadWithValidate($data)) {
			$this->save();
			return true;
		}
		$this->getErrors();
		return false;
	}


}
