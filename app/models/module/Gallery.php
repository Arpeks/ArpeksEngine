<?php

namespace app\models\module;


use core\base\Model;

/**
 * @property string title
 * @property string description
 * @property string img
 */
class Gallery extends Model
{

	protected static string $table = "gallery";

	protected array $attributes = [
		'title' => '',
		'description' => '',
		'img' => ''
	];

	protected array $rules = [
		'required' => [
			['title']
		]
	];

}
