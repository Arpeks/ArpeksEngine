<?php

namespace app\models\module;


use core\base\Model;

/**
 * @property int is_read
 * @property int is_trash
 * @property int is_to
 * @property string name
 * @property string email
 * @property string subject
 * @property string message
 * @property string time
 */
class Message extends Model
{

	protected static string $table = "contact";

	protected array $attributes = [
		'is_read' => '0',
		'is_trash' => '0',
		'is_to' => '0',
		'name' => '',
		'email' => '',
		'subject' => '',
		'message' => '',
		'time' => ''
	];

}
