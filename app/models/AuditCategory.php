<?php

namespace app\models;

use core\base\Model;

/**
 * @property string name
 */
class AuditCategory extends Model
{
	const int UNKNOWN = 1;
	const int USER_CONTROLLER = 2;
	const int AJAX_CONTROLLER = 3;
	const int BACKUP_CONTROLLER = 4;
	const int SETTINGS_CONTROLLER = 5;
	const int MODULE_CONTROLLER = 6;

	protected static string $table = "audit_category";
	protected static string $parentClass = Audit::class;

	protected array $attributes = [
		'name' => '',
	];

	protected array $rules = [
		'required' => [
			['name']
		]
	];

}
