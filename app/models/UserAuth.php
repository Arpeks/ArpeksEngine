<?php


namespace app\models;


use core\base\Model;

/**
 * @property int user_id
 * @property string secret
 * @property string time_act
 */
class UserAuth extends Model
{

	protected static string $table = "user_auth";
	protected static string $parentClass = User::class;

	protected array $attributes = [
		'user_id' => '',
		'secret' => '',
		'time_act' => ''
	];

	protected array $rules = [
		'required' => [
			['user_id'],
			['secret']
		]
	];

	public static function check2FA(int $user_id): bool
	{
		return !empty(self::getOne('`user_id` = ?', [$user_id]));
	}

}
