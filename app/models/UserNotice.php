<?php

namespace app\models;

use core\base\Model;

/**
 * @property int user_id
 * @property string notice
 */
class UserNotice extends Model
{
	protected static string $table = "user_notice";
	protected static string $parentClass = User::class;

	protected array $attributes = [
		'user_id' => '',
		'notice' => '',
	];

	protected array $rules = [
		'required' => [
			['user_id']
		]
	];

}
