<?php


namespace app\models;


use core\base\EngineException;
use core\base\Model;
use RedBeanPHP\RedException\SQL;

/**
 * @property int user_id
 * @property int category_id
 * @property string action
 * @property mixed action_before
 * @property mixed action_after
 * @property string time
 */
class Audit extends Model
{

	protected static string $table = "audit";
	protected static string $parentClass = User::class;

	protected array $attributes = [
		'user_id' => '',
		'category_id' => '',
		'action' => '',
		'action_before' => '',
		'action_after' => '',
		'time' => ''
	];

	protected array $rules = [
		'required' => [
			['user_id'],
			['action'],
			['action_before'],
			['action_after']
		]
	];

	/**
	 * Сохраняет действие пользователя в базу данных аудита
	 * @param string $action Действие пользователя
	 * @param int $category Идентификатор места, где произошло действие пользователя
	 * @param mixed|null $before До изменения
	 * @param mixed|null $after После изменения
	 * @param int $id Идентификатор пользователя
	 * @return boolean Флаг успешной записи
	 * @throws EngineException|SQL
	 * @since 232
	 */
	public function audit(string $action, int $category = 0, mixed $before = null, mixed $after = null, int $id = 0): bool
	{
		$id = ($id > 0) ? $id : $_SESSION['user']['id'];

		if (User::getCount('id = ?', [$id]) != 1) {
			throw new EngineException('User with id=' . $id . ' not found', EngineException::DATABASE_ERROR);
		}

		$data = array(
			"user_id" => $id,
			"action" => $action,
			"action_before" => serialize($before),
			"action_after" => serialize($after),
			"time" => getTimestamp()
		);

		if ($category > 1) {
			$data['category_id'] = $category;
		}

		if ($this->loadWithValidate($data)) {
			$this->save();
			return true;
		}

		$this->getErrors();
		return false;
	}

}
