<?php


namespace app\models;


use core\base\EngineException;
use core\base\Model;
use RedBeanPHP\OODBBean;
use RedBeanPHP\RedException\SQL;

/**
 * @property int user_id
 * @property string hash
 * @property string time_end
 */
class UserForgot extends Model
{

	protected static string $table = "user_forgot";
	protected static string $parentClass = User::class;

	protected array $attributes = [
		'user_id' => '',
		'hash' => '',
		'time_end' => ''
	];

	protected array $rules = [
		'required' => [
			['user_id'],
			['hash'],
			['time_end']
		]
	];

	/**
	 * @throws SQL
	 * @throws EngineException
	 */
	public static function sendForgotMail(OODBBean|User $user): bool
	{
		$forgot = new self;

		$temp = [
			"time_end" => time() + 86400,
			"user_id" => $user->getID(),
			"hash" => md5(print_r($user->getProperties(), true) . uniqid() . microtime())
		];

		if ($forgot->loadWithValidate($temp)) {
			$forgot->save();
		}

		if (isActiveSMTP()) {
			$url = ADMIN . "/user/forgot-password?hash=" . $temp['hash'];
			return sendMail("Восстановление пароля",
				"Была инициирована процедура восстановления пароля.
							Если это были не Вы - проигнорируйте это сообщение, иначе перейдите по ссылке ниже для получения нового пароля.
							<hr>
							<a href='$url' target='_blank'>$url</a>
							<hr>
							После авторизации смените пароль в настройках пользователя
							",
				$user->email);
		} else {
			$_SESSION['error'] = __('Your password was found in the cracked password database. Please contact your administrator to give you a new password.');
			return false;
		}


	}

}
