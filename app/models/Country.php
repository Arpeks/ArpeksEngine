<?php

namespace app\models;

use core\base\Model;

/**
 * @property string id2
 * @property string id3
 * @property string name
 * @property string name_ru
 * @property string code_phone
 * @property string domain
 */
class Country extends Model
{

	protected static string $table = "countries";

	protected array $attributes = [
		'id2' => '',
		'id3' => '',
		'name' => '',
		'name_ru' => '',
		'code_phone' => '',
		'domain' => ''
	];

	protected array $rules = [
		'required' => [
			['id2'],
			['id3'],
			['name'],
			['name_ru'],
			['code_phone'],
			['domain']
		]
	];

}
