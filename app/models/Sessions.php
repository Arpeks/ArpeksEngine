<?php


namespace app\models;


use core\base\Model;
use core\Cache;
use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;
use MaxMind\Db\Reader\InvalidDatabaseException;
use R;
use RedBeanPHP\OODBBean;
use RedBeanPHP\RedException\SQL;

/**
 * @property int user_id
 * @property int is_active
 * @property string ip
 * @property string country
 * @property string time
 * @property string session
 */
class Sessions extends Model
{

	protected static string $table = "user_session";
	protected static string $parentClass = User::class;

	protected array $attributes = [
		'user_id' => '',
		'is_active' => '1',
		'ip' => '',
		'country' => 'null',
		'time' => '',
		'session' => '{}'
	];

	protected array $rules = [
		'required' => [
			['user_id'],
			['ip'],
			['country'],
			['session']
		]
	];


	/**
	 * @throws SQL
	 */
	public static function active(): bool
	{
		$newSession = new self;
		$data = self::getGeoData();

		if ($newSession->loadWithValidate(json_decode($data, true))) {
			$id = $newSession->save();
			$key = "u" . $_SESSION['user']['id'] . "s" . $id;
			Cache::set("session/" . $key, ["id" => $id, "data" => $data], TIME_SAVE_PASSWORD);
			setcookie("auth_key", $key, time() + TIME_SAVE_PASSWORD, "/");
			return true;
		}
		return false;
	}

	public static function getGeoData(): string
	{
		$ip = getIP();
		$session['ip'] = $ip;

		try {
			$lang = "en";
			$reader = new Reader(CONF . '/GeoLite2-City.mmdb', [$lang]);
			$record = $reader->city($ip);

			$session['city'] = $record->city->name;
			$session['region'] = !empty($record->subdivisions[0]->names[$lang]) ? $record->subdivisions[0]->names[$lang] : "null";
			$session['country'] = $record->country->name;
			$session['countryCode'] = $record->country->isoCode;
			$session['continentName'] = $record->continent->name;
			$session['continentCode'] = $record->continent->code;
			$session['timezone'] = $record->location->timeZone;
		} catch (InvalidDatabaseException|AddressNotFoundException) {
			$session['city'] = "null";
			$session['region'] = "null";
			$session['country'] = "null";
			$session['countryCode'] = "null";
			$session['continentName'] = "null";
			$session['continentCode'] = "null";
			$session['timezone'] = "null";
		}

		$session['HTTP_USER_AGENT'] = (isset($_SESSION['HTTP_USER_AGENT'])) ? $_SESSION['HTTP_USER_AGENT'] : $_SERVER['HTTP_USER_AGENT'];

		if (User::checkAuth(false)) {
			$data = array(
				'user_id' => $_SESSION['user']['id'],
				'is_active' => '1',
				'ip' => $session['ip'],
				'country' => $session['country'] . " - " . $session['city'],
				'time' => getTimestamp(),
				'HTTP_USER_AGENT' => $session['HTTP_USER_AGENT'],
				'session' => json_encode($session)
			);
		} else {
			$data = $session;
		}

		return json_encode($data);
	}

	public static function check(string $sessionId): bool
	{
		$session = self::getCurrent($sessionId);

		if ($session instanceof OODBBean || $session instanceof Sessions) {
			if ($session->is_active) {
				return true;
			}
		}

		if (!empty($_SESSION['user'])) {
			Cache::deleteCache("session/" . $sessionId);
			setcookie("auth_key", "", 0, "/");
		}

		return false;
	}

	public static function getCurrent(string $sessionId = ""): OODBBean|static|null
	{
		if (empty($_COOKIE['auth_key']) && $sessionId != "") return null;
		if ($sessionId == "") $sessionId = $_COOKIE['auth_key'];
		$id = explode("s", $sessionId)[1];
		return self::getOne("`id` = ?", [$id]);
	}

	public static function deactivate(OODBBean|Sessions|null $session): bool
	{
		if ($session === null) return false;

		$id = $session->getID();
		$sess = self::getCurrent();
		if ($sess instanceof OODBBean) {
			$sess = $sess->getID();
		}

		try {
			$session->is_active = '0';
			R::store($session);
		} catch (SQL) {
			return false;
		} finally {
			if ($id == $sess) {
				Cache::deleteCache("session/" . $_COOKIE['auth_key']);
				setcookie("auth_key", "", 0, "/");
			}
		}
		return true;
	}

	public static function getAllActiveSession(int $user_id): array
	{
		return Sessions::getAll("`is_active` = 1 AND `user_id` = ?", [$user_id]);
	}

}
