<?php


namespace app\models;


use core\base\Model;

/**
 * @property int role_id
 * @property string name
 * @property string code
 */
class RolePermissions extends Model
{

	protected static string $table = "role_permissions";
	protected static string $parentClass = UserRole::class;

	protected array $attributes = [
		'role_id' => '',
		'name' => '',
		'code' => ''
	];

}
