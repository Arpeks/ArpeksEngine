<?php


namespace app\models;


use core\base\Model;
use RedBeanPHP\OODBBean;

/**
 * @property string name
 * @property int parent_id
 */
class UserRole extends Model
{

	protected static string $table = "role_user";

	protected array $attributes = [
		'name' => '',
		'parent_id' => '',
	];

	/**
	 * @param string $name Имя роли
	 * @return OODBBean|UserRole|NULL
	 */
	public static function getByName(string $name): OODBBean|UserRole|null
	{
		return self::getOne("`name` = ?", [$name]);
	}

	public static function getPermissionsByRoleID(int $idRole): array
	{
		$roles = self::getAllRolesByParentID($idRole);
		$retPermission = array();
		foreach ($roles as $id => $role) {
			$permissions = RolePermissions::getAll("`role_id` = ?", [$id]);
			foreach ($permissions as $idPermission => $permission) {
				$retPermission[$idPermission] = $permission->code;
			}
		}
		return $retPermission;
	}

	public static function getAllRolesByParentID(int $idRole): array
	{
		$roles = self::getAll();
		$newRoles = array();
		$return = array();

		/* @var OODBBean|UserRole $role */
		foreach ($roles as $role) {
			$newRoles[$role->getID()] = array(
				"name" => $role->name,
				"parent_id" => $role->parent_id
			);
		}

		self::deleteTreeElement($newRoles, $newRoles[$idRole]['parent_id'], $idRole);

		$list = $newRoles;
		foreach ($list as $id => $newRole) {
			if ($id !== $idRole && !isset($newRoles[$newRole['parent_id']]))
				unset($newRoles[$id]);
		}

		foreach ($newRoles as $id => $newRole) {
			$return[$id] = $newRole['name'];
		}

		return $return;
	}

	private static function deleteTreeElement(array &$tree, int $start_id, int $id): void
	{
		$newParentId = $tree[$start_id]['parent_id'];
		if ($id != $start_id) {
			if ($newParentId != $start_id) self::deleteTreeElement($tree, $newParentId, $id);
			unset($tree[$start_id]);
		} else if ($id != 1) {
			$ret[$start_id] = $tree[$start_id];
			$tree = $ret;
		}
	}

}
