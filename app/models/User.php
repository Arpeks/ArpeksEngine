<?php

namespace app\models;


use core\App;
use core\base\EngineException;
use core\base\Model;
use R;
use RedBeanPHP\OODBBean;
use RedBeanPHP\RedException\SQL;

/**
 * @property string nickname
 * @property string password
 * @property string name
 * @property string email
 * @property int role_id
 * @property string reg_date
 * @property string reg_ip
 * @property string last_ip
 * @property string last_action
 * @property int online
 * @property string img
 * @property int language_id
 */
class User extends Model
{

	protected static string $table = "user";
	protected static string $parentClass = UserRole::class;

	protected array $attributes = [
		'nickname' => '',
		'password' => '',
		'name' => '',
		'email' => '',
		'role_id' => '4',
		'reg_date' => '',
		'reg_ip' => '',
		'last_ip' => '',
		'last_action' => '',
		'online' => '0',
		'img' => 'default.png',
		'language_id' => '214'
	];
	protected array $rules = [
		'required' => [
			['email'],
			['password'],
			['name']
		],
		'email' => [
			['email']
		],
		'lengthMin' => [
			['password', 6]
		]
	];
	private User|OODBBean|null $userTemp = null;

	public static function getLanguage(): Country|OODBBean
	{
		if (!User::checkAuth() || !isset($_SESSION['user']['language_id'])) return Country::getByID(2);
		return Country::getByID($_SESSION['user']['language_id']);
	}

	public static function checkAuth(bool $cookie = true): bool
	{
		if ($cookie)
			return !empty($_COOKIE['auth_key']) && Sessions::check($_COOKIE['auth_key']) && isset($_SESSION['user']);
		else
			return isset($_SESSION['user']);
	}

	/**
	 * @throws SQL
	 */
	public static function logout(): void
	{
		$userObj = null;
		if (self::checkAuth()) {
			$userObj = self::getByID($_SESSION['user']['id']);
			$userObj->online = '0';
			R::store($userObj);
		}

		if (!empty($_COOKIE['auth_key'])) {
			$sess = explode("s", $_COOKIE['auth_key'])[1];
			Sessions::deactivate(Sessions::getOne("`id` = " . $sess));
		}

		unset($_SESSION['user'], $_SESSION['tfa_status']);
		doAction("user_logout", ['user' => $userObj]);
	}

	/**
	 * @throws SQL
	 */
	public static function updateLastAction(): bool
	{
		self::authBySession();

		if (self::checkAuth()) {
			$user = self::getByID($_SESSION['user']['id']);
			$user->last_action = getTimestamp();
			$user->last_ip = getIP();
			$user->online = 1;
			R::store($user);
			return true;
		}

		return false;
	}

	public static function authBySession(): void
	{
		if (!empty($_COOKIE['auth_key']) && !isset($_SESSION['user'])) {
			$uid = explode("s", $_COOKIE['auth_key'])[0];
			$user = self::getByID(str_replace("u", "", $uid));
			self::setUserSession($user);
		}
	}

	private static function setUserSession(OODBBean $user): void
	{
		$_SESSION['user'] = $user->getProperties();
		$_SESSION['user']['tfa'] = UserAuth::check2FA($user->getID());
		$_SESSION['tfa_status'] = (App::isWhiteListIP() || (isset($_SESSION87597['recaptcha']) && $_SESSION['recaptcha']['score'] >= 0.9)) ? "ok" : "none";
		$_SESSION['HTTP_USER_AGENT'] = $_SERVER['HTTP_USER_AGENT'];
		$_SESSION['user']['permissions'] = UserRole::getPermissionsByRoleID($_SESSION['user']['role_id']);
		unset($_SESSION['user']['password']);
	}

	public function checkUnique(): bool
	{
		$user = User::getOne('`email` = ? OR `nickname` = ?', [$this->attributes['email'], $this->attributes['nickname']]);
		if ($user) {
			$this->errors['unique'][] = "Этот email или логин уже занят";
			return false;
		}
		return true;
	}

	/**
	 * @throws SQL
	 * @throws EngineException
	 */
	public function login(): bool
	{
		$userRawNameSearch = "nickname";
		$login = (!empty(trim($_POST['login']))) ? trim($_POST['login']) : null;
		$password = (!empty(trim($_POST['password']))) ? trim($_POST['password']) : null;

		if (filter_var($login, FILTER_VALIDATE_EMAIL)) $userRawNameSearch = "email";

		if ($login && $password) {
			$user = User::getOne("`$userRawNameSearch` = ?", [$login]);
			$this->userTemp = $user;

			if ($user) {
				if (password_verify($password, $user->password)) {
					$checkPwnedPass = $this->checkPwnedPassword($password);

					if ($checkPwnedPass && !App::isWhiteListIP() && $_SESSION['recaptcha']['score'] < 0.8) {
						UserForgot::sendForgotMail($user);
						return true;
					}

					$user->online = '1';
					self::setUserSession($user);

					R::store($user);
					Sessions::active();
					doAction("user_login", ['isAdmin' => self::isAdmin(), 'user' => $user]);
					return true;
				}
			}
		}
		return false;
	}

	public function checkPwnedPassword(string $password): bool
	{
		$sha1Pass = strtoupper(sha1($password));
		$firstFiveChars = substr($sha1Pass, 0, 5);
		$sha1Pass = substr($sha1Pass, 5, strlen($sha1Pass) - 5);

		$url = "https://api.pwnedpasswords.com/range/" . $firstFiveChars;
		$curl = aeCurl($url);

		if ($curl['errno']) {
			$_SESSION['error'] = $curl['errmsg'];
			return true;
		}

		$content = explode(PHP_EOL, $curl['content']);

		foreach ($content as $value) {
			$hash = explode(":", $value)[0];
			if ($sha1Pass === $hash) {
                $_SESSION['error'] = __('Your password has been compromised. A message with a link to reset your password was sent to your e-mail.');
				return true;
			}
		}

		return false;
	}

	public static function isAdmin(): bool
	{
		return (self::checkAuth(false) && self::checkPermission("admin_panel_access"));
	}

	public static function checkPermission(string $code, OODBBean|User $user = null): bool
	{
		if (!isset($_SESSION['user']) || !isset($_SESSION['user']['id']))
			return false;
		$role_id = ($user instanceof OODBBean || $user instanceof User) ? $user->role_id : self::getByID($_SESSION['user']['id'])->role_id;
		$permissions = UserRole::getPermissionsByRoleID($role_id);
		return in_array($code, $permissions);
	}

	public function getUserTemp(): OODBBean|User|null
	{
		return $this->userTemp;
	}


}
