<?php

namespace app\models;


use core\base\Model;

/**
 * @property int is_close
 * @property string text
 * @property string time
 * @property string time_complete
 */
class Todo extends Model
{

	protected static string $table = "todolist";

	protected array $attributes = [
		'is_close' => '0',
		'text' => '',
		'time' => '',
		'time_complete' => ''
	];

}
