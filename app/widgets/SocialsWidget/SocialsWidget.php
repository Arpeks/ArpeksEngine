<?php

namespace app\widgets\SocialsWidget;


use core\base\Widgets;

class SocialsWidget extends Widgets
{

	private array $data = [
		[
			'url' => 'https://vk.com/arpeks.menethil',
			'fa-code' => 'vk'
		],
		[
			'url' => 'https://www.youtube.com/c/ArpeksMenethil',
			'fa-code' => 'youtube'
		],
		[
			'url' => 'https://www.instagram.com/Arpeks',
			'fa-code' => 'instagram'
		],
		[
			'url' => 'https://gitlab.com/Arpeks',
			'fa-code' => 'gitlab'
		]
	];

	public function __construct()
	{
		$this->setName(__CLASS__);
		$this->setData($this->data);
	}

}