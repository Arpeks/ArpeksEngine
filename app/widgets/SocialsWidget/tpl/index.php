<?php foreach ($data as $social): ?>
    <a href="<?= $social['url'] ?>" class="text-white fb-ic ml-0" rel="nofollow" target="_blank">
        <i class="fab fa-<?= $social['fa-code'] ?> white-text mr-4"></i>
    </a>
<?php endforeach; ?>