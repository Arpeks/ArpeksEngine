<div class="col-md-3">
    <a href="<?= ADMIN ?>/module/mailbox<?php if ($data['inbox'] == 1) {
		echo '/send';
	} ?>" class="btn btn-primary btn-block mb-3"><?php if ($data['inbox'] == 1) {
			echo __('Send', 'mailbox');
		} else {
			echo __('Back to Inbox', 'mailbox');
		} ?></a>
    <div class="card box-solid">
        <div class="card-header with-border">
            <h3 class="card-title"><?= __('Folders', 'mailbox') ?></h3>
        </div>
        <div class="card-body p-0">
            <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                    <a href="<?= ADMIN ?>/module/mailbox" class="nav-link">
                        <i class="fas fa-inbox"></i> <?= __('Read', 'mailbox') ?>
                        <span class="badge bg-primary float-right"><?= $data['messageCount'] ?></span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?= ADMIN ?>/module/mailbox/to" class="nav-link">
                        <i class="far fa-envelope"></i> <?= __('To', 'mailbox') ?>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?= ADMIN ?>/module/mailbox/trash" class="nav-link">
                        <i class="far fa-trash-alt"></i> <?= __('Trash', 'mailbox') ?>
                    </a>
                </li>
            </ul>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<!-- /.col -->