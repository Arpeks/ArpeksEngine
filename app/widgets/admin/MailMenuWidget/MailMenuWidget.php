<?php

namespace app\widgets\admin\MailMenuWidget;


use core\base\Widgets;

class MailMenuWidget extends Widgets
{

	public function __construct($messageCount, $inbox = 1)
	{
		$this->setName(__CLASS__);
		$this->setData(["messageCount" => $messageCount, "inbox" => $inbox]);
		$this->setAdmin(true);
	}

}