<?php

namespace app\widgets\admin\UserProfileWidget;


use core\base\Widgets;

class UserProfileWidget extends Widgets
{

	public function __construct($user)
	{
		$this->setData($user);
		$this->setName(__CLASS__);
		$this->setAdmin(true);
	}

}