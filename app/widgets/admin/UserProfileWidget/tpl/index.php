<?php

use app\models\{User, UserRole};

$data = $data[0];
$is_access = $data->id == $_SESSION['user']['id'] || User::checkPermission("admin_access_all");

?>
<div class="card card-primary">
    <div class="card-body box-profile">

        <div class="text-center">
            <img class="profile-user-img img-fluid img-circle <?= ($data->online) ? "on" : "off"; ?>line"
                 src="img/users/<?= $data->img; ?>" alt=""/>
        </div>

        <h3 class="profile-username text-center"><?= $data->name; ?></h3>

        <p class="text-muted text-center">
			<?= UserRole::getByID($data->role_id)->name ?>
        </p>

        <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>
        <p class="text-muted"><?= $data->email; ?></p>
        <hr>

        <strong><i class="fa fa-server margin-r-5"></i> <?= __('Registration IP') ?></strong>
        <p class="text-muted"><?= $data->reg_ip; ?></p>
        <hr>

        <strong><i class="fa fa-server margin-r-5"></i> <?= __('Last IP') ?></strong>
        <p class="text-muted"><?= $data->last_ip; ?></p>
        <hr>

        <strong><i class="fa fa-calendar margin-r-5"></i> <?= __('Registration date') ?></strong>
        <p class="text-muted"><?= date("d.m.Y H:i:s", strtotime($data->reg_date)); ?></p>
        <hr>

        <strong><i class="fa fa-calendar margin-r-5"></i> <?= __('Last action time') ?></strong>
        <p class="text-muted"><?= date("d.m.Y H:i:s", strtotime($data->last_action)); ?></p>

    </div>
</div>