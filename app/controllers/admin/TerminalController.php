<?php


namespace app\controllers\admin;

use core\MenuGenerator;
use Exception;
use JetBrains\PhpStorm\NoReturn;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;


class TerminalController extends AppController
{

	private static string $consoleKey = "admin-console";
	public bool $isActive = false;

	private array $color_text = array(
		"command" => "\033[1;32;49m ",
		"normal" => "\033[0m ",
		"warning" => "\033[1;33;49m ",
		"fatal" => "\033[1;31;49m ",
	);

	private array $format_text = array(
		"command" => "[CMD] >> ",
		"info" => "[INFO] >> ",
		"warning" => "[WARN] >> ",
		"fatal" => "[ERROR] >> ",
	);

	/**
	 * @route /admin/terminal
	 */
	public function indexAction(): void
	{
		$this->setMeta("terminal");
		$_SESSION[self::$consoleKey] = array();
	}

	public static function getKey(): string
	{
		return self::$consoleKey;
	}

	/**
	 * @route /admin/terminal/clear-session
	 */
	#[NoReturn]
	public function clearSessionAction(): void
	{
		if ($_COOKIE["ARPEKS_SEC"] == h($_GET['ASEC']))
			unset($_SESSION[self::$consoleKey]);
		exit;
	}

	/**
	 * @route /admin/terminal/ajax
	 */
	public function ajaxAction(): void
	{
		$tempPostData = json_decode($_POST['data'], true);
		$ret = array();

		$data = (empty($_SESSION[self::$consoleKey])) ? array() : $_SESSION[self::$consoleKey];
		$_SESSION[self::$consoleKey] = $tempPostData;

		if ($data == $tempPostData) {
			echo json_encode(['NO']);
			exit;
		}

		if ($_COOKIE["ARPEKS_SEC"] != h($_GET['ASEC'])) {
			echo json_encode([
				$this->color_text['fatal'] . $this->format_text['fatal'] . " Security key no validate!" . $this->color_text['normal'],
				"update" => "NO"
			]);
			exit;
		}

		foreach ($tempPostData as $j => $commandData) {
			if (!empty($data[$j]) && $data[$j] == $commandData) continue;
			$tmp = explode(" ", $commandData['command']);
			$tmp2 = array();
			foreach ($tmp as $i => $v) {
				if ($i === 0) $key = 'command';
				else $key = 'argc' . $i;
				$tmp2[$key] = h($v);
			}
			$input = new ArrayInput($tmp2);
			$output = new BufferedOutput();
			$time = date("[Y-m-d H:i:s] ", $commandData['time']);
			$ret[] = $this->color_text['command'] . $time . $this->format_text['command'] . $commandData['command'] . $this->color_text['normal'];

			try {
				if (!(isset($ret[$i]) && $ret[$i] == $commandData)) {
					$this->terminal->run($input, $output);
				}
			} catch (Exception $e) {
				$ret[] = $this->color_text['fatal'] . $time . $this->format_text['fatal'] . $e->getMessage() . $this->color_text['normal'];
				continue;
			}
			$content = $output->fetch();

			$contTemp = explode(PHP_EOL, $content);
			foreach ($contTemp as $item) {
				if (trim($item) == '') continue;
				$ret[] = $this->color_text['normal'] . $time . $this->format_text['info'] . $item . $this->color_text['normal'];
			}
		}
		echo json_encode($ret);
		exit;
	}

	static function addMenu(): void
	{
		MenuGenerator::addMenu(__('Terminal'), ADMIN . "/terminal", "fas fa-terminal");
	}
}
