<?php


namespace app\controllers\admin\module;


use app\controllers\admin\ModuleController;
use core\interfaces\Module;
use core\MenuGenerator;

class ExplorerController extends ModuleController implements Module
{

	public static array $explorer = array("back" => "", "folders" => [], "files" => []);
	protected static string $author = "Arpeks";
	protected static string $description = "Позволяет просматривать файлы на сервере";
	protected static bool $active = true;

	public static function setMenuModule(): void
	{
		MenuGenerator::addModulePage(__('File explorer', 'explorer'), ADMIN . "/module/explorer", "fas fa-folder");
	}

	public static function cachePage(): bool
	{
		return false;
	}

	public static function getFileList(): array
	{
		return array(
			APP_CONTROLLER . "/admin/module/" . getClassName(__CLASS__) . ".php",
			WWW_THEME . "/admin/template/module/explorer",
			WWW_THEME . "/admin/js/pages/modules/explorer.js"
		);
	}

	public static function getDatabaseTableName(): string|array
	{
		return [];
	}

	/**
	 * @route /admin/module/explorer
	 */
	public function indexAction(): void
	{
		if (!self::isModuleManagerActive('Explorer')) {
			$_SESSION['error'] = __("Module 'Explorer' disabled", 'explorer');
			redirect(ADMIN . "/module");
		}

		$path = (isset($_GET['path'])) ? rtrim($_GET['path'], "/") : "";
		$path = ROOT . "/" . str_replace("..", "", $path);

		if (!empty($path)) {
			$tmp = explode("/", $path);
			array_pop($tmp);
			$tmp = implode("/", $tmp);
			if ($tmp == "")
				$tmp = "/";
			self::$explorer['back'] = str_replace(ROOT, "", $tmp);
		} else
			self::$explorer['back'] = "/";

		if (!is_file($path)) {
			$scan = scandir($path);
			sort($scan);

			$i_1 = 0;
			$i_2 = 0;
			foreach ($scan as $f) {
				if (trim($f, ".") == "" || ($f[0] == '.' && $f != ".htaccess"))
					continue;
				if (is_dir("$path/$f")) {
					self::$explorer['folders'][$i_1]['file'] = trim($f, "/");
					self::$explorer['folders'][$i_1]['size'] = getSizeName(getFolderSize("$path/$f"));
					self::$explorer['folders'][$i_1]['path'] = trim(str_replace(ROOT, "", $path) . "/$f", "/");
					$i_1++;
				} else {
					self::$explorer['files'][$i_2]['file'] = trim($f, "/");
					self::$explorer['files'][$i_2]['size'] = getSizeName(getFileSize("$path/$f"));
					self::$explorer['files'][$i_2]['path'] = trim(str_replace(ROOT, "", $path) . "/$f", "/");
					$i_2++;
				}
			}
		}

		if (is_file($path) && in_array(getExtension($path), ['zip', 'tar', 'gz'])) {
			header('Content-Type: application/octet-stream');
			header("Content-Transfer-Encoding: Binary");
			header("Content-disposition: attachment; filename=\"" . basename($path) . "\"");
			readfile($path);
		}

		$files = array_merge(self::$explorer['folders'], self::$explorer['files']);
		$explorer = self::$explorer;

		$this->setScripts([
			PATH_ADMINLTE . "/plugins/codemirror/codemirror.js",
			PATH_ADMINLTE . "/plugins/codemirror/addon/mode/loadmode.js",
			PATH_ADMINLTE . "/plugins/codemirror/mode/meta.js",
		]);

		$this->setData(compact("explorer", "path", "files"));
		$this->setMeta(__('File explorer', 'explorer'));
	}
}
