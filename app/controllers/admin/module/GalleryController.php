<?php

namespace app\controllers\admin\module;


use app\controllers\admin\ModuleController;
use app\models\AuditCategory;
use app\models\module\Gallery;
use core\App;
use core\base\EngineException;
use core\Cache;
use core\interfaces\Module;
use core\libs\SimpleImage;
use core\MenuGenerator;
use JetBrains\PhpStorm\NoReturn;
use R;
use RedBeanPHP\RedException\SQL;

class GalleryController extends ModuleController implements Module
{

	protected static string $author = "Arpeks";
	protected static string $description = "Модуль галереи для сайта";
	protected static bool $active = true;

	/**
	 * @route /admin/gallery
	 */
	public function indexAction(): void
	{
		if (!self::isModuleManagerActive('Gallery')) {
			$_SESSION['error'] = __("Module 'Gallery' disabled", 'gallery');
			redirect(ADMIN . "/module");
		}

		$gallery = Gallery::getAll();
		$this->setMeta(__('Gallery', 'gallery'));
		$this->setData(compact('gallery'));
	}

	/**
	 * @throws SQL|EngineException
	 * @route /admin/gallery/add
	 */
	public function addAction(): void
	{
		if (!self::isModuleManagerActive('Gallery')) {
			$_SESSION['error'] = __("Module 'Gallery' disabled", 'gallery');
			redirect(ADMIN . "/module");
		}

		if (!empty($_POST)) {
			if (SECRET_KEY !== $_POST['secret_key']) {
				throw new EngineException("SECRET_KEY not valid", 604);
			}
			$gallery = new Gallery();

			$data = $_POST;
			$data['img'] = $_SESSION['gallery-image'];
			if ($gallery->loadWithValidate($data) && $id = $gallery->save()) {
				if (!empty($_SESSION['gallery-image'])) {
					$upload = uploadImage($_SESSION['gallery-image'], false, getThemePath(true) . "/img/gallery/");
					if (!$upload)
						$_SESSION['error'] = __('Upload image error', 'gallery');

					$image = new SimpleImage($upload);
					$image->convert();

					unset($_SESSION['gallery-image']);
				}
				$_SESSION['success'] = __('Post added', 'gallery');
				$tmp_name = clearSpace($data['title']);
				$this->audit->audit("Добавил новый фото-пост $tmp_name#$id", AuditCategory::MODULE_CONTROLLER);
			} else {
				$gallery->getErrors();
			}
			redirect(ADMIN . "/module/gallery");
		}
		$this->setMeta(__('Add post', 'gallery'));
	}

	/**
	 * @throws SQL|EngineException
	 * @route /admin/gallery/delete
	 */
	#[NoReturn] public function deleteAction(): void
	{
		if (!self::isModuleManagerActive('Gallery')) {
			$_SESSION['error'] = __("Module 'Gallery' disabled", 'gallery');
			redirect(ADMIN . "/module");
		}

		$id = $this->getRequestID();
		$gallery = Gallery::getByID($id);
		$image = $gallery->img;
		if (!$gallery) {
			$_SESSION['error'] = __('Post not found', 'gallery');
		} else {
			R::trash($gallery);
			$path = WWW_THEME . "/" . App::$app->get('layout') . "/img/gallery/" . $image;
			if (file_exists($path))
				unlink($path);

			$path = WWW_THEME . "/" . App::$app->get('layout') . "/img/gallery/" . getFileName($image) . ".webp";
			if (file_exists($path))
				unlink($path);

			$_SESSION['success'] = __('Post deleted', 'gallery');
			$tmp_name = clearSpace($gallery->title);
			$this->audit->audit("Удалил пост $tmp_name#$id", AuditCategory::MODULE_CONTROLLER, $gallery);
		}
		redirect(ADMIN . "/module/gallery");
	}

	/**
	 * @throws SQL|EngineException
	 * @route /admin/gallery/edit
	 */
	public function editAction(): void
	{
		if (!self::isModuleManagerActive('Gallery')) {
			$_SESSION['error'] = __("Module 'Gallery' disabled", 'gallery');
			redirect(ADMIN . "/module");
		}

		if (!empty($_POST)) {
			if (SECRET_KEY !== $_POST['secret_key']) {
				throw new EngineException("SECRET_KEY not valid", 604);
			}
			$id = $this->getRequestID(false);
			$galleryDB = Gallery::getByID($id);
			if (!empty($_SESSION['gallery-image']))
				$_POST['img'] = $_SESSION['gallery-image'];
			else
				$_POST['img'] = $galleryDB->img;
			$gallery = new Gallery();
			$data = $_POST;
			if (!$gallery->loadWithValidate($data)) {
				$gallery->getErrors();
				redirect();
			}

			if ($gallery->update($id)) {
				$_SESSION['success'] = __("Post updated", 'gallery');;
			}

			if (!empty($_SESSION['gallery-image'])) {
				$upload = uploadImage($galleryDB->img, $_SESSION['gallery-image'], getThemePath(true) . "/img/gallery/");
				if (!$upload)
					$_SESSION['error'] = __("Upload image error", 'gallery');
				$image = new SimpleImage($upload);
				$image->convert();
				unset($_SESSION['gallery-image']);
			}

			$this->audit->audit("Изменил данные фото-поста с ID:$id", AuditCategory::MODULE_CONTROLLER, $galleryDB, $gallery);
			redirect();
		}

		$gallery_id = $this->getRequestID();
		$gallery = Gallery::getByID($gallery_id);
		$this->setData(compact('gallery'));
		$this->setMeta(__('Edit post', 'gallery'));
	}

	/**
	 * @route /admin/gallery/add-image
	 */
	public function addImageAction(): void
	{
		$this->uploadImage("gallery", 1280, 800);
	}

	public static function setMenuModule(): void
	{
		MenuGenerator::addModulePage(__('Gallery', 'gallery'), ADMIN . "#", "fas fa-images", [
			["name" => __('Gallery', 'gallery'), "url" => ADMIN . "/module/gallery"],
			["name" => __('Add post', 'gallery'), "url" => ADMIN . "/module/gallery/add"]
		]);
	}

	public static function cachePage(): void
	{
		Cache::addCache(__('Gallery cache', 'gallery'), __('Cache for gallery', 'gallery'), "gallery");
	}

	public static function getFileList(): array
	{
		return array(
			APP_CONTROLLER . "/admin/module/" . getClassName(__CLASS__) . ".php",
			APP_MODEL . "/module/Gallery.php",
			WWW_THEME . "/admin/template/module/gallery",
			WWW_THEME . "/admin/js/pages/modules/gallery.js"
		);
	}

	public static function getDatabaseTableName(): string|array
	{
		return Gallery::getTable();
	}

}
