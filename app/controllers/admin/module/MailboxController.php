<?php

namespace app\controllers\admin\module;


use app\controllers\admin\ModuleController;
use app\models\AuditCategory;
use app\models\module\Message;
use app\widgets\admin\MailMenuWidget\MailMenuWidget;
use core\base\EngineException;
use core\Cache;
use core\interfaces\Module;
use core\MenuGenerator;
use Exception;
use JetBrains\PhpStorm\NoReturn;
use RedBeanPHP\OODBBean;
use RedBeanPHP\RedException\SQL;

class MailboxController extends ModuleController implements Module
{

	protected static string $author = "Arpeks";
	protected static string $description = "Модуль почты для сайта";
	protected static bool $active = true;

	private function getMailMenuPage($inbox): void
	{
		$messageCount = Message::getCount("is_read = 0 AND `is_trash` = 0 AND `is_to` = 0");
		$mailWidget = new MailMenuWidget($messageCount, $inbox);
		$this->setData(compact('mailWidget'));
	}

	/**
	 * @route /admin/module/mailbox
	 */
	public function indexAction($sql = "`is_trash` = 0 AND `is_to` = 0"): void
	{
		if (!self::isModuleManagerActive('Mailbox')) {
			$_SESSION['error'] = __("Module 'Mailbox' disabled", 'mailbox');
			redirect(ADMIN . "/module");
		}

		$count = Message::getCount($sql);
		$messagesObj = array_reverse(Message::getAll($sql));

		$messages = [];
		for ($i = 0; $i < $count; $i++) {
			if (!isset($messagesObj[$i])) continue;
			$messages[$i] = hArr($messagesObj[$i]);
		}

		$this->getMailMenuPage(1);
		$this->setMeta(__("Mailbox", 'mailbox'));
		$this->setData(compact('messages'));
	}

	/**
	 * @throws SQL|EngineException
	 * @route /admin/module/mailbox/delete
	 */
	public function deleteAction(): void
	{
		if (!self::isModuleManagerActive('Mailbox')) {
			$_SESSION['error'] = __("Module 'Mailbox' disabled", 'mailbox');
			redirect(ADMIN . "/module");
		}

		if (SECRET_KEY !== $_POST['secret_key']) {
			throw new EngineException("SECRET_KEY not valid", 604);
		}
		$id = $this->getRequestID(false);

		if ($id == -1) {
			$finds = Message::getAll("`is_trash` = 1");
			$tmp = $finds;
			foreach ($finds as $find) {
				\R::trash($find);
			}

			Cache::deleteCache("menu");
			$_SESSION['success'] = __("All messages deleted", 'mailbox');
			$this->audit->audit("Очистил корзину", AuditCategory::MODULE_CONTROLLER, $tmp);
			redirect(ADMIN . "/module/mailbox/trash");
		}
		$message = Message::getByID($id);

		if ($message->is_trash == 1) {
			$this->audit->audit("Удалил письмо с темой: `{$message->subject}`", AuditCategory::MODULE_CONTROLLER, $message);
			\R::trash($message);
		} else {
			$message->is_trash = 1;
			\R::store($message);
			$_SESSION['success'] = __("Message deleted", 'mailbox');
		}
		Cache::deleteCache("menu");
		redirect(ADMIN . "/module/mailbox/trash");
	}

	/**
	 * @throws SQL|EngineException
	 * @route /admin/module/mailbox/delete-checkboxes
	 */
	public function deleteCheckboxesAction(): void
	{
		if (!self::isModuleManagerActive('Mailbox')) {
			$_SESSION['error'] = __("Module 'Mailbox' disabled", 'mailbox');
			redirect(ADMIN . "/module");
		}

		if (SECRET_KEY !== $_POST['secret_key']) {
			throw new EngineException("SECRET_KEY not valid", 604);
		}
		$ids = array();
		foreach ($_POST as $id => $item) {
			if (is_int($id))
				$ids[] = $id;
		}

		if (isset($_POST['return'])) {
			$this->returnMessage($ids);
		}
		if (isset($_POST['delete'])) {
			$count = count($ids);
			if ($this->getRequestID(false) == -1) {
				$finds = Message::getAll("`id` = " . implode(" OR `id` = ", $ids));

				/* @var OODBBean $find */
				foreach ($finds as $find) {
					$this->audit->audit("Удалил письмо с темой: `{$find->subject}`", AuditCategory::MODULE_CONTROLLER, $find);
					\R::trash($find);
				}
				if ($count > 1) {
					$_SESSION['success'] = __("All messages deleted", 'mailbox');
				} else {
					$_SESSION['success'] = __("Message deleted", 'mailbox');
				}

				Cache::deleteCache("menu");
				redirect(ADMIN . "/mailbox/trash");
			}

			foreach ($ids as $id) {
				$message = Message::getByID($id);
				$message->is_trash = 1;
				\R::store($message);
				if ($count > 1) {
					$_SESSION['success'] = __("All messages moved to trash", 'mailbox');
				} else {
					$_SESSION['success'] = __("Message moved to trash", 'mailbox');
				}
			}
			Cache::deleteCache("menu");
		}
		redirect();
	}

	/**
	 * @route /admin/module/mailbox/trash
	 */
	public function trashAction(): void
	{
		if (!self::isModuleManagerActive('Mailbox')) {
			$_SESSION['error'] = __("Module 'Mailbox' disabled", 'mailbox');
			redirect(ADMIN . "/module");
		}

		$this->indexAction("`is_trash` = 1");
		$this->getMailMenuPage(2);
		$this->setMeta(__("Trash", 'mailbox'));
	}

	/**
	 * @route /admin/module/mailbox/to
	 */
	public function toAction(): void
	{
		if (!self::isModuleManagerActive('Mailbox')) {
			$_SESSION['error'] = __("Module 'Mailbox' disabled", 'mailbox');
			redirect(ADMIN . "/module");
		}

		$this->indexAction("`is_to` = 1 AND `is_trash` = 0");
		$this->getMailMenuPage(2);
		$this->setMeta(__("To", 'mailbox'));
	}

	/**
	 * @throws Exception
	 * @route /admin/module/mailbox/read
	 */
	public function readAction(): void
	{
		if (!self::isModuleManagerActive('Mailbox')) {
			$_SESSION['error'] = __("Module 'Mailbox' disabled", 'mailbox');
			redirect(ADMIN . "/module");
		}

		$id = $this->getRequestID();
		$message = Message::getByID($id);

		if ($message->is_read == '0') {
			$message->is_read = '1';
			\R::store($message);

			$messageCount1 = Message::getCount("is_read = '0'");
			$messageCount = $messageCount1;
			$this->setData(compact('messageCount'));
		}

		$message->message = h($message->message, $this->allowTags);
		$message->subject = h($message->subject);
		$message->email = h($message->email);
		$message->time = changeDateFormat($message->time);

		$this->getMailMenuPage(2);
		$this->setData(compact(['message']));
		$this->setMeta(__("Read", 'mailbox'));
		Cache::deleteCache("menu");
	}

	/**
	 * @throws EngineException|SQL
	 * @route /admin/module/mailbox/send
	 */
	public function sendAction(): void
	{
		if (!self::isModuleManagerActive('Mailbox')) {
			$_SESSION['error'] = __("Module 'Mailbox' disabled", 'mailbox');
			redirect(ADMIN . "/module");
		}

		if (!empty($_POST)) {
			if (SECRET_KEY !== $_POST['secret_key']) {
				throw new EngineException("SECRET_KEY not valid", 604);
			}
			$mess = new Message();
			$data = $_POST;
			$data['email'] = $_POST['to'];
			$data['is_to'] = 1;
			$data['is_read'] = 1;
			$data['name'] = $this->app['admin_email'];
			$data['time'] = getTimestamp();
			if ($mess->loadWithValidate($data)) {
				if ($mess->save() && sendMail($data['subject'], $data['message'], $data['to'], true)) {
					$_SESSION['success'] = __("Message sent", 'mailbox');
					$this->audit->audit("Отправил сообщение на тему: `" . $data['subject'] . "` на почту: " . $data['to'], AuditCategory::MODULE_CONTROLLER, $data);
				} else {
					$_SESSION['error'] = __("Message not sent", 'mailbox');
					$this->audit->audit("Неудачная попытка отправки сообщения на тему: `" . $data['subject'] . "` на почту: " . $data['to'], AuditCategory::MODULE_CONTROLLER, $data);
				}
			} else $mess->getErrors();

			Cache::deleteCache("menu");
			redirect(ADMIN . "/module/mailbox/to");
		}

		$this->setMeta(__("Send", 'mailbox'));
		$this->getMailMenuPage(2);
	}

	/**
	 * @throws SQL|EngineException
	 */
	#[NoReturn] private function returnMessage(array $ids): void
	{
		if (!self::isModuleManagerActive('Mailbox')) {
			$_SESSION['error'] = __("Module 'Mailbox' disabled", 'mailbox');
			redirect(ADMIN . "/module");
		}

		$count = count($ids);
		$finds = Message::getAll("`id` = " . implode(" OR `id` = ", $ids));

		/* @var OODBBean $mess */
		foreach ($finds as $mess) {
			$this->audit->audit("Восстановил письмо с темой: `{$mess->subject}`", AuditCategory::MODULE_CONTROLLER, $mess);
			$mess->is_trash = '0';
			\R::store($mess);
		}
		if ($count > 1) {
			$_SESSION['success'] = __("Messages restored", 'mailbox') . ":$count";
		} else {
			$_SESSION['success'] = __("Message restored", 'mailbox');
		}

		Cache::deleteCache("menu");
		redirect(ADMIN . "/module/mailbox/trash");
	}

	static function setMenuModule(): void
	{
		$messageCount = Message::getCount("is_read = '0' AND `is_trash` = '0'");
		MenuGenerator::addModulePage(__("Mailbox", 'mailbox'), ADMIN . "#", "fa fa-envelope", [
			["name" => __("Read", 'mailbox'), "url" => ADMIN . "/module/mailbox", "icon" => "fas fa-book-reader"],
			["name" => __("Send", 'mailbox'), "url" => ADMIN . "/module/mailbox/send", "icon" => "fas fa-share"]
		], [
			["color" => "#00a65a", "class" => "menu-count-message", "text" => $messageCount],
		]);
	}

	public static function cachePage(): bool
	{
		return false;
	}

	public static function getFileList(): array
	{
		return array(
			__FILE__,
			APP_MODEL . "/module/Message.php",
			WWW_THEME . "/admin/template/module/mailbox",
			LANG . "/modules/Mailbox",
			WWW_THEME . "/admin/js/pages/modules/mailbox.js"
		);
	}

	public static function getDatabaseTableName(): string|array
	{
		return Message::getTable();
	}
}
