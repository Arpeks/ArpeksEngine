<?php

namespace app\controllers\admin;


use app\models\AuditCategory;
use app\models\Country;
use core\{base\EngineException, Cache, ErrorHandler, interfaces\Module, MenuGenerator};
use JetBrains\PhpStorm\NoReturn;
use RedBeanPHP\RedException\SQL;

class SettingsController extends AppController
{

	static function addMenu(): void
	{
		MenuGenerator::addMenu(__('Settings'), ADMIN . "#", "fa fa-cogs", [
			["name" => __('Settings'), "url" => ADMIN . "/settings/global", "icon" => "fas fa-cog"],
			["name" => __('Theme Editor'), "url" => ADMIN . "/settings/theme-editor", "icon" => "fas fa-edit"],
			["name" => __('Cache'), "url" => ADMIN . "/settings/cache", "icon" => "fas fa-sd-card"]
		]);
	}

	/**
	 * @throws SQL|EngineException
	 * @route /admin/settings/theme-editor
	 */
	public function themeEditorAction(): void
	{
		$layout = $this->app['layout'];
		$view = str_replace("\\", "/", WWW_THEME . "/$layout");

		$files = array(
			"style.css" => [
				"description" => "Таблица стилей",
				"path" => $view . "/css/style.css",
				"prefix" => "css/"
			],
			"variable.css" => [
				"description" => "Переменные используемые в таблице стилей",
				"path" => $view . "/css/variable.css",
				"prefix" => "css/"
			],
			"media.css" => [
				"description" => "Адаптация стилей под мобильные устройства",
				"path" => $view . "/css/media.css",
				"prefix" => "css/"
			],
			"functions.php" => [
				"description" => "Функции темы",
				"path" => $view . "/functions.php"
			],
			"common.js" => [
				"description" => "JavaScript файл",
				"path" => $view . "/js/common.js",
				"prefix" => "js/"
			],
			"header.php" => [
				"description" => "Заголовок",
				"path" => $view . "/template/header.php"
			],
			"footer.php" => [
				"description" => "Подвал",
				"path" => $view . "/template/footer.php"
			],
			"techworks.php" => [
				"description" => "Шаблон технических работ",
				"path" => $view . "/template/techworks.php"
			],
			"mail.php" => [
				"description" => "Шаблон письма",
				"path" => $view . "/template/mail.php"
			],
			"errors.php" => [
				"description" => "Шаблон вывода ошибок",
				"path" => $view . "/template/errors.php"
			],
			"separator-1" => true
		);

		foreach ($files as $file => $param) {
			if ((!is_array($param) && !$param) || (is_array($param) && !file_exists($param['path']))) {
				unset($files[$file]);
			}
		}

		generateThemeListFiles($view . "/template", $files);

		if (!empty($_POST)) {
			$data = $_POST;
			if (!hash_equals($_SESSION['csrf'], $data['csrf'] ?? '')) {
				$_SESSION['error'] = __('Token CSRF not valid!');
				ErrorHandler::errorHandler(new EngineException('Token CSRF not valid!', EngineException::CSRF_NOT_VALID), false);
				redirect(ADMIN . "/settings/cache");
			}
			$text = htmlspecialchars_decode($data['text']);
			$filename = str_replace($view, "", $data['file']);
			$file = $files[$filename]['path'];

			$before = null;
			$after = $text;

			if (file_exists($file)) {
				$fp = fopen($file, "r");
				$before = fread($fp, filesize($file));
				fclose($fp);

				$fp = fopen($file, "w");
				fwrite($fp, $text);
				fclose($fp);
				$encode = ["response" => __('File successfully edited'), 'type' => 'info'];
			} else $encode = ["response" => __('File write error'), 'type' => 'error'];

			if ($encode['type'] == 'info')
				$this->audit->audit("Успешное редактирование файла: " . basename($file), AuditCategory::SETTINGS_CONTROLLER, $before, $after);
			else
				$this->audit->audit("Ошибка при редактировании файла: " . basename($file), AuditCategory::SETTINGS_CONTROLLER, $before, $after);

			echo json_encode($encode, JSON_UNESCAPED_UNICODE);
			exit;
		}


		$get = (!empty($_GET['file']) && isset($files[$_GET['file']])) ? $_GET['file'] : "style.css";

		$mode = getExtension($get);
		if ($mode == 'js') {
			$mode = 'text/javascript';
		}

		$filepath = str_replace(DIRECTORY_SEPARATOR, "/", $files[$get]['path']);
		$tempRoot = str_replace(DIRECTORY_SEPARATOR, "/", ROOT);
		$ajax = array(
			"mode" => $mode,
			"filename" => $get,
			"filepath" => str_replace($tempRoot, "", $filepath)
		);

		if (!empty($_GET['ajax'])) {
			$this->isView = false;
			echo json_encode($ajax);
			return;
		}

		foreach ($files as $filename => $file) {
			if ((!is_array($file) && !$file) || (is_array($file) && !file_exists($file['path']))) {
				$fp = fopen($file['path'], "w");
				fwrite($fp, "");
				fclose($fp);
			}
		}

		$this->setScripts([
			PATH_ADMINLTE . "/plugins/codemirror/codemirror.js",
			PATH_ADMINLTE . "/plugins/codemirror/addon/mode/loadmode.js",
			PATH_ADMINLTE . "/plugins/codemirror/mode/meta.js",
			PATH_ADMINLTE . "/plugins/codemirror/addon/mode/simple.js",
			PATH_ADMINLTE . "/plugins/codemirror/addon/fold/xml-fold.js",
			PATH_ADMINLTE . "/plugins/codemirror/addon/comment/continuecomment.js",
			PATH_ADMINLTE . "/plugins/codemirror/addon/comment/comment.js",
			PATH_ADMINLTE . "/plugins/codemirror/addon/edit/closebrackets.js",
			PATH_ADMINLTE . "/plugins/codemirror/addon/edit/closetag.js",
			PATH_ADMINLTE . "/plugins/codemirror/addon/edit/continuelist.js",
			PATH_ADMINLTE . "/plugins/codemirror/addon/edit/matchbrackets.js",
			PATH_ADMINLTE . "/plugins/codemirror/addon/edit/matchtags.js",
			PATH_ADMINLTE . "/plugins/codemirror/addon/edit/trailingspace.js",
		]);

		$this->setData(compact("files", "layout", 'ajax'));
		$this->setMeta('Theme Editor');
	}

	public function globalAction(): void
	{
		$countries = Country::getAll("ORDER BY code_phone");
		$domain = Country::getOne("domain = ?", [$this->app['phone_country_code']])->domain;

		$this->setMeta('Settings');
		$this->setData(compact("countries", "domain"));
	}

	/**
	 * @route /admin/settings
	 */
	#[NoReturn] public function indexAction(): void
	{
		redirect(ADMIN . "/settings/global");
	}

	/**
	 * @route /admin/settings/cache
	 */
	public function cacheAction(): void
	{
		$namespaceModule = 'app\\controllers\\admin\\module\\';
		foreach (ModuleController::getActiveModules() as $module) {
			if (empty($module)) continue;
			/* @var Module $class */
			$class = $namespaceModule . $module . "Controller";
			$class::cachePage();
		}

		$this->setMeta('Cache');
		$cacheList = Cache::getSize();
		$allSize = 0;

		foreach ($cacheList as $i => $cache) {
			if ($i === 0 || !is_numeric($cache['size_byte'])) continue;
			$allSize += $cache['size_byte'];
		}
		$allSize = getSizeName($allSize);

		$this->setData(compact('cacheList', 'allSize'));
	}

	/**
	 * @throws EngineException|SQL
	 * @route /admin/settings/cache-delete
	 */
	#[NoReturn] public function cacheDeleteAction(): void
	{
		$data = array_merge([], $_POST, $_GET);
		if (isset($data['key']))
			$key = $data['key'];
		else {
			$_SESSION['error'] = __('Key not found');
			redirect(ADMIN . "/settings/cache");
		}

		$data = hArr($data);
		if (!hash_equals($_SESSION['csrf'], $data['csrf'] ?? '')) {
			$_SESSION['error'] = __('Token CSRF not valid!');
			ErrorHandler::errorHandler(new EngineException('Token CSRF not valid!', EngineException::CSRF_NOT_VALID), false);
			redirect(ADMIN . "/settings/cache");
		}

		$before = null;
		if ($key != "all") {
			$before = Cache::get($key);
			Cache::deleteCache($key);
		}
		$return = "";

		switch ($key) {
			case "all":
				Cache::deleteAll();
				$return = __('All cache cleared');
				break;
			case "tmp-images":
				$return = __('Temporary image folder cleared');
				break;
			case "cms_error_log":
				$_SESSION['success'] = __('Log error cleared');
				redirect(ADMIN . "/info/error");
			case "cms_error_log_ip":
				$_SESSION['success'] = __('Log IP cleared');
				redirect(ADMIN . "/info/block-ip");
			case "curl":
				$return = __('Curl cache cleared');
				break;
			default:
				$return = __('Cache cleared');
		}

		$_SESSION['success'] = $return;

		$this->audit->audit("Очистил кеш используя кеш-ключ: $key", AuditCategory::SETTINGS_CONTROLLER, $before);
		redirect(ADMIN . "/settings/cache");
	}
}
