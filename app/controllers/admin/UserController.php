<?php

namespace app\controllers\admin;

use app\models\AuditCategory;
use app\models\User;
use app\models\UserAuth;
use app\models\UserForgot;
use app\models\UserRole;
use core\base\EngineException;
use core\MenuGenerator;
use GoogleAuthenticator;
use JetBrains\PhpStorm\NoReturn;
use R;
use RedBeanPHP\RedException\SQL;

class UserController extends AppController
{

	protected string $userLayout = WWW_THEME . "/admin/template/auth.php";

	/**
	 * @throws EngineException|SQL
	 * @route /admin/user/forgot-password
	 */
	public function forgotPasswordAction(): void
	{
		$forgot = UserForgot::getOne("`hash` = ?", [$_GET['hash']]);
		if (!$forgot) {
			$_SESSION['error'] = __("Hash is not transmitted correctly");
			redirect(ADMIN . "/user/login-admin");
		}
		if ($forgot->time_end < time()) {
			$_SESSION['error'] = __("The link time has expired, repeat the password recovery procedure");
			redirect(ADMIN . "/user/login-admin");
		}
		$user = User::getByID($forgot->user_id);
		if (!$user) {
			$_SESSION['error'] = __("The user with this hash has been blocked");
			redirect(ADMIN . "/user/login-admin");
		}
		$newPassword = generatePassword();
		$newPasswordHash = password_hash($newPassword, PASSWORD_DEFAULT);

		$userModel = new User();
		$userModel->loadWithValidate(array_merge([], $user->getProperties(), ["password" => $newPasswordHash]));
		$userModel->update($user->getID());

		$this->audit->audit("Успешное восстановление пароль с IP: " . getIP(), AuditCategory::USER_CONTROLLER, $user->getProperties(), $userModel->getProperties(), $user->getID());
		R::trash($forgot);

		$this->setData(compact("newPassword"));
		$this->setLayout('admin');
	}

	/**
	 * @throws SQL|EngineException
	 * @route /admin/user/forgot-user
	 */
	public function forgotUserAction(): void
	{
		if (!isActiveSMTP()) {
			$_SESSION['error'] = __("Password recovery is disabled on this site. Ask the administrator to give you a new password.");
			redirect(ADMIN . "/user/login-admin");
		}

		if (!empty($_POST)) {
			$user = User::getOne('`email` = ?', [$_POST['email']]);
			if (!$user) {
				$_SESSION['error'] = __("No user with this data was found");
				redirect();
			}

			if (UserForgot::sendForgotMail($user)) {
				$_SESSION['success'] = __("The letter has been sent.");
				redirect(ADMIN . "/user/login-admin");
			}
		}

		$this->setLayout('admin');
	}

	/**
	 * @throws EngineException|SQL
	 * @route /admin/user/tfa
	 */
	public function tfaAction(): void
	{
		if (!empty($_POST)) {
			$_POST = hArr($_POST);
			if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) {
				echo json_encode([
					"error" => __('Token CSRF not valid!'),
					'csrf' => false
				]);
				die;
			}
			$postCode = $_POST['code'];
			$ga = new GoogleAuthenticator();
			$user = User::getByID($_SESSION['user']['id']);
			$auth = UserAuth::getOne('`user_id` = ?', [$user->getID()]);
			$code = $ga->getCode($auth->secret);
			if ($code == $postCode) {
				$_SESSION['tfa_status'] = "ok";
				$this->audit->audit("Пройдена двухэтапная аутентификация с IP: " . getIP(), AuditCategory::USER_CONTROLLER);
				echo json_encode(['OK']);
			} else {
				$this->audit->audit("Не удачная попытка пройти двухэтапную аутентификацию с IP: " . getIP(), AuditCategory::USER_CONTROLLER);
				echo json_encode(['ERROR', "Неверный код доступа"]);
			}
			exit;
		}

		$this->setLayout('admin');
	}

	/**
	 * @throws EngineException|SQL
	 * @route /admin/user/login-admin
	 */
	public function loginAdminAction(): void
	{
		if (!empty($_POST)) {
			$user = new User();

			if (!checkReCaptcha($_POST['recaptcha_response'])) {
				echo __("Captcha not valid");
				redirect(ADMIN . "/user/login-admin");
			}

			if ($user->login() && User::isAdmin()) {
				$this->audit->audit("Успешная авторизация с IP: " . getIP(), AuditCategory::USER_CONTROLLER);
				redirect(ADMIN);
			} else {
				if (isset($_COOKIE['auth_key'])) {
					$_SESSION['error'] = __("No user with this data was found");
                    if (isset($user->getUserTemp()->id))
                        $this->audit->audit("Не удачная авторизация с IP: " . getIP(), AuditCategory::USER_CONTROLLER, null, null, $user->getUserTemp()->id);
				} else {
					$_SESSION['error'] = __("Session time has expired. Re-authorize.");
                    if (isset($user->getUserTemp()->id))
                        $this->audit->audit("Время сессии истекло. Повторите авторизацию", AuditCategory::USER_CONTROLLER, null, null, $user->getUserTemp()->id);
				}
				redirect(ADMIN . "/user/login-admin");
			}
		}

		if (isset($_COOKIE['auth_key']))
			if (User::isAdmin())
				redirect(ADMIN);
			else
				User::logout();

		$this->setLayout('admin');
	}

	/**
	 * @route /admin/user/add
	 */
	public function addAction(): void
	{
		$this->setMeta('Add user');
		$roles = UserRole::getAll();

		$this->setData(compact('roles'));
		$this->userLayout = false;
	}

	/**
	 * @route /admin/user
	 */
	public function indexAction(): void
	{
		$users = User::getAll();
		$roles = UserRole::getAll();

		$this->userLayout = false;
		$this->setMeta("Users list");
		$this->setData(compact('users', 'roles'));
	}

	/**
	 * @throws EngineException|SQL
	 * @route /admin/user/delete
	 */
	#[NoReturn] public function deleteAction(): void
	{
		$id = $this->getRequestID();
		$user = User::getByID($id);

		if ($user->getID() == $_SESSION['user']['id']) {
			$_SESSION['error'] = __("You can't block yourself.");
			redirect();
		}

		$currRole = UserRole::getByID($user->role_id);
		$roleBanned = UserRole::getOne('`name` = ?', ['banned']);
		$roleUser = UserRole::getOne('`name` = ?', ['user']);

		if ($currRole->name === 'root') {
			$_SESSION['error'] = __("ROOT user cannot be blocked.");
			redirect();
		}

		$banned_id = match ($currRole->name) {
			"banned" => $roleUser->getID(),
			default => $roleBanned->getID(),
		};

		if ($banned_id < 1) {
			$_SESSION['error'] = __("Role `banned` not found.");
			redirect();
		}

		$newUser = new User();
		$newUser->loadWithValidate(array_merge([], $user->getProperties(), ['role_id' => $banned_id]));

		if ($newUser->update($user->getID())) {
			if ($banned_id == $roleBanned->getID()) {
				$_SESSION['success'] = __("User blocked.");
				$this->audit->audit("Заблокировал пользователя: " . $user->name . "#" . $user->getID(), AuditCategory::USER_CONTROLLER, $user->getProperties(), $newUser->getProperties());
				$this->audit->audit("Был заблокирован пользователем: " . $_SESSION['user']['name'] . "#" . $_SESSION['user']['id'], AuditCategory::USER_CONTROLLER, $user->getProperties(), $newUser->getProperties(), $user->getID());
			} else {
				$_SESSION['success'] = __("User unlocked.");
				$this->audit->audit("Разблокировал пользователя: " . $user->name . "#" . $user->getID(), AuditCategory::USER_CONTROLLER, $user->getProperties(), $newUser->getProperties());
				$this->audit->audit("Был разблокирован пользователем: " . $_SESSION['user']['name'] . "#" . $_SESSION['user']['id'], AuditCategory::USER_CONTROLLER, $user->getProperties(), $newUser->getProperties(), $user->getID());
			}

		}

		redirect(ADMIN . "/user");
	}

	/**
	 * @throws EngineException|SQL
	 * @route /admin/user/edit
	 */
	public function editAction(): void
	{
		if (!empty($_POST)) {
			if (SECRET_KEY !== $_POST['secret_key']) {
				throw new EngineException("SECRET_KEY not valid", 604);
			}
			$id = $this->getRequestID(false);
			$userDB = User::getByID($id);
			$newData = $_POST;

			$filter = true;
			$filterArray = applyFilter('user_edit', ['id' => $id, 'user' => $userDB, "new_data" => $newData]);
			if (!$filterArray) $filterArray = array();
			foreach ($filterArray as $sf) {
				if (!$sf) {
					$filter = false;
					break;
				}
			}

			if (!$filter) {
				$_SESSION['error'] = __("Filter error 'user_edit'");
				redirect();
			}

			if (!empty($_SESSION['profile-image'])) {
				$filename = $_SESSION['profile-image'];
				$ext = explode(".", $filename)[1];
				$newData['img'] = "$id.$ext";
			} else $newData['img'] = $userDB->img;

			$newData['name'] = clearSpace($newData['name']);
			$newData['surname'] = clearSpace($newData['surname']);
			$newData['name'] = $newData['surname'] . " " . $newData['name'];
			$tmp_name = $newData['name'];
			unset($newData['surname']);
			$tmp = array();

			$user = new User();
			$data = $newData;
			if (empty($data['password'])) unset($data['password']);


			$user->load(array_merge([], $userDB->getProperties(), $data));
			if (!empty($data['password'])) {
				$tmp['password'] = password_hash($user->getProperty('password'), PASSWORD_DEFAULT);
				$user->loadWithValidate($tmp);
			}
			if (!$user->validate() || ($data['email'] != $user->getProperty('email') && !$user->checkUnique())) {
				$user->getErrors();
				redirect();
			}

			if ($user->update($id)) {
				$_SESSION['success'] = __('Changes have been saved');
			}

			if (!empty($_SESSION['profile-image'])) {
				$newFilename = $data['img'];
				$pathTmp = WWW_THEME . '/admin/img/users/' . $userDB->img;
				if ($userDB->img != "default.png" && file_exists($pathTmp)) unlink($pathTmp);
				if (copy(WWW . '/tmp/' . $filename . ".cache", WWW_THEME . "/admin/img/users/$newFilename")) {
					unlink(WWW . '/tmp/' . $filename . ".cache");
				}

				unset($_SESSION['profile-image']);
			}

			$this->audit->audit("Изменил данные пользователя $tmp_name#$id", AuditCategory::USER_CONTROLLER, $userDB->getProperties(), $user->getProperties());
			redirect();
		}

		$this->userLayout = false;
		redirect(ADMIN . "/profile");
	}

	/**
	 * @throws SQL|EngineException
	 * @route /admin/user/signup
	 */
	public function signupAction(): void
	{
		if (empty($_POST)) redirect();
		if (SECRET_KEY !== $_POST['secret_key']) {
			throw new EngineException("SECRET_KEY not valid", 604);
		}

		$_POST['name'] = clearSpace($_POST['name']);
		$_POST['surname'] = clearSpace($_POST['surname']);
		$_POST['name'] = $_POST['surname'] . " " . $_POST['name'];
		unset($_POST['surname']);

		$user = new User();
		$data = $_POST;
		$user->load($data);

		$ip = getIP();
		$timestamp = getTimestamp();
		$tmp = array(
			'reg_ip' => $ip,
			'last_ip' => $ip,
			'reg_date' => $timestamp,
			'last_action' => $timestamp,
		);

		if (!$user->loadWithValidate($tmp) || !$user->checkUnique()) {
			$user->getErrors();
		} else {
			$tmp = array(
				'password' => password_hash($user->getProperty('password'), PASSWORD_DEFAULT)
			);
			$user->loadWithValidate($tmp);
			if ($id = $user->save()) {
				$_SESSION['success'] = __("User successfully logged in.");
				$this->audit->audit("Успешная регистрация на сайте с IP: $ip", AuditCategory::USER_CONTROLLER, null, $user->getProperties(), $id);
			}
		}
		redirect();
	}

	/**
	 * @throws SQL|EngineException
	 * @route /admin/user/login
	 */
	#[NoReturn] public function loginAction(): void
	{
		if (empty($_POST)) redirect();
		$user = new User();
		if (!$user->login()) {
			$_SESSION['error'] = __("Login/Password entered incorrectly");
		} else {
			$this->audit->audit("Успешная авторизация на сайте с IP: " . getIP(), AuditCategory::USER_CONTROLLER);
		}
		redirect();
	}

	/**
	 * @throws SQL
	 * @route /admin/user/logout
	 */
	#[NoReturn] public function logoutAction(): void
	{
		User::logout();
		redirect(ADMIN . "/user/login-admin");
	}

	/**
	 * @route /admin/user/add-image
	 */
	public function addImageAction(): void
	{
		$this->uploadImage("profile", 215, 215);
	}

	static function addMenu(): void
	{
		MenuGenerator::addMenu(__('Users'), ADMIN . "#", "fa fa-users", [
			["name" => __('Users list'), "url" => ADMIN . "/user", "icon" => "fas fa-users"],
			["name" => __('Add user'), "url" => ADMIN . "/user/add", "icon" => "fas fa-user-plus"]
		]);
	}
}
