<?php


namespace app\controllers\admin;


use app\models\AuditCategory;
use Archive_Tar;
use core\App;
use core\base\EngineException;
use core\MenuGenerator;
use DOMDocument;
use Exception;
use Ifsnop\Mysqldump\Mysqldump;
use JetBrains\PhpStorm\NoReturn;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RedBeanPHP\RedException\SQL;
use SimpleXMLElement;
use ZipArchive;

class BackupController extends AppController
{

	private string $archivePath = TEMP . DIRECTORY_SEPARATOR . "backup" . DIRECTORY_SEPARATOR;

	static function addMenu(): void
	{
		MenuGenerator::addMenu(__('Backup'), ADMIN . "#", "fa fa-database", [
			["name" => __('Export'), "url" => ADMIN . "/backup/export", "icon" => "fas fa-cloud-download-alt"],
			["name" => __('Import'), "url" => ADMIN . "/backup/import", "icon" => "fas fa-cloud-upload-alt"]
		]);
	}

	/**
	 * @route /admin/backup
	 */
	#[NoReturn] public function indexAction(): void
	{
		redirect(PATH . "/" . QUERY_STRING . "/export");
	}

	/**
	 * @throws EngineException|SQL
	 * @route /admin/backup/export
	 */
	public function exportAction(): void
	{
		$archiveHash = substr(md5(uniqid(microtime(), true)), rand(5, 15), 12);

		if ($_POST) {

			$_POST = hArr($_POST);
			if (!hash_equals($_SESSION['csrf'], $_POST['csrf'] ?? '')) {
				throw new EngineException('Token CSRF not valid!', EngineException::CSRF_NOT_VALID);
			}

			$files = array();
			$zip = new ZipArchive();

			if (isset($_POST['backup_database'])) {
				try {
					$backup_name = ENV->DBNAME . '-' . date('Ymd') . '.sql';
					$db = require CONF . "/db.php";

					$dump = new Mysqldump($db['dsn'], $db['user'], $db['password']);
					$dump->start(TEMP . DIRECTORY_SEPARATOR . $backup_name);
					$files['backup_database'] = TEMP . DIRECTORY_SEPARATOR . $backup_name;
				} catch (\Exception $e) {
					echo 'mysqldump-php error: ' . $e->getMessage();
				}
			}

			if (isset($_POST['backup_settings_xml'])) {
				$params = getParams();

				$xml = new SimpleXMLElement('<?xml version="1.0"?><params></params>');
				array2xml($params, $xml);

				$backup_name = clearSpace(App::$app->get('site_name')) . '-settings.xml';
				$files['backup_settings_xml'] = TEMP . DIRECTORY_SEPARATOR . $backup_name;

				$dom = new DOMDocument("1.0");
				$dom->preserveWhiteSpace = false;
				$dom->formatOutput = true;
				$dom->loadXML($xml->asXML());

				$fp = fopen(TEMP . DIRECTORY_SEPARATOR . $backup_name, "w");
				fwrite($fp, $dom->saveXML());
				fclose($fp);
			}

			if (isset($_POST['backup_files'])) {
				$backup_name = App::CMS_VERSION . '-user-files.zip';
				$files['backup_files'] = TEMP . DIRECTORY_SEPARATOR . $backup_name;
				$zip->open(TEMP . DIRECTORY_SEPARATOR . $backup_name, ZipArchive::CREATE | ZipArchive::OVERWRITE);

				$filesZip = new RecursiveIteratorIterator(
					new RecursiveDirectoryIterator(ROOT),
					RecursiveIteratorIterator::LEAVES_ONLY
				);

				foreach ($filesZip as $name => $file) {
					if (
						str_contains($name, APP) ||
						str_contains($name, WWW_THEME) ||
						str_contains($name, CORE) ||
						(
							str_contains($name, CONF) &&
							$name != CONF . DIRECTORY_SEPARATOR . "params.php"
						)
					) {
						if (!$file->isDir()) {
							$filePath = replaceBack2Slash($file->getRealPath());
							$relativePath = substr($filePath, strlen(ROOT) + 1);
							$zip->addFile($filePath, $relativePath);
						}
					}
				}

				$zip->addFile(replaceBack2Slash(ROOT . DIRECTORY_SEPARATOR . "composer.json"), "composer.json");
				$zip->close();
			}
			$files['list'] = TEMP . DIRECTORY_SEPARATOR . "list";

			$filesList = base64_encode(serialize($files));
			$fp = fopen(TEMP . DIRECTORY_SEPARATOR . "list", "w");
			fwrite($fp, $filesList);
			fclose($fp);

			$archiveName = changeDateFormat(getTimestamp(), $_POST['backup_archive_name']) . "-" . $archiveHash . "." . $_POST['backup_archive_ext'];
			if (!file_exists($this->archivePath))
				mkdir($this->archivePath);
			$this->archivePath .= $archiveName;

			switch ($_POST['backup_archive_ext']) {
				case "tar":
					try {
						$tar = new Archive_Tar($this->archivePath);
						$tar->addModify($files, '', TEMP . DIRECTORY_SEPARATOR);
						$tar->_close();
					} catch (\Exception $e) {
						throw new EngineException($e->getMessage(), EngineException::BACKUP_ERROR);
					}
					break;
				case "tar.gz":
					try {
						$tarGz = new Archive_Tar($this->archivePath, 'gz');
						$tarGz->addModify($files, '', TEMP . DIRECTORY_SEPARATOR);
						$tarGz->_close();
					} catch (\Exception $e) {
						throw new EngineException($e->getMessage(), EngineException::BACKUP_ERROR);
					}
					break;
				default:
					$zip->open($this->archivePath, ZipArchive::CREATE | ZipArchive::OVERWRITE);
					foreach ($files as $file) {
						$relativePath = substr($file, strlen(TEMP) + 1);
						$zip->addFile(replaceBack2Slash($file), $relativePath);
					}
					$zip->setArchiveComment($filesList);
					$zip->close();
			}

			foreach ($files as $file) {
				unlink($file);
			}

			$_SESSION['success'] = "Резервная копия создана.";
			$this->audit->audit("Создал резервную копию формата " . $_POST['backup_archive_ext'], AuditCategory::BACKUP_CONTROLLER, $_POST);
			redirect();
		}

		$params = getParams();
		$archiveName_pattern = !empty($params['archive_backup-Name']) ? $params['archive_backup-Name'] : "Y-m-d_H-i-s";
		$archiveExt = !empty($params['archive_backup-Ext']) ? $params['archive_backup-Ext'] : "zip";

		$this->setMeta("Export");
		$this->setData(compact('archiveName_pattern', 'archiveExt', 'archiveHash'));
	}

	/**
	 * @throws EngineException|Exception
	 * @route /admin/backup/import
	 */
	public function importAction(): void
	{
		if ($_POST) {
			$_POST = hArr($_POST);
			if (!hash_equals($_SESSION['csrf'], $_POST['csrf'] ?? '')) {
				throw new EngineException('Token CSRF not valid!', EngineException::CSRF_NOT_VALID);
			}

			if ($_POST['secret_key'] !== SECRET_KEY)
				throw new EngineException("SECRET_KEY not valid!", EngineException::SECRET_KEY_NOT_VALID);

			$archive = $_POST['archive'];
			$ext = getExtension($archive);
			$zip = new ZipArchive();
			$fullPathArchive = ROOT . DIRECTORY_SEPARATOR . $_POST['archive'];

			switch ($ext) {
				case "tar":
				case "gz":
					$tar = new Archive_Tar($fullPathArchive);
					$tar->extract(TEMP);
					$tar->_close();
					break;
				default:
					$zip->open($fullPathArchive);
					$zip->extractTo(TEMP);
					$zip->close();
			}
			unlink($fullPathArchive);

			$methods = unserialize(base64_decode(file_get_contents(TEMP . DIRECTORY_SEPARATOR . "list")));
			unlink(TEMP . DIRECTORY_SEPARATOR . "list");

			if (isset($_POST['backup_settings_xml'])) {
				$xml = new SimpleXMLElement(file_get_contents($methods['backup_settings_xml']));
				$json = json_encode($xml);
				$array = json_decode($json, TRUE);
				foreach ($array as $i => $item) {
					if (empty($item))
						$array[$i] = "";
				}
				$params = getParams();

				$fp = fopen(CONF . DIRECTORY_SEPARATOR . "params.php", "w");
				fwrite($fp, serialize($array));
				fclose($fp);

				unlink($methods['backup_settings_xml']);
				$this->audit->audit("Восстановил настройки системы", AuditCategory::BACKUP_CONTROLLER, $params, $array);
			}
			if (isset($_POST['backup_files'])) {
				$zip->open($methods['backup_files']);
				$zip->extractTo(ROOT);
				$zip->close();
				unlink($methods['backup_files']);
				$this->audit->audit("Восстановил файлы", AuditCategory::BACKUP_CONTROLLER);
			}

			$this->audit->audit("Успешно восстановил систему", AuditCategory::BACKUP_CONTROLLER, array_keys($methods));
			$_SESSION['success'] = "Система восстановлена";
			redirect();
		}


		$archives = array();
        if (file_exists($this->archivePath))
            foreach (scandir($this->archivePath) as $f) {
                if (!in_array(getExtension($f), ['zip', 'tar', 'gz']))
                    continue;
                $archives[] = $f;
            }

		$this->setData(compact('archives'));
		$this->setMeta("Import");
	}

	/**
	 * @route /admin/backup/import-archive
	 */
	#[NoReturn] public function importArchiveAction(): void
	{
		$_POST = hArr($_POST);
		if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) {
			echo json_encode([
				"error" => __('Token CSRF not valid!'),
				'csrf' => false
			]);
			die;
		}

		$zip = new ZipArchive();
		$uploadDir = $this->archivePath;


		$data = $_POST;
		$newArchive = $uploadDir . $data['name'];
		$ext = getExtension($data['name']);

		$res = array(
			"filename" => $data['name'],
			"backup" => array(),
			"filePath" => substr($newArchive, strlen(ROOT) + 1)
		);
		$filesList = array();

		switch ($ext) {
			case "tar":
			case "gz":
				$tar = new Archive_Tar($newArchive);
				$filesList = unserialize(base64_decode($tar->extractInString('list')));
				$tar->_close();
				break;
			case "zip":
				$zip->open($newArchive);
				$comment = $zip->getArchiveComment();
				$filesList = unserialize(base64_decode($comment));
				$zip->close();
		}
		$this->extractedAnswer($filesList, $res);

	}

	/**
	 * @param array $filesList
	 * @param array $res
	 * @return void
	 */
	#[NoReturn] private function extractedAnswer(array $filesList, array $res): void
	{
		if (isset($filesList['backup_settings_xml'])) {
			$res['backup']['backup_settings_xml']['text'] = "Восстановление настроек";
			$res['backup']['backup_settings_xml']['name'] = "backup_settings_xml";
		}
		if (isset($filesList['backup_files'])) {
			$res['backup']['backup_files']['text'] = "Восстановление файлов";
			$res['backup']['backup_files']['name'] = "backup_files";
		}

		exit(json_encode($res));
	}

	/**
	 * @route /admin/backup/import-file
	 */
	public function importFileAction(): void
	{
		if (isset($_GET['upload'])) {
			$file = $_FILES['importBackup'];
			$uploadDir = $this->archivePath;
			$newArchive = $uploadDir . $file['name'];
			if (@move_uploaded_file($file['tmp_name'], $newArchive)) {
				$ext = getExtension($file['name']);
				$zip = new ZipArchive();
				$res = array(
					"filename" => $file['name'],
					"backup" => array(),
					"filePath" => substr($newArchive, strlen(ROOT) + 1)
				);
				$filesList = array();

				switch ($ext) {
					case "tar":
					case "gz":
						$tar = new Archive_Tar($newArchive);
						$filesList = unserialize(base64_decode($tar->extractInString('list')));
						$tar->_close();
						break;
					default:
						$zip->open($newArchive);
						$comment = $zip->getArchiveComment();
						$filesList = unserialize(base64_decode($comment));
						$zip->close();
				}
				$this->extractedAnswer($filesList, $res);
			}
		}
	}
}
