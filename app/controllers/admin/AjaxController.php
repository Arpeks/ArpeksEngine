<?php


namespace app\controllers\admin;

use app\models\{Audit, AuditCategory, Chat, Country, Sessions, Todo, User, UserNotice};
use core\{App, base\EngineException, Cache, ErrorHandler, libs\ScanSnapshot};
use JetBrains\PhpStorm\NoReturn;
use PHPMailer\PHPMailer\Exception;
use R;
use RedBeanPHP\OODBBean;
use RedBeanPHP\RedException\SQL;
use Wkhooy\ObsceneCensorRus;
use ZipArchive;

class AjaxController extends AppController
{

	static function addMenu(): void
	{
	}

	/**
	 * @throws EngineException|SQL
	 * @route /admin/ajax/bug-report
	 */
	public function bugReportAction(): void
	{
		$_POST = hArr($_POST);
		checkCSRF($_POST['csrf'], [
			"error" => __('Token CSRF not valid!'),
			'isSuccess' => false,
			'csrf' => false,
			'smtp' => isActiveSMTP()
		]) ?: die;

		$web = App::$app->getAll();

		unset(
			$web['secret_key'], $web['tinify_key'],
			$web['pushover_token'], $web['pushover_group_token'],
			$web['smtp_login'], $web['smtp_password']
		);

		$data['web-info'] = $web;
		$data['user'] = $_SESSION['user'];
		$text = json_encode($data);
		$time = getTimestamp();

		$fp = fopen(LOGS . "/mail.log", "w");
		fwrite($fp, $text);
		fclose($fp);

		$attachments = array(LOGS . "/mail.log", LOGS . '/errors.log', LOGS . '/block_ip.log');

		$this->audit->audit("Отправил Bug-Report", AuditCategory::AJAX_CONTROLLER);

		try {
			$mail = sendMail('BugReport от ' . $time, "Домен: " . SERVER['host'] . "<br />Дата: " . $time . "<br />Сообщение: " . $_POST['message'], base64_decode("bWlzaGEubnViaWtvdkB5YW5kZXgucnU"), false, $attachments);
			if ($mail) {
				unlink(LOGS . "/mail.log");
				echo json_encode([
					"success" => 'Сообщение отправлено',
					'isSuccess' => true,
					'smtp' => true,
					'csrf' => true
				]);
			} else {
				throw new EngineException(__("SMTP Deactivated!"));
			}
		} catch (EngineException $e) {
			echo json_encode([
				"error" => $e->getMessage(),
				'isSuccess' => false,
				'smtp' => false,
				'csrf' => true
			]);

			$comment = "Домен: " . SERVER['host'] . PHP_EOL . "Дата: " . $time . PHP_EOL . "Сообщение: " . $_POST['message'];
			$zip = new ZipArchive();
			$zipFilename = TEMP . DIRECTORY_SEPARATOR . 'mail.zip';
			$zip->open($zipFilename, ZipArchive::CREATE);
			$zip->setArchiveComment($comment);
			$zip->addFromString('mail-send.log', $comment);
			foreach ($attachments as $file) {
				if (file_exists($file)) {
					$zip->addFile($file, basename($file));
				}
			}
			$zip->close();

			ErrorHandler::errorHandler($e, false);
		} finally {
			exit;
		}
	}

	/**
	 * @throws EngineException|SQL
	 * @route /admin/ajax/todo-edit
	 */
	#[NoReturn] public function todoEditAction(): void
	{
		$_POST = hArr($_POST);
		checkCSRF($_POST['csrf']) ?: die;

		$id = $this->getRequestID(false);
		$todoObj = Todo::getByID($id);
		$_POST['time'] = $todoObj->time;
		$_POST['text'] = h($_POST['task']);
		$_POST['time_complete'] = $todoObj->time_complete;
		$_POST['is_close'] = $todoObj->is_close;

		$todo = new Todo();
		$todo->load($_POST);
		$todo->update($id);
		$this->audit->audit("Изменил задачу #$id на '" . $_POST['text'] . "'", AuditCategory::AJAX_CONTROLLER);
		echo json_encode(["text" => $_POST['text']]);
		die;
	}

	/**
	 * @throws EngineException|SQL
	 * @route /admin/ajax/todo-set
	 */
	#[NoReturn] public function todoSetAction(): void
	{
		$_POST = hArr($_POST);
		checkCSRF($_POST['csrf']) ?: die;

		$id = $this->getRequestID(false);
		$todoObj = Todo::getByID($id);
		$_POST['time'] = $todoObj->time;
		$_POST['text'] = $todoObj->text;

		$todo = new Todo();
		$_POST['is_close'] = ($_POST['is_close']) ? '1' : '0';
		$_POST['time_complete'] = getTimestamp();
		$todo->load($_POST);
		$todo->update($id);
		$this->audit->audit("Изменил флаг выполнения задачи #$id на {$_POST['is_close']}: " . $todoObj->text, AuditCategory::AJAX_CONTROLLER);
		echo "OK";
		die;
	}

	/**
	 * @throws EngineException|SQL
	 * @route /admin/ajax/todo-add
	 */
	#[NoReturn] public function todoAddAction(): void
	{
		$_POST = hArr($_POST);
		checkCSRF($_POST['csrf']) ?: die;

		$todo = new Todo();
		$data['text'] = h($_POST['task']);
		$data['time'] = getTimestamp();
		$data['time_complete'] = getTimestamp();
		$todo->load($data);
		$id = $todo->save();
		$this->audit->audit("Установил новую задачу #$id: " . $data['text'], AuditCategory::AJAX_CONTROLLER);
		echo $id;
		die;
	}

	/**
	 * @throws EngineException|SQL
	 * @route /admin/ajax/todo-delete
	 */
	#[NoReturn] public function todoDeleteAction(): void
	{
		$_POST = hArr($_POST);
		checkCSRF($_POST['csrf']) ?: die;

		$id = $this->getRequestID(false);
		$todo = Todo::getByID($id);
		$txt = $todo->text;
		$tmp = $todo;
		R::trash($todo);
		$this->audit->audit("Удалил задачу #$id: $txt", AuditCategory::AJAX_CONTROLLER, $tmp);
		echo "OK";
		die;
	}

	/**
	 * @throws EngineException|SQL
	 * @route /admin/ajax/get-chat
	 */
	#[NoReturn] public function getChatAction(): void
	{
		checkCSRF($_POST['csrf'], [
			"error" => __('Token CSRF not valid!'),
			'isSuccess' => false,
			'csrf' => false,
		]) ?: die;

		$data = $_POST;
		$chatDB = Chat::getAll();
		$last_id = $data['last_id'];

		$chat = array();
		foreach ($chatDB as $id => $item) {
			$chat[$id] = $item->getProperties();
			$user = User::getByID($chat[$id]['user_id'])->getProperties();
			$chat[$id]['user']['name'] = $user['name'];
			$chat[$id]['user']['online'] = $user['online'];
			$chat[$id]['user']['img'] = $user['img'];
			$chat[$id]['isNew'] = $last_id < $id;
			$chat['count_message'] = App::$app->get('count_message_admin_chat');

			$time = strtotime($chat[$id]['time']);
			$month = $this->monthsList[date("n", $time) - 1];
			$year = date("Y", $time);
			$day = date("d", $time);
			$hour = date("H", $time);
			$minute = date("i", $time);
			$chat[$id]['time'] = "$day $month $year $hour:$minute";
		}
		echo json_encode($chat);
		die;
	}

	/**
	 * @throws SQL
	 * @route /admin/ajax/send-chat
	 */
	#[NoReturn] public function sendChatAction(): void
	{
		$_POST = hArr($_POST);
		checkCSRF($_POST['csrf']) ?: die;

		$chat = new Chat();
		$data = $_POST;
		$data['user_id'] = intval($data['user_id']);
		$data['time'] = getTimestamp();
		$id = -1;

		ObsceneCensorRus::filterText($data['message']);

		if ($chat->loadWithValidate($data)) {
			$filterArray = applyFilter("admin_chat_send", ["chat" => $chat, "data" => $data]);
			if (!$filterArray) $filterArray = array();
			foreach ($filterArray as $fa) {
				if (!$fa) exit;
			}
			doAction("admin_chat_check", ["chat" => $chat, "data" => $data]);
			$id = $chat->save();
		}
		echo json_encode([$id]);
		die;
	}

	/**
	 * @route /admin/ajax/get-info
	 */
	#[NoReturn] public function getInfoAction(): void
	{
		if (!Cache::get('ajax-info-admin')) {
			$user = User::getCount("`online` = '1'");
			$updates = Cache::get("cms-check-update")['version']['server'];
			$uniq = array();
			$userCount = User::getCount("`role_id` != 5");

			foreach (Cache::get('uniq-users') as $ip => $tUniq) {
				if (trim($tUniq['isBot']) == '0') $uniq[$ip] = $tUniq;
			}

			$json = array(
				"users" => $user,
				"settings-update" => intval($updates),
				"uniq-users" => count($uniq),
				"users-all" => $userCount
			);
			Cache::set('ajax-info-admin', $json, 20);
		} else $json = Cache::get('ajax-info-admin');
		echo json_encode($json);
		die;
	}

	/**
	 * @throws SQL
	 * @route /admin/ajax/send-notice
	 */
	#[NoReturn] public function sendNoticeAction(): void
	{
		$_POST = hArr($_POST);
		checkCSRF($_POST['csrf']) ?: die;

		$data = $_POST;
		$notice = $data['notice'];
		$uid = $_SESSION['user']['id'];

		$data = array(
			"user_id" => intval($uid),
			"notice" => $notice
		);
		$uNoticeNew = new UserNotice();
		$uNoticeNew->load($data);


		$uNotice = UserNotice::getOne("`user_id` = ?", [$uid]);
		if (empty($uNotice)) {
			$uNoticeNew->save();
		} else {
			$uNoticeNew->update($uNotice->getID());
		}


		echo json_encode(['success' => true]);
		die;
	}

	/**
	 * @throws EngineException|SQL
	 * @route /admin/ajax/cache-clear
	 */
	public function cacheClearAction(): void
	{
		$_POST = hArr($_POST);
		checkCSRF($_POST['csrf']) ?: die;

		$data = $_POST;
		if (empty($data['key'])) return;
		$this->audit->audit("Очистил кеш используя кеш-ключ: " . $data['key'], AuditCategory::AJAX_CONTROLLER);

		if ($data['key'] == 'all') {
			$cache = Cache::deleteAll();
		} else {
			$cache = Cache::deleteCache($data['key']);
		}

		echo json_encode(["isClear" => $cache]);
		die;
	}

	/**
	 * @route /admin/ajax/take-snapshot
	 */
	#[NoReturn] public function takeSnapshotAction(): void
	{
		$_POST = hArr($_POST);
		if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) {
			echo json_encode([
				"error" => __('Token CSRF not valid!'),
				'csrf' => false
			]);
			die;
		}

		$scanner = ScanSnapshot::instance();
		$scanner->clear();
		$scanner->snap = false;
		$scanner->scan();
		$scanner->write();

		echo json_encode(['isSuccess' => true]);
		die;
	}

	/**
	 * @route /admin/ajax/scan
	 */
	#[NoReturn] public function scanAction(): void
	{
		$_POST = hArr($_POST);
		checkCSRF($_POST['csrf'], [
			"error" => __('Token CSRF not valid!'),
			'csrf' => false
		]) ?: die;

		$scanner = ScanSnapshot::instance();
		echo json_encode($scanner->scan());
		die;
	}

	/**
	 * @throws EngineException
	 * @route /admin/ajax/core-update
	 */
	public function coreUpdateAction(): void
	{
		ini_set('max_execution_time', 0);

		$_POST = hArr($_POST);
		checkCSRF($_POST['csrf'], [
			"error" => __('Token CSRF not valid!'),
			'csrf' => false
		]) ?: die;

		$data = $_POST;
		$answer = "";
		$error = 0;
		$newData = array();
		$zip = new ZipArchive();

		$lastRelease = App::$git->getReleases()[0];
		$lastTag = $lastRelease['tag_name'];

		$tag = "ae-";
		foreach (str_split(App::CMS_VERSION) as $item) {
			$tag .= "$item.";
		}
		$currTag = rtrim($tag, ".");

		$lastUrlUpdate = $lastRelease['assets']['sources'][0]['url'];
		$tmpName = pathinfo(basename($lastUrlUpdate))["filename"];

		switch ($data['action']) {
			case "downloadUpdate":
				try {
					$curl = curl_init();
					$fp = fopen(CACHE . '/update.zip', 'w');

					$options = array(
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_HEADER => false,
						CURLOPT_FOLLOWLOCATION => true,
						CURLOPT_ENCODING => "",
						CURLOPT_USERAGENT => "curl-arpeks-engine",
						CURLOPT_AUTOREFERER => true,
						CURLOPT_CONNECTTIMEOUT => 120,
						CURLOPT_TIMEOUT => 120,
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_SSL_VERIFYPEER => false,
						CURLOPT_URL => $lastUrlUpdate,
						CURLOPT_FILE => $fp
					);
					curl_setopt_array($curl, $options);
					curl_exec($curl);
					curl_close($curl);
					fflush($fp);
					fclose($fp);
					$answer = "Файл успешно скачан";
				} catch (\Exception $e) {
					$error = 1;
					$answer = "Ошибка скачивания обновления";
				}
				break;
			case "getFileListUpdate":
				$list = App::$git->getQuery("/repository/compare", [
					'from' => $lastTag, // last version
					'to' => $currTag, // local version
					'straight' => true
				])['diffs'];

				$newData = array(
					"count" => 0,
					"delete" => [],
					"files" => []
				);

				foreach ($list as $i => $item) {
					if ($item['renamed_file'] != "" || $item['old_path'] != $item['new_path']) {
						$newData['delete'][] = $item['old_path'];
						$newData['files'][] = $item['new_path'];
					} else if ($item['deleted_file'] != "") {
						$newData['delete'][] = $item['old_path'];
					} else {
						$newData['files'][] = $item['new_path'];
					}
					$newData['count']++;
					unset($list[$i]['diff']);
				}
				break;
			case "techwork":
				if (file_exists(CONF . "/params.php")) {
					$fp = fopen(CONF . "/params.php", "r");
					$params = unserialize(fread($fp, filesize(CONF . "/params.php")));
					fclose($fp);
				} else {
					$error = 3;
					$answer = "Файл не существует (File: " . CONF . "/params.php)";
					break;
				}

				if ($data['enable']) {
					$params['techwork'] = 'on';
					$answer = "Обслуживание включено";
				} else {
					unset($params['techwork']);
					$answer = "Обслуживание отключено";
				}

				if (file_exists(CONF . "/params.php")) {
					$fp = fopen(CONF . "/params.php", "w");
					fwrite($fp, serialize($params));
					fclose($fp);
				}
				break;
			case "fileExtract":
				$newZip = new ZipArchive();
				$file = realpath(CACHE . "/update.zip");
				$res = $zip->open($file);
				if ($res === TRUE) {
					if (!empty($data['listFiles']) && is_array($data['listFiles'])) {
						foreach ($data['listFiles'] as $f) {
							$tmp = str_replace("\\", "/", $tmpName . DIRECTORY_SEPARATOR . $f);
							$zip->extractTo(CACHE, $tmp);
							$full_path = str_replace("\\", "/", CACHE . "/" . $tmp);

							$newZip->open(CACHE . "/extract.zip", ZipArchive::CREATE);
							$newZip->addFile($full_path, $f);
						}
					}
				}

				$newZip->extractTo(ROOT);

				$zip->close();
				$newZip->close();
				break;
			case "fileDelete":
				/*
				 * Временно отключено
				foreach ($data['listFiles'] As $f) {
					// @unlink(ROOT . DIRECTORY_SEPARATOR . $f);
				}
				*/
				break;
			case "deleteArchive":
				if (@unlink(CACHE . "/update.zip" && @unlink(CACHE . "/extract.zip"))) {
					$answer = "Файл успешно удалён";
				} else {
					$error = 7;
					$answer = "Ошибка удаления файла";
				}
				if (file_exists(TEMP . "/update.sql")) {
					$sql = file_get_contents(TEMP . "/update.sql");
					R::exec($sql);
					unlink(TEMP . "/update.sql");
				}
				Cache::deleteAll();
				break;
		}

		if ($error !== 0) {
			throw new EngineException($answer . " | Step: " . $error, 609);
		}

		echo json_encode(["answer" => $answer, 'error' => $error, "action" => $data['action'], "data" => $newData]);
		die;
	}

	/**
	 * @throws SQL
	 * @route /admin/ajax/user-logout
	 */
	public function userLogoutAction(): void
	{
		$_POST = hArr($_POST);
		checkCSRF($_POST['csrf'], [
			"error" => __('Token CSRF not valid!'),
			'csrf' => false
		]) ?: die;

		$data = $_POST;

		if (empty($data['session']) && !is_int($data['session']))
			return;

		$isCancel = false;
		$tmp = Sessions::getCurrent();
		$isExit = false;

		if ($tmp instanceof OODBBean) {
			$isExit = ($data['session'] == $tmp->getID());
		}

		$session = Sessions::getOne("`id` = " . $data['session']);
		if (User::checkPermission("admin_access_all") || $session->user_id == $_SESSION['user']['id']) {
			$isCancel = Sessions::deactivate($session);
		}

		if ($isExit) User::logout();

		echo json_encode(['isCancel' => $isCancel, 'isExit' => $isExit]);
		die;
	}

	/**
	 * @throws EngineException|SQL|Exception
	 * @route /admin/ajax/settings-global
	 */
	#[NoReturn] public function settingsGlobalAction(): void
	{
		$_POST = hArr($_POST);
		checkCSRF($_POST['csrf'], [
			"error" => __('Token CSRF not valid!'),
			'csrf' => false
		]) ?: die;
		$data = $_POST;

		$host = @explode("@", $data['admin_email'])[1];
		if (SERVER['host'] !== $host && !empty($data['smtp_active'])) {
			$mail = generateMailer($data);
			$data['smtp_active'] = 'on';

			if ($data['admin_email'] !== $data['smtp_login'] . "@" . $host || !$mail->smtpConnect()) {
				$return['error'] = "Ошибка. Не верно настроен SMTP-сервер";
				$this->audit->audit("Попытка изменения глобальных настроек сайта, пренебрегая SMTP авторизацией", AuditCategory::AJAX_CONTROLLER);
			}
		}

		setParams($data, false);
		$this->audit->audit("Изменил глобальный настройки сайта", AuditCategory::AJAX_CONTROLLER);
		Cache::deleteAll();
		redirect();
	}

	/**
	 * @route /admin/ajax/backup-archive
	 */
	#[NoReturn] public function backupArchiveAction(): void
	{
		$_POST = hArr($_POST);
		checkCSRF($_POST['csrf'], [
			"error" => __('Token CSRF not valid!'),
			'csrf' => false
		]) ?: die;

		$data = $_POST;
		$return = array();

		$name = $data['name'];
		$ext = $data['ext'];

		setParams(['archive_backup-Name' => $name, 'archive_backup-Ext' => $ext]);
		$return['name'] = changeDateFormat(getTimestamp(), $name);
		$return['ext'] = $ext;
		$return['hash'] = substr(md5(uniqid(microtime(), true)), rand(5, 15), 12);

		echo json_encode($return);
		die;
	}

	/**
	 * @route /admin/ajax/get-content-theme
	 */
	#[NoReturn] public function getContentThemeAction(): void
	{
		$_POST = hArr($_POST);
		checkCSRF($_POST['csrf'], [
			"error" => __('Token CSRF not valid!'),
			'csrf' => false
		]) ?: die;
		$data = $_POST;

		$fp = fopen(ROOT . $data['file'], "r");
		$content = fread($fp, filesize(ROOT . $data['file']));
		fclose($fp);

		echo json_encode([$content]);
		die;
	}

	/**
	 * @throws SQL
	 * @route /admin/ajax/change-language
	 */
	#[NoReturn] public function changeLanguageAction(): void
	{
		$_POST = hArr($_POST);
		checkCSRF($_POST['csrf'], [
			"error" => __('Token CSRF not valid!'),
			'csrf' => false
		]) ?: die;
		$data = $_POST;

		$newLang = $data['newLang'];
		$country = Country::getOne("`id2` = ?", [$newLang]);

		$user = User::getByID($_SESSION['user']['id']);
		$user->language_id = $country->getID();

		$nUser = new User();
		$nUser->loadWithValidate($user->getProperties());
		$nUser->update($user->getID());
		Cache::deleteAll();
		$_SESSION['user']['language_id'] = $country->getID();

		echo json_encode(["OK"]);
		die;
	}

	#[NoReturn] public function getAuditAction(): void
	{
		$_POST = hArr($_POST);
		checkCSRF($_POST['csrf'], [
			"error" => __('Token CSRF not valid!')
		]) ?: die;
		$id = intval($_POST['id']);

		$audit = Audit::getByID($id);
		$audit->action_before = unserialize($audit->action_before);
		$audit->action_after = unserialize($audit->action_after);

		ob_start();
		print_r($audit->getProperties());
		echo htmlspecialchars(ob_get_clean());
		die;
	}
}
