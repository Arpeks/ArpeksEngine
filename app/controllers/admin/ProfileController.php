<?php

namespace app\controllers\admin;

use app\models\Audit;
use app\models\AuditCategory;
use app\models\Sessions;
use app\models\User;
use app\models\UserAuth;
use app\models\UserRole;
use app\widgets\admin\UserProfileWidget\UserProfileWidget;
use core\MenuGenerator;
use GoogleAuthenticator;
use JetBrains\PhpStorm\NoReturn;
use RedBeanPHP\OODBBean;

class ProfileController extends AppController
{

	private function getProfilePage($user): UserProfileWidget
	{
		return new UserProfileWidget($user);
	}

	/**
	 * @route /admin/profile
	 */
	public function indexAction(): void
	{
		$id = !empty($_GET['id']) ? (int)$_GET['id'] : $_SESSION['user']['id'];
		$users = User::getAll();
		$user = $users[$id];

		if ($user->id != $id && $_SESSION['user']['role_id'] != 1)
			redirect("/admin/user");

		$profileHtml = $this->getProfilePage($user);
		$is_access = (($user->id == $_SESSION['user']['id']) || (User::checkPermission("admin_access_all")));
		$currRole = UserRole::getByID($user->role_id);
		$isRoleEdit = User::checkPermission("admin_role_edit") && $currRole->getID() != 1;
		$isActive2FA = UserAuth::check2FA($id);

		$this->setData(compact("user", 'profileHtml', 'is_access', 'users', 'isRoleEdit', 'currRole', 'isActive2FA'));
		$this->setMeta("Profile");
	}

	/**
	 * @route /admin/profile/tfa
	 */
	#[NoReturn]
	public function tfaAction(): void
	{
		$id = $_SESSION['user']['id'];
		$ga = new GoogleAuthenticator();

		if (!empty($_POST)) {
			if ($_POST['action'] == "enable") {
				$code = $ga->getCode($_POST['secret']);
				if ($code == $_POST['code']) {
					$auth = new UserAuth();
					if ($auth->loadWithValidate(['user_id' => $id, 'secret' => $_POST['secret'], 'time_act' => getTimestamp()])) {
						$auth->save();
						$_SESSION['success'] = "Двухэтапная аутентификация успешно активирована!";
						redirect("/admin/profile");
						exit();
					}
				}
			} else if ($_POST['action'] == "disable") {
				$pass = $_POST['password'];
				$users = User::getAll();
				$user = $users[$id];

				if (password_verify($pass, $user->password)) {
					$auth = UserAuth::getOne('`user_id` = ?', [$id]);
					\R::trash($auth);
					$_SESSION['success'] = "Двухэтапная аутентификация успешно деактивирована!";
				} else {
					$_SESSION['error'] = "Вы ввели не верный пароль от учетной записи.";
				}
				redirect("/admin/profile");
			}
		}
		$_SESSION['error'] = "Произошла ошибка во время активации двухэтапной аутентификации. Повторите попытку позднее.";
		redirect("/admin/profile");
	}

	/**
	 * @route /admin/profile/session
	 */
	public function sessionAction(): void
	{
		$this->indexAction();
		$sessionAuth = array();

		/* @var OODBBean $user */
		$user = $this->getData('user');
		$id = $user->getID();

		if (!(User::checkPermission("admin_edit_information") || $user->id == $_SESSION['user']['id']))
			redirect("/admin/user");

		$sessions = Sessions::getAll("`user_id` = $id ORDER BY `id` DESC LIMIT 100");

		/* @var OODBBean $sess */
		foreach ($sessions as $id => $sess) {
			$sessionAuth[$id] = (object)$sess->getProperties();
			if (!empty($_GET['act']) && $_GET['act'] == 'removeAll') {
				Sessions::deactivate($sess);
			}
		}
		if (!empty($_GET['act']) && $_GET['act'] == 'removeAll') {
			User::logout();
			redirect(ADMIN . "/user/login-admin");
		}

		Sessions::getGeoData();

		$this->setData(compact("sessionAuth"));
		$this->setMeta("Session");
	}

	/**
	 * @route /admin/profile/audit
	 */
	public function auditAction(): void
	{
		$this->indexAction();

		/* @var OODBBean $user */
		$user = $this->getData('user');
		$id = $user->getID();

		$audits = Audit::getAll("`user_id` = $id ORDER BY `id` DESC LIMIT 100");

		$auditCat_OODBean = AuditCategory::getAll();
		$auditCategory = array();
		foreach ($auditCat_OODBean as $id => $category) {
			$auditCategory[$id] = $category->name;
		}

		$this->setData(compact("audits", "auditCategory"));
		$this->setMeta("Audit");
	}

	/**
	 * @route /admin/profile/barcode
	 */
	#[NoReturn]
	public function getBarcodeAction(): void
	{
		$chl = !empty($_GET['chl']) ? $_GET['chl'] : false;
		$s = !empty($_GET['s']) ? $_GET['s'] : 200;
		getQrCode(urldecode($chl), $s);
		exit();
	}

	static function addMenu(): void
	{
		MenuGenerator::addMenu(__('Profile'), ADMIN . "#", "fas fa-user-secret", [
			["name" => __("Edit profile"), "url" => ADMIN . "/profile", "icon" => "fas fa-edit"],
			["name" => __("Audit"), "url" => ADMIN . "/profile/audit", "icon" => "fas fa-history"],
			["name" => __("Session"), "url" => ADMIN . "/profile/session", "icon" => "fas fa-history"],
		]);
	}
}
