<?php


namespace app\controllers\admin;


use core\Cache;
use core\libs\Plugins;
use core\MenuGenerator;
use JetBrains\PhpStorm\NoReturn;

class PluginsController extends AppController
{

	public bool $isActive = false;

	private const string CMS_PLUGIN_DOMAIN = "https://engine.myarpeks.ru";

	/**
	 * @route /admin/plugins
	 */
	public function indexAction(): void
	{
		$plugins = Plugins::instance();
		$pluginList = $plugins->pluginList;

		if (!($server_plugins = Cache::get("plugin-list"))) {
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, self::CMS_PLUGIN_DOMAIN . '/plugins/list.php');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$server_plugins = unserialize(curl_exec($curl));
			curl_close($curl);
			Cache::set("plugin-list", $server_plugins, 5);
		}

		if (!$server_plugins) $server_plugins = array();

		$pluginList = array_merge([], $server_plugins, $pluginList);
		$pluginsInfo = array();
		foreach ($pluginList as $plugin => $param) {
			$fp = @fopen(self::CMS_PLUGIN_DOMAIN . "/plugins/" . $plugin . "/", "r");
			if ($fp) {
				$infoServer = unserialize(fread($fp, 4096));
				fclose($fp);
			} else $infoServer['version'] = '0.1';

			if (!isset($param['deactivated'])) $param['deactivated'] = true;
			if (empty($param['description'])) $param['description'] = "No description";
			if (empty($param['author'])) $param['author'] = "No author";
			if (empty($param['version'])) $param['version'] = "0.1";
			if (empty($param['name'])) $param['name'] = $plugin;

			$pluginsInfo[$plugin] = $param;
			$pluginsInfo[$plugin]['name'] = $param['name'];
			$pluginsInfo[$plugin]['version'] = $param['version'];
			$pluginsInfo[$plugin]['name-2'] = $param['name'];
			$pluginsInfo[$plugin]['url'] = $plugin;
			$pluginsInfo[$plugin]['deactivated'] = $param['deactivated'];
			$lastVersion = $infoServer['version'];

			if (isset($param['url-server'])) {
				$pluginsInfo[$plugin]['isDownload'] = true;
			} else {
				$pluginsInfo[$plugin]['isUpdate'] = ($param['version'] != $lastVersion);
			}
		}

		$this->setMeta('plugin_list');
		$this->setData(compact('pluginsInfo'));
	}

	#[NoReturn]
	public function deleteAction(): void
	{
		$plugin = $_GET['name'];
		$path = APP_PLUGIN . "/" . ucwords($plugin);
		$pathLang = LANG . "/plugins/" . ucwords($plugin);

		removeDirectory($path, false);
		removeDirectory($pathLang, false);

		redirect(ADMIN . "/plugins");
	}

	#[NoReturn]
	public function disableAction(): void
	{
		$this->togglePlugin(true);
	}

	#[NoReturn]
	public function enableAction(): void
	{
		$this->togglePlugin(false);
	}

	#[NoReturn]
	private function togglePlugin(bool $deactivated): void
	{
		$key = "";
		$plugins = Plugins::instance();
		if (isset($_POST['name']))
			$key = $_POST['name'];
		else if (isset($_GET['name']))
			$key = $_GET['name'];
		else {
			$_SESSION['error'] = "Название плагина не передано";
			redirect(ADMIN . "/plugins");
		}
		$plugins->setParam($key, ['deactivated' => $deactivated]);
		Cache::deleteCache("menu");
		redirect(ADMIN . "/plugins");
	}

	#[NoReturn]
	public function downloadAction(): void
	{
		$this->updateAction(true);
	}

	#[NoReturn]
	public function updateAction($isDownload = false): void
	{
		if (isset($_POST['name']))
			$key = $_POST['name'];
		else if (isset($_GET['name']))
			$key = $_GET['name'];
		else {
			$_SESSION['error'] = "Название плагина не передано";
			redirect(ADMIN . "/plugins");
		}

		$curl = curl_init(self::CMS_PLUGIN_DOMAIN . "/plugins/" . $key . "/plugin.zip");
		$fp = fopen(TEMP . '/plugin.zip', 'w');
		curl_setopt($curl, CURLOPT_FILE, $fp);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_exec($curl);
		curl_close($curl);
		fflush($fp);
		fclose($fp);

		$fpInfo = fopen(self::CMS_PLUGIN_DOMAIN . "/plugins/" . $key . "/", "r");
		$info = unserialize(fread($fpInfo, 2048));
		fclose($fpInfo);

		$zip = new \ZipArchive();
		$file = realpath(TEMP . "/plugin.zip");
		$res = $zip->open($file);
		if ($res === TRUE) {
			$zip->extractTo(APP_PLUGIN);
			$zip->close();
			unlink(TEMP . '/plugin.zip');
			if (file_exists(TEMP . "/plugin.sql")) {
				$sql = file_get_contents(TEMP . "/plugin.sql");
				\R::exec($sql);
				unlink(TEMP . "/plugin.sql");
			}

			Plugins::setParam($key, $info);
			Plugins::setParam($key, ["deactivated" => true]);
		}

		redirect(ADMIN . "/plugins");
	}

	public function managerAction(): void
	{
		if (empty($this->getRoute('pluginName')))
			redirect(ADMIN . "/plugins");

		$plugins = Plugins::instance();
		$pluginName = $this->getRoute('pluginName');
		$pluginList = $plugins->pluginList;

		$path = APP_PLUGIN . "/" . $pluginName . "/defaultSettings.php";
		if (!empty($_GET['settings']) && $_GET['settings'] == 'default' && file_exists($path)) {
			$default = @include $path;
			Plugins::setParam($pluginName, $default);
			redirect(ADMIN . "/plugins/manager/" . $pluginName);
		}

		if (!empty($_POST)) {
			Plugins::setParam($pluginName, $_POST);
			redirect(ADMIN . "/plugins/manager/" . $pluginName);
		}

		if (empty($pluginName) || !isset($pluginList[$pluginName]) || $pluginList[$pluginName]['deactivated']) redirect(ADMIN . "/plugins");
		$params = $pluginList[$pluginName];


		if (file_exists($path)) {
			$default = @include $path;
			foreach ($default as $key => $value) {
				if (isset($params[$key])) unset($default[$key]);
				else if ($value == "on") $default[$key] = false;
			}
			$params = array_merge($params, $default);
		}

		ob_start();
		include_once APP_PLUGIN . "/" . $pluginName . "/pageSettings.php";
		$codeHTML = ob_get_clean();

		$this->setMeta(__("Plugins Settings") . ": " . $pluginName, "", "", false);
		$this->setData(compact('codeHTML', 'params'));
	}

	static function addMenu(): void
	{
		MenuGenerator::addMenu(__('Plugins'), ADMIN . "/plugins", "fa fa-plug");
	}
}
