<?php

namespace app\controllers\admin;

use app\models\Todo;
use app\models\User;
use app\models\UserNotice;
use core\Cache;
use core\MenuGenerator;
use DateTime;
use Exception;
use JetBrains\PhpStorm\NoReturn;
use R;

class MainController extends AppController
{

	static function addMenu(): void
	{
		MenuGenerator::addMenu(__('Main'), ADMIN, "fas fa-tachometer-alt");
	}

	/**
	 * @throws Exception
	 * @route /admin
	 */
	#[NoReturn]
	public function indexAction(): void
	{
		$start_date = new DateTime();

		$userOnlineCount = User::getCount("online = '1'");
		$todoDB = Todo::getAll();
		$notice = UserNotice::getOne("`user_id` = ?", [$_SESSION['user']['id']]);

		if (!empty($notice->notice))
			$notice = $notice->notice;
		else
			$notice = "";

		$todo = [];
		foreach ($todoDB as $id => $do) {
			$todo[$id]['id'] = $do->id;
			$todo[$id]['is_close'] = $do->is_close;
			$todo[$id]['text'] = $do->text;
			$todo[$id]['time'] = $do->time;

			$delta = time() - strtotime($todo[$id]['time']);
			$since_start = $start_date->diff(new DateTime($todo[$id]['time']));

			switch (true) {
				case in_array($delta, range(0, 3599)):
					$todo[$id]['label'] = "danger";
					$todo[$id]['delta'] = $since_start->i . " " . __("min");
					break;
				case in_array($delta, range(3600, 86399)):
					$todo[$id]['label'] = "info";
					$todo[$id]['delta'] = $since_start->h . " " . __("h");
					break;
				case in_array($delta, range(86400, 172799)):
					$todo[$id]['label'] = "warning";
					$todo[$id]['delta'] = $since_start->d . " " . __("day");
					break;
				case in_array($delta, range(172800, 345599)):
					$todo[$id]['label'] = "success";
					$todo[$id]['delta'] = $since_start->d . " " . __("days");
					break;
				default:
					if ($this->layout != 'admin')
						$todo[$id]['label'] = "default";
					else
						$todo[$id]['label'] = "secondary";
					$todo[$id]['delta'] = $since_start->d . " " . __("days");
			}
		}
		$todo = array_reverse($todo);

		$user = User::getAll();
		$userBanned = User::getAll('`role_id` = ?', [5]);
		$databaseSize = R::getRow('SELECT table_schema AS "database", SUM(data_length + index_length) AS "size" FROM information_schema.TABLES WHERE TABLE_SCHEMA=? GROUP BY table_schema;', [ENV->DBNAME])['size'];

		$cacheList = Cache::getSize();
		$cacheSize = 0;

		$maxUploadSize = $this->getSize(ini_get("upload_max_filesize"));
		$diskSpace = disk_free_space(ROOT);

		$memoryLimit = $this->getSize(ini_get("memory_limit"));
		$disabledFunctions = __("Undefined");
		if (ini_get('disable_functions')) {
			$disFunc = explode(",", ini_get('disable_functions'));
			$disabledFunctions = implode(", ", $disFunc);
		}

		$redis = extension_loaded('redis');
		$redisServer = $_SESSION['redis'] ?? false;

		if ($redisServer) {
			$cacheSize = $cacheList['redis']['size_byte'] + $cacheList['tmp-images']['size_byte'];
		} else {
			foreach ($cacheList as $i => $cache) {
				if ($i === 0 || !is_numeric($cache['size_byte']) || $i === 'redis')
					continue;
				$cacheSize += $cache['size_byte'];
			}
		}
		$gdInfo = "";
		foreach (gd_info() as $key => $value) {
			$val = ($key != "GD Version" && $key != "FreeType Linkage") ? ($value == 1) ? "Enabled" : "Disabled" : $value;
			$gdInfo .= $key . ": " . $val . ", ";
		}
		$gdInfo = rtrim($gdInfo, ", ");

		$todo = array_reverse($todo);
		$this->setData(
			compact(
				'todo',
				'userOnlineCount',
				'user',
				'userBanned',
				'databaseSize',
				'cacheSize',
				'maxUploadSize',
				'diskSpace',
				'gdInfo',
				'memoryLimit',
				'notice',
				'redisServer',
				'redis',
				'disabledFunctions'
			)
		);
	}

	private function getSize(string $valueINI): float
	{
		$value = intval($valueINI);
		$size = $valueINI[strlen($valueINI) - 1];
		$value *= match ($size) {
			"K" => 1024,
			"M" => 1024 * 1024,
			"G" => 1024 * 1024 * 1024,
			"T" => 1024 * 1024 * 1024 * 1024,
			"P" => 1024 * 1024 * 1024 * 1024 * 1024,
		};
		return $value;
	}
}
