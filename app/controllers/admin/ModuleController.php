<?php


namespace app\controllers\admin;


use core\Cache;
use core\interfaces\Module;
use core\MenuGenerator;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\NoReturn;
use R;

class ModuleController extends AppController
{

	protected static string $author = "No author";
	protected static string $description = "No description";

	public static function getActiveModuleByName(string $name): array
	{
		$modules = self::getActiveModules();
		$module = array();
		$namespace = 'app\\controllers\\admin\\module\\';

		foreach ($modules as $oneModule) {
			if ($oneModule == $name) {
				$className = $namespace . $oneModule . "Controller";
				if (class_exists($className) && in_array("app\controllers\admin\ModuleController", class_parents($className)))
					$module = $className::getModuleInformation();
			}
		}

		return $module;
	}

	public static function getActiveModules(): array
	{
		$modulesCache = (array)Cache::get("-modules-active");
		$namespace = "app\\controllers\\admin\\module\\";
		$modules = array();
		foreach ($modulesCache As $module) {
			/* @var ModuleController $class */
			$class = $namespace . $module . "Controller";
			if( $class::$active )
				$modules[] = $module;
		}
		return $modules;
	}

	#[ArrayShape(["author" => "string", "description" => "string", "active_module" => 'bool'])]
	public static function getModuleInformation(): array
	{
		return [
			"author" => static::$author,
			"description" => static::$description,
			"active_module" => static::$active
		];
	}

	static function addMenu(): void
	{
		MenuGenerator::addMenu(__('Modules'), ADMIN . "/module", "fa fa-archive");
	}

	/**
	 * @route /admin/module
	 */
	public function indexAction(): void
	{
		$moduleInfo = [];
		$namespace = 'app\\controllers\\admin\\module\\';
		$activeModules = self::getActiveModules();

		foreach (scandir(APP_CONTROLLER . "/admin/module/") as $file) {
			if ($file[0] == '.')
				continue;
			$file = str_replace(".php", "", $file);

			/* @var ModuleController $class */
			$class = $namespace . $file;

			$name = str_replace("Controller", "", $file);
			$moduleInfo[$name] = $class::getModuleInformation();
			$moduleInfo[$name]['active'] = in_array($name, $activeModules);

			if (!$moduleInfo[$name]['active_module'])
				unset($moduleInfo[$name]);
		}

		$this->setMeta("Modules");
		$this->setData(compact("moduleInfo"));
	}

	/**
	 * @route /admin/module/enable
	 */
	#[NoReturn] public function enableAction(): void
	{
		$this->setActiveModule($_GET['name'], true);
		Cache::deleteCache("menu");
		redirect(ADMIN . "/module");
	}

	private function setActiveModule(string $name, bool $status): void
	{
		$temp = self::getActiveModules();
		$name = ucwords(h($name));


		if ($status)
			$temp[] = $name;
		else
			deleteArrValue($temp, [$name]);

		Cache::set("-modules-active", $temp, 86400);
	}

	/**
	 * @route /admin/module/disable
	 */
	#[NoReturn] public function disableAction(): void
	{
		$this->setActiveModule($_GET['name'], false);
		Cache::deleteCache("menu");
		redirect(ADMIN . "/module");
	}

	/**
	 * @route /admin/module/remove
	 */
	#[NoReturn] public function removeAction(): void
	{
		$this->setActiveModule($_GET['name'], false);
		Cache::deleteCache("menu");

		/** @var Module $class */
		$class = "app\\controllers\\admin\\module\\" . $_GET['name'] . "Controller";
		$databaseTable = $class::getDatabaseTableName();
		$fileList = $class::getFileList();

		if ($databaseTable != "") {
			if (is_array($databaseTable)) {
				foreach ($databaseTable as $table)
					$this->dropTable($table);
			} else {
				$this->dropTable($databaseTable);
			}
		}

		foreach ($fileList as $f) {
			if (is_dir($f))
				removeDirectory($f, false);
			else
				unlink($f);
		}

		redirect(ADMIN . "/module");
	}

	private function dropTable(string $table): void
	{
		$isExistsTable = R::exec("SHOW TABLES FROM `" . ENV->DBNAME . "` like '" . $table . "';");
		if ($isExistsTable == 1)
			R::exec("DROP TABLE {$table}");
	}
}
