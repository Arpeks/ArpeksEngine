<?php

namespace app\controllers\admin;


use app\controllers\admin\libs\DefaultController;
use app\models\{Country, Sessions, User, UserForgot};
use core\{App, Cache, MenuGenerator};
use core\libs\{Plugins, SimpleImage};
use DateInterval;
use DateTime;
use Exception;
use R;
use RedBeanPHP\RedException\SQL;
use Symfony\Component\Console\Application;

abstract class AppController extends DefaultController
{

	public string $layout = "admin";

	protected array $user;
	protected Application $terminal;
	protected string $action;

	protected array $allowTags = [
		"<p>", "<a>", "<b>", "<i>", "<s>", "<q>", "<br><br/><br />",
		"<abbr>", "<address>", "<bdo>", "<blockquote>", "<cite>",
		"<code>", "<del>", "<ins>", "<dfn>", "<em>", "<wbr>", "<var>", "<u>",
		"<sup>", "<sub>", "<plaintext>", "<small>", "<nobr>", "<kbd>",
		"<h1>", "<h2>", "<h3>", "<h4>", "<h5>", "<h6>"
	];

	/**
	 * @throws Exception
	 */
	public function __construct($route)
	{
		parent::__construct($route);

		$plugins = Plugins::instance();
		$this->layout = "admin";
		$this->action = $route['action'];

		$this->addCache();

		$this->terminal = new Application("AG_Terminal", "0.0.1");
		$this->terminal->setAutoExit(false);
		$this->registerTerminalCommands();
		$this->terminal->getDefinition();
		$this->terminal->setCatchExceptions(false);

		if (!defined("ADMIN_LAYOUT")) define("ADMIN_LAYOUT", $this->layout);

		$this->setMeta(__('Admin Panel'));

		if (!empty($_COOKIE['auth_key']) && !Sessions::check($_COOKIE['auth_key'])) {
			User::logout();
		}

		if (!User::isAdmin() && !in_array($route['action'], ['login-admin', 'forgot-user', 'forgot-password'])) {
			redirect(ADMIN . '/user/login-admin');
		}

		if (!in_array($route['action'], ['login-admin', 'forgot-user', 'forgot-password'])) {
			$this->user = $_SESSION['user'];
			$session = $this->user;

			$time = strtotime($session['reg_date']);
			$num_month = date("n", $time);
			$month = $this->monthsList[$num_month];
			$year = date("Y", $time);
			$session['reg'] = "$month, $year";

			MenuGenerator::generateMenu();
		} else {
			$session = [];
		}

		$this->checkTfaAuth();

		if (!($langList = Cache::get("country-list"))) {
			$langList = Country::getAll();
			Cache::set("country-list", $langList);
		}

		doAction("admin_menu_edit", ["object" => $this]);

		$appObj = App::$app;
		$menu = MenuGenerator::getMenu();
		$this->checkOnline();


		$check_cms = Cache::get('cms-check-update');
		if (!empty($check_cms)) {
			$local_version = $check_cms['version']['local'];
			$server_version = $check_cms['version']['server'];
		} else {
			$local_version = $appObj->AE_CMS_VERSION_LOCAL;
			$server_version = 0;
		}
		$isUpdate = ($server_version === -1 || $server_version > $local_version);

		$recaptcha_site_key = ENV->RECAPTCHA_SITE_KEY;

		$this->setStyles([
			"https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700",
			PATH_ADMINLTE . "/plugins/fontawesome-free/css/all.min.css",
			PATH_ADMINLTE . "/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css",
			PATH_ADMINLTE . "/plugins/icheck-bootstrap/icheck-bootstrap.min.css",
			PATH_ADMINLTE . "/plugins/jqvmap/jqvmap.min.css",
			PATH_ADMINLTE . "/dist/css/adminlte.min.css",
			PATH_ADMINLTE . "/plugins/overlayScrollbars/css/OverlayScrollbars.min.css",
			PATH_ADMINLTE . "/plugins/daterangepicker/daterangepicker.css",
			PATH_ADMINLTE . "/plugins/summernote/summernote-bs4.css",
			PATH_ADMINLTE . "/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css",
			PATH_ADMINLTE . "/plugins/datatables-bs4/css/dataTables.bootstrap4.css",
			PATH_ADMINLTE . "/plugins/codemirror/codemirror.css",
			"css/ionicons.min.css",
			"css/main.css",
			"css/bootstrap-icons.css",
		]);

		if (isset($appObj->dark_mode)) {
			$this->setStyle('css/sweetalert2-dark.css');
		}

		$this->setScripts([
			PATH_ADMINLTE . "/plugins/jquery-ui/jquery-ui.min.js",
			PATH_ADMINLTE . "/plugins/bootstrap/js/bootstrap.bundle.min.js",
			PATH_ADMINLTE . "/plugins/chart.js/Chart.min.js",
			PATH_ADMINLTE . "/plugins/sparklines/sparkline.js",
			PATH_ADMINLTE . "/plugins/jqvmap/jquery.vmap.min.js",
			PATH_ADMINLTE . "/plugins/jqvmap/maps/jquery.vmap.usa.js",
			PATH_ADMINLTE . "/plugins/jquery-knob/jquery.knob.min.js",
			PATH_ADMINLTE . "/plugins/moment/moment.min.js",
			PATH_ADMINLTE . "/plugins/daterangepicker/daterangepicker.js",
			PATH_ADMINLTE . "/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js",
			PATH_ADMINLTE . "/plugins/summernote/summernote-bs4.min.js",
			PATH_ADMINLTE . "/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js",
			PATH_ADMINLTE . "/dist/js/adminlte.js",
			PATH_ADMINLTE . "/plugins/datatables/jquery.dataTables.js",
			PATH_ADMINLTE . "/plugins/datatables-bs4/js/dataTables.bootstrap4.js",
			PATH_ADMINLTE . "/plugins/inputmask/jquery.inputmask.js",
			PATH_ADMINLTE . "/plugins/jquery-validation/jquery.validate.min.js",
			PATH_ADMINLTE . "/plugins/jquery-validation/additional-methods.min.js",
			"js/libs/jquery/jquery.cookie.js",
			"js/libs/ajaxupload.js"
		]);
		$this->setScript("js/libs/sweetalert2.js", false, false, "module");
		$this->setScript("js/functions.js", false, true, "module");
		$this->setScript("js/main.js", false, true, "module");

		$this->setData(compact('appObj', 'session', 'menu', 'langList', 'isUpdate', 'local_version', 'server_version', 'recaptcha_site_key'));
	}

	protected function addCache(): void
	{
		Cache::addCache(__("Temporary images"), __("Temporary images cache for AdminPanel"), "tmp-images", true, "/public/tmp");
		if ($_SESSION['redis']) Cache::addCache(__("Redis"), __("Redis database"), "redis");
		Cache::addCache(__("Information"), __("Information cache for AdminPanel"), "ajax-info-admin");
		Cache::addCache(__("Version"), __("Version cache for AdminPanel"), "cms-check-update");
		Cache::addCache(__("Pushover"), __("Pushover cache"), "pushover-check");
		Cache::addCache(__("Unique users"), __("Unique users cache"), "uniq-users");
		Cache::addCache(__("Git"), __("Git cache for AdminPanel"), "git-commits");
		Cache::addCache(__("History"), __("History cache for AdminPanel"), "git-releases");
		Cache::addCache(__("Backup"), __("Backup cache for website"), "cms_backup", true, "/tmp/update/");
		Cache::addCache(__('Curl'), __('Cache curl requests'), 'curl', false, "/tmp/logs/ae_curl.log");
	}

	private function registerTerminalCommands(): void
	{
		foreach (scandir(LIBS . "/terminalCommands/") as $file) {
			if ($file[0] == '.') continue;
			$file = mb_substr($file, 0, mb_strlen($file) - 4);
			$className = '\\core\\libs\\terminalCommands\\' . $file;
			$this->terminal->add(new $className);
		}
	}

	protected function checkTfaAuth(): void
	{
		if (!empty($_SESSION['user']['tfa']) && isset($_SESSION['tfa_status']) && $_SESSION['tfa_status'] == "none" && $this->action !== 'logout' && $this->action !== 'tfa' && $this->action !== 'login-admin') {
			redirect(ADMIN . "/user/tfa");
		}
	}

	/**
	 * @throws SQL|Exception
	 */
	public function checkOnline(): void
	{
		if (!(Cache::get("check-online"))) {
			$users = User::getAll();
			$dt = new DateTime(getTimestamp());

			foreach ($users as $user) {
				$diff = $dt->diff(new DateTime($user->last_action));
				if ($diff->i >= 3) {
					$user->online = 0;
					R::store($user);
				}

				foreach (Sessions::getAllActiveSession($user->getID()) as $session) {
					$sessionTime = new DateTime($session->time);
					$sessionTime->add(new DateInterval("PT" . TIME_SAVE_PASSWORD . "S"));
					if ($dt->diff($sessionTime)->invert) {
						$session->is_active = 0;
						R::store($session);
					}
				}
			}

			foreach (UserForgot::getAll() as $forgot) {
				$diff = $dt->diff(new DateTime(getTimestamp($forgot->time_end)));
				if ($diff->invert) {
					R::trash($forgot);
				}
			}

			Cache::set("check-online", $users, 60);
		}
	}

	abstract static function addMenu();

	public static function isModuleManagerActive(string $name): bool
	{
		return in_array($name, ModuleController::getActiveModules()) && static::$active;
	}

	/**
	 * @route /admin/add-image
	 */
	public function addImageAction(): void
	{
		$this->uploadImage("app", 1920, 1080);
	}

	public function uploadImage(string $session, int $width = -1, int $height = -1): void
	{
		if (isset($_GET['upload'])) {
			$name = $_POST['name'];
			$this->uploadImg($name, $width, $height, $session . "-image");
		}
	}

	public function uploadImg(string $name, int $width, int $height, string $session): void
	{
		$uploadDir = WWW . '/tmp/';

		if (!file_exists($uploadDir)) {
			mkdir($uploadDir);
		}

		if (file_exists($_SESSION['tmp_image_profile'])) {
			unlink($_SESSION['tmp_image_profile']);
		}

		$ext = strtolower(preg_replace("#.+\.([a-z]+)$#i", "$1", $_FILES[$name]['name'])); // расширение картинки
		$types = array("image/gif", "image/png", "image/jpeg", "image/pjpeg", "image/x-png", "image/webp"); // массив допустимых расширений
		if ($_FILES[$name]['size'] > 1048576 * 5) { // 5Мб
			$res = array("error" => __("Error! The file is too large. Maximum file size is") . " 5 " . __('Mb') . '.');
			exit(json_encode($res));
		}
		if ($_FILES[$name]['error']) {
			$res = array("error" => __("Error! The file is too large. Maximum file size is") . " 5 " . __('Mb') . '.');
			exit(json_encode($res));
		}
		if (!in_array($_FILES[$name]['type'], $types)) {
			$res = array("error" => __("Error! The file type is not allowed. Only:") . " " . implode(", ", $types));
			exit(json_encode($res));
		}
		$new_name = md5(time()) . "." . $ext;
		$uploadfile = $uploadDir . $new_name . ".cache";

		$_SESSION['tmp_image_profile'] = $uploadfile;

		if (@move_uploaded_file($_FILES[$name]['tmp_name'], $uploadfile)) {
			$_SESSION[$session] = $new_name;

			$image = new SimpleImage($uploadfile);

			if ($width == -1) {
				$image->resizeToHeight($height);
			} else if ($height == -1) {
				$image->resizeToWidth($width);
			} else {
				$image->resize($width, $height);
			}

			$image->save();

			$res = array("file" => $new_name);
			exit(json_encode($res));
		}
	}

}
