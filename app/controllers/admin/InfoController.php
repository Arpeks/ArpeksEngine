<?php

namespace app\controllers\admin;

use core\App;
use core\base\EngineException;
use core\Cache;
use core\MenuGenerator;
use JetBrains\PhpStorm\NoReturn;

class InfoController extends AppController
{

	static function addMenu(): void
	{
		MenuGenerator::addMenu(__('Information'), ADMIN . "#", "fa fa-book", [
			["name" => __('History'), "url" => ADMIN . "/info/history", "icon" => "fas fa-history"],
			["name" => __('Hook'), "url" => ADMIN . "/info/hook", "icon" => "fas fa-info"],
			["name" => __('Error'), "url" => ADMIN . "/info/error", "icon" => "fas fa-bug"],
			["name" => __('Block IP'), "url" => ADMIN . "/info/block-ip", "icon" => "fas fa-user-lock"]
		]);
	}

	/**
	 * @route /admin/info
	 */
	#[NoReturn] public function indexAction(): void
	{
		redirect(ADMIN . "/info/history");
	}

	/**
	 * @throws EngineException
	 * @route /admin/info/history
	 */
	public function historyAction(): void
	{
		$app = $this->app;
		$git = App::$git;
		$historyList = array();

		if (!($releases = Cache::get('git-releases'))) {
			$releases = $git->getReleases();
			Cache::set('git-releases', $releases);
		}

		if (is_array($releases))
			foreach ($releases as $id => $release) {
				$historyList[$id]['build_num'] = preg_replace("/ae-([0-9])\.?([0-9])\.?([0-9])/", "$1$2$3", $release['tag_name']);
				$historyList[$id]['description'] = explode("\n", str_replace("- ", "", $release['description']));
				$historyList[$id]['timestamp'] = strtotime($release['released_at']);
			}


		$historyLocal = include_once CONF . "/history.php";
		$historyList = array_merge([], $historyList, array_reverse($historyLocal));

		foreach ($historyList as $id => $release) {
			$descTemp = "<ul>";
			foreach ($release['description'] as $desc) {
				$desc = preg_replace("~`([^`]*)`~", "<code>$1</code>", $desc);
				$descTemp .= "<li>" . $desc . "</li>&#x000A;";
			}
			$descTemp .= "</ul>";
			$release['description'] = $descTemp;

			$tmpDate = changeDateFormat($release['timestamp']);
			$month = date('n', $release['timestamp']);
			$release['date'] = preg_replace("~\.([0-9]+)\.~", ' ' . getShortMonth($month) . ' ', $tmpDate);
			$historyList[$id] = $release;
		}

		$this->setData(compact('historyList', 'app'));
		$this->setMeta('History');
	}

	/**
	 * @route /admin/info/error
	 */
	public function errorAction(): void
	{
		$errorsAll = App::$errors->get('e-web');
		$errors = array();
		for ($i = 0; $i < MAX_VIEW_LAST_ERROR; $i++) {
			if (!empty($errorsAll[$i]))
				$errors[$i] = $errorsAll[$i];
			else
				break;
		}
		$this->setData(compact('errors'));
		$this->setMeta('Error');
	}

	/**
	 * @route /admin/info/block-ip
	 */
	public function blockIpAction(): void
	{
		$errorsAll = App::$errors->get('e-ip');
		$errors = array();
		for ($i = 0; $i < MAX_VIEW_LAST_ERROR; $i++) {
			if (!empty($errorsAll[$i]))
				$errors[$i] = $errorsAll[$i];
			else
				break;
		}
		$this->setData(compact('errors'));
		$this->setMeta('Block IP');
	}

	/**
	 * @route /admin/info/hook
	 */
	public function hookAction(): void
	{
		$app = $this->app;
		$hooks = include_once CONF . "/hookList.php";
		$hookList = array();

		$isActiveExplorerModule = !empty(ModuleController::getActiveModuleByName("Explorer"));

		foreach ($hooks as $hook) {
			if ($isActiveExplorerModule) {
				$hook['file'] = "<a target='_blank' href='" . ADMIN . "/module/explorer?path=" . $hook['file'] . "'>" . basename($hook['file']) . "</a>";
			} else {
				$hook['file'] = basename($hook['file']);
			}

			if (!$hook['params']) $hook['params'] = __("Params not found");

			$hookList[$hook['type']][$hook['name']] = $hook;
		}
		ksort($hookList['filter']);
		ksort($hookList['hook']);

		$this->setData(compact('hookList', 'app'));
		$this->setMeta('Hook');
	}
}
