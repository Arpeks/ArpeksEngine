<?php

namespace app\controllers\admin\libs;

enum ChangeFreqSitemap: string
{
	case always = 'always';
	case hourly = 'hourly';
	case daily = 'daily';
	case weekly = 'weekly';
	case monthly = 'monthly';
	case yearly = 'yearly';
	case never = 'never';
}
