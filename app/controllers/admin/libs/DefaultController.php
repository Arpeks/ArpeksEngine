<?php

namespace app\controllers\admin\libs;


use app\models\Audit;
use core\App;
use core\base\Controller;

class DefaultController extends Controller
{

	protected array $app;
	protected Audit $audit;

	protected static bool $active = true;

	public function __construct($route)
	{
		parent::__construct($route);
		$this->app = App::$app->getAll();
		$this->audit = App::$audit;

		$app = $this->app;
		$this->setData(compact('app'));
	}

}
