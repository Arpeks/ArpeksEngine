<?php

namespace app\controllers\admin\libs;

enum Menu
{
	case Main;
//    case Terminal;
	case Profile;
	case User;
	case Module;
//    case Plugins;
	case Settings;
	case Backup;
	case Info;
}
