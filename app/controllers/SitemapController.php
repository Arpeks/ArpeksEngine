<?php

namespace app\controllers;

use JetBrains\PhpStorm\NoReturn;

class SitemapController
{

	/**
	 * @route /sitemap
	 * @route /sitemap.xml
	 */
	#[NoReturn]
	public function indexAction(): void
	{
		global $pages;
		$sitemap = '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
		$sitemap .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . PHP_EOL;

		require_once(CONF . '/sitemap.php');

		foreach ($pages as $page) {
			if (isset($page['table'])) {
				foreach ($page['table']::getAll() as $row) {
					$sitemap .= addUrlToSitemap('/' . trim($row->uri, '/'), strtotime($row->last_update), $page['changeFreq'], $page['priority']);
				}
			} else {
				$sitemap .= addUrlToSitemap($page['uri'], $page['lastMod'], $page['changeFreq'], $page['priority']);
			}

		}

		$sitemap .= '</urlset>';

		header('Content-Type: application/xml');
		echo $sitemap;
		die;
	}
}
