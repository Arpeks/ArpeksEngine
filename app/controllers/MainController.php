<?php

namespace app\controllers;


use app\controllers\admin\libs\DefaultController;
use app\models\module\Gallery;
use app\models\module\Message;
use app\widgets\SocialsWidget\SocialsWidget;
use core\base\EngineException;
use core\Cache;
use RedBeanPHP\RedException\SQL;

class MainController extends DefaultController
{

	/**
	 * @route /
	 */
	public function indexAction(): void
	{
		$this->app["monthList"] = $this->monthsList;
		$app = $this->app;

		if (!Cache::get('gallery')) {
			$gallery = Gallery::getAll();
			Cache::set('gallery', $gallery);
		} else $gallery = Cache::get('gallery');

		$socials = new SocialsWidget();

		$this->setMeta("", $app['site_description'], $app['site_keywords']);
		$this->setData(compact('gallery', 'socials'));
	}

	/**
	 * @throws SQL|EngineException
	 * @route /send-contact
	 */
	public function sendContactAction()
	{
		if (empty($_POST)) return false;
		$mess = new Message();
		$data = $_POST;
		$return = array();

		if (hash_equals($_SESSION['csrf'], $data['csrf'])) {
			$data['time'] = getTimestamp();
			if ($mess->loadWithValidate($data)) {
				if ($mess->save()) $return = ['status' => 'ok'];
			} else $return = ['status' => 'error', 'description' => $mess->getErrors(false)];
		} else $return = ['status' => 'error', 'description' => __('Token CSRF not valid!')];

		if ($return['status'] == 'error') throw new EngineException($return['description'], 606);

		echo json_encode($return, JSON_UNESCAPED_SLASHES + JSON_UNESCAPED_UNICODE);
		die;
	}

}
