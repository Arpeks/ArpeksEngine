<?php

namespace core;

use app\controllers\admin\libs\Menu;
use app\controllers\admin\MainController;
use app\controllers\admin\ModuleController;
use core\interfaces\Module;
use core\libs\Plugins;
use core\registry\MenuRegistry;
use core\traits\Registry;
use JetBrains\PhpStorm\ArrayShape;
use Throwable;

/**
 * @author Arpeks
 * @package core
 * @version 235
 */
abstract class MenuGenerator
{

	use Registry;

	private static MenuRegistry $menu;

	/**
	 * @return void
	 * @global Plugins $plugins
	 */
	public static function generateMenu(): void
	{
		self::$menu = MenuRegistry::instance();

		$namespace = 'app\\controllers\\admin\\';

		$menuClasses = Menu::cases();

		self::addMenu(__('Home'), PATH, "fas fa-tags", array(), array(), true);
		for ($i = 0; $i < sizeof($menuClasses); $i++) {
			/** @var MainController $class */
			$class = $namespace . $menuClasses[$i]->name . "Controller";
			$class::addMenu();
		}

		$namespaceModule = $namespace . 'module\\';

		foreach (ModuleController::getActiveModules() as $module) {
			try {
				if (empty($module)) continue;
				/* @var Module $class */
				$class = $namespaceModule . $module . "Controller";
				$class::setMenuModule();
			} catch (Throwable $e) {
				continue;
			}
		}

		$plugins = Plugins::instance();

		foreach ($plugins->pluginList as $plugin => $param) {
			if ((isset($param['deactivated']) && $param['deactivated']) || !file_exists(APP_PLUGIN . "/" . $plugin . "/admin.php")) continue;
			include_once APP_PLUGIN . "/" . $plugin . "/admin.php";
		}
	}


	public static function addMenu($name, $url, $iconClass = 'far fa-circle', $tree = array(), $append = array(), $blank = false): void
	{
		$tmp = self::generateTreeAndAppend($tree, $append);

		$merge = array(
			"url" => h($url),
			"icon" => h($iconClass),
			"blank" => $blank
		);

		if (str_contains($name, "plugin:")) {
			$tmp_name = str_replace("plugin:", "", $name);
			$merge['name'] = __($tmp_name, $tmp_name);
		} else {
			$merge['name'] = $name;
		}

		self::$menu->set($name, array_merge($merge, $tmp));
	}


	public static function addModulePage($name, $url, $iconClass = "far fa-circle", $tree = array(), $append = array(), $blank = false): void
	{
		self::addMenu("ae_delimiter_modules", __('Modules'));
		self::addMenu($name, $url, $iconClass, $tree, $append, $blank);
	}


	public static function deleteMenu(string $name): bool
	{
		$menuCases = Menu::cases();
		$menu = array();
		for ($i = 0; $i < sizeof($menuCases); $i++) {
			/** @var MainController $class */
			$menu[$i] = $menuCases[$i]->name . "Menu";
		}

		if (in_array($name, $menu)) return false;
		return self::$menu->remove($name);
	}

	public static function addSubPluginMenu($name, $iconClass = "far fa-circle", $tree = false, $append = false, $blank = false): void
	{
		self::addMenu("ae_delimiter_plugins", __('Plugins'));
		self::addMenu("plugin:" . $name, ADMIN . "/plugins/manager/" . h(clearSpace($name)), $iconClass, $tree, $append, $blank);
	}

	public static function isPluginSettingsMenu(string $name): bool
	{
		return self::getMenu()->isset("plugin:" . $name);
	}

	public static function getMenu(): ?MenuRegistry
	{
		self::$menu = MenuRegistry::instance();
		return self::$menu;
	}

	#[ArrayShape(["is_tree" => "bool", "tree" => "array[]", "is_append" => "bool", "append" => "array[]"])]
	private static function generateTreeAndAppend($tree = array(), $append = array()): array
	{
		$tmpTree = array();
		$is_tree = array();

		if (!empty($tree)) {
			foreach ($tree as $item) {
				if (empty($item['url']) || empty($item['name'])) continue;
				$tmpTree[h($item['name'])] = [
					'name' => __(h($item['name'])),
					'url' => h($item['url']),
					'icon' => (!empty($item['icon'])) ? $item['icon'] : "far fa-circle"
				];
			}
			$is_tree = true;
		}

		$is_append = array();
		$tmpAppend = array();

		if (!empty($append)) {
			foreach ($append as $item) {
				if (empty($item['color']) || !isset($item['text'])) continue;
				$tmpAppend[] = [
					'text' => h($item['text']),
					'color' => h($item['color']),
					'class' => h($item['class'])
				];
			}
			$is_append = true;
		}

		return [
			"is_tree" => $is_tree,
			"tree" => $tmpTree,
			"is_append" => $is_append,
			"append" => $tmpAppend,
		];
	}

}
