<?php

namespace core;


use core\base\EngineException;
use core\base\Model;
use core\traits\Singletone;
use R;
use RedBeanPHP\RedException;

/**
 * @author Arpeks
 * @package core
 * @version 232
 */
class DataBase
{

	use Singletone;

	private static bool $isConnect = false;

	/**
	 * @throws EngineException
	 */
	protected function __construct()
	{
		$db = require_once CONF . "/db.php";
		R::setup($db['dsn'], $db['user'], $db['password']);
		self::$isConnect = R::testConnection();

		if (!self::$isConnect) {
			throw new EngineException("No database connection", EngineException::DATABASE_ERROR);
		}

		try {
			R::useFeatureSet('novice/latest');
			R::debug(false, 2);
			R::ext('xdispense', function ($type) {
				return R::getRedBean()->dispense($type);
			});
		} catch (RedException $e) {
			throw new EngineException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
	}

	public static function isConnect(): bool
	{
		return self::$isConnect;
	}

	/**
	 * Возвращает все таблицы базы данных
	 * @return array Массив со списком таблиц
	 * @since 232
	 */
	public function getAllTables(): array
	{
		$tables = array();

		/** @var Model $class */
		foreach (getFileList(APP_MODEL) as $filename) {
			if ($filename[0] == '.' || is_dir($filename))
				continue;
			$class = str_replace(ROOT, "", $filename);
			$class = str_replace(".php", "", $class);
			$class = str_replace("/", "\\", $class);

			$tables[$class] = $class::getTable();
		}

		return $tables;
	}


}
