<?php

namespace core;


use app\models\Country;
use RedBeanPHP\OODBBean;

require_once CONF . DIRECTORY_SEPARATOR . 'bindtextdomain.php';

/**
 * @author Arpeks
 * @package core
 * @version 235
 */
abstract class Language
{

	private static string $language = USER_LANGUAGE;

	/**
	 * Возвращает язык
	 * @param bool $short
	 * @return string
	 */
	public static function getLanguage(bool $short): string
	{
		return (!$short) ? self::$language : explode("_", self::$language)[0];
	}

	/**
	 * Устанавливает язык
	 * @param OODBBean|Country $country Объект базы данных таблицы Countries.
	 */
	public static function setLanguage(OODBBean|Country $country): void
	{
		$lang = $country->language ?? USER_LANGUAGE;
		self::$language = $lang;
		setlocale(LC_ALL, $lang);

		setBindTextDomain('main', false);
		textdomain('main');
	}

}
