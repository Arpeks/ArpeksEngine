<?php

namespace core\traits;

/**
 * @author Arpeks
 * @package core\traits
 * @version 235
 */
trait Singletone
{

	private static array $instances;

	/**
	 * @return static
	 */
	public static function instance(): static
	{
		if (!isset(self::$instances[static::class])) {
			self::$instances[static::class] = new static;
		}
		return self::$instances[static::class];
	}

}
