<?php

namespace core\traits;

/**
 * @author Arpeks
 * @package core\traits
 * @version 235
 */
trait Registry
{

	private static array $properties = array();


	public function __set(string $name, mixed $value): void
	{
		$this->set($name, $value);
	}

	public function __get(string $key): mixed
	{
		return $this->get($key);
	}

	public function set(string $key, mixed $value): void
	{
		self::$properties[$key] = $value;
	}

	public function get(string $key): mixed
	{
		if (isset(self::$properties[$key]))
			return self::$properties[$key];
		return null;
	}

	public function getAll(): array
	{
		return self::$properties;
	}

	public function remove(string|array $key): bool
	{
		switch (gettype($key)) {
			case "array":
				foreach ($key as $oneKey)
					if (isset(self::$properties[$oneKey])) {
						unset(self::$properties[$oneKey]);
					}
				return true;
			case "string":
				if (isset(self::$properties[$key])) {
					unset(self::$properties[$key]);
					return true;
				}
				break;
		}

		return false;
	}

	public function clear(): void
	{
		self::$properties = array();
	}

	public function isset(string $key): bool
	{
		return isset(self::$properties[$key]);
	}

	public static function import(array $data): void
	{
		foreach ($data as $key => $value) {
			self::$properties[$key] = $value;
		}
	}

}