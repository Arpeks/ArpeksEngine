<?php


namespace core\interfaces;

/**
 * @author Arpeks
 * @package core\interfaces
 * @version 235
 */
interface Module
{
	static function setMenuModule();

	static function cachePage();

	static function getFileList(): array;

	static function getDatabaseTableName(): string|array;
}