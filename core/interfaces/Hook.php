<?php


namespace core\interfaces;

/**
 * @author Arpeks
 * @package core\interfaces
 * @version 235
 */
interface Hook
{
	function run(PluginManager $hookName, array $param);
}