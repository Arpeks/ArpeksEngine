<?php


namespace core\interfaces;

/**
 * @author Arpeks
 * @package core\interfaces
 * @version 235
 */
interface PluginManager
{
	function registerHook(Hook $hook);

	function registerFilter(Hook $filter);

	function delete(Hook $hook);

	function createHook(string $hookName, array $param = array());

	function createFilter(string $filterName, array $param = array());
}