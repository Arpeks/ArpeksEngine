<?php

namespace core;

use core\base\Controller;
use core\base\EngineException;

/**
 * @author Arpeks
 * @package core
 * @version 235
 */
class Router
{

	protected static array $routes = array();
	protected static array $route = array();

	public static function add(string $regexp, array $route = []): void
	{
		self::$routes[$regexp] = $route;
	}

	/**
	 * @throws EngineException
	 */
	public static function dispatch(string $url): void
	{
		$query = $url;

		$search = ['wlmmanifest.xml'];
		foreach ($search as $s) {
			if (str_contains($url, $s)) {
				header("HTTP/1.1 404 Not Found");
				return;
			}
		}

		$url = self::removeQueryString($url);

		if (self::matchRoute($url)) {
			$controller = str_replace("/", "\\", "app\\controllers\\" . self::$route['prefix'] . self::$route['controller'] . "Controller");
			if (class_exists($controller)) {
				/* @var Controller $controllerObject */
				$controllerObject = new $controller(self::$route);
				$action = camelCase(self::$route['action']) . 'Action';

				if (method_exists($controllerObject, $action)) {
					doAction('url_after_parse', ["query" => $query, 'object' => $controllerObject]);
					$controllerObject->$action();
					$controllerObject->getView();
					$_SESSION['last_url'] = PATH . '/' . $query;
				} else {
					throw new EngineException("$controller::$action not found!", 404);
				}
			} else {
				throw new EngineException("$controller not found!", 404);
			}
		} else {
			throw new EngineException('Page not found! Page: {' . $url . '}', 404);
		}
	}

	public static function matchRoute($url)
	{
		foreach (self::$routes as $pattern => $route) {
			if (preg_match("#$pattern#", $url, $matches)) {
				foreach ($matches as $key => $value) {
					if (is_string($key)) {
						$route[$key] = $value;
					}
				}
				if (empty($route['action']))
					$route['action'] = 'index';
				if (!isset($route['prefix']))
					$route['prefix'] = '';
				else {
					$route['prefix'] .= "/";
				}
				$route['controller'] = pascalCase($route['controller']);
				self::$route = $route;
				return $route;
			}
		}
		return false;
	}

	public static function removeQueryString($url): string
	{
		if ($url) {
			$params = explode("&", $url, 2);
			if (!str_contains($params[0], '=')) {
				return rtrim($params[0], "/");
			}
		}
		return "";
	}

}
