<?php

namespace core;

use core\base\EngineErrors;
use core\base\EngineException;
use core\traits\Singletone;
use DateTime;
use JetBrains\PhpStorm\NoReturn;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Throwable;

/**
 * @author Arpeks
 * @package core
 * @version 235
 */
class ErrorHandler
{

	private static Logger $logger;
	private static Logger $ipBlock;

	use Singletone;

	private function __construct()
	{
		$dateFormat = "Y-m-d H:i:s";
		$output = "[%datetime%] %channel%.%level_name%: %message%\n";
		$firePHP = new FirePHPHandler();

		self::$logger = new Logger('AG_Logger');
		$stream = new StreamHandler(LOGS . '/errors.log');
		$stream->setFormatter(new LineFormatter($output, $dateFormat, false, false, false));
		self::$logger->pushHandler($stream);
		self::$logger->pushHandler($firePHP);

		self::$ipBlock = new Logger('BLOCK_IP');
		$streamBlockIP = new StreamHandler(LOGS . '/block_ip.log');
		$streamBlockIP->setFormatter(new LineFormatter($output, $dateFormat, false, false, false));
		self::$ipBlock->pushHandler($streamBlockIP);
		self::$ipBlock->pushHandler($firePHP);

		if(isset(ENV->DISPLAY_ERRORS) && ENV->DISPLAY_ERRORS) {
			$firePHP = new FirePHPHandler();
			self::$logger->pushHandler($firePHP);
			self::$ipBlock->pushHandler($firePHP);
		}

		self::$logger->setTimezone((new DateTime)->getTimezone());
		self::$ipBlock->setTimezone((new DateTime)->getTimezone());

		ini_set('display_errors', ENV->DISPLAY_ERRORS);
		error_reporting(E_ALL);
	}

	/**
	 * @return Logger
	 */
	public static function getIpBlock(): Logger
	{
		return self::$ipBlock;
	}

	/**
	 * @return Logger
	 */
	public static function getLogger(): Logger
	{
		return self::$logger;
	}


	public static function errorHandler(Throwable $error, bool $isDisplayError = true): void
	{
		$e = EngineErrors::fromThrowable($error);
		self::engineErrorHandler($e, $isDisplayError);
	}

	public static function engineErrorHandler(EngineErrors $e, bool $isDisplayError): void
	{
		self::logErrors($e);
		if ($isDisplayError)
			self::displayError($e);
	}

	private static function logErrors(EngineErrors $e): void
	{
		if ($e->getCode() == EngineException::IP_BLOCK) {
			self::$ipBlock->warning($e->getMessage());
		} else {

			$server['HTTP_REFERER'] = (empty($_SERVER['HTTP_REFERER'])) ? "" : $_SERVER['HTTP_REFERER'];
			$server['QUERY_STRING'] = $_SERVER['QUERY_STRING'];
			$server['REQUEST_URI'] = $_SERVER['REQUEST_URI'];
			$server['REQUEST_SCHEME'] = $_SERVER['REQUEST_SCHEME'];
			$server['REQUEST_TIME'] = $_SERVER['REQUEST_TIME'];
			$server['argv'] = !empty($_SERVER['argv']) ? $_SERVER['argv'] : array();


			$error = sprintf("Код: %u; Ошибка: %s; IP: %s; Текст: %s; Файл: %s; Строка: %u; Браузер: %s; SERVER: %s; Route: %s; Backtrace: %s",
				$e->getCode(), $e->getErrorName(), getIP(), $e->getMessage(), $e->getFile(), $e->getLine(), str_replace(";", ",", json_encode(detectBrowser())),
				json_encode($server), json_encode((!empty($_SESSION['route'])) ? $_SESSION['route'] : array()), json_encode($e->getBacktrace())
			);
			self::$logger->error($error);
		}
	}

	#[NoReturn] private static function displayError(EngineErrors $e): void
	{
		http_response_code(200);

		App::$variable->set("error_code", $e->getCode());
		App::$variable->set("error_file", $e->getFile());
		App::$variable->set("error_line", $e->getLine());
		App::$variable->set("error_message", $e->getMessage());
		App::$variable->set("error_name", $e->getErrorName());
		App::$variable->base_path_layout = PATH . "/ag-themes/" . App::$app->layout . "/";

		ob_start();
		$file = WWW_THEME . "/" . App::$app->layout . "/template/errors.php";
		if (str_contains($_SERVER['REQUEST_URI'], "admin") || !file_exists($file)) {
			include_once WWW_THEME . "/admin/template/errors.php";
		} else {
			include_once $file;
		}
		echo App::$variable->parser(ob_get_clean());
		die;
	}

}
