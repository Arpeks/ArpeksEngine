<?php

namespace core;

use app\models\Audit;
use app\models\Country;
use app\models\User;
use core\base\EngineException;
use core\libs\BBCode;
use core\libs\GitLab;
use core\libs\Plugins;
use core\libs\Pushover;
use core\libs\RedisSingleton;
use core\registry\AppRegistry;
use core\registry\ErrorsRegistry;
use core\registry\VariableRegistry;
use Exception;
use JetBrains\PhpStorm\ArrayShape;
use RedBeanPHP\RedException\SQL;
use Throwable;

/**
 * @author Arpeks
 * @link https://vk.com/id105420853
 * @version 236
 * @package core
 */
class App
{
	const string CMS_NAME = "Arpeks Engine";
	const int CMS_YEAR = 2023;
	const int CMS_VERSION = 236;
	const array MONTH_LIST = array("янв", "фев", "март", "апр", "май", "июнь", "июль", "авг", "сен", "окт", "нояб", "дек");
	public static AppRegistry $app;
	public static Audit $audit;
	public static ErrorsRegistry $errors;
	public static VariableRegistry $variable;
	public static GitLab $git;
	public static Pushover $push;
	public static BBCode $bbCode;
	public static Plugins $plugins;
	private static bool $whiteListIP = false;
	private string|array|null $cmsVersion;
	private int $cmsBuildNumber;

	/**
	 * @throws EngineException
	 * @throws SQL
	 */
	public function __construct()
	{
		$query = rtrim($_SERVER['QUERY_STRING'], '/');
		$ip = getIP();
		$bot = '';
		self::$whiteListIP = in_array($ip, explode(",", ENV->WHITELIST_IPS));

		if (!defined("QUERY_STRING")) define("QUERY_STRING", $query);

		if (!file_exists(TEMP)) mkdir(TEMP);
		if (!file_exists(LOGS)) mkdir(LOGS);
		if (!file_exists(CACHE)) mkdir(CACHE);
		if (!file_exists(APP_PLUGIN)) mkdir(APP_PLUGIN);
		if (!file_exists(TEMP_UPDATE)) mkdir(TEMP_UPDATE);

		// Redis
		if (!isset($_SESSION['redis'])) {
			$_SESSION['redis'] = false;
			try {
				if (extension_loaded('redis')) {
					if (RedisSingleton::instance())
						$_SESSION['redis'] = true;
				}
			} catch (Exception $e) {
			}
		}

		self::$app = AppRegistry::instance();
		self::$git = GitLab::instance();
		self::$errors = ErrorsRegistry::instance();
		self::$variable = VariableRegistry::instance();
		self::$bbCode = BBCode::instance();

		require_once CONF . "/bbcode.php";
		require_once CONF . "/bbcode_user.php";

		try {
			self::$git->setProjectNumber(17088136);

			if (!($build = Cache::get('git-commits'))) {
				$lstRelease = self::$git->getLastRelease(self::isWhiteListIP() ? 0 : 1);
				if (is_array($lstRelease)) {
					$lstCommit = $lstRelease['commit'] ?? false;
					$build = self::$git->getAllCommits("", ["ref_name" => self::isWhiteListIP() ? "dev" : "master", "since" => $lstCommit['committed_date']]);
					Cache::set('git-commits', $build);
				} else throw new Exception;
			}
			$this->cmsBuildNumber = count($build);

			if (!($releases = Cache::get('git-releases'))) {
				$releases = self::$git->getReleases();
				Cache::set('git-releases', $releases);
			}
			$this->cmsVersion = preg_replace("/ae-([0-9])\.?([0-9])\.?([0-9])/", "$1$2$3", $releases[0]['tag_name']);
		} catch (Throwable $e) {
			$this->cmsBuildNumber = 0;
			$this->cmsVersion = null;
		}

		$this->setApp();
		DataBase::instance();
		$this->setApp();

		User::updateLastAction();
		Language::setLanguage(User::getLanguage());

		self::$plugins = Plugins::instance();
		self::$errors->addAll();

		if (!isset($_SERVER['HTTPS']) && self::$app->isset("https")) {
			redirect("https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
		}

		// Уникальные пользователи
		$uniq = Cache::get('uniq-users');
		if (empty($uniq) || !isset($uniq[$ip])) {
			$_SESSION['browser'] = detectBrowser();
			isBot($bot, $_SESSION['browser']);

			$uniq[$ip] = [
				"ip" => $ip,
				"isBot" => empty($bot) ? '0' : $bot,
				"last_update" => getTimestamp(),
				"time" => time(),
				"browser" => $_SESSION['browser']
			];
			Cache::set('uniq-users', $uniq, 86400);
		}

		self::$audit = new Audit();
		$spam_defense = self::$app->spam_defense;
		self::$push = new Pushover();

		if (!($cacheCheckCms = Cache::get('cms-check-update'))) {
			$cacheCheckCms = array(
				"ip" => $ip,
				"version" => [
					"local" => self::CMS_VERSION,
					"server" => $this->cmsVersion
				],
				"cms_info" => App::getCMS_Info()
			);
			Cache::set('cms-check-update', $cacheCheckCms, 86400);
		}

		if (str_contains($query, "admin")) {
			require_once WWW_THEME . "/admin/functions.php";
		} else {
			$fileLayout = WWW_THEME . "/" . App::$app->layout . "/functions.php";
			!file_exists($fileLayout) ?: require_once $fileLayout;
		}

		if (isset($spam_defense) && $spam_defense == 'on' && !self::isWhiteListIP()) {
			if (empty($_SESSION['host']) || $ip !== $_SESSION['host']['ip'])
				$_SESSION['host'] = dnsBlackListCheckIP($ip);

			$spamFilter = true;
			$spamFilterArray = applyFilter('spam_defense', ['spam_defense' => $spam_defense, 'ip' => $ip]);
			if (!$spamFilterArray) $spamFilterArray = array();
			foreach ($spamFilterArray as $sf) {
				if (!$sf) {
					$spamFilter = false;
					break;
				}
			}

			if ($_SESSION['host']['blacklist'] >= 5 || !$spamFilter) {
				echo '<div style="text-align: center;"><h1>' . __("Your IP address is blocked on this resource") . '</h1></div>';
				$str = "IP: " . $ip . "; DNSBL: [";
				foreach ($_SESSION['host']['hosts'] as $host => $key)
					$str .= "\"$host($key)\", ";
				$str = rtrim($str, ", ");
				$str .= "]";
				throw new EngineException($str, EngineException::IP_BLOCK);
			}
		}

		$_SESSION['layout'] = App::$app->layout;
		doAction('url_before_parse', ["query" => $query, 'check' => $cacheCheckCms, 'object' => &$this]);

		Router::dispatch($query);
	}

	public static function isWhiteListIP(): bool
	{
		return self::$whiteListIP;
	}

	protected function setApp(): void
	{
		if (DataBase::isConnect()) {
			$country = Country::getOne("id2 = ?", array(mb_strtoupper(App::$app->phone_country_code)));
			$phone = str_replace([' ', '(', ')', '-'], "", App::$app->phone);
			self::$variable->phone_full = $country->code_phone . " " . App::$app->phone;
			self::$variable->phone_full_url = $country->code_phone . $phone;
		} else {
			$params = self::compareParams(getParams());

			self::$app->AE_CMS_NAME = self::CMS_NAME;
			self::$app->AE_CMS_YEAR = self::CMS_YEAR;
			self::$app->AE_CMS_VERSION_LOCAL = self::CMS_VERSION;
			self::$app->AE_CMS_VERSION = $this->cmsVersion;
			self::$app->AE_CMS_BUILD_NUMBER = $this->cmsBuildNumber;

			foreach ($params as $k => $v) {
				self::$app->$k = $v;
				if (in_array($k, ['layout', 'tinify_key', 'debug', 'secret_key'])) {
					if (!defined(strtoupper($k))) define(strtoupper($k), $v);
				}
			}

			if (!defined('LAYOUT')) define("LAYOUT", "none");
			if (!defined('TINIFY_KEY')) define("TINIFY_KEY", "");
			if (!defined('SECRET_KEY')) {
				define("SECRET_KEY", $params['secret_key']);
				self::$app->secret_key = $params['secret_key'];
			}

			setParams($params);

			self::$variable::import(self::$app->getAll());
			self::$variable->remove(['smtp_login',
				'smtp_password', 'smtp_server', 'smtp_security', 'smtp_port', 'archive_backup-Name',
				'archive_backup-Ext', 'pushover_group_token', 'pushover_token', 'tinify_key', 'count_message_admin_chat',
				'pagination', 'secret_key', 'AE_CMS_VERSION', 'https', 'spam_defense', 'dark_mode']);
			self::$variable->copyright = getCopyright();

			self::$variable->logo_path = file_exists(PATH . "/ag-themes/" . $params['layout'] . "/img/logo.png") ? PATH . "/ag-themes/" . $params['layout'] . "/img/logo.png" : PATH . "/ag-themes/admin/img/logo.png";
		}
	}

	public static function compareParams(array $params): array
	{
		$paramsDefault = include CONF . "/params_default.php";
		$paramsDefault['secret_key'] = md5(uniqid() .
			self::$app->get('AE_CMS_VERSION') .
			self::$app->get('AE_CMS_BUILD_NUMBER') .
			self::$app->get('AE_CMS_NAME') .
			self::$app->get('AE_CMS_YEAR') .
			base64_decode("QXJwZWtzTWVuZXRpbDJBcnBla3NHcm91cA"));
		foreach ($paramsDefault as $name => $value) {
			if (empty($params[$name])) $params[$name] = $value;
		}
		return $params;
	}

	#[ArrayShape(['AE_CMS_NAME' => "string", 'AE_CMS_YEAR' => "int", 'AE_CMS_VERSION' => "mixed|null", 'AE_CMS_VERSION_LOCAL' => "mixed|null", 'AE_CMS_BUILD_NUMBER' => "mixed|null"])]
	public static function getCMS_Info(): array
	{
		return [
			'AE_CMS_NAME' => self::CMS_NAME,
			'AE_CMS_YEAR' => self::CMS_YEAR,
			'AE_CMS_VERSION' => self::$app->get('AE_CMS_VERSION'),
			'AE_CMS_VERSION_LOCAL' => self::$app->get('AE_CMS_VERSION_LOCAL'),
			'AE_CMS_BUILD_NUMBER' => self::$app->get('AE_CMS_BUILD_NUMBER')
		];
	}

}
