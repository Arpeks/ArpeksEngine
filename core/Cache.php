<?php

namespace core;

use core\libs\RedisSingleton;
use core\registry\CacheRegistry;

/**
 * @author Arpeks
 * @package core
 * @version 235
 */
abstract class Cache
{

	private static CacheRegistry $cacheList;
	private static array $exclusionList = array('modulesActive.cache');

	public static function set(string $key, mixed $data, int $seconds = 3600): bool
	{
		if ($seconds) {
			$content['data'] = $data;

			if ($_SESSION['redis'] && $key[0] !== '-') {
				try {
					$redis = RedisSingleton::instance();

					$key = $_SERVER['HTTP_HOST'] . "__" . camelCase($key);

					if (in_array($key, self::$exclusionList)) {
						$redis->set($key, serialize($content['data']));
						return true;
					}

					$ttl = $redis->ttl($key);
					if ($ttl > $seconds) {
						$redis->set($key, serialize($content['data']), ['ex' => $ttl]);
					} else {
						$redis->set($key, serialize($content['data']), ['ex' => $seconds]);
					}
					return true;
				} catch (\RedisException $e) {
					$_SESSION['redis'] = false;
					ErrorHandler::errorHandler($e, false);
				}
			}

			$content['end_time'] = ($key[0] !== '-') ? time() + $seconds : -1;
			$key = camelCase($key);
			$tmp = explode("/", $key);
			unset($tmp[array_key_last($tmp)]);
			$tmp = implode("/", $tmp);

			if (!file_exists(CACHE . "/" . $tmp))
				mkdir(CACHE . "/" . $tmp, 0755, true);

			if (file_put_contents(CACHE . '/' . $key . '.cache', serialize($content))) {
				return true;
			}
		}
		return false;
	}

	public static function get(string $key, bool $isRemove = false, bool $returnOverTime = true)
	{
		$currentTime = time();
		$fileName = camelCase($key);

		if ($_SESSION['redis'] && $key[0] !== '-') {
			try {
				$redis = RedisSingleton::instance();
				$key = $_SERVER['HTTP_HOST'] . "__" . $fileName;
				$fileContent = $redis->get($key);
				$content = unserialize($fileContent);

				if ($content === false || !is_array($content)) {
					return array();
				}

				if ($isRemove) {
					$redis->del($key);
				}

				if ($returnOverTime) {
					return $content;
				}

			} catch (\RedisException $e) {
				$_SESSION['redis'] = false;
				ErrorHandler::errorHandler($e, false);
			}
		}

		$file = CACHE . '/' . $fileName . '.cache';
		if (file_exists($file)) {
			$fp = fopen($file, "r");
			$fileContent = fread($fp, filesize($file));
			fclose($fp);

			$content = unserialize($fileContent);
			$isExpired = $currentTime > $content['end_time'];

			if( $key[0] === '-' && $content['end_time'] === -1 ) return $content['data'];

			if (empty($content['end_time']) || empty($content['data'])) {
				self::delete($key);
				return array();
			}

			if ($isRemove || $isExpired) {
				self::delete($key);
			}

			if (!$isExpired || $returnOverTime) {
				return $content['data'];
			}
		}
		return array();
	}


	private static function delete(string $key): bool
	{
		doAction("cache_clear", ["key" => $key]);

		if ($key == null || $key[0] === '-')
			return false;

		$key = camelCase($key);
		switch ($key) {
			case 'tmp-images':
				$path = WWW . "/tmp/";
				if (file_exists($path)) {
					removeDirectory($path);
				}
				return true;
			case 'cms_error_log':
				$fp = fopen(LOGS . "/errors.log", "w");
				fwrite($fp, "");
				fclose($fp);
				return true;
			case 'cms_error_log_ip':
				$fp = fopen(LOGS . "/block_ip.log", "w");
				fwrite($fp, "");
				fclose($fp);
				return true;
			case 'cms_backup':
				$path = TEMP . "/update/";
				if (file_exists($path)) {
					removeDirectory($path);
				}
				return true;
			case "curl":
				$fp = fopen(LOGS . "/ae_curl.log", "w");
				fwrite($fp, "");
				fclose($fp);
				return true;
			case "redis":
				try {
					unset($_SESSION['redis']);
					$redis = RedisSingleton::instance();
					$redis->flushAll();
				} catch (\RedisException $e) {
					ErrorHandler::errorHandler($e, false);
				} finally {
					return true;
				}
			default:
				if ($_SESSION['redis']) {
					try {
						$redis = RedisSingleton::instance();
						$key = $_SERVER['HTTP_HOST'] . "__" . $key;
						$redis->del($key);
						return true;
					} catch (\RedisException $e) {
						$_SESSION['redis'] = false;
						ErrorHandler::errorHandler($e, false);
					}
				}

				$file = CACHE . '/' . $key . '.cache';
				if (file_exists($file)) {
					unlink($file);
					return true;
				}
		}
		return false;
	}

	public static function deleteAll(): bool
	{
		removeDirectory(WWW . "/tmp");
		removeDirectory(CACHE, true, self::$exclusionList);
		self::delete('redis');
		doAction("cache_clear_all");
		return true;
	}

	private static function addByteSize(): void
	{
		foreach (self::$cacheList->getAll() as $i => $cache) {
			if (is_file($cache['path'])) {
				$filesize = getFileSize($cache['path']);
			} else {
				$filesize = getFolderSize($cache['path']);
			}
			$cacheList = $cache;
			$cacheList['size'] = $filesize;
			$cacheList['size_byte'] = $filesize;
			self::$cacheList->set($i, $cacheList);
		}
	}

	private static function getTimeEnd(string $key): int
	{
		$file = CACHE . '/' . $key . '.cache';
		if (file_exists($file)) {
			$content = unserialize(file_get_contents($file));
			if (!isset($content['end_time']))
				return -1;
			return $content['end_time'];
		}
		return -1;
	}

	public static function getSize(): array
	{
		self::$cacheList = CacheRegistry::instance();
		self::addByteSize();
		foreach (self::$cacheList->getAll() as $i => $cache) {
			$cacheList = $cache;
			$cacheList['dataKey'] = camelCase($cacheList['dataKey']);

			if ($_SESSION['redis'] && !file_exists($cacheList['path']) && $cacheList['dataKey'] != 'redis') {
				try {
					$redis = RedisSingleton::instance();

					$current = self::get($cacheList['dataKey']);
					$cacheList['dataKey'] = $_SERVER['HTTP_HOST'] . "__" . $cacheList['dataKey'];
					$ttl = $redis->ttl($cacheList['dataKey']);

					if( $ttl < 0 ) {
						$cacheList['size_byte'] = 0;
						$cacheList['size'] = "0 byte";
						$cacheList['end_time'] = 0;
						$cacheList['cache'] = array();
					} else {
						$cacheList['size_byte'] = $redis->strlen($cacheList['dataKey']);
						$cacheList['size'] = getSizeName($cacheList['size_byte']);
						$cacheList['end_time'] = time() + $ttl;
						$cacheList['cache'] = $current;
					}

					self::$cacheList->set($i, $cacheList);
					continue;
				} catch (\RedisException $e) {
					$_SESSION['redis'] = false;
					ErrorHandler::errorHandler($e, false);
				}
			}

			if (!is_numeric($cache['size_byte'])) {
				$cacheList['size'] = "0 " . __("byte");
				$cacheList['size_byte'] = 0;
			} else {
				$cacheList['size'] = getSizeName($cache['size_byte']);
			}
			$cacheList['end_time'] = self::getTimeEnd($i);
			$cacheList['cache'] = self::get($cacheList['dataKey']);
			self::$cacheList->set($i, $cacheList);
		}

		if ($_SESSION['redis']) {
			try {
				$redis = RedisSingleton::instance();
				$cacheList = self::$cacheList->get('redis');
				$cacheList['size_byte'] = 0;

				foreach ($redis->keys("*") as $key) {
					$cacheList['size_byte'] += $redis->strlen($key);
				}
				$cacheList['size'] = getSizeName($cacheList['size_byte']);
				self::$cacheList->set('redis', $cacheList);
			} catch (\RedisException $e) {
				$_SESSION['redis'] = false;
				ErrorHandler::errorHandler($e, false);
			}
		}

		return self::$cacheList->getAll();
	}

	public static function addCache(string $name, string $description, string $dataKey, bool $is_folder = false, string $path = "/tmp/cache"): void
	{
		self::$cacheList = CacheRegistry::instance();
		if (!$is_folder && !is_file(ROOT . $path)) {
			$path .= "/" . $dataKey . ".cache";
		}

		self::$cacheList->set($dataKey, [
			"name" => $name,
			"description" => $description,
			"dataKey" => $dataKey,
			"path" => ROOT . $path
		]);
	}

	public static function deleteCache(string $dataKey): bool
	{
		self::$cacheList = CacheRegistry::instance();
		self::$cacheList->remove($dataKey);
		return self::delete($dataKey);
	}

	public static function getCache(): array
	{
		self::$cacheList = CacheRegistry::instance();
		return self::$cacheList->getAll();
	}

}
