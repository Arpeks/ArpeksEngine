<?php

namespace core\registry;

use core\base\EngineException;
use core\traits\Registry;
use Dotenv\Dotenv;

/**
 * @property string DBHOST
 * @property int DBPORT
 * @property string DBCHARSET
 * @property string DBUSER
 * @property string DBPASS
 * @property string DBNAME
 * @property string DBPREFIX
 * @property string DBDRIVER
 *
 * @property bool DISPLAY_ERRORS
 *
 * @property string REDIS_HOST
 * @property int REDIS_PORT
 *
 * @property string WHITELIST_IPS
 *
 * @property string RECAPTCHA_SECRET_KEY
 * @property string RECAPTCHA_SITE_KEY
 *
 * @author Arpeks
 * @package core\registry
 * @version 235
 */
class EnvRegistry
{

	use Registry;

	/**
	 * @throws EngineException
	 */
	public function __construct()
	{
		try {
			$dotenv = Dotenv::createImmutable(ROOT);
			$dotenv->load();

			foreach ($_ENV as $key => $val) {
				$this->set($key, $val);
			}

		} catch (\Exception $e) {
			throw new EngineException($e->getMessage(), $e->getCode(), $e->getPrevious());
		}
	}

}

if (!defined("ENV"))
	define("ENV", new EnvRegistry());
