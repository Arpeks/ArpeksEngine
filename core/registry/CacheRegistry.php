<?php

namespace core\registry;

use core\traits\Registry;
use core\traits\Singletone;

/**
 * @author Arpeks
 * @package core\registry
 * @version 235
 */
class CacheRegistry
{
	use Registry;
	use Singletone;
}