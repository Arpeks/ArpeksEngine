<?php

namespace core\registry;

use core\traits\Registry;
use core\traits\Singletone;

/**
 * @property string AE_CMS_NAME
 * @property int AE_CMS_YEAR
 * @property int AE_CMS_VERSION_LOCAL
 * @property int AE_CMS_VERSION
 * @property int AE_CMS_BUILD_NUMBER
 *
 * @property string https
 * @property string spam_defense
 * @property string dark_mode
 * @property string secret_key
 * @property string csrf
 *
 * @property int count_message_admin_chat
 * @property int pagination
 *
 * @property string site_name
 * @property string site_description
 * @property string site_keywords
 * @property string layout
 *
 * @property string admin_email
 * @property string phone_country_code
 * @property string phone
 *
 * @property string smtp_active
 * @property string smtp_server
 * @property int smtp_port
 * @property string smtp_security
 * @property string smtp_login
 * @property string smtp_password
 *
 * @property string archive_backup-Name
 * @property string archive_backup-Ext
 *
 * @author Arpeks
 * @package core\registry
 * @version 235
 */
class AppRegistry
{
	use Singletone;
	use Registry;
}
