<?php


namespace core\registry;


use core\App;
use core\traits\Registry;
use core\traits\Singletone;

/**
 * @author Arpeks
 * @package core\registry
 * @version 235
 */
class ErrorsRegistry
{

	use Registry;
	use Singletone;


	public function addAll(): void
	{
		$tmp = array();
		$tmp['e-ip'] = (file_exists(LOGS . "/block_ip.log")) ? $this->parseBlockIP() : array();
		$tmp['e-web'] = (file_exists(LOGS . "/errors.log")) ? $this->parseErrors() : array();

		foreach ($tmp as $index => $value)
			$this->$index = $value;

	}

	private function parseBlockIP(): array
	{
		if (!file_exists(LOGS . "/block_ip.log") || filesize(LOGS . "/block_ip.log") == 0)
			return array();

		$file = fopen(LOGS . "/block_ip.log", "r");
		$context = fread($file, filesize(LOGS . "/block_ip.log"));
		fclose($file);

		$context = explode("\n", $context);
		$errors = array();
		for ($i = 0; $i < sizeof($context); $i++) {
			if ($context[$i] == "") continue;

			$line = explode(" BLOCK_IP.WARNING: ", $context[$i]);
			$errors[$i]['time'] = $this->getTime($line);
			$ip = explode(": ", $line[0]);
			$dnsbl = explode(": ", $line[1]);
			$errors[$i]['ip'] = $ip[1];
			$errors[$i]['dnsbl'] = json_decode($dnsbl[1]);
		}

		return array_reverse($errors);
	}

	private function parseErrors(): array
	{
		if (!file_exists(LOGS . "/errors.log") || filesize(LOGS . "/errors.log") == 0)
			return array();

		$file = fopen(LOGS . "/errors.log", "r");
		$context = fread($file, filesize(LOGS . "/errors.log"));
		fclose($file);

		$context = explode("\n", $context);
		$errors = array();
		for ($i = 0; $i < sizeof($context); $i++) {
			if ($context[$i] == "") continue;

			$line = explode(" AG_Logger.ERROR: ", $context[$i]);
			$errors[$i]['time'] = $this->getTime($line);
			$errors[$i]['code'] = explode(": ", $line[0])[1];
			$errors[$i]['error_name'] = explode(": ", $line[1])[1];
			$errors[$i]['ip'] = explode(": ", $line[2])[1];
			$errors[$i]['text'] = explode(": ", $line[3])[1];
			$errors[$i]['file'] = explode(": ", $line[4])[1];
			$errors[$i]['line'] = explode(": ", $line[5])[1];
			$errors[$i]['browser'] = json_decode(explode(": ", $line[6])[1], true);
			$errors[$i]['SERVER'] = json_decode(explode(": ", $line[7])[1], true);
			$errors[$i]['route'] = json_decode(explode(": ", $line[8])[1], true);
			$errors[$i]['backtrace'] = json_decode(explode(": ", $line[9])[1], true);

		}

		return array_reverse($errors);
	}

	private function getTime(array &$line): string
	{
		$time = str_replace("[", "", $line[0]);
		$time = str_replace("]", "", $time);
		$time = strtotime($time);
		$line = explode("; ", $line[1]);

		$day = date("d", $time);
		$month = App::MONTH_LIST[date("n", $time) - 1];
		$year = date("Y", $time);
		$hourMinSec = date("H:i:s", $time);

		return "$day $month $year $hourMinSec";
	}
}