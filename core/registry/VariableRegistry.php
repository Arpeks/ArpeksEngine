<?php

namespace core\registry;


use core\traits\Registry;
use core\traits\Singletone;

/**
 * @property string copyright
 * @property string phone_full
 * @property string phone_full_url
 * @property string site_title
 * @property string meta
 * @property string meta_description
 * @property string base_path
 * @property string base_path_layout
 * @property string csrf
 * @property string input_csrf
 * @property string $scripts
 * @property string $styles
 * @property string $logo_path
 *
 * @author Arpeks
 * @package core\registry
 * @version 235
 */
class VariableRegistry
{
	use Registry;
	use Singletone;

	public function parser(string $html): string
	{
		$variableList = $this->getAll();
		foreach ($variableList as $key => $value) {
			if (is_array($value) || is_object($value) || is_null($value)) continue;
			$html = str_replace("{" . $key . "}", $value, $html);
		}
		return $html;
	}
}
