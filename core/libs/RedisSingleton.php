<?php

namespace core\libs;

use core\base\EngineException;
use core\ErrorHandler;
use Redis;


class RedisSingleton
{
	private static Redis $redis;

	public static function instance(): ?Redis
	{
		if (!isset(self::$redis)) {
			try {
				self::$redis = static::create();
			} catch (EngineException $e) {
				ErrorHandler::errorHandler($e);
			}
		}
		return self::$redis;
	}

	/**
	 * @return ?Redis
	 * @throws EngineException
	 */
	public static function create(): ?Redis
	{
		try {
			self::$redis = new Redis();
			self::$redis->connect(ENV->REDIS_HOST, ENV->REDIS_PORT);
			if (self::$redis->ping()) return self::$redis;
		} catch (\RedisException $e) {
			throw new EngineException(__("Redis error connect"), EngineException::REDIS_ERROR);
		}
		return null;
	}

	public function __destruct()
	{
		try {
			self::$redis->close();
		} catch (\RedisException $e) {
		}
	}


}
