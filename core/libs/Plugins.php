<?php


namespace core\libs;

use core\interfaces\{Hook, PluginManager};
use core\traits\Singletone;


/**
 * @author Arpeks
 * @package core\libs
 * @version 235
 */
class Plugins implements PluginManager
{

	private array $_eventHook;
	private array $_eventFilter;

	public array $pluginList;

	use Singletone;

	private function __construct()
	{
		$this->_eventHook = array();
		$this->pluginList = self::pluginList();
		$this->includePlugins();
	}

	public function registerHook(Hook $hook)
	{
		$this->_eventHook[] = $hook;
	}

	public function registerFilter(Hook $filter)
	{
		$this->_eventFilter[] = $filter;
	}

	public function delete(Hook $hook)
	{
		if ($id = array_search($hook, $this->_eventHook, TRUE)) {
			unset($this->_eventHook[$id]);
		} else if ($id = array_search($hook, $this->_eventFilter, TRUE)) {
			unset($this->_eventFilter[$id]);
		}
	}

	public function createFilter($filterName, array $param = array()): bool|array
	{
		if (!isset($this->_eventFilter)) return false;

		$callback = array();
		/* @var EventHook $eventFilter */
		foreach ($this->_eventFilter as $eventFilter) {
			if ($eventFilter->getHookName() == $filterName) {
				$callback[] = $eventFilter->run($this, $param);
			}
		}
		return $callback;
	}


	public function createHook($hookName, array $param = array())
	{
		if (!isset($this->_eventHook)) return false;

		/* @var EventHook $eventHook */
		foreach ($this->_eventHook as $eventHook) {
			if ($eventHook->getHookName() == $hookName) {
				$eventHook->run($this, $param);
			}
		}
	}

	public function includePlugins()
	{
		foreach ($this->pluginList as $plugin => $param) {
			if ($param['deactivated']) continue;
			include_once APP_PLUGIN . "/" . $plugin . "/index.php";
		}
	}

	public static function pluginList(): array
	{
		$list = array();
		foreach (scandir(APP_PLUGIN) as $item) {
			if ($item[0] == '.') continue;

			$path = APP_PLUGIN . "/" . $item;
			if (is_dir($path)) {
				if (file_exists($path . "/param.php")) {
					$param = unserialize(file_get_contents($path . "/param.php"));
				} else {
					$param = [
						"deactivated" => true
					];
					$fp = fopen($path . "/param.php", "w");
					fwrite($fp, serialize($param));
					fclose($fp);
				}
				$param['uri'] = (!empty($param['name'])) ? clearSpace($param['name']) : "";
				$list[$item] = $param;
			}
		}
		return $list;
	}

	public static function setParam(string $plugin, array $param)
	{
		$filename = APP_PLUGIN . "/" . $plugin . "/param.php";
		$params = array();

		if (file_exists($filename)) {
			$fp = fopen($filename, "r");
			$params = unserialize(fread($fp, filesize($filename)));
			fclose($fp);
		}

		foreach ($params as $key => $value) {
			if ($value == "on" && !isset($param[$key])) unset($params[$key]);
		}

		$newParam = array_merge([], $params, $param);

		$fp = fopen($filename, "w");
		fwrite($fp, serialize($newParam));
		fclose($fp);
	}

	/**
	 * @param bool $returnParams Возвращает список активных плагинов с параметрами или без них
	 * @return array
	 */
	public static function getActivePlugins(bool $returnParams = true): array
	{
		$plugins = array();
		foreach (self::pluginList() as $name => $param) {
			if (!$param['deactivated']) {
				$plugins[$name] = ($returnParams) ? $param : $name;
			}
		}
		return $plugins;
	}

}