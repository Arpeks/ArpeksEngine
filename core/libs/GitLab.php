<?php


namespace core\libs;


use core\base\EngineException;
use core\traits\Registry;
use core\traits\Singletone;

/**
 * @author Arpeks
 * @package core\libs
 * @version 235
 */
class GitLab
{

	use Singletone;
	use Registry;

	const GITLAB_API_URL = "https://gitlab.com/api/v4/projects/";

	public function setProjectNumber(int $project_number): void
	{
		$this->set("project_number", $project_number);
	}

	public function setPrivateToken(string $private_token): void
	{
		$this->set("private_token", $private_token);
	}

	/**
	 * @throws EngineException
	 */
	public function getReleases(string $query = "", array $params = array())
	{
		return $this->returnAnswer($this->curl("/releases/" . $query, $params));
	}

	private function returnAnswer(array $answer)
	{
		return json_decode($answer['content'], true);
	}

	/**
	 * @throws EngineException
	 */
	private function curl(string $url, array $params = array()): array
	{
		if (empty($this->get("project_number"))) {
			throw new EngineException("Project number is invalid", EngineException::GIT_ERROR);
		}

		$params['per_page'] = 100;
		return aeCurl(
			self::GITLAB_API_URL . $this->get("project_number") . $url,
			$params,
			array('Private-Token: ' . $this->get('private_token'))
		);
	}

	/**
	 * @throws EngineException
	 */
	public function getLastRelease(int $offset = 0)
	{
		return $this->returnAnswer($this->curl("/releases/", ["order_by" => "released_at", "sort" => "desc"]))[$offset] ?? false;
	}

	/**
	 * @throws EngineException
	 */
	public function getAllCommits(string $query = "", array $params = array()): array
	{
		$page = 1;
		$return = array();
		$build = array(true);
		while (!empty($build)) {
			$params['page'] = $page++;
			$build = $this->getCommits($query, $params);
			$return = array_merge($return, $build);
		}
		return $return;
	}

	/**
	 * @throws EngineException
	 */
	public function getCommits(string $query = "", array $params = array())
	{
		return $this->returnAnswer($this->curl("/repository/commits/" . $query, $params)) ?? false;
	}

	/**
	 * @throws EngineException
	 */
	public function getQuery(string $query, array $params = array())
	{
		return $this->returnAnswer($this->curl($query, $params));
	}

}
