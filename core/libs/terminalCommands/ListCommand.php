<?php


namespace core\libs\terminalCommands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListCommand extends Command
{

	protected static $defaultName = 'list';

	protected function configure(): void
	{
		$this
			->setDescription("Список всех команд")
			->setHelp('Выводит список всех команд');
	}

	protected function execute(InputInterface $input, OutputInterface $output): OutputInterface
	{

		foreach (scandir(LIBS . "/terminalCommands/") as $file) {
			if ($file[0] == '.') continue;
			$file = str_replace("Command.php", "", $file);
			$command = self::getApplication()->find($file)->getName();
			$description = self::getApplication()->find($file)->getDescription();
			$output->writeln($command . " - " . $description);
		}
		return $output;
	}

}
