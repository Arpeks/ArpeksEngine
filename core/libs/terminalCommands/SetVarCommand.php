<?php


namespace core\libs\terminalCommands;

use core\App;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetVarCommand extends Command
{

	protected static $defaultName = 'setvar';

	protected function configure(): void
	{
		$this
			->setDescription("Установить значение переменной")
			->setHelp('Устанавливает значение переменной, переданной в данную функцию')
			->addArgument('arg1', InputArgument::REQUIRED, 'Название переменной')
			->addArgument('arg2', InputArgument::REQUIRED, 'Новое значение');
	}

	protected function execute(InputInterface $input, OutputInterface $output): OutputInterface
	{

		$varName = mb_strtolower($input->getArgument('argc1'));
		$newValue = rtrim($input->getArgument('argc2'), ";");
		$app = App::$app;

		if ($app->isset($varName)) {

			if (in_array($varName, ["techwork", "spam_defense", "https", "dark_mode"])) {
				$app->$varName = match ($newValue) {
					"off", "false" => "",
					default => "on",
				};
			} else $app->$varName = $newValue;

			$fp = fopen(CONF . "/params.php", 'w');
			fwrite($fp, serialize(App::compareParams($app->getAll())));
			fclose($fp);

		}

		$output->write($varName . " = " . json_encode($app->$varName));
		return $output;
	}

}
