<?php


namespace core\libs\terminalCommands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HelpCommand extends Command
{

	protected static $defaultName = 'help';

	protected function configure(): void
	{
		$tmpHelpString = <<<'EOF'
Выводит помощь по определенной команде. Пример:
help %command.name%
Пример:
help list
EOF;

		$this
			->setDescription("Помощь по командам")
			->setAliases(['?'])
			->setHelp($tmpHelpString)
			->addArgument("argc1", InputArgument::OPTIONAL, "Название команды", 'help');
	}

	protected function execute(InputInterface $input, OutputInterface $output): OutputInterface
	{

		$command = $this::getApplication()->find($input->getArgument('argc1'));
		$output->writeln($command->getHelp());
		return $output;
	}

}
