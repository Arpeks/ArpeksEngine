<?php

namespace core\libs\terminalCommands;

use core\App;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class VarListCommand extends Command
{
	protected static $defaultName = 'varlist';

	protected function configure(): void
	{
		$this
			->setDescription("Выводит список всех переменных сайта и их значения")
			->setHelp('Выводит список всех переменных сайта и их значения');
	}

	protected function execute(InputInterface $input, OutputInterface $output): OutputInterface
	{

		$app = App::$app;
		$vars = $app->getAll();

		foreach ($vars as $key => $value) {
			$output->writeln($key . " = " . $value);
		}

		return $output;
	}
}
