<?php
include_once("FixedBitNotation.php");


class GoogleAuthenticator
{

	private $pass_code_length = 6;
	private $pin;
	private $secret_length = 10;

	public function __construct()
	{
		$this->pin = pow(10, $this->pass_code_length);
	}

	public function checkCode($secret, $code)
	{
		$time = floor(time() / 30);
		for ($i = -1; $i <= 1; $i++)
			if ($this->getCode($secret, $time + $i) == $code)
				return true;

		return false;
	}

	public function getCode($secret, $time = null)
	{
		if (!$time) $time = floor(time() / 30);

		$base32 = new FixedBitNotation(5, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567', true, true);
		$secret = $base32->decode($secret);

		$time = pack("N", $time);
		$time = str_pad($time, 8, chr(0), STR_PAD_LEFT);

		$hash = hash_hmac('sha1', $time, $secret, true);
		$offset = ord(substr($hash, -1));
		$offset = $offset & 0xF;

		$truncatedHash = $this->hashToInt($hash, $offset) & 0x7FFFFFFF;
		$pinValue = str_pad($truncatedHash % $this->pin, 6, "0", STR_PAD_LEFT);;
		return $pinValue;
	}

	protected function hashToInt($bytes, $start)
	{
		$input = substr($bytes, $start, strlen($bytes) - $start);
		$val2 = unpack("N", substr($input, 0, 4));
		return $val2[1];
	}

	public function getUrl($user, $sitename, $secret): string
	{
		$encoderURL = sprintf("otpauth://totp/%s:%s?secret=%s&issuer=%s", $sitename, $user, $secret, $sitename);
		return $encoderURL;
	}

	public function generateSecret(): string
	{
		$secret = "";
		for ($i = 1; $i <= $this->secret_length; $i++) {
			$c = rand(0, 255);
			$secret .= pack("c", $c);
		}
		$base32 = new FixedBitNotation(5, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567', true, true);
		return $base32->encode($secret);
	}

}
