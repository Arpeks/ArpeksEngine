<?php


namespace core\libs;

use core\{App, base\EngineException, Cache};

/**
 * @author Arpeks
 * @package core\libs
 * @version 235
 */
class Pushover
{

	private static bool $active = true;

	private string $app_token;
	private string $group_token;

	const post_url = "https://api.pushover.net/1/messages.json";
	const limits_url = "https://api.pushover.net/1/apps/limits.json";
	const validate_url = "https://api.pushover.net/1/users/validate.json";

	/**
	 * @throws EngineException
	 */
	public function __construct()
	{
		$app = App::$app;
		$this->app_token = $app->get('pushover_token') ?? "";
		$this->group_token = $app->get('pushover_group_token') ?? "";

		if ($this->app_token == '' || $this->group_token == '')
			self::$active = false;

		$this->check();
	}

	/**
	 * Функция отправки уведомления на устройство пользователей
	 * @param string $title Заголовок уведомления
	 * @param string $message Текст уведомления.
	 * Поддерживаются HTML теги: <br />
	 * &lt;b><b>word</b>&lt;/b><br />
	 * &lt;i><i>word</i>&lt;/i><br />
	 * &lt;u><u>word</u>&lt;/u><br />
	 * &lt;font color="#cc11cc"><font color="#cc11cc">word</font>&lt;/font><br />
	 * &lt;a href="http://example.com/"><a href="http://example.com/">word</a>&lt;/a>
	 * @param int $priority Приоритет сообщения. Поддерживаются числа от -2 до 1.<br />
	 * Lowest Priority (-2) - отправка сообщения без всплывающего уведомления, звука и вибрации<br />
	 * Low Priority (-1) - отправка сообщения со всплывающим уведомлением, но без звука и вибрации<br />
	 * Normal Priority (0) - отправка сообщения со всплывающим уведомлением, натсройки звука и вибрации берется из системы<br />
	 * High Priority (1) - отправка сообщения с обходом тихого часа на устройстве
	 * @param array $devices Массив названий устройств. Для отправки уведомлений на все устройства оставьте пустым.
	 * @param string $url Ссылка, которая отправится вместе с уведомлением
	 * @param string $url_title Текст ссылки
	 * @return string
	 * @throws EngineException
	 */
	public function sendNotify(string $title, string $message, int $priority = 0, array $devices = array(), string $url = ADMIN, string $url_title = SERVER['host']): string
	{
		$this->check();
		if (!self::$active) return "";

		if (!empty($devices)) {
			$device = implode(",", $devices);
		}

		if ($priority < -2 || $priority > 1)
			$priority = 0;

		$arr = array(
			"token" => $this->app_token,
			"user" => $this->group_token,
			"message" => $message,
			"title" => $title,
			"priority" => $priority,
			"sound" => "siren",
			"timestamp" => time(),
			"url" => $url,
			"url_title" => $url_title,
			"html" => 1
		);

		if (!empty($device)) {
			$arr['device'] = $device;
		}

		return $this->send($arr);
	}

	private function send(array $postParams = array()): bool|string
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::post_url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postParams));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		$response = curl_exec($ch);
		curl_close($ch);

		doAction('pushover_send_notification', ["params" => $postParams, 'url' => self::post_url]);
		return $response;
	}

	/**
	 * @throws EngineException
	 */
	private function check(): void
	{
		if (!($temp = Cache::get('pushover-check'))) {
			$temp = array(
				"validation" => $this->checkValidation(),
				"limits" => $this->checkLimits(),
				"is_active" => self::$active
			);

			if (self::$active)
				Cache::set('pushover-check', $temp, 600);
		}
	}

	/**
	 * @throws EngineException
	 */
	private function checkValidation()
	{
		if (!self::$active) return false;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::validate_url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(["token" => $this->app_token, "user" => $this->group_token]));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		$response = json_decode(curl_exec($ch), true);
		curl_close($ch);

		if ($response['status'] != 1) {
			self::$active = false;
			throw new EngineException("Pushover validation error", 608);
		}

		return $response;
	}

	/**
	 * @throws EngineException
	 */
	private function checkLimits()
	{
		if (!self::$active) return false;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::limits_url . '?' . http_build_query(["token" => $this->app_token]));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		$response = json_decode(curl_exec($ch), true);
		curl_close($ch);

		if ($response['status'] != 1) {
			self::$active = false;
			throw new EngineException("Exceeded maximum push-notification limit. Reset: " . getTimestamp($response['reset']), 608);
		}

		return $response;
	}

}
