<?php

namespace core\libs;

use core\traits\Registry;
use core\traits\Singletone;

/**
 * @author Arpeks
 * @package core\libs
 * @version 235
 */
class ScanSnapshot
{
	const PATH_SNAPSHOT = CONF . DIRECTORY_SEPARATOR . "snap.db";
	public bool $snap = false;
	private int $count = 0;

	use Singletone;
	use Registry;

	public function __construct()
	{
		if (!file_exists(self::PATH_SNAPSHOT))
			$this->scan();
		else
			$this->parse();
	}

	public function scan(): array
	{
		$snapData = array();
		if ($this->snap)
			$snapData = $this->getAll();

		$badFiles = array();
		$this->clear();
		$scanData = $this->getFileList(ROOT);

		if ($this->snap) {
			foreach ($scanData as $file => $hash) {
				$file = ROOT . ltrim($file, ".");

				if (!isset($snapData[$file]) || $hash !== $snapData[$file]) {
					if (!isset($snapData[$file])) {
						$badFiles[] = array(
							"filename" => str_replace(ROOT, ".", $file),
							"fileSize" => getSizeName(filesize($file)),
							"fileDate" => date("d.m.Y H:i:s", filemtime($file)),
							"type" => __("Unknown to distribution kit"),
							"hash" => md5_file($file)
						);
					} else {
						$badFiles[] = array(
							"filename" => str_replace(ROOT, ".", $file),
							"fileSize" => getSizeName(filesize($file)),
							"fileDate" => date("d.m.Y H:i:s", filemtime($file)),
							"type" => __("Does not match the taken snapshots"),
							"hash" => md5_file($file)
						);
					}
				}
			}
		} else {
			foreach ($scanData as $file => $hash) {
				$file = ROOT . ltrim($file, ".");
				$badFiles[] = array(
					"filename" => str_replace(ROOT, ".", $file),
					"fileSize" => getSizeName(filesize($file)),
					"fileDate" => date("d.m.Y H:i:s", filemtime($file)),
					"type" => __("Unknown to distribution kit"),
					"hash" => md5_file($file)
				);
			}
		}
		return $badFiles;
	}

	private function getFileList(string $path): array
	{
		$fileList = array_merge(array(),
			getFileList(APP),
			getFileList(CONF, array(CONF . DIRECTORY_SEPARATOR . "param.php", CONF . DIRECTORY_SEPARATOR . "snap.db")),
			getFileList(CORE),
			getFileList(ROOT . DIRECTORY_SEPARATOR . "public" . DIRECTORY_SEPARATOR . "ag-themes", array(ADMIN_THEME . DIRECTORY_SEPARATOR . 'vendor'))
		);
		
		foreach ($fileList as $file) {
			$this->count++;
			$this->set(str_replace(ROOT, ".", $file), md5_file($file));
		}

		return $this->getAll();
	}

	public function parse(): void
	{
		$fileContents = file(self::PATH_SNAPSHOT);

		foreach ($fileContents as $name => $value) {
			$fileContents[$name] = explode("|", trim($value));
			$this->set(ROOT . ltrim($fileContents[$name][0], "."), $fileContents[$name][1]);
		}
		$this->snap = true;
	}

	public function write(): void
	{
		$fp = fopen(self::PATH_SNAPSHOT, "w");

		foreach ($this->getAll() as $file => $hash) {
			$str = sprintf("%s|%s\n", str_replace(ROOT, ".", $file), $hash);
			fwrite($fp, $str);
		}

		$this->snap = true;
		fclose($fp);
	}

}
