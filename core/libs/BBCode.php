<?php

namespace core\libs;

use core\base\EngineException;
use core\registry\BBCodeRegistry;

final class BBCode extends BBCodeRegistry
{
	public static function addTag($tag, $replacement): void
	{
		BBCode::instance()->set($tag, $replacement);
	}

	/**
	 * @throws EngineException
	 */
	public static function parse(string $text): string
	{
		$pattern = '/{(\w+)([^}]*)}(.*?)(\{\/\1})/';
		$html = preg_replace_callback($pattern, function ($matches) {
			$tag = $matches[1];
			$attributes = trim($matches[2]);
			$content = trim($matches[3]);

			$tag = BBCode::instance()->get($tag);
			if (!is_null($tag)) {
				$replacement = $tag;
				$replacement = str_replace('$1', $attributes, $replacement);
				return str_replace('$2', $content, $replacement);
			} else {
				return $matches[0];
			}
		}, $text);

		$pattern = '/{([^}]*)}/';
		$html = preg_replace_callback($pattern, function ($matches) {
			$tag = $matches[1];

			$tag = BBCode::instance()->get($tag);
			if (!is_null($tag)) {
				return $tag;
			} else {
				return $matches[0];
			}
		}, $html);

		if (!is_string($html))
			throw new EngineException('BBCode is invalid!', EngineException::BBCODE_ERROR);

		return $html;
	}
}