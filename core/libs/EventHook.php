<?php


namespace core\libs;

use core\interfaces\{Hook, PluginManager};


/**
 * @author Arpeks
 * @package core\libs
 * @version 235
 */
class EventHook implements Hook
{

	private string $_hookName;
	private string $_functionName;

	public function __construct(string $hookName, string $functionName)
	{
		$this->_hookName = $hookName;
		$this->_functionName = $functionName;
	}

	public function run(PluginManager $pluginManager, array $param)
	{
		$plugins = Plugins::instance();
		$param = array_merge([], $param, ['plugins' => $plugins->pluginList]);
		if (function_exists($this->_functionName)) {
			return call_user_func($this->_functionName, $param);
		}
		return false;
	}

	public function getHookName(): string
	{
		return $this->_hookName;
	}

}