<?php

namespace core\libs;

use core\base\EngineException;
use Tinify;

/**
 * @author Arpeks
 * @package core\libs
 * @version 235
 */
class SimpleImage
{

	private mixed $image;
	private int $image_type = IMAGETYPE_UNKNOWN;
	private array $image_info = array();
	private string $file_name = "";

	public function __construct(string $filename = null)
	{
		if (TINIFY_KEY != "") Tinify\setKey(TINIFY_KEY);
		if ($filename != null && file_exists($filename)) {
			$this->load($filename);
		}
	}

	function load(string $filename): void
	{
		$this->file_name = $filename;
		$this->image_info = getimagesize($filename);
		$this->image_type = $this->image_info[2];

		switch ($this->image_type) {
			case IMAGETYPE_JPEG:
				$this->image = imagecreatefromjpeg($filename);
				break;
			case IMAGETYPE_GIF:
				$this->image = imagecreatefromgif($filename);
				break;
			case IMAGETYPE_PNG:
				$this->image = imagecreatefrompng($filename);
				break;
			case IMAGETYPE_WEBP:
				$this->image = imagecreatefromwbmp($filename);
				break;
		}
	}


	function save(int $compression = 75, string $permissions = null): void
	{
		if ($this->image_type == IMAGETYPE_UNKNOWN) {
			echo 'Сначала нужно загрузить изображение функцией SimpleImage::load(str $filename)';
			return;
		}

		switch ($this->image_type) {
			case IMAGETYPE_JPEG:
				imagejpeg($this->image, $this->file_name, $compression);
				break;
			case IMAGETYPE_GIF:
				imagegif($this->image, $this->file_name);
				break;
			case IMAGETYPE_PNG:
				imagepng($this->image, $this->file_name);
				break;
			case IMAGETYPE_WEBP:
				imagewebp($this->image, $this->file_name);
				break;
		}

		if ($permissions != null) {
			chmod($this->file_name, $permissions);
		}

		if (TINIFY_KEY != "") {
			if (Tinify\getCompressionCount() >= 500) {
				new EngineException("The number of uploaded images has exceeded the allowed number", 602);
			} else {
				$source = Tinify\fromFile($this->file_name);
				$source->toFile($this->file_name);
			}
		}
	}

	function convert(int $format = IMAGETYPE_WEBP): void
	{
		if ($this->image_type == IMAGETYPE_UNKNOWN) {
			echo 'Сначала нужно загрузить изображение функцией SimpleImage::load(str $filename)';
			return;
		}

		if ($this->image_type == $format) {
			echo 'Нету смысла конвертировать файл в тот же формат';
			return;
		}

		$file_name = getPath($this->file_name) . DIRECTORY_SEPARATOR . getFileName($this->file_name);
		switch ($format) {
			case IMAGETYPE_JPEG:
				imagejpeg($this->image, $file_name . ".jpg", 75);
				break;
			case IMAGETYPE_GIF:
				imagegif($this->image, $file_name . ".gif");
				break;
			case IMAGETYPE_PNG:
				imagepng($this->image, $file_name . ".png");
				break;
			case IMAGETYPE_WEBP:
				imagewebp($this->image, $file_name . ".webp");
				break;
		}
	}

	public function getImageType(): int
	{
		return $this->image_type;
	}

	public function setImageType(int $image_type = IMAGETYPE_UNKNOWN): void
	{
		$this->image_type = $image_type;
	}

	public function getImageInfo(): array
	{
		return $this->image_info;
	}

	public function setImageInfo(array $image_info): void
	{
		$this->image_info = $image_info;
	}

	public function getFileName(): string
	{
		return $this->file_name;
	}

	public function setFileName(string $file_name): void
	{
		$this->file_name = $file_name;
	}

	function output(int $image_type = IMAGETYPE_WEBP): void
	{
		switch ($this->image_type) {
			case IMAGETYPE_JPEG:
				imagejpeg($this->image);
				break;
			case IMAGETYPE_GIF:
				imagegif($this->image);
				break;
			case IMAGETYPE_PNG:
				imagepng($this->image);
				break;
			case IMAGETYPE_WEBP:
				imagewebp($this->image);
				break;
		}
	}

	function getWidth(): false|int
	{
		return imagesx($this->image);
	}

	function getHeight(): false|int
	{
		return imagesy($this->image);
	}

	function resizeToHeight(int $height): void
	{
		$ratio = $height / $this->getHeight();
		$width = intval($this->getWidth() * $ratio);
		$this->resize($width, $height);
	}

	function resizeToWidth(int $width): void
	{
		$ratio = $width / $this->getWidth();
		$height = intval($this->getHeight() * $ratio);
		$this->resize($width, $height);
	}

	function scale(int $scale): void
	{
		$width = intval($this->getWidth() * $scale / 100);
		$height = intval($this->getHeight() * $scale / 100);
		$this->resize($width, $height);
	}

	function resize(int $width, int $height): void
	{
		$new_image = imagecreatetruecolor($width, $height);

		if ($this->image_type == IMAGETYPE_PNG) {
			imagesavealpha($new_image, true);
			imagealphablending($new_image, false);
		}

		imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
		$this->image = $new_image;
	}
}
