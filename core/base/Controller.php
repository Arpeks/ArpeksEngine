<?php

namespace core\base;


use app\models\User;
use core\App;

/**
 * @author Arpeks
 * @package core\base
 * @version 236
 */
abstract class Controller
{

	protected string $layout = '';
	protected string $userLayout = '';
	protected bool $isActive = true;
	protected bool $isView = true;
	protected array $monthsList;
	private array $route;
	private string $view;
	private array $data = array();
	private array $meta = ['title' => '', 'description' => '', 'keywords' => ''];
	private array $styles = array();
	private array $scripts = array();

	public function __construct($route)
	{

		$app = App::$app->getAll();

		$this->monthsList = [
			1 => __("January"),
			2 => __('February'),
			3 => __('March'),
			4 => __('April'),
			5 => __('May'),
			6 => __('June'),
			7 => __('July'),
			8 => __('August'),
			9 => __('September'),
			10 => __('October'),
			11 => __('November'),
			12 => __('December')
		];

		if ($this->layout != 'admin' && isset($app['techwork']) && !User::checkPermission("access_site_to_techwork")) {
			$this->userLayout = WWW_THEME . "/{$app['layout']}/template/techworks.php";
			$this->isView = true;
		}

		$this->route = $route;
		$this->view = $route['action'];
		$this->meta['title'] = App::$app->get('site_name');
	}

	public function getRoute(string $index): mixed
	{
		if (isset($this->data[$index]))
			return $this->data[$index];
		return null;
	}

	public function getMeta(): array
	{
		return $this->meta;
	}

	public function setMeta(string $title = "", string $description = "", string $keywords = "", bool $isLanguage = true): void
	{
		if ($title == "") {
			$title = App::$app->get('site_name');
		} else {
			if ($isLanguage)
				$title = __($title);
			$title .= " | " . App::$app->get('site_name');
		}

		$this->meta['title'] = $title;
		$this->meta['description'] = str_replace("'", "\"", $description);
		$this->meta['keywords'] = str_replace("'", "\"", $keywords);
	}

	public function isActive(): bool
	{
		return $this->isActive;
	}

	public function setActive(bool $is_active): void
	{
		$this->isActive = $is_active;
	}

	public function isView(): bool
	{
		return $this->isView;
	}

	public function getUserLayout(): string
	{
		return $this->userLayout;
	}

	public function setUserLayout(string $uLayout): void
	{
		$this->userLayout = $uLayout;
	}

	public function getLayout(): string
	{
		return $this->layout;
	}

	public function setLayout(string $layout): void
	{
		$this->layout = $layout;
	}

	public function getData(string|int $index): mixed
	{
		if (isset($this->data[$index]))
			return $this->data[$index];
		return false;
	}

	public function setData(array $data): void
	{
		$this->data = array_merge($this->data, $data);
	}

	public function setFile(string $fileName): void
	{
		$this->view = $fileName;
	}

	public function setStyles(array $paths): void
	{
		foreach ($paths as $path) {
			$this->setStyle($path);
		}
	}

	public function setStyle(string $path, string $rel = "stylesheet", string $type = "text/css", string|bool $as = false): void
	{
		if ($as !== false && $as !== '' && $rel !== 'modulepreload') {
			$rel = 'preload';
		} else $as = false;

		$this->styles[] = array(
			"href" => $path,
			"rel" => $rel,
			"type" => $type,
			"as" => $as
		);
	}

	public function setScripts(array $paths): void
	{
		foreach ($paths as $path) {
			$this->setScript($path);
		}
	}

	public function setScript(string $src, bool $async = false, bool $defer = false, string $type = "text/javascript", $nomodule = false): void
	{
		$this->scripts[] = array(
			"src" => $src,
			"type" => $type,
			"async" => $async,
			"defer" => $defer,
			"nomodule" => $nomodule
		);
	}

	/**
	 * @throws EngineException
	 */
	public function getView(): void
	{
		if (!$this->isActive)
			redirect();

		$viewObject = new View($this->route, $this->layout, $this->view, $this->meta);
		$viewObject->render($this->data, $this->scripts, $this->styles, $this->isView, $this->userLayout);
	}

	public function setView(bool $is_view): void
	{
		$this->isView = $is_view;
	}

	public function getRequestID(bool $get = true): ?int
	{
		return intval($this->getRequest('id', $get));
	}

	public function getRequest(string $param, bool $get = true): mixed
	{
		$data = ($get) ? $_GET : $_POST;
		if (!isset($data[$param])) {
			$_SESSION['error'] = "Укажите " . mb_strtoupper($param) . " для продолжения работы";
			redirect();
		}
		return !empty($data[$param]) ? $data[$param] : null;
	}

}
