<?php

namespace core\base;


use core\Language;
use R;
use RedBeanPHP\OODBBean;
use RedBeanPHP\RedException\SQL;
use Valitron\Validator;

/**
 * @author Arpeks
 * @package core\base
 * @version 236
 *
 * @property int id
 */
abstract class Model
{

	protected static string $table;
	protected static string $parentClass = "";

	protected array $attributes;
	protected array $errors;
	protected array $rules = array();

	public static function getParentClass(): string
	{
		return static::$parentClass;
	}

	public static function getAll(string $addSQL = "", array $bindings = array()): array
	{
		return R::findAll(self::getTable(), $addSQL, $bindings);
	}

	public static function getTable(): string
	{
		if (ENV->DBPREFIX == "")
			return static::$table;
		return trim(ENV->DBPREFIX, "_") . "_" . static::$table;
	}

	public static function getCount(string $addSQL = '', array $bindings = array()): int
	{
		return R::count(self::getTable(), $addSQL, $bindings);
	}

	public static function getOne(string $sql = "", array $bindings = array()): OODBBean|static|null
	{
		return R::findOne(self::getTable(), $sql, $bindings);
	}

	public static function getByID(int $id): OODBBean|static
	{
		return R::load(self::getTable(), $id);
	}

	public function load(array $data): void
	{
		foreach ($this->attributes as $name => $value) {
			if (isset($data[$name])) {
				$this->attributes[$name] = $data[$name];
			}
		}
	}

	public function getProperty(string $index): mixed
	{
		if (isset($this->attributes[$index]))
			return $this->attributes[$index];
		return false;
	}

	public function getProperties(): array
	{
		return $this->attributes;
	}

	public function loadWithValidate(array $data): bool
	{
		$this->load($data);
		return $this->validate();
	}

	public function validate(): bool
	{
		$lang = Language::getLanguage(true);
		Validator::lang($lang);
		$v = new Validator($this->attributes);
		$v->rules($this->rules);
		if ($v->validate())
			return true;
		$this->errors = $v->errors();
		return false;
	}

	public function getErrors(bool $is_sess = true): array
	{
		if ($is_sess) {
			$errors = '<ul>';
			foreach ($this->errors as $error) {
				foreach ($error as $item) {
					$errors .= "<li>$item</li>";
				}
			}
			$errors .= '</ul>';
			$_SESSION['error'] = $errors;
		} else {
			$_SESSION['error'] = $this->errors;
		}
		return $this->errors;
	}

	/**
	 * @throws SQL
	 */
	public function save(): int|string
	{
		$tbl = R::xdispense(self::getTable());
		foreach ($this->attributes as $name => $value) {
			$tbl->$name = $value;
		}
		return R::store($tbl);
	}

	/**
	 * @throws SQL
	 */
	public function update(int $id): int|string
	{
		$bean = R::load(self::getTable(), $id);
		foreach ($this->attributes as $name => $value) {
			$bean->$name = $value;
		}
		return R::store($bean);
	}

}
