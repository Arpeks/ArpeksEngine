<?php


namespace core\base;


use Exception;
use JetBrains\PhpStorm\NoReturn;
use Throwable;

/**
 * @author Arpeks
 * @package core\base
 * @version 235
 */
class EngineException extends Exception
{

	const int FORBIDDEN = 403;
	const int FILE_NOT_FOUND = 404;
	const int INTERNAL_SERVER_ERROR = 500;

	const int DATABASE_ERROR = 601;
	const int TINIFY_ERROR_MAX_IMAGE = 602;
	const int PHPMAILER_SEND_ERROR = 603;
	const int SECRET_KEY_NOT_VALID = 604;
	const int IP_BLOCK = 605;
	const int CSRF_NOT_VALID = 606;
	const int GIT_ERROR = 607;
	const int PUSHOVER_ERROR = 608;
	const int UPDATE_ERROR = 609;
	const int BACKUP_ERROR = 610;
	const int BBCODE_ERROR = 611;
	const int REDIS_ERROR = 612;

	#[NoReturn]
	public function __construct($message = "", $code = 0, Throwable $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}

}
