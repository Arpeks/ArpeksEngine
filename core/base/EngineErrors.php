<?php

namespace core\base;

use Throwable;

/**
 * @author Arpeks
 * @package core\base
 * @version 235
 */
class EngineErrors
{

	private int $code;
	private string $e_name;
	private string $message;
	private string $file;
	private int $line;
	private array $backtrace = [];

	public function __construct(int $errno, string $errstr, string $errfile, int $errline, array $backtrace = array())
	{
		$this->code = $errno;
		$this->message = $errstr;
		$this->file = $errfile;
		$this->line = $errline;
		$this->e_name = $this->getErrorNameByCode($errno);

		if( count($backtrace) > 1 ) {
			unset($backtrace[0]);
		}

		$this->backtrace = $backtrace;
	}

	public static function getErrorNameByCode(int $code): string
	{
		return match ($code) {
			E_ERROR => "Fatal Error",
			E_WARNING => "Warning Error",
			E_PARSE => "Parse Error",
			E_NOTICE => "Notice",
			E_USER_ERROR => "User Fatal Error",
			E_USER_WARNING => "User Warning Error",
			E_USER_NOTICE => "User Notice",
			E_RECOVERABLE_ERROR => "Recoverable Error",
			E_DEPRECATED => "Deprecated",
			E_USER_DEPRECATED => "User Deprecated",
			EngineException::FORBIDDEN => "Forbidden",
			EngineException::FILE_NOT_FOUND => "Not Found",
			EngineException::INTERNAL_SERVER_ERROR => "Internal Server Error",
			EngineException::DATABASE_ERROR => "Error is DataBase",
			EngineException::TINIFY_ERROR_MAX_IMAGE => "Error is Tinify",
			EngineException::PHPMAILER_SEND_ERROR => "Error is PHPMailer",
			EngineException::SECRET_KEY_NOT_VALID => "Error is SECRET_KEY",
			EngineException::IP_BLOCK => "IP is blocked",
			EngineException::CSRF_NOT_VALID => "Token CSRF not valid!",
			EngineException::GIT_ERROR => "Error is Git",
			EngineException::PUSHOVER_ERROR => "Pushover Error",
			EngineException::UPDATE_ERROR => "Update Error",
			EngineException::BACKUP_ERROR => "Backup/Restore Error",
			EngineException::REDIS_ERROR => "Redis Error",
			default => "Unknown Error",
		};
	}

	public static function fromArray(array $array): self
	{
		$newObj = new self($array['code'], $array['message'], $array['file'], $array['line']);
		$newObj->setBacktrace($array['backtrace']);
		return $newObj;
	}

	public static function fromThrowable(Throwable $throwable): self
	{
		$newObj = new self($throwable->getCode(), $throwable->getMessage(), $throwable->getFile(), $throwable->getLine());
		$newObj->setBacktrace($throwable->getTrace());
		return $newObj;
	}

	/**
	 * @return int
	 */
	public function getCode(): int
	{
		return $this->code;
	}

	/**
	 * @return string
	 */
	public function getMessage(): string
	{
		return $this->message;
	}

	/**
	 * @return string
	 */
	public function getFile(): string
	{
		return $this->file;
	}

	/**
	 * @return int
	 */
	public function getLine(): int
	{
		return $this->line;
	}

	/**
	 * @return string
	 */
	public function getErrorName(): string
	{
		return $this->e_name;
	}

	/**
	 * @return array
	 */
	public function getBacktrace(): array
	{
		return $this->backtrace;
	}

	/**
	 * @param array $backtrace
	 */
	public function setBacktrace(array $backtrace): void
	{
		$this->backtrace = $backtrace;
	}

	public function toArray(): array
	{
		$ret = array();
		foreach ($this as $key => $value) {
			$ret[$key] = $value;
		}
		return $ret;
	}

}
