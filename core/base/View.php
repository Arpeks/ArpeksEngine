<?php

namespace core\base;


use core\App;
use core\ErrorHandler;
use core\libs\Plugins;

/**
 * @author Arpeks
 * @package core\base
 * @version 236
 */
class View
{

	private array $route;
	private string $controller;
	private string $view;
	private string $prefix;
	private array $meta;
	private string $layout;

	/**
	 * @throws EngineException
	 */
	public function __construct(array $route, $layout = "", $view = '', array $meta = [])
	{
		$this->route = $route;
		$this->controller = $route['controller'];
		$this->view = $view;
		$this->prefix = $route['prefix'];
		$this->meta = $meta;
		$this->layout = $layout != "" ? $layout : LAYOUT;

		if ($this->layout == "none")
			throw new EngineException("Layout not found", 404);
	}

	/**
	 * @throws EngineException
	 */
	public function render(array $data, array $scripts, array $styles, bool $isView = true, string $uLayout = ""): void
	{
		extract($data);
		$app = App::$app->getAll();
		$content = "";
		$csrf = generateCSRF();
		$controller = mb_strtolower($this->controller);
		$notFound = false;
		App::$variable->scripts = '';
		App::$variable->styles = '';

		foreach ($styles as $style) {
			$tag = "<link href='$style[href]' rel='$style[rel]' type='$style[type]'";
			if ($style['as'] !== false) {
				$tag .= " as='$style[as]'";
			}
			$tag .= " />" . PHP_EOL;
			App::$variable->styles .= $tag;
		}

		foreach ($scripts as $script) {
			$tag = "<script src='$script[src]' type='$script[type]'";
			if ($script['async'] !== false) {
				$tag .= " async";
			}
			if ($script['defer'] !== false) {
				$tag .= " defer";
			}
			if ($script['nomodule'] !== false) {
				$tag .= " nomodule";
			}
			$tag .= "></script>" . PHP_EOL;
			App::$variable->scripts .= $tag;
		}

		$javaScriptFilename = (str_contains($this->prefix, 'module') ? "modules/$controller" : "$controller");
		if( file_exists(WWW_THEME . "/$this->layout/js/pages/$javaScriptFilename.js") ) {
			App::$variable->scripts .= "<script src='js/pages/$javaScriptFilename.js?v=1' type='module' defer></script>" . PHP_EOL;
		} else {
			App::$variable->scripts .= "<script type='module'>import { notifySweetAlert } from './js/functions.js';notifySweetAlert('js:pages/$javaScriptFilename.js not found','error')</script>" . PHP_EOL;
		}

		App::$variable->csrf = $csrf;
		App::$variable->input_csrf = "<input type='hidden' name='csrf' value='$csrf'>";
		App::$variable->site_title = $this->getTitle();
		App::$variable->meta = $this->getMeta();
		App::$variable->meta_description = $this->getMeta(true);
		App::$variable->base_path = PATH . "/ag-themes/" . $this->layout . "/";
		App::$variable->base_path_layout = PATH . "/ag-themes/" . $app['layout'] . "/";
		App::$variable::import($data);

		$this->prefix = trim(str_replace("admin", "", $this->prefix), "/\\");
		if ($this->prefix !== "") {
			$this->prefix = $this->prefix . "/";
		}

		$viewFile = mb_strtolower(str_replace("\\", "/", WWW_THEME . "/$this->layout/template/$this->prefix" . "$this->controller/$this->view.php"));
		$layout = App::$app->get("layout");

		if (is_file($viewFile)) {
			ob_start();
			@include_once $viewFile;
			$content = ob_get_clean();
		} else {
			$content = "<b>{$layout}</b> template not found!";
			$notFound = true;
		}

		ob_start();
		if ($notFound) {
			$e = new EngineErrors(EngineException::FILE_NOT_FOUND, $content, __FILE__, __LINE__);
			ErrorHandler::instance()::engineErrorHandler($e, true);
		} else if ($isView) {
			if (!empty($uLayout)) {
				if (is_file($uLayout)) {
					include_once $uLayout;
				} else {
					throw new EngineException("$uLayout not found!", 404);
				}
			} else {
				!file_exists(WWW_THEME . "/$this->layout/template/header.php") ?: include_once WWW_THEME . "/$this->layout/template/header.php";
				echo $content . PHP_EOL;
				!file_exists(WWW_THEME . "/$this->layout/template/footer.php") ?: include_once WWW_THEME . "/$this->layout/template/footer.php";
			}
		}
		echo App::$variable->parser(ob_get_clean());

		doAction("site_loading_complete", ['viewFile' => $viewFile, 'layout' => $this->layout, "plugins" => Plugins::getActivePlugins(false)]);
		$_SESSION['route'] = $this->route;
		exit;
	}

	public function getTitle(): string
	{
		return explode(" | ", $this->meta['title'])[0];
	}

	public function getMeta(bool $descriptionInTitle = false): string
	{
		$output = "<title>{$this->getTitle()}";
		if ($descriptionInTitle && !empty($this->getDescription()))
			$output .= ' &#8211; ' . $this->getDescription();
		$output .= "</title>" . PHP_EOL;

		$output .= "<meta name='description' content='{$this->getDescription()}'>" . PHP_EOL;
		$output .= "<meta name='keywords' content='{$this->getKeywords()}'>" . PHP_EOL;
		return $output;
	}

	public function getDescription()
	{
		return $this->meta['description'];
	}

	public function getKeywords()
	{
		return $this->meta['keywords'];
	}

}
