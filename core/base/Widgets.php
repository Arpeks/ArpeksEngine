<?php

namespace core\base;

/**
 * @author Arpeks
 * @package core\base
 * @version 236
 */
abstract class Widgets
{

	private string $_name = __CLASS__;
	private array $_data = array();
	private string $_tpl = "";
	private bool $is_admin = false;

	protected function setData($data = array()): void
	{
		if (!is_array($data)) {
			$data = array($data);
		}
		$this->_data = array_merge(array(), $this->_data, $data);
	}

	protected function setName(string|object $name = __CLASS__): void
	{
		$this->_name = getClassName($name);
	}

	public function getName(): string
	{
		return ($this->_name == "") ? getClassName($this) : $this->_name;
	}

	protected function setAdmin(bool $is_admin): void
	{
		$this->is_admin = $is_admin;
	}

	protected function setTemplate(string $tpl = "index.php"): void
	{
		$this->_tpl = APP_WIDGET . DIRECTORY_SEPARATOR;
		if ($this->is_admin) $this->_tpl .= "admin" . DIRECTORY_SEPARATOR;
		$this->_tpl .= $this->getName() . DIRECTORY_SEPARATOR . "tpl" . DIRECTORY_SEPARATOR . pathinfo($tpl, PATHINFO_BASENAME);
	}

	/**
	 * @throws EngineException
	 */
	public function render(): void
	{
		$data = $this->_data;
		$className = $this->getName();

		if (!$this->_tpl) $this->setTemplate();

		if (file_exists($this->_tpl)) {
			ob_start();
			require $this->_tpl;
			echo ob_get_clean();
		} else {
			throw new EngineException("FILE_NOT_FOUND(" . $this->_tpl . ");", EngineException::FILE_NOT_FOUND);
		}
	}

}