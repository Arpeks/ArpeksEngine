<?php

use core\base\EngineErrors;
use core\ErrorHandler;
use RedBeanPHP\Facade;

define("ROOT", dirname(__DIR__));
const WWW = ROOT . DIRECTORY_SEPARATOR . "public";
const WWW_THEME = WWW . DIRECTORY_SEPARATOR . 'ag-themes';
const APP = ROOT . DIRECTORY_SEPARATOR . 'app';
const APP_CONTROLLER = APP . DIRECTORY_SEPARATOR . 'controllers';
const APP_MODEL = APP . DIRECTORY_SEPARATOR . 'models';
const APP_WIDGET = APP . DIRECTORY_SEPARATOR . 'widgets';
const APP_PLUGIN = APP . DIRECTORY_SEPARATOR . 'plugins';
const CORE = ROOT . DIRECTORY_SEPARATOR . 'core';
const LIBS = CORE . DIRECTORY_SEPARATOR . 'libs';
const TEMP = ROOT . DIRECTORY_SEPARATOR . 'tmp';
const CONF = ROOT . DIRECTORY_SEPARATOR . 'config';
const LANG = ROOT . DIRECTORY_SEPARATOR . 'locales';
const CACHE = TEMP . DIRECTORY_SEPARATOR . 'cache';
const LOGS = TEMP . DIRECTORY_SEPARATOR . 'logs';
const TEMP_UPDATE = TEMP . DIRECTORY_SEPARATOR . "update";
const ADMIN_THEME = ROOT . DIRECTORY_SEPARATOR . "public" . DIRECTORY_SEPARATOR . "ag-themes" . DIRECTORY_SEPARATOR . "admin";
const PATH_ADMINLTE = "vendor" . DIRECTORY_SEPARATOR . "almasaeed2010" . DIRECTORY_SEPARATOR . "adminlte";

define("SERVER", parse_url((isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"));
const PATH = SERVER['scheme'] . "://" . SERVER['host'];
const ADMIN_URI = "admin";
const ADMIN = PATH . "/" . ADMIN_URI;

const AE_COMPARE_GREATER = "great";
const AE_COMPARE_GREATER_AND_EQUAL = "great-equal";
const AE_COMPARE_LESS = "less";
const AE_COMPARE_LESS_AND_EQUAL = "less-equal";
const AE_COMPARE_EQUAL = "equal";
const AE_COMPARE_EQUAL_NOT = "not-equal";

const MAX_VIEW_LAST_ERROR = 100;
const TIME_SAVE_PASSWORD = 86400 * 7;

require_once ROOT . "/vendor/autoload.php";

require_once CORE . "/registry/EnvRegistry.php";
require_once CONF . "/functions.php";
require_once LIBS . '/GoogleAuthenticator/GoogleAuthenticator.php';

require_once CONF . "/routes_default.php";
require_once CONF . "/routes_user.php";


function handleShutdown(): void
{
	$error = error_get_last();
	if ($error !== null) {
		$err = array(
			"code" => $error['type'],
			"message" => $error['message'],
			"file" => $error['file'],
			"line" => $error['line']
		);

		$backtrace = debug_backtrace();
		unset($backtrace[0]);
		$err['backtrace'] = $backtrace;
		$error = EngineErrors::fromArray($err);
		ErrorHandler::instance()::engineErrorHandler($error, true);
	}
}

ErrorHandler::instance();
register_shutdown_function("handleShutdown");
set_error_handler("userErrorHandler");


if (!class_exists('\R') && class_exists("\RedBeanPHP\Facade")) {
	/**
	 * @method static dispense(string $getTable)
	 */
	class R extends Facade
	{
	}
}

if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
	$accept_lang = explode(",", $_SERVER['HTTP_ACCEPT_LANGUAGE'], 2)[0];
	define("USER_LANGUAGE", $accept_lang);
	unset($accept_lang);
} else define("USER_LANGUAGE", "en");
