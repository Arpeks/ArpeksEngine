<?php

/**
 * @author Arpeks
 * @description All engine function
 * @link https://vk.com/arpeks.menethil
 * @version 236
 */

use app\controllers\admin\libs\ChangeFreqSitemap;
use chillerlan\QRCode\{QRCode, QROptions};
use core\{App, base\EngineErrors, base\EngineException, ErrorHandler, interfaces\LanguageDomains, Language};
use core\libs\{EventHook, Plugins};
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\NoReturn;
use PHPMailer\PHPMailer\PHPMailer;
use Wkhooy\ObsceneCensorRus;

// ==========================================================================================
// ==========================================================================================
// ==========================================================================================

/**
 * Перевод даты в нужный формат
 * @param string $sourceDate Старый формат даты
 * @param string $newFormat Новый формат даты
 * @return string
 */
function changeDateFormat(string $sourceDate, string $newFormat = "d.m.Y H:i"): string
{
	$source = (is_numeric($sourceDate)) ? getTimestamp($sourceDate) : $sourceDate;
	try {
		$r = new DateTime($source);
	} catch (Exception $e) {
		$r = new DateTime();
		ErrorHandler::errorHandler($e, false);
	}
	return $r->format($newFormat);
}

/**
 * Перевод месяца по его номеру в короткое название
 * @param int $month Номер месяца
 * @return string
 */
function getShortMonth(int $month): string
{
	$monthList = App::MONTH_LIST;
	return $monthList[$month - 1] ?? "";
}

/**
 * Возвращает название класса исключая пространство имен (namespace)
 * @param string|object $className Название или объект класса включай пространство имен namespace
 * @return string
 */
function getClassName(string|object $className): string
{
	if (is_object(($className)))
		$className = get_class($className);
	$classNameParts = explode('\\', $className);
	return end($classNameParts);
}

/**
 * Вывод массива на экран (debugger mode)
 * @param mixed $arr Данные для вывода на экран
 * @param bool $die Флаг завершения скрипта после данной функции
 * @param bool $return Вернет информацию вместо вывода в браузер если стоит <b>TRUE</b>
 * @return string
 */
function debug(mixed $arr, bool $die = true, bool $return = false): string
{
	ob_start();
	echo "<pre>";
	print_r($arr);
	echo "</pre>";
	$text = ob_get_clean();

	if ($return) return $text;
	else echo $text;

	if ($die) die;
	return '';
}

/**
 * Удаляет из массива <b>$arr</b> значения, которые есть в массиве <b>$values</b>
 * @param array &$arr Ссылка на исходный массив
 * @param array $values Массив со значениями, которые надо удалить
 */
function deleteArrValue(array &$arr, array $values): void
{
	foreach ($arr as $i => $v) {
		if (in_array($v, $values))
			unset($arr[$i]);
	}
}

/**
 * Удаляет из массива <b>$arr</b> индексы, которые есть в массиве <b>$indexes</b>
 * @param array &$arr Ссылка на исходный массив
 * @param array $indexes Массив с индексами, которые надо удалить
 * @deprecated 236 Будет удален в будущих версиях
 */
function deleteArrIndexes(array &$arr, array $indexes): void
{
	_deprecatedFunction(__FUNCTION__, 236);
	foreach ($indexes as $index) {
		unset($arr[$index]);
	}
}

/**
 * Редирект на указанную страницу
 * @param string $http На какой action перейти
 */
#[NoReturn] function redirect(string $http = ""): void
{
	if (!$http) {
		if (!empty($_SERVER['HTTP_REFERER'])) $redirect = $_SERVER['HTTP_REFERER'];
		else $redirect = !empty($_SESSION['last_url']) ? $_SESSION['last_url'] : PATH;
	} else {
		$redirect = $http;
	}
	header("Location: $redirect");
	exit();
}

/**
 * Возвращает дату в формате TIMESTAMP
 * @param int $time Время в UNIX формате
 * @return string
 */
function getTimestamp(int $time = 0): string
{
	if (!$time) $time = time();
	return date("Y-m-d H:i:s", $time);
}

/**
 * Удаляет пробелы из переданной строки
 * @param string $str Искомая строка
 * @return string
 */
function clearSpace(string $str): string
{
	return str_replace(" ", "", $str);
}

/**
 * Получает IP-адрес пользователя
 * @return string IP-адрес пользователя
 */
function getIP(): string
{
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	$ip = filter_var($ip, FILTER_VALIDATE_IP);
	return $ip ?: 'Unknown';
}

/**
 * Определяет, входит ли текущий IP-адрес пользователя в диапазон
 * <br>
 * Пример: <b>isIPIn("127.0.0.1/24")</b>
 * @param string $netMask Диапазон вхождения IP-адресов
 * @return bool
 */
function isIPIn(string $netMask): bool
{
	list($rangeIP, $subnet) = explode('/', $netMask);
	$subnetMask = -1 << (32 - $subnet);
	$rangeLong = ip2long($rangeIP);
	$userLong = ip2long(getIP());
	return ($userLong & $subnetMask) === ($rangeLong & $subnetMask);
}

/**
 * Функция возвращающая расширение переданного файла
 * @param string $filePath Путь к файлу
 * @return string
 */
function getExtension(string $filePath): string
{
	return pathinfo($filePath, PATHINFO_EXTENSION);
}

/**
 * Функция возвращающая имя переданного файла без расширения
 * @param string $filePath Путь к файлу
 * @return string
 */
function getFileName(string $filePath): string
{
	return pathinfo($filePath, PATHINFO_FILENAME);
}

/**
 * Функция возвращающая путь до переданного файла
 * @param string $filePath Путь к файлу
 * @return string
 */
function getPath(string $filePath): string
{
	return pathinfo($filePath, PATHINFO_DIRNAME);
}

/**
 * Защищает текст, удаляя из него запрещенные теги
 * @param string $str Исходная строка
 * @param array $allowTags Разрешенные теги
 * @return string
 */
function h(string $str, array $allowTags = []): string
{
	return strip_tags(trim($str), implode("", $allowTags));
}

/**
 * Проходит по массиву и выполняет функцию {@see h} для каждого элемента массива
 * @param mixed $arr Исходный массив или объект
 * @param array $allowTags Разрешенные теги
 * @return mixed
 */
function hArr(mixed $arr, array $allowTags = []): mixed
{
	if (!empty($arr))
		foreach ($arr as $i => $item) {
			if (is_array($item) || is_object($item)) hArr($item, $allowTags);
			else $arr[$i] = h($item, $allowTags);
		}
	return $arr;
}

/**
 * Перевод русского текста в английский методе транслитерации
 * @param string $string Исходная строка
 * @return string
 */
function getTranslit(string $string): string
{
	$converter = array(
		'а' => 'a', 'б' => 'b', 'в' => 'v',
		'г' => 'g', 'д' => 'd', 'е' => 'e',
		'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
		'и' => 'i', 'й' => 'y', 'к' => 'k',
		'л' => 'l', 'м' => 'm', 'н' => 'n',
		'о' => 'o', 'п' => 'p', 'р' => 'r',
		'с' => 's', 'т' => 't', 'у' => 'u',
		'ф' => 'f', 'х' => 'h', 'ц' => 'c',
		'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
		'ь' => '\'', 'ы' => 'y', 'ъ' => '\'',
		'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

		'А' => 'A', 'Б' => 'B', 'В' => 'V',
		'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
		'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
		'И' => 'I', 'Й' => 'Y', 'К' => 'K',
		'Л' => 'L', 'М' => 'M', 'Н' => 'N',
		'О' => 'O', 'П' => 'P', 'Р' => 'R',
		'С' => 'S', 'Т' => 'T', 'У' => 'U',
		'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
		'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
		'Ь' => '\'', 'Ы' => 'Y', 'Ъ' => '\'',
		'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
	);
	return strtr($string, $converter);
}

/**
 * Преобразование текста в формат url
 * @param string $str Исходная строка
 * @return string
 */
function str2url(string $str): string
{
	$str = getTranslit($str);
	$str = strtolower($str);
	$str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
	return trim($str, "-");
}

/**
 * Сравнивает значения двух массивов по ключу
 * @param array $arrayOne Первый массив для сравнения
 * @param array $arrayTwo Второй массив для сравнения
 * @param integer|string $index Индекс массива, по которому сравнивает
 * @param string $compare_method Метод сравнения
 * @return bool Флаг сравнения
 */
function compareArrayByIndex(array $arrayOne, array $arrayTwo, int|string $index, string $compare_method = AE_COMPARE_EQUAL): bool
{
	if (!isset($arrayOne[$index]) || !isset($arrayTwo[$index])) return false;

	$valueOne = intval($arrayOne[$index]);
	$valueTwo = intval($arrayTwo[$index]);

	return match ($compare_method) {
		AE_COMPARE_EQUAL => $valueOne === $valueTwo,
		AE_COMPARE_EQUAL_NOT => $valueOne !== $valueTwo,
		AE_COMPARE_GREATER => $valueOne > $valueTwo,
		AE_COMPARE_GREATER_AND_EQUAL => $valueOne >= $valueTwo,
		AE_COMPARE_LESS => $valueOne < $valueTwo,
		AE_COMPARE_LESS_AND_EQUAL => $valueOne <= $valueTwo,
		default => false,
	};
}

/**
 * Обрезание текста по заданному количеству символов
 * @param string $string Исходная строка
 * @param int $start Начало отсчета символов
 * @param int $end Конец отсчета символов
 * @return string
 */
function substring(string $string, int $start = 0, int $end = 100): string
{
	$string = mb_substr($string, $start, mb_strlen($string));
	$points = false;

	while ($end < mb_strlen($string)) {
		$str = explode(" ", $string);
		unset($str[count($str) - 1]);
		$string = trim(implode(" ", $str));
		$points = true;
	}

	return ($points) ? "$string..." : $string;
}

/**
 * Возвращает путь к выбранной теме на сайте
 * @param bool $is_absolute_path Флаг абсолютного пути
 * @return string
 */
function getThemePath(bool $is_absolute_path = false): string
{
	$path = ($is_absolute_path) ? WWW_THEME : PATH . "/ag-themes";
	return $path . "/" . $_SESSION['layout'];
}

/**
 * Генерация копирайта CMS.<br /><b>Без него использовать движок запрещено</b>
 * @return string
 */
function getCopyright(): string
{
	$params = App::$app->getAll();

	$y = date("Y");
	$engine = "<a rel='nofollow' target='_blank' href='https://gitlab.com/Arpeks/ArpeksEngine' style='color:#ffffff;'>" . $params['AE_CMS_NAME'] . "</a> " . $params['AE_CMS_VERSION_LOCAL'] . "." . $params['AE_CMS_BUILD_NUMBER'];

	return "<strong><span>Copyright &copy; 2016–$y <a href='https://vk.com/arpeks.menethil' target='_blank' rel='nofollow' style='color: #ffffff'>Arpeks Menethil</a>.</strong> All Rights Reserved.</span><br />
        <i>Powered by $engine &copy; $y</i>";
}

/**
 * Определяет размер файла
 * @param string $file Полный путь к файлу
 * @return int
 */
function getFileSize(string $file): int
{
	if (!file_exists($file)) return -1;
	return filesize($file);
}

/**
 * Определяет размер папки
 * @param string $folder Полный путь к папке
 * @return int|string
 */
function getFolderSize(string $folder): int|string
{
	if (!file_exists($folder)) return "Путь не найден";
	$size = 0;
	foreach (scandir($folder) as $file) {
		if (trim($file, ".") == "") continue;
		$path = $folder . DIRECTORY_SEPARATOR . "$file";
		if (is_dir($path))
			$size += getFolderSize($path);
		else
			$size += getFileSize($path);
	}
	return $size;
}

/**
 * Возвращает список файлов в директории
 * @param string $folder Полный путь к папке
 * @param array $exception Массив путей с исключениями из выходного массива
 * @return array|string
 */
function getFileList(string $folder, array $exception = array()): array|string
{
	if (!file_exists($folder)) return "Путь не найден";
	$files = array();
	
	$exception_pattern = array_map(function ($path) {
		return preg_quote($path, '~');
	}, $exception);

	foreach (scandir($folder) as $file) {
		if (trim($file, ".") == "") {
			continue;
		}
		$path = $folder . DIRECTORY_SEPARATOR . $file;

		$ignore = false;
		foreach ($exception_pattern as $pattern) {
			if (preg_match("~$pattern~", $path)) {
				$ignore = true;
				break;
			}
		}
		if ($ignore) {
			continue;
		}

		if (is_dir($path)) {
			$files = array_merge($files, getFileList($path, $exception));
		} else {
			$files[] = $path;
		}
	}

	return $files;
}

/**
 * Возвращает информацию о браузере
 * @return string
 */
function getBrowserInfo(): string
{
	return $_SERVER['HTTP_USER_AGENT'];
}

/**
 * Пытается определить браузер по User-Agent
 * @return array
 */
function detectBrowser(): array
{
	if (empty(getBrowserInfo()))
		return array(
			'name' => 'unrecognized',
			'version' => 'unknown',
			'platform' => 'unrecognized',
			'userAgent' => ''
		);

	$userAgent = getBrowserInfo();
	return [
		'name' => match (true) {
			preg_match('/MSIE/i', $userAgent) && !preg_match('/Opera/i', $userAgent) => 'Internet Explorer',
			preg_match('/Firefox/i', $userAgent) => 'Mozilla Firefox',
			preg_match('/Chrome/i', $userAgent) => 'Google Chrome',
			preg_match('/Safari/i', $userAgent) => 'Apple Safari',
			preg_match('/Opera/i', $userAgent) => 'Opera',
			preg_match('/Netscape/i', $userAgent) => 'Netscape',
			default => 'Unknown'
		},
		'version' => match (true) {
			preg_match('/MSIE/i', $userAgent) && !preg_match('/Opera/i', $userAgent) => preg_replace('/.*?MSIE.(\S+).*/', '$1', $userAgent),
			preg_match('/Firefox/i', $userAgent) => preg_replace('/.*?Firefox.(\S+).*/', '$1', $userAgent),
			preg_match('/Chrome/i', $userAgent) => preg_replace('/.*?Chrome.(\S+).*/', '$1', $userAgent),
			preg_match('/Safari/i', $userAgent) => preg_replace('/.*?Version.(\S+).*/', '$1', $userAgent),
			preg_match('/Opera/i', $userAgent) => preg_replace('/.*?Version.(\S+).*/', '$1', $userAgent),
			preg_match('/Netscape/i', $userAgent) => preg_replace('/.*?Netscape.(\S+).*/', '$1', $userAgent),
			default => 'Unknown'
		},
		'platform' => match (true) {
			preg_match('/linux/i', $userAgent) => 'Linux',
			preg_match('/macintosh|mac os x/i', $userAgent) => 'Mac OS X',
			preg_match('/windows|win32/i', $userAgent) => 'Windows',
			default => 'Unknown'
		},
		'userAgent' => $userAgent
	];
}

/**
 * Переводит байты в КБ, МБ, ГБ
 * @param int $size Размер в байтах
 * @return string
 */
function getSizeName(int $size): string
{
	if ($size > 1024) { // Если размер больше 1 Кб
		$size = ($size / 1024);
		if ($size > 1024) {
			$size = ($size / 1024);
			if ($size > 1024) {
				$size = ($size / 1024);
				$size = round($size, 2);
				return $size . " " . __("Gb");
			} else {
				$size = round($size, 2);
				return $size . " " . __("Mb");
			}
		} else {
			$size = round($size, 2);
			return $size . " " . __("Kb");
		}
	}
	$size = round($size, 2);
	return $size . " " . __("byte");
}

/**
 * Возвращает массив DNSBL-хостов
 * @return array
 */
function getDNSBlackListHosts(): array
{
	return array(
		"all.s5h.net",
		"b.barracudacentral.org",
		"cbl.abuseat.org",
		"pbl.spamhaus.org",
		"rbl.megarbl.net",
		"sbl.spamhaus.org",
		"spam.dnsbl.sorbs.net",
		"xbl.spamhaus.org",
		"z.mailspike.net",
		"zen.spamhaus.org"
	);
}

/**
 * Проверяет IP на наличие в чёрном списке на DNSBL сервисах
 * @param string $ipaddress IP-адрес для проверки
 * @return array
 */
#[ArrayShape(['ip' => "string", 'hosts' => "array", 'blacklist' => "int|mixed"])]
function dnsBlackListCheckIP(string $ipaddress): array
{
	$ipaddress = h($ipaddress);

	if ($ipaddress == '') {
		$ipaddress = getIP();
	}
	$dnsbl = getDNSBlackListHosts();
	$result = array('ip' => $ipaddress, 'hosts' => array(), 'blacklist' => 0);
	$reverse_ip = implode(".", array_reverse(explode(".", $ipaddress)));
	foreach ($dnsbl as $host) {
		$is_listed = !App::isWhiteListIP() && checkdnsrr($reverse_ip . "." . $host . ".", "A") ? 1 : 0;
		$result['hosts'][$host] = $is_listed;
		if ($is_listed) {
			$result['blacklist']++;
		}
	}

	return $result;
}

/**
 * Генерирует CSRF-токен
 * @return string
 */
function generateCSRF(): string
{
	$csrf_token = hash('sha256', uniqid());
	$_SESSION['csrf'] = $csrf_token;
	return $csrf_token;
}

/**
 * Преобразует входящую строку в вид PascalCase
 * @param string $name Исходная строка
 * @return string
 */
function pascalCase(string $name): string
{
	return clearSpace(ucwords(str_replace("-", " ", $name)));
}

/**
 * Удаляет указанные символы из строки
 * @param string $string Исходная строка
 * @param array $chars Символы для удаления
 * @return string Строка без указанных символов
 */
function removeChars(string $string, array $chars = array("-", "_", ".", "/", "\\", " ")): string
{
	return str_replace($chars, "", $string);
}

/**
 * Преобразует входящую строку в вид camelCase
 * @param string $name Исходная строка
 * @return string
 */
function camelCase(string $name): string
{
	return lcfirst(pascalCase($name));
}

/**
 * Привязывает функцию к хуку
 * @param string $hook Название хука
 * @param string $func Название функции
 */
function addAction(string $hook, string $func): void
{
	$plugins = Plugins::instance();
	$plugins->registerHook(new EventHook($hook, $func));
}

/**
 * Выполняет хук
 * @param string $hook Название хука
 * @param array $params Параметры функции
 */
function doAction(string $hook, array $params = array()): void
{
	$plugins = Plugins::instance();
	$plugins->createHook($hook, $params);
}

/**
 * Привязывает функцию к фильтру
 * @param string $filter Название фильтра
 * @param string $func Название функции
 */
function addFilter(string $filter, string $func): void
{
	$plugins = Plugins::instance();
	$plugins->registerFilter(new EventHook($filter, $func));
}

/**
 * Выполняет фильтр
 * @param string $filter Название хука
 * @param array $params Параметры функции
 * @return mixed
 * @global Plugins $plugins
 */
function applyFilter(string $filter, array $params = array()): mixed
{
	$plugins = Plugins::instance();
	return $plugins->createFilter($filter, $params);
}

/**
 * <p>Возвращает (или устанавливает, если передан параметр <b>$setParam</b>) значение из сессии пользователя</p>
 * <p>Или возвращает <b>false</b>, если такого атрибута не существует</p>
 * @param string $attr Название атрибута
 * @param mixed $setParam Новое значение атрибута
 * @return mixed
 * @deprecated 236 Будет удален в будущих версиях
 */
function getUserAttribute(string $attr, mixed $setParam = false): mixed
{
	_deprecatedFunction(__FUNCTION__, 236);
	if (!isset($_SESSION['user'][$attr]))
		return false;
	if ($setParam)
		$_SESSION['user'][$attr] = $setParam;
	return $_SESSION['user'][$attr];
}

/**
 * Проверяет, является ли посетитель роботом поисковой системы
 * @param string $botName Ссылка на имя бота
 * @param array $browser Массив информации о браузере пользователя. См. {@see detectBrowser()}
 * @return bool
 */
function isBot(string &$botName = '', array $browser = array()): bool
{
	if (empty($browser['userAgent'])) {
		$browser = detectBrowser();
		$_SESSION['browser'] = $browser;
	}

	$bots = array(
		'rambler', 'googlebot', 'aport', 'yahoo', 'msnbot', 'turtle', 'mail.ru', 'omsktele',
		'yetibot', 'picsearch', 'sape.bot', 'sape_context', 'gigabot', 'snapbot', 'alexa.com',
		'megadownload.net', 'askpeter.info', 'igde.ru', 'ask.com', 'qwartabot', 'yanga.co.uk',
		'scoutjet', 'similarpages', 'oozbot', 'shrinktheweb.com', 'aboutusbot', 'followsite.com',
		'dataparksearch', 'google-sitemaps', 'appEngine-google', 'feedfetcher-google',
		'liveinternet.ru', 'xml-sitemaps.com', 'agama', 'metadatalabs.com', 'h1.hrn.ru',
		'googlealert.com', 'seo-rus.com', 'yaDirectBot', 'yandeG', 'yandex',
		'yandexSomething', 'Copyscape.com', 'AdsBot-Google', 'domaintools.com',
		'Nigma.ru', 'bing.com', 'dotnetdotcom', 'curl', 'bot'
	);

	foreach ($bots as $bot)
		if (stripos($browser['userAgent'], $bot) !== false) {
			$botName = $bot;
			return true;
		}
	return false;
}

/**
 * Генерирует пароль с заданной длиной
 * @param int $length Длина пароля
 * @return string
 */
function generatePassword(int $length = 16): string
{
	$pass = "";
	$abc = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	for ($i = 0; $i < $length; $i++) {
		$pass .= $abc[rand(0, strlen($abc) - 1)];
	}
	return $pass;
}

/**
 * Отправляет curl-запрос и возвращает его ответ
 * @param string $url Ссылка на страницу
 * @param array $params Параметры запроса
 * @param array $header Заголовки запроса
 * @param string $method Метод запроса (get/post)
 * @return array Массив с информацией о запросе
 */
function aeCurl(string $url, array $params = array(), array $header = array(), string $method = "get"): array
{
	$options = array(
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HEADER => false,
		CURLOPT_HTTPHEADER => $header,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_ENCODING => "",
		CURLOPT_USERAGENT => "curl-arpeks-engine-" . App::CMS_VERSION,
		CURLOPT_AUTOREFERER => true,
		CURLOPT_CONNECTTIMEOUT => 120,
		CURLOPT_TIMEOUT => 120,
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_URL => $url,
	);

	if (mb_strtolower($method) == "post") {
		$options[CURLOPT_POSTFIELDS] = $params;
		$options[CURLOPT_POST] = true;
	} else if (!empty($params)) {
		$options[CURLOPT_URL] .= "?" . http_build_query($params);
	}

	$ch = curl_init();
	curl_setopt_array($ch, $options);
	$content = curl_exec($ch);
	$header = curl_getinfo($ch);
	$header['options'] = $options;
	$header['errno'] = curl_errno($ch);
	$header['errmsg'] = curl_error($ch);
	$header['content'] = $content;
	curl_close($ch);

	$log = array(
		"time" => time(),
		"url" => $url,
		"params" => $params,
		"header" => $header,
		"method" => $method,
		"content" => $content
	);
	loggingAction("ae_curl", $log);

	return $header;
}

/**
 * Проверяет подлинность ReCaptcha v3. Ответ будет возвращаться в переменную $_SESSION['recaptcha']
 * @param string $recaptcha_response Ответ reCaptcha, присланный пользователем
 * @return bool Подлинность ответа
 */
function checkReCaptcha(string $recaptcha_response): bool
{
	$recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
	$recaptcha_secret = ENV->RECAPTCHA_SECRET_KEY;

	if ($recaptcha_secret !== "") {
		$recaptcha = json_decode(file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response . '&remoteip=' . getIP()), true);

		$_SESSION['recaptcha'] = $recaptcha;
		return $recaptcha['success'];
	}
	return true;
}

/**
 * Записывает действия в систему логов, который поможет понять, что делал пользователь
 * @param string $type Название файла/системы, которая вызвала событие
 * @param array $dataLog Данные для логирования
 * @return bool
 * @since 233
 */
function loggingAction(string $type, array $dataLog): bool
{
	if (!file_exists(LOGS)) {
		mkdir(LOGS, 0755, true);
	}
	$fileName = LOGS . "/$type.log";
	$fileSize = getFileSize($fileName);
	$content = array();

	if (file_exists($fileName) && $fileSize > 0) {
		$text = file_get_contents($fileName);
		$content = explode(PHP_EOL, $text);
	}

	if (count($content) >= 100) {
		unset($content[0]);
	}
	$content[] = serialize($dataLog); 

	$fp = fopen($fileName, "w");
	fwrite($fp, implode(PHP_EOL, $content));
	return fclose($fp);
}

/**
 * Возвращает переведенный на выбранный язык текст
 * @param string $text Текст для перевода
 * @param string $module Устанавливает домен, в котором будет производиться поиск
 * @return string Переведенная строка
 * @since 236
 */
function __(string $text, string $module = 'main'): string
{
	if (Language::getLanguage(false) === 'en_US')
		return $text;

	textdomain($module);
	return gettext($text);
}

/**
 * Устанавливает домен
 * @param string $domain Название домена
 * @param bool $isModule Объект является модулем
 * @return void
 * @since 236
 */
function setBindTextDomain(string $domain, bool $isModule = true): void
{
	if ($isModule) {
		bindtextdomain($domain, LANG . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $domain);
	} else {
		bindtextdomain($domain, LANG . DIRECTORY_SEPARATOR . $domain);
	}
}

/**
 * Устанавливает параметры системы
 * @param array $params Нужные изменения
 * @param bool $isMerge Объединять настройки
 * @return bool Флаг изменения
 */
function setParams(array $params, bool $isMerge = true): bool
{
	$newParams = ($isMerge) ? array_merge(getParams(), $params) : $params;
	if (count($newParams) != 0) {
		$fp = fopen(CONF . "/params.php", "w");
		fwrite($fp, serialize(App::compareParams($newParams)));
		fclose($fp);
		return true;
	}
	return false;
}

/**
 * Получает параметры системы
 * @return array Массив настроек системы
 */
function getParams(): array
{
	$params = array();
	if (file_exists(CONF . "/params.php")) {
		$fp = fopen(CONF . "/params.php", "r");
		$params = unserialize(fread($fp, filesize(CONF . "/params.php")));
		fclose($fp);
	}
	return $params;
}

/**
 * Генерирует QR-код с зашифрованным сообщением
 * @param string $data Сообщение, которое нужно зашифровать
 * @param int $endSize Конечный размер изображения
 * @return void Картинка для тега IMG
 */
function getQrCode(string $data = "", int $endSize = 200): void
{
	$index = md5(microtime() . uniqid());

	if (empty($data)) $data = "test_message";

	/* Генерация QR-кода во временный файл */
	$options = new QROptions([
		'imageTransparent' => false,
		'outputType' => QRCode::OUTPUT_IMAGE_PNG,
		'addQuietzone' => false,
		'scale' => 10
	]);
	(new QRCode($options))->render($data, TEMP . '/qr-' . $index . '.png');

	/* Конвертация PNG8 в PNG24 */
	$im = imagecreatefrompng(TEMP . '/qr-' . $index . '.png');


	$width = imagesx($im);
	$height = imagesy($im);

	$dst = imagecreatetruecolor($width, $height);
	imagecopy($dst, $im, 0, 0, 0, 0, $width, $height);
	imagedestroy($im);

	/* Наложение логотипа */
	$logo = imagecreatefrompng(WWW_THEME . '/admin/img/logo.png');
	$logo_width = imagesx($logo);
	$logo_height = imagesy($logo);

	$new_width = $width / 3;
	$new_height = $logo_height / ($logo_width / $new_width);

	$x = ceil(($width - $new_width) / 2);
	$y = ceil(($height - $new_height) / 2);

	$new_width = ceil($new_width);
	$new_height = ceil($new_height);

	imagecopyresampled($dst, $logo, $x, $y, 0, 0, $new_width, $new_height, $logo_width, $logo_height);

	$endImage = imagecreatetruecolor($endSize, $endSize);
	imagecopyresampled($endImage, $dst, 0, 0, 0, 0, $endSize, $endSize, imagesx($dst), imagesy($dst));

	unlink(TEMP . '/qr-' . $index . '.png');

	/* Вывод в браузер */
	header('Content-Type: image/x-png');
	imagepng($endImage);
}

/**
 * Функция генерации цвета в шестнадцатеричном формате по десятичному числу
 * @param int $integer Число из которого будет генерироваться цвет. Если установить на 0, то будет использоваться локальная версия ядра
 * @return string Сгенерированный цвета в HEX формате
 * @since 233
 */
function createColor(int $integer = 0): string
{
	if ($integer === 0) $integer = App::$app->get("AE_CMS_VERSION_LOCAL");
	$number = max(0, min($integer, 16777215));

	// Преобразуем число в шестнадцатеричный формат и добавляем ведущие нули, если необходимо
	return mb_strimwidth(md5($number), 0, 6, '0');
}

/**
 * Функция возвращает цвет текста в зависимости от цвета фона
 * @param string $backgroundColor Цвет фона
 * @return string Цвет текста в зависимости от цвета фона
 * @since 236
 */
function getTextColor(string $backgroundColor): string
{
	$r = hexdec(substr($backgroundColor, 0, 2));
	$g = hexdec(substr($backgroundColor, 2, 2));
	$b = hexdec(substr($backgroundColor, 4, 2));

	// Вычисление яркости
	$brightness = (299 * $r + 587 * $g + 114 * $b) / 1000;

	// Возврат чёрного или белого цвета в зависимости от яркости
	return ($brightness > 128) ? '000000' : 'FFFFFF';
}

/**
 * Функция возвращает флаг включен/выключен SMTP
 * @return bool
 */
function isActiveSMTP(): bool
{
    try {
        $app = App::$app->getAll();
        $mail = generateMailer($app);
        return App::$app->smtp_active === 'on' && $mail->smtpConnect();
    } catch (\Exception $e) {
        return false;
    }
}

/**
 * Вызывает ошибку при использовании устаревшей функции
 * @param string $function Название функции
 * @param string $version Версия ядра с которой функция считается устаревшей
 * @param string $replacement Новое название функции, если она была переименована
 */
function _deprecatedFunction(string $function, string $version, string $replacement = ''): void
{
	try {
		if ($replacement != '') {
			throw new EngineException(
				sprintf(
					'%s is <b>deprecated</b> since a version %s! Use %s instead',
					$function,
					$version,
					$replacement
				),
				E_USER_DEPRECATED
			);
		} else {
			throw new EngineException(
				sprintf(
					"%s is <b>deprecated</b> since a version %s with no alternative available",
					$function,
					$version
				),
				E_USER_DEPRECATED
			);
		}
	} catch (EngineException $e) {
		ErrorHandler::instance()::errorHandler($e);
	}
}

/**
 * Вызывает ошибку при использовании устаревшего аргумента функции
 * @param string $function Название функции
 * @param string $version Версия ядра с которой функция считается устаревшей
 * @param string $message Сообщение для пользователя
 */
function _deprecatedArgument(string $function, string $version, string $message = ''): void
{
	try {
		if ($message) {
			throw new EngineException(
				sprintf(
					"%s was called with an argument that is <b>deprecated</b> since a version %s! %s",
					$function,
					$version,
					$message
				),
				E_USER_DEPRECATED
			);
		} else {
			throw new EngineException(
				sprintf(
					"%s was called with an argument that is <b>deprecated</b> since a version %s with no alternative available",
					$function,
					$version
				),
				E_USER_DEPRECATED
			);
		}
	} catch (EngineException $e) {
		ErrorHandler::instance()::errorHandler($e);
	}

}

/**
 * Генерирует объект класса <b>PHPMailer</b>
 * @param array $smtpData Массив с настройками SMTP
 * @return PHPMailer
 * @since 235
 */
function generateMailer(array $smtpData): PHPMailer
{
	$mail = new PHPMailer;
	$mail->CharSet = PHPMailer::CHARSET_UTF8;
	$mail->Encoding = PHPMailer::ENCODING_8BIT;

	$mail->SMTPDebug = 0;
	$mail->Timeout = 10;
	$mail->isHTML();

	$mail->isSMTP();
	$mail->SMTPAuth = true;
	$mail->Host = $smtpData['smtp_server'];
	$mail->Username = $smtpData['smtp_login'];
	$mail->Password = $smtpData['smtp_password'];
	$mail->SMTPSecure = $smtpData['smtp_security'];
	$mail->Port = $smtpData['smtp_port'];
	$mail->SMTPOptions = [
		'ssl' => array(
			'verify_peer' => false,
			'verify_peer_name' => false
		)
	];

	return $mail;
}

/**
 * Функция отправки сообщения
 * @param string $emailSubject Тема письма
 * @param string $emailContent Тело письма
 * @param string $to Получатель
 * @param bool $redirect После отправки перейти на предыдущую страницу
 * @param array $attachments Список файлов прикрепленные к письму
 * @param string $attachment_extension Список файлов прикрепленные к письму
 * @return bool
 * @throws EngineException
 * @since 200
 */
function sendMail(string $emailSubject, string $emailContent, string $to, bool $redirect = false, array $attachments = array(), string $attachment_extension = "txt"): bool
{
	$app = App::$app->getAll();

	if (!isset($app['smtp_active'])) {
		throw new EngineException("SMTP deactivated", EngineException::PHPMAILER_SEND_ERROR);
	}

	try {
		$mail = generateMailer($app);
		$mail->setFrom($app['admin_email'], $app['site_name']); // Отправитель
		$mail->addAddress($to);  // Получатель
	} catch (Exception $e) {
		throw new EngineException($e->getMessage(), EngineException::PHPMAILER_SEND_ERROR);
	}

	$mail->Subject = $emailSubject;

	foreach ($attachments as $attachment) {
		if (file_exists($attachment)) {
			$filename = basename($attachment);
			if (pathinfo($attachment, PATHINFO_EXTENSION) != $attachment_extension)
				$filename .= '.' . $attachment_extension;
			try {
				$mail->addAttachment($attachment, $filename);
			} catch (Exception $e) {
				throw new EngineException($e->getMessage(), EngineException::PHPMAILER_SEND_ERROR);
			}
		}
	}

	ObsceneCensorRus::filterText($emailContent);
	$mail->Body = getBodyMail(compact('emailContent', 'app'));

	doAction("mail_send", ["mail" => $mail, "body" => $emailContent]);

	try {
		$send = $mail->send();
		if ($send === false) {
			throw new EngineException($mail->ErrorInfo, EngineException::PHPMAILER_SEND_ERROR);
		}

		if ($redirect) {
			redirect();
		}
		return $send;
	} catch (Exception $e) {
		return false;
	}
}

/**
 * Удаляет директорию со всеми вложенными файлами и папками
 * @param string $dir Путь к директории
 * @param bool $isMain Флаг, если установлено<br/><b>FALSE</b> - также удаляем саму директорию, если <b>TRUE</b> - удаляем только вложенные в директорию файлы и папки
 * @param array $exclusionList
 * @return void
 * @since 236
 */
function removeDirectory(string $dir, bool $isMain = true, array $exclusionList = array()): void
{
	if ($objs = glob($dir . "/{,.}*", GLOB_BRACE)) {
		foreach ($objs as $obj) {
			$name = basename($obj);
			if ($name == '.' || $name == '..' || $name[0] == '-' || in_array($name, $exclusionList)) continue;

			if (is_dir($obj))
				removeDirectory($obj, false);
			else
				unlink($obj);
		}
	}
	if (!$isMain) rmdir($dir);
}

/**
 * Добавления ссылок в SITEMAP
 * @param string $uri - URI страницы
 * @param ?int $lastMod - Дата последней модификации
 * @param ChangeFreqSitemap $changeFreq - Как часто обновляется страница
 * @param float $priority - Приоритет
 * @return string Возвращает строку формата XML для SITEMAP
 */
function addUrlToSitemap(string $uri, int $lastMod = null, ChangeFreqSitemap $changeFreq = ChangeFreqSitemap::monthly, float $priority = 0.5): string
{
	$entry = "<url>" . PHP_EOL;
	$entry .= "\t<loc>" . PATH . "$uri</loc>" . PHP_EOL;
	if ($lastMod) {
		$entry .= "\t<lastmod>" . date("Y-m-d", $lastMod) . "</lastmod>" . PHP_EOL;
	}
	$entry .= "\t<changefreq>{$changeFreq->value}</changefreq>" . PHP_EOL;
	$entry .= "\t<priority>$priority</priority>" . PHP_EOL;
	$entry .= "</url>" . PHP_EOL;
	return $entry;
}

/**
 * Сравнивает CSRF-токен, который передается с тем, что сохранен
 * @param string $csrf CSRF-токен
 * @param array $value Массив значений, который вернет функция при не совпадении токенов. Ключ `error` обязателен!
 * @return bool Флаг совпадения токенов
 */
function checkCSRF(string $csrf, array $value = array()): bool
{
	if (empty($value['error']))
		$value["error"] = __('Token CSRF not valid!');

	if (isset($_SESSION['csrf']) && !hash_equals($_SESSION['csrf'], $csrf)) {
		echo json_encode($value);
		return false;
	}
	return true;
}

/**
 * Функция отслеживание ошибок
 * @param int $errno Номер ошибки
 * @param string $errstr Текст ошибки
 * @param string $errfile Файл, в котором произошла ошибка
 * @param int $errline Строка с ошибкой
 * @return void
 */
function userErrorHandler(int $errno, string $errstr, string $errfile, int $errline): void
{
	$error = new EngineErrors($errno, $errstr, $errfile, $errline, debug_backtrace());
	ErrorHandler::instance()::engineErrorHandler($error, true);
}
