<?php

use core\Router;

Router::add("^" . ADMIN_URI . "/module/?(?P<controller>[a-z0-9-]+)/?(?P<action>[a-z0-9-]+)?$", ['prefix' => 'admin\module']);

Router::add("^" . ADMIN_URI . "/plugins/manager/(?P<pluginName>[a-zA-Z0-9-]+)$", ['controller' => 'Plugins', 'action' => 'manager', 'prefix' => 'admin']);

Router::add('^' . ADMIN_URI . '$', ['controller' => 'Main', 'action' => 'index', 'prefix' => 'admin']);
Router::add('^' . ADMIN_URI . '/errors?$', ['controller' => 'Main', 'action' => 'error', 'prefix' => 'admin']);
Router::add('^' . ADMIN_URI . '/(?P<controller>[a-z0-9-]+)/?(?P<action>[a-z0-9-]+)?$', ['prefix' => 'admin']);

Router::add('^sitemap\.xml$', ['controller' => 'Sitemap', 'action' => 'index']);
Router::add('^$', ['controller' => 'Main', 'action' => 'index']);
Router::add('^(?P<controller>[a-z0-9-]+)/?(?P<action>[a-z0-9-]+)?$');
