<?php

return [
	'dsn' => ENV->DBDRIVER . ":host=" . ENV->DBHOST . ";port=" . ENV->DBPORT . ";dbname=" . ENV->DBNAME . ";charset=" . ENV->DBCHARSET,
	'user' => ENV->DBUSER,
	'password' => ENV->DBPASS
];
