<?php

/*
 * Заполни массив страницами сайта.
 * Каждый элемент должен быть массивом с ключами:
 * uri - URI страницы
 * lastMod - дата последнего изменения страницы (0 - чтобы не выводить)
 * или
 * table - класс модели с колонками last_update и uri
 *
 *
 * changeFreq - частота изменения страницы (ChangeFreqSitemap)
 * priority - приоритет от 0.0 до 1.0
 */

use app\controllers\admin\libs\ChangeFreqSitemap;

global $pages;

$pages = array(
	[
		'uri' => '/',
		'lastMod' => 0,
		'changeFreq' => ChangeFreqSitemap::weekly,
		'priority' => 0.9
	]
);
