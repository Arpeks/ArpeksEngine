<?php

return array(
	[
		"name" => "url_after_parse",
		"description" => "Срабатывает после разбора ссылки на любой страницы сайта, но до вывода страницы на экран",
		"file" => "core/Router.php",
		"engine_version" => "219",
		"type" => "hook",
		"params" => array(
			"query" => "Ссылка на текущую страницу",
			"object" => "Ссылка на контроллер"
		)
	],
	[
		"name" => "url_before_parse",
		"description" => "Срабатывает перед разбором ссылки на любой страницы сайта",
		"file" => "core/App.php",
		"engine_version" => "219",
		"type" => "hook",
		"params" => array(
			"query" => "Ссылка на текущую страницу",
			"check" => "Кеш проверки обновления",
			"object" => "Ссылка на класс App"
		)
	],
	[
		"name" => "login_admin_auth_button",
		"description" => "Вызывается с окна авторизации администратора после вывода кнопки 'Войти'",
		"file" => "public/ag-themes/admin/template/user/login-admin.php",
		"engine_version" => "219",
		"type" => "hook",
		"params" => array(
			"query" => "Ссылка на текущую страницу"
		)
	],
	[
		"name" => "user_login",
		"description" => "Запускается при авторизации пользователя",
		"file" => "app/models/User.php",
		"engine_version" => "219",
		"type" => "hook",
		"params" => array(
			'isAdmin' => 'Проверка на авторизацию в Админ-Панели',
			'user' => 'Объект пользователя, который вошел в систему'
		)
	],
	[
		"name" => "user_session_login",
		"description" => "Запускается при авторизации пользователя через сессию",
		"file" => "config/functions.php",
		"engine_version" => "222",
		"type" => "hook",
		"params" => array(
			'user' => 'Объект пользователя, который вошел в систему',
			'session' => 'Объект сессии которая использовалась для авторизации'
		)
	],
	[
		"name" => "user_logout",
		"description" => "Запускается при выходе пользователя из системы",
		"file" => "app/models/User.php",
		"engine_version" => "232",
		"type" => "hook",
		"params" => array(
			'user' => 'Объект пользователя, который вышел из системы'
		)
	],
	[
		"name" => "spam_defense",
		"description" => "Вызывается, перед проверкой СПАМ защитой",
		"file" => "core/App.php",
		"engine_version" => "219",
		"type" => "filter",
		"params" => array(
			'spam_defense' => 'Состояние СПАМ защиты',
			'ip' => 'IP-адрес пользователя'
		)
	],
	[
		"name" => "site_loading_complete",
		"description" => "Вызывается после полной прогрузки страницы",
		"file" => "core/base/View.php",
		"engine_version" => "219",
		"type" => "hook",
		"params" => array(
			'viewFile' => 'Подключаемый файл',
			'layout' => 'Используемый шаблон',
			'plugins' => 'Активные плагины'
		)
	],
	[
		"name" => "cache_clear_all",
		"description" => "Вызывается при полной очистки кеша сайта",
		"file" => "core/Cache.php",
		"engine_version" => "219",
		"type" => "hook",
		"params" => array()
	],
	[
		"name" => "cache_clear",
		"description" => "Вызывается при очистки отдельной части кеша",
		"file" => "core/Cache.php",
		"engine_version" => "220",
		"type" => "hook",
		"params" => array(
			"key" => 'Ключ кеша'
		)
	],
	[
		"name" => "admin_chat_send",
		"description" => "Вызывается перед отправкой сообщения в чат для админов",
		"file" => "app/controllers/admin/AjaxController.php",
		"engine_version" => "219",
		"type" => "filter",
		"params" => array(
			"chat" => 'Объект чата',
			"data" => 'Данные для проверки'
		)
	],
	[
		"name" => "admin_chat_check",
		"description" => "Вызывается перед отправкой сообщения в чат для админов",
		"file" => "app/controllers/admin/AjaxController.php",
		"engine_version" => "220",
		"type" => "hook",
		"params" => array(
			"chat" => 'Объект чата',
			"data" => 'Данные для проверки'
		)
	],
	[
		"name" => "admin_menu_edit",
		"description" => "Служит для редактирования меню в AdminPanel и вызывается сразу после её генерации",
		"file" => "app/controllers/admin/AppController.php",
		"engine_version" => "219",
		"type" => "hook",
		"params" => array(
			"object" => 'Объект главного контроллера админки'
		)
	],
	[
		"name" => "mail_send",
		"description" => "Вызывается перед непосредственной отправкой сообщения",
		"file" => "config/functions.php",
		"engine_version" => "220",
		"type" => "hook",
		"params" => array(
			"mail" => 'Объект PHPMailer',
			"body" => 'Оригинальное тело письма'
		)
	],
	[
		"name" => "pushover_send_notification",
		"description" => "Вызывается после отправки уведомления через Pushover",
		"file" => "core/libs/Pushover.php",
		"engine_version" => "222",
		"type" => "hook",
		"params" => array(
			"params" => 'Параметры уведомления',
			"url" => 'Ссылка на Pushover API'
		)
	],
	[
		"name" => "user_edit",
		"description" => "Флаг проверки данных пользователя перед сохранением",
		"file" => "app/controllers/admin/UserController.php",
		"engine_version" => "223",
		"type" => "filter",
		"params" => array(
			"id" => "ID пользователя",
			"user" => "Данные пользователя",
			"new_data" => "Новые данные"
		)
	],
);
