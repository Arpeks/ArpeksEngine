<?php

use core\App;

App::$bbCode::addTag('hr', '<hr>');
App::$bbCode::addTag('br', '<br>');
App::$bbCode::addTag('url', '<a href="$1">$2</a>');
App::$bbCode::addTag('urlBlank', '<a href="$1" target="_blank">$2</a>');
App::$bbCode::addTag('i', '<i>$1</i>');
App::$bbCode::addTag('b', '<b>$1</b>');
