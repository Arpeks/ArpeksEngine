<?php

return [
	"site_name" => "AG Engine",
	"site_description" => "Описание сайта/лозунг",
	"site_keywords" => "ag engine, keywords",
	"layout" => "agengine",
	"pagination" => 10,
	"admin_email" => "admin@" . $_SERVER['HTTP_HOST'],
	"phone_country_code" => "ru",
	"phone" => "(999) 999-99-99",
	"smtp_server" => "smtp.yandex.ru",
	"smtp_security" => "ssl",
	"smtp_port" => 465,
	"count_message_admin_chat" => 1000,
	"archive_backup-Name" => "Ymd_His",
	"archive_backup-Ext" => "zip",
];
