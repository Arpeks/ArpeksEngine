��          �   %   �      P     Q     f     �     �     �     �     �     �     �     �     �     �     �               !     3     M     R     _     d     w          �     �  Y  �  (   �  @     &   Q     x     �     �  
   �     �     �  
   �  !   �  9     7   >  -   v  '   �  -   �  '   �     "     3     S  0   f     �  
   �     �     �                                             
                                                                   	             All messages deleted All messages moved to trash Back to Inbox Clear Delete Discard Folders From List of messages Mailbox Message deleted Message moved to trash Message not sent Message restored Message sent Messages restored Module 'Mailbox' disabled Read Read message Send Send a new message Subject Time To Trash Project-Id-Version: 
PO-Revision-Date: 2024-11-18 20:41+0300
Last-Translator: Балакин Д.М. <my@arpeks.ru>
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.5
X-Poedit-Basepath: ../../../../..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: app/controllers/admin/module/MailboxController.php
X-Poedit-SearchPath-1: app/models/module/Message.php
X-Poedit-SearchPath-2: public/ag-themes/admin/template/module/mailbox
X-Poedit-SearchPath-3: app/widgets/admin/MailMenuWidget
 Все сообщения удалены Все сообщения перенесены в корзину Вернуться в входящие Очистить Удалить Удалить Папки От Список сообщений Почта Сообщение удалено Сообщение перенесено в корзину Ошибка при отправки сообщения Сообщение восстановлено Сообщение отправлено Сообщения восстановлены Модуль 'Mailbox' отключен Входящие Прочитать письмо Отправить Отправить новое сообщение Тема письма Время Кому Корзина 