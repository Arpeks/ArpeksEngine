��          �      \      �     �     �     �     �  	         
                &     *  
   D     O     \     k     x     �     �     �     �  !  �     �     �     �       '   )     Q     `     x     �  '   �     �     �     �       '   +     S     b     u  ;   �                                         
                                          	                Actions Add post Cache for gallery Description Edit post Gallery Gallery cache Image Key Module 'Gallery' disabled Post added Post deleted Post not found Post updated Recommended size Remove Save Title Upload image error Project-Id-Version: 
PO-Revision-Date: 2024-11-18 20:40+0300
Last-Translator: Балакин Д.М. <my@arpeks.ru>
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.5
X-Poedit-Basepath: ../../../../..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: app/controllers/admin/module/GalleryController.php
X-Poedit-SearchPath-1: public/ag-themes/admin/template/module/gallery
X-Poedit-SearchPath-2: app/models/module/Gallery.php
 Действие Добавить пост Кэш для галереи Описание Редактировать запись Галерея Галерея кэша Изображение Ключ Модуль 'Gallery' отключен Пост добавлен Пост удален Пост не найден Запись обновлена Рекомендуемый размер Удалить Сохранить Заголовок Ошибка при загрузке изображения 