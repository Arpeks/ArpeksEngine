<?php

use core\App;
use core\ErrorHandler;

try {
	if (!session_id())
		session_start();
	
	require_once dirname(__DIR__) . "/config/init.php";
	new App();
} catch (Exception $t) {
	ErrorHandler::instance()::errorHandler($t);
}
