<?php

require_once dirname(__DIR__) . "/config/init.php";

$zipFilename = TEMP . DIRECTORY_SEPARATOR . 'mail.zip';

if (file_exists($zipFilename)) {
	header('Content-Type: application/zip');
	header('Content-Disposition: attachment; filename="logs.zip"');
	header('Content-Length: ' . filesize($zipFilename));
	readfile($zipFilename);
	unlink($zipFilename);

	if (file_exists(LOGS . '/mail-send.log'))
		unlink(LOGS . '/mail-send.log');
}

exit;
