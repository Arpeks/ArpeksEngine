<?php

use app\models\User;

$tmp_name = explode(" ", $user->name);
$name = $tmp_name[1];
$surname = $tmp_name[0];

$curr_id = !empty($_GET['id']) ? (int)$_GET['id'] : (int)$_SESSION['user']['id'];

?>
<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-md-3"><?= $profileHtml->render() ?></div>
        <!-- /.col -->
		<?php if ($is_access): ?>
            <div class="col-md-9">
                <div class="card card-primary">
                    <div class="card-body">

                        <div class="row mb-2">
                            <div class="col-4">
                                <a class="btn btn-danger" href="<?= ADMIN ?>/profile/session?act=removeAll">
									<?= __('Clear all sessions') ?>
                                </a>
                            </div>
							<?php if (User::checkPermission("admin_access_all")) : ?>
                                <div class="col-4">
                                    <select class="custom-select"
                                            onchange="location.href='<?= ADMIN ?>/profile/session?id='+this.options[this.selectedIndex].value">
										<?php foreach ($users as $id => $user) : ?>
                                            <option value="<?= $id ?>"<?= ($curr_id == $id) ? " selected" : "" ?>><?= $user->name ?></option>
										<?php endforeach; ?>
                                    </select>
                                </div>
							<?php endif; ?>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered table-hover dataTable"
                                   data-page-length='<?= $app['pagination'] ?>'>
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>IP</th>
                                    <th><?= __('Country') ?></th>
                                    <th><?= __('Time') ?></th>
                                    <th><?= __('Information') ?></th>
                                </tr>
                                </thead>
                                <tbody>
								<?php
								foreach ($sessionAuth as $sessID => $item):
									$color = ($item->is_active) ? "green-light" : "red-light";
									if (isset($_COOKIE['auth_key']) && $sessID == explode("s", $_COOKIE['auth_key'])[1]) $color = "light";
									?>
                                    <tr id="session-<?= $item->id ?>" class="bg-<?= $color; ?>">
                                        <td><?= $sessID; ?></td>
                                        <td><?= $item->ip; ?></td>
                                        <td><?= $item->country; ?></td>
                                        <td><?= changeDateFormat($item->time); ?></td>
                                        <td class="text-center">
                                        <span style="cursor: pointer" data-toggle="modal"
                                              data-target="#modal-session-<?= $sessID; ?>"><i
                                                    class="fa fa-edit"></i></span>
											<?php if ($item->is_active): ?>
                                                <span class="c-pointer text-red clearCache"
                                                      data-success="Сессия завершена"
                                                      data-error="Ошибка при завершении сессии"
                                                      data-url="<?= ADMIN ?>/ajax/user-logout"
                                                      data-session="<?= $item->id ?>">
                                            <i class="fa fa-fw fa-times"></i>
                                        </span>
											<?php endif; ?>
                                        </td>
                                    </tr>
								<?php endforeach; ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>IP</th>
                                    <th><?= __('Country') ?></th>
                                    <th><?= __('Time') ?></th>
                                    <th><?= __('Information') ?></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>

						<?php foreach ($sessionAuth as $sessID => $item): ?>
                            <div class="modal fade" id="modal-session-<?= $sessID; ?>" style="display: none;">
                                <div class="modal-dialog modal-xl modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Session #<?= $sessID ?></h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <pre><?php foreach (json_decode($item->session, true) as $ind => $sess) {
		                                            echo $ind . " => " . $sess . PHP_EOL;
	                                            } ?></pre>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default pull-right"
                                                    data-dismiss="modal">Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
						<?php endforeach; ?>

                    </div>
                </div>
            </div>
            <!-- /.col -->
		<?php endif; ?>
    </div>
    <!-- /.row -->

</section>
<!-- /.content -->
