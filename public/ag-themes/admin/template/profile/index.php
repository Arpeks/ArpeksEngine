<?php

use app\models\UserRole;

$tmp_name = explode(" ", $user->name);
$name = $tmp_name[1];
$surname = $tmp_name[0];

if (!$isActive2FA) {
	$ga = new GoogleAuthenticator();
	$secretKey = $ga->generateSecret();
	$url = $ga->getUrl($_SESSION['user']['email'], $app['site_name'], $secretKey);

	list($viewSecret_one, $viewSecret_two, $viewSecret_three, $viewSecret_four) = sscanf($secretKey, "%4s %4s %4s %4s");
	$viewSecret = implode(" ", [$viewSecret_one, $viewSecret_two, $viewSecret_three, $viewSecret_four]);
}

?>
<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-md-3"><?= $profileHtml->render() ?></div>
        <!-- /.col -->
		<?php if ($is_access): ?>
            <div class="col-md-9">
                <div class="card card-primary">
                    <div class="card-body">
                        <div class="<?= (!empty($audits)) ? "" : "active "; ?>tab-pane" id="settings">
                            <form action="<?= ADMIN; ?>/user/edit" method="post" data-toggle="validator">
                                <div class="box-body">

                                    <div class="row">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="form-group has-feedback">
                                                <label for="nickname">Nickname</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                        class="fas fa-user"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" name="nickname"
                                                           id="nickname"
                                                           placeholder="Nickname" value="<?= h($user->nickname); ?>"
                                                           required>
                                                    <span class="glyphicon form-control-feedback"
                                                          aria-hidden="true"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12">
                                            <div class="form-group has-feedback">
                                                <label for="email">Email</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                        class="fas fa-envelope"></i></span>
                                                    </div>
                                                    <input type="email" class="form-control" name="email" id="email"
                                                           autocomplete="username"
                                                           value="<?= h($user->email); ?>" required
                                                           placeholder="Email">
                                                    <span class="glyphicon form-control-feedback"
                                                          aria-hidden="true"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="form-group has-feedback">
                                                <label for="password"><?= __('Password') ?></label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                        class="fas fa-lock"></i></span>
                                                    </div>
                                                    <input type="password" class="form-control" name="password"
                                                           id="password"
                                                           autocomplete="current-password"
                                                           placeholder="<?= __('Enter your password if you want to change it') ?>">
                                                    <span class="glyphicon form-control-feedback"
                                                          aria-hidden="true"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12">
                                            <div class="form-group has-feedback">
                                                <label for="rePassword"><?= __('Repeat password') ?></label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                        class="fas fa-lock"></i></span>
                                                    </div>
                                                    <input type="rePassword" class="form-control" name="rePassword"
														   id="repassword"
                                                           autocomplete="new-password"
                                                           placeholder="<?= __('Repeat password') ?>">
                                                    <span class="glyphicon form-control-feedback"
                                                          aria-hidden="true"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="form-group has-feedback">
                                                <label for="name"><?= __('Name') ?></label>
                                                <input type="text" class="form-control" name="name" id="name"
                                                       placeholder="<?= __('Name') ?>" value="<?= h($name); ?>"
                                                       required>
                                                <span class="glyphicon form-control-feedback"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12">
                                            <div class="form-group has-feedback">
                                                <label for="surname"><?= __('Surname') ?></label>
                                                <input type="text" class="form-control" name="surname"
                                                       placeholder="<?= __('Surname') ?>" id="surname"
                                                       value="<?= h($surname); ?>" required>
                                                <span class="glyphicon form-control-feedback"
                                                      aria-hidden="true"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="role"><?= __('Role') ?></label>
										<?php if ($isRoleEdit): ?>
                                            <select name="role_id" id="role" class="form-control">
												<?php foreach (UserRole::getAll('`id` > 1') as $roles): ?>
                                                    <option value="<?= $roles->getID() ?>"<?php if ($roles->getID() == $user->role_id) {
														echo " selected='selected'";
													} ?>><?= $roles->name ?></option>
												<?php endforeach; ?>
                                            </select>
										<?php else: ?>
                                            <input type="text" name="role" class="form-control" placeholder="Роль"
                                                   value="<?= $currRole->name ?>" readonly/>
										<?php endif; ?>
                                    </div>

                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th style="width: 50%"><?= __('Name') ?></th>
                                            <th style="width: 50%"><?= __('Value') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><?= __('Two factor authentication') ?></td>
                                            <td>
                                                <div class="form-check has-feedback">
													<?php if ($isActive2FA): ?>
                                                        <span data-toggle="modal" data-target="#modal-tfa-disable"
                                                              class="nav-link c-pointer btn btn-danger"><?= __('Disable') ?></span>
													<?php else: ?>
                                                        <span data-toggle="modal" data-target="#modal-tfa-enable"
                                                              class="nav-link c-pointer btn btn-success"><?= __('Enable') ?></span>
													<?php endif; ?>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <div class="form-group">
                                        <div class="card card-danger card-solid file-upload">
                                            <div class="card-header">
                                                <h3 class="card-title"><?= __('Image profile') ?></h3>
                                            </div>
                                            <div class="card-body">
                                                <div class="single">
                                                    <img src="img/users/<?= $user->img ?>" alt=""
                                                         style="max-height: 150px;">
                                                </div>
                                                <div id="single" data-name="single" data-url="/user/add-image"
                                                     class="btn btn-success mt-2">Choose file
                                                </div>
                                                <p><small><?= __('Required image size') ?>: 215х215</small></p>
                                            </div>
                                            <div class="overlay" style="display: none">
                                                <i class="fas fa-sync-alt fa-3x fa-spin"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <input type="hidden" name="id" value="<?= $user->id; ?>">
                                    <input type="hidden" name="secret_key" value="<?= SECRET_KEY ?>"/>
                                    <button type="submit" class="btn btn-primary"><?= __('Save') ?></button>
                                </div>
                            </form>
                        </div>
                        <!-- /.tab-pane -->

                    </div>
                </div>
            </div>
            <!-- /.col -->
		<?php endif; ?>
    </div>
    <!-- /.row -->

</section>
<!-- /.content -->

<?php if (!$isActive2FA): ?>
    <div class="modal fade" id="modal-tfa-enable" style="display: none;">
        <form action="<?= ADMIN ?>/profile/tfa" method="post" data-toggle="validator" novalidate="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-light">
                        <h4 class="modal-title"><?= __('Setup 2FA') ?></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="container">


                            <div class="form-group has-feedback">
                                <div class="row">
                                    <div class="col-10 offset-1">
										<?= __('Applications for generating two-factor authentication confirmation codes allow you to receive codes even without a network or cellular connection. Use any application to generate two-step authentication codes.') ?>
                                        <br>
										<?= __('Example') ?>, <b>Google Authenticator</b> <?= __('for') ?> <a
                                                href="https://apps.apple.com/ru/app/google-authenticator/id388497605"
                                                target="_blank" rel="nofollow">iPhone</a>
										<?= __('and') ?>
                                        <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=ru&gl=US"
                                           target="_blank" rel="nofollow">Android</a>.
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-10 offset-1">
										<?= __('Scan the QR code in the app or enter the secret key below. Then, enter the confirmation code from the app to confirm that the app is set up correctly.') ?>
                                    </div>
                                </div>
                                <hr>

                                <div class="text-center">
                                    <div class="row justify-content-center">
                                        <div class="col-4">
                                            <div class="h4"><?= __('QR-Code') ?></div>
                                            <img src="<?= ADMIN ?>/profile/get-barcode?chl=<?= urlencode($url) ?>"
                                                 alt="qrCode" width="200" height="200"/>
                                        </div>
                                    </div>

                                    <div class="row justify-content-center mt-3">
                                        <div class="col-4">
                                            <div class="h4"><?= __('Secret key') ?></div>
                                            <b>
												<?= $viewSecret ?>
                                            </b>
                                        </div>
                                    </div>

                                    <div class="row justify-content-center mt-3">
                                        <div class="col-4">
                                            <div class="h4"><?= __('Code') ?></div>
                                            <input type="text" name="code" id="code" maxlength="6" required
                                                   class="form-control" value=""/>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" value="enable">
                        <input type="hidden" name="secret" value="<?= $secretKey ?>">
                        <input type="submit" class="btn btn-primary" style="float:right;" value="<?= __('Save') ?>">
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        </form>
        <!-- /.modal-dialog -->
    </div>
<?php else: ?>
    <div class="modal fade" id="modal-tfa-disable" style="display: none;">
        <form action="<?= ADMIN ?>/profile/tfa" method="post" data-toggle="validator" novalidate="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-light">
                        <h4 class="modal-title"><?= __('Disable 2FA') ?></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group has-feedback">
                            <div class="row">
                                <div class="col-10 offset-1">
									<?= __('If you are sure you want to disable two-step authentication, enter the password for your account and confirm submit the form.') ?>
                                </div>
                            </div>
                            <hr>
                            <div class="text-center">
                                <div class="row justify-content-center">
                                    <div class="col-5">
                                        <div class="h4"><?= __('Password for your account') ?></div>
                                        <input type="password" name="password" id="current-password" required
                                               autocomplete="current-password"
                                               class="form-control" value=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" name="action" value="disable">
                        <input type="submit" class="btn btn-danger" style="float:right;" value="<?= __('Save') ?>">
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        </form>
        <!-- /.modal-dialog -->
    </div>
<?php endif; ?>
