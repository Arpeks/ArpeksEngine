<?php

use app\models\User;

$curr_id = !empty($_GET['id']) ? (int)$_GET['id'] : (int)$_SESSION['user']['id'];

?>
<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-md-3"><?= $profileHtml->render() ?></div>
        <!-- /.col -->
		<?php if ($is_access): ?>
            <div class="col-md-9">
                <div class="card box-primary">
                    <div class="card-body">

						<?php if (User::checkPermission("admin_access_all")) : ?>
                            <div class="row mb-2">
                                <div class="col-4"></div>
                                <div class="col-4">
                                    <select class="custom-select"
                                            onchange="location.href='<?= ADMIN ?>/profile/audit?id='+this.options[this.selectedIndex].value">
										<?php foreach ($users as $id => $user) : ?>
                                            <option value="<?= $id ?>"<?= ($curr_id == $id) ? " selected" : "" ?>><?= $user->name ?></option>
										<?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-4"></div>
                            </div>
						<?php endif; ?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover dataTable"
                                   data-page-length='<?= $app['pagination'] ?>' id="auditTable">
                                <thead>
                                <tr class="text-center">
                                    <th>ID</th>
                                    <th><?= __('Category') ?></th>
                                    <th><?= __('Action') ?></th>
                                    <th><?= __('Time') ?></th>
                                </tr>
                                </thead>
                                <tbody>
								<?php if (!empty($audits) && is_array($audits)) foreach ($audits as $id => $audit): ?>
                                    <tr style="cursor: pointer" class="audit_row">
                                        <td class="audit_id"><?= $id ?></td>
                                        <td class="text-center audit_category" style="min-width: 140px"><?= $auditCategory[$audit->category_id] ?></td>
                                        <td class="audit_action"><?= $audit->action ?></td>
                                        <td class="audit_time" style="min-width: 140px"><?= date("d.m.Y H:i:s", strtotime($audit->time)); ?></td>
                                    </tr>
								<?php endforeach; ?>
                                </tbody>
                                <tfoot>
                                <tr class="text-center">
                                    <th>ID</th>
                                    <th><?= __('Category') ?></th>
                                    <th><?= __('Action') ?></th>
                                    <th><?= __('Time') ?></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /.col -->
		<?php endif; ?>
    </div>
    <!-- /.row -->

</section>
<!-- /.content -->

<div id="modal" role="dialog">
    <div id="modal-backdrop"></div>
    <div class="modal-dialog modal-xl modal-dialog-centered" id="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal-title"></h4>
                <button type="button" id="modal_close" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body" id="modal-body"></div>
        </div>
    </div>
</div>
