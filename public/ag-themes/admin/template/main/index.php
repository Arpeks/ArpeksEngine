<?php

use app\models\User;
use core\MenuGenerator;

/* @var MenuGenerator $menuReg */
$menuReg = MenuGenerator::getMenu();

?>
<!-- Main content -->
<section class="content">
    <div class="row">

        <div class="col-lg-4 col-xs-8">
			<div class="small-box bg-danger">
				<div class="inner count-uniq-users box-info">
                    <h3>∞</h3>
                    <p><?= __('Unique visitors') ?></p>
                </div>
                <div class="icon">
                    <i class="bi bi-pie-chart-fill"></i>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-xs-8">
            <div class="small-box bg-info">
				<div class="inner count-users-all box-info">
                    <h3>∞</h3>
                    <p><?= __('All users') ?></p>
                </div>
                <div class="icon">
                    <i class="bi bi-people-fill"></i>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-xs-8">
			<div class="small-box bg-success box-info">
                <div class="inner count-settings-update">
                    <h3>∞</h3>
                    <p><?= __('Last version') ?></p>
                </div>
                <div class="icon">
                    <i class="bi bi-cloud-arrow-down-fill"></i>
                </div>
            </div>
        </div>

    </div>
    <!-- /.row -->

    <div class="row">

        <section class="col-lg-7 col-xs-8">
            <div class="card direct-chat direct-chat-primary">
                <div class="card-header ui-sortable-handle">
                    <h3 class="card-title">
                        <i class="fa fa-comments"></i>
						<?= __('Chat') ?>
                    </h3>
                    <div class="card-tools">
						<button type="button" class="btn btn-tool" data-toggle="tooltip"
								title="<?= __("Online user list") ?>"
                                data-widget="chat-pane-toggle">
                            <i class="fa fa-users"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->

                <div class="card-body">
                    <!-- Conversations are loaded here -->
                    <div class="direct-chat-messages chat" id="chat-box"></div>
                    <!--/.direct-chat-messages-->
                    <div class="direct-chat-contacts">
                        <ul class="contacts-list">
	                        <?php foreach (User::getAll("`role_id` <> 5 AND `online` = 1") as $item): ?>
                                <li>
                                    <a href="<?= ADMIN ?>/profile?id=<?= $item->id ?>">
                                        <img class="contacts-list-img <?php if ($item->online) echo "on"; else echo "off"; ?>line"
											 src="img/users/<?= $item->img ?>" alt="">
                                        <div class="contacts-list-info mt-2">
                                      <span class="contacts-list-name">
                                        <?= $item->name ?>
                                      </span>
                                        </div>
                                    </a>
                                </li>
							<?php endforeach; ?>
                        </ul>
                        <!-- /.contatcts-list -->
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <div class="input-group">
						<label for="chat-message"></label>
						<input type="text" name="chat-message" data-id="<?= $_SESSION['user']['id'] ?>"
							   id="chat-message" placeholder="<?= __('Message') ?>..." maxlength="255"
							   class="form-control">
						<span class="input-group-append">
                            <button type="button" class="btn btn-primary" name="chat-send" id="chat-send"><?= __('Send') ?></button>
                        </span>
                    </div>
                </div>
                <!-- /.card-footer-->

            </div>
        </section>

        <section class="col-lg-5 col-xs-8">
            <!-- TO DO List -->
            <div class="card">
                <div class="card-header ui-sortable-handle">
                    <h3 class="card-title">
                        <i class="ion ion-clipboard mr-1"></i>
						<?= __('Tasks') ?>
                    </h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body card-todo">
                    <ul class="todo-list ui-sortable" data-widget="todo-list" id="card-todo">

						<?php if (!empty($todo)) : foreach ($todo as $do) : ?>

                            <li>

                                <div class="row">
                                    <!-- checkbox -->
                                    <div class="col-1">
                                        <div class="icheck-primary d-inline ml-1">
                                            <input type="checkbox" class="setTodoCheck" data-id="<?= $do['id'] ?>"
                                                   name="todo<?= $do['id'] ?>"
                                                   id="todoCheck<?= $do['id'] ?>"<?= ($do['is_close'] == '1') ? ' checked' : ''; ?>
                                                   />
                                            <label for="todoCheck<?= $do['id'] ?>"></label>
                                        </div>
                                    </div>
                                    <!-- todo text -->
                                    <div class="col-8">
                                        <span id="textTodo<?= $do['id'] ?>" class="text"><?= $do['text'] ?></span>
                                    </div>
                                    <!-- Emphasis label -->
                                    <div class="col-1">
                                        <small class="badge c-pointer badge-<?= $do['label'] ?>"
                                               title="<?= $do['time'] ?>"><i
                                                    class="far fa-clock"></i> <?= $do['delta'] ?></small>
                                    </div>
                                    <!-- General tools such as edit or delete-->
                                    <div class="col-2">
                                        <div class="tools">
                                            <i class="fas fa-edit" data-toggle="modal" data-id="<?= $do['id'] ?>" data-target="#editTodoModal"></i>
                                            <i class="fas fa-trash fa-delete-todo" data-id="<?= $do['id'] ?>"></i>
                                        </div>
                                    </div>
                                </div>

                            </li>

						<?php endforeach; endif; ?>

                    </ul>
                </div>
                <!-- /.card-body -->
                <div class="card-footer clearfix">
                    <button type="button" class="btn btn-info float-right" data-toggle="modal"
                            data-target="#addTodoModal"><i class="fas fa-plus"></i> <?= __('Add task') ?>
                    </button>
                </div>
            </div>
            <!-- /.box -->
        </section>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline card-outline-tabs">
                <div class="card-header p-0 border-bottom-0">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="tab-statistics" data-toggle="pill" href="#statistics"
                               role="tab"
                               aria-controls="statistics"
                               aria-selected="true">
                                <i class="fa fa-chart-bar"></i>
								<?= __("Overall statistics of the website") ?>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="tab-notepad" data-toggle="pill" href="#notepad" role="tab"
                               aria-controls="notepad" aria-selected="false">
                                <i class="fa fa-edit"></i>
								<?= __("Notepad") ?>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="tab-checksys" data-toggle="pill" href="#checksys"
                               role="tab" aria-controls="checksys"
                               aria-selected="false">
                                <i class="fa fa-cog"></i>
								<?= __("System auto-check") ?>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane fade active show" id="statistics" role="tabpanel"
                             aria-labelledby="tab-global">
                            <div class="table-responsive">
                                <table class="table table-sm" id="table-statistic">
                                    <tbody>

                                    <tr>
                                        <td class="col-md-3 border-0"><?= __("AG Engine version") ?>:</td>
                                        <td class="col-md-9 border-0">
											<?= $appObj->AE_CMS_VERSION_LOCAL ?>.<?= $appObj->AE_CMS_BUILD_NUMBER ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td><?= __("Operation Status of the website") ?>:</td>
                                        <td>
											<?php
											if ($appObj->isset('techwork')) {
												echo "<span class='text-red'>" . __("Maintenance works") . "</span>";
											} else {
												echo "<span class='text-green'>" . __("Work") . "</span>";
											}
											?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= __("Registered users") ?>:</td>
                                        <td>
											<?php echo count($user); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= __("Banned") ?>:</td>
                                        <td class="text-red">
											<?php echo count($userBanned); ?>
											/
											(<?php echo round(count($userBanned) / count($user) * 100) ?>%)
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
								<span class="btn btn-info btn-sm" id="scanButton"
									  data-url="<?= ADMIN ?>/ajax/scan" data-id="#modalScan">
                                    <i class="fas fa-shield-virus text-white"></i> <?= __("Scan files") ?>
                                </span>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="notepad" role="tabpanel"
                             aria-labelledby="tab-notepad">
							<label for="noticeText"></label>
							<textarea name="notice" id="noticeText" style="width:100%;height:200px;"
									  class="form-control"
									  placeholder="<?= __('Here you can save your own notes') ?>"><?= $notice ?></textarea>
							<br>
                            <button id="noticeTextButton"
                                    class="btn bg-teal btn-sm">
                                <i class="fa fa-save"></i> <?= __('Save') ?>
                            </button>
                        </div>

                        <div class="tab-pane fade" id="checksys" role="tabpanel"
                             aria-labelledby="tab-checksys">


                            <table class="table table-sm" id="table-checksys">
                                <tbody>

                                <tr>
                                    <td><?= __("Operating system") ?>:</td>
                                    <td>
										<?= php_uname("s") . " " . php_uname("r") ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= __("PHP version") ?>:</td>
                                    <td><?= phpversion(); ?></td>
                                </tr>
                                <tr>
                                    <td><?= __("MySQL version") ?>:</td>
                                    <td>
										<?= R::getDatabaseServerVersion() ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= __("Image driver") ?>:</td>
                                    <td>
										<?= $gdInfo ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= __("Redis support") ?>:</td>
                                    <td>
										<?= ($redis) ? "On" : "Off" ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= __("Redis Server connect") ?>:</td>
                                    <td>
										<?= ($redisServer) ? "On" : "Off" ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= __("RAM allocated") ?>:</td>
                                    <td>
										<?= getSizeName($memoryLimit) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= __("Disabled functions") ?>:</td>
                                    <td>
										<?= $disabledFunctions ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= __("Database size") ?>:</td>
                                    <td><?= getSizeName($databaseSize) ?></td>
                                </tr>
                                <tr>
                                    <td><?= __("Total cache size") ?>:</td>
                                    <td id="cacheSize">
										<?= getSizeName($cacheSize) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= __("Maximum size of the uploaded image") ?>:</td>
                                    <td>
										<?= getSizeName($maxUploadSize) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= __("Available disk space") ?>:</td>
                                    <td>
										<?= getSizeName($diskSpace) ?>
                                    </td>
                                </tr>

                                </tbody>
                            </table>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->

<div class="modal fade" id="editTodoModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="taskEditModalTitle"><?= __('Edit task') ?> #<span class="idTaskEdit"></span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="red-text">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
						<label for="editTaskTodoText" class="form-control-label"><?= __('Text') ?>:</label>
                        <input type="text" class="form-control" id="editTaskTodoText" data-id="-1"
                               name="editTaskTodoText">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                            class="fa fa-close"></i> <?= __('Close') ?>
                </button>
                <button type="button" class="btn btn-primary" id="editTaskButton"><i
                            class="fa fa-plus"></i> <?= __('Save') ?>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalScan">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
				<h4 class="modal-title"><?= __('Scan file') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="red-text">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <span id="scan_start">
                    <p><?= __('Loading') ?>...</p>
                    <div class="preloader_scan">
                        <div class="preloader_scan__loader">
                            <div class="spinner-grow text-info" role="status" style="width: 4rem; height: 4rem;"></div>
                        </div>
                    </div>
                </span>

				<table class="table table-sm table-striped" id="dataTableScan">
                    <thead>
                    <tr>
                        <th style="width: 50%" class="border-top-0"><?= __("Filename"); ?></th>
                        <th style="width: 10%" class="border-top-0"><?= __("Size"); ?></th>
                        <th style="width: 15%" class="border-top-0"><?= __("Date"); ?></th>
                        <th style="width: 25%" class="border-top-0"></th>
                    </tr>
                    </thead>
                </table>


            </div>
            <div class="modal-footer">
				<button class="btn bg-success disabled" id="restartScan" disabled
						data-url="<?= ADMIN ?>/ajax/scan" data-id="#modalScan">
                    <i class="fas fa-sync-alt"></i>
                </button>
                <button class="btn bg-warning disabled" id="takeSnapshot" disabled
						data-url="<?= ADMIN ?>/ajax/take-snapshot" data-id="#modalScan"
                        data-text="<?= __('Are you sure that the list of found files is safe, and you want to make a new snapshot of the files') ?>"
                        data-success="<?= __('Snapshot of the system and template files is taken successfully') ?>">
                    <i class="fa fa-magic"></i> <?= __("Take a snapshot") ?>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addTodoModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?= __('Add task') ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="red-text">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="taskTodoText" class="form-control-label"><?= __('Text') ?>:</label>
                        <input type="text" class="form-control" id="taskTodoText" name="taskTodoText">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                            class="fa fa-close"></i> <?= __('Close') ?>
                </button>
                <button type="button" class="btn btn-primary" id="addTaskButton"><i
                            class="fa fa-plus"></i> <?= __('Add') ?>
                </button>
            </div>
        </div>
    </div>
</div>
