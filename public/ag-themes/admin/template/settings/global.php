<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline card-outline-tabs">
							<form id="formGlobalSettings" method="post" action="<?= ADMIN ?>/ajax/settings-global">
                    <div class="card-header p-0 border-bottom-0">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="tab-global" data-toggle="pill" href="#global" role="tab"
                                   aria-controls="global" aria-selected="true"><?= __("Core settings") ?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-tokens" data-toggle="pill" href="#tokens" role="tab"
                                   aria-controls="tokens" aria-selected="false"><?= __("Tokens settings") ?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-information" data-toggle="pill" href="#information"
                                   role="tab" aria-controls="information"
                                   aria-selected="false"><?= __("Information settings") ?></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tab-smtp" data-toggle="pill" href="#smtp" role="tab"
                                   aria-controls="smtp" aria-selected="false"><?= __("SMTP settings") ?></a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane fade active show" id="global" role="tabpanel"
                                 aria-labelledby="tab-global">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th><?= __('Key') ?></th>
                                            <th><?= __('Value') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><?= __('Maintenance mode') ?></td>
                                            <td>
                                                <div class="form-check has-feedback">
                                                    <input type="checkbox"
                                                           name="techwork"<?= (isset($app['techwork']) && $app['techwork']) ? " checked" : ""; ?> />
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= __('Use HTTPS protocol') ?></td>
                                            <td>
                                                <div class="form-check has-feedback">
                                                    <input type="checkbox"
                                                           name="https"<?= (isset($app['https']) && $app['https']) ? " checked" : ""; ?> />
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= __('Spam protection') ?></td>
                                            <td>
                                                <div class="form-check has-feedback">
                                                    <input type="checkbox"
                                                           name="spam_defense"<?= (isset($app['spam_defense']) && $app['spam_defense']) ? " checked" : ""; ?> />
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= __('Dark mode') ?></td>
                                            <td>
                                                <div class="form-check has-feedback">
                                                    <input type="checkbox"
                                                           name="dark_mode"<?= (isset($app['dark_mode']) && $app['dark_mode']) ? " checked" : ""; ?> />
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= __('Site name') ?></td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <input class="form-control" type="text" name="site_name"
                                                           value="<?= (!(isset($app['site_name']) && $app['site_name'])) ? "" : $app['site_name']; ?>">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= __('Description') ?></td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <input class="form-control" type="text" name="site_description"
                                                           value="<?= (!(isset($app['site_description']) && $app['site_description'])) ? "" : $app['site_description']; ?>">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= __('Keywords') ?></td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <input class="form-control" type="text" name="site_keywords"
                                                           value="<?= (!(isset($app['site_keywords']) && $app['site_keywords'])) ? "" : $app['site_keywords']; ?>">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= __('Secret key') ?></td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <input class="form-control" type="text" name="secret_key"
                                                           value="<?= (!(isset($app['secret_key']) && $app['secret_key'])) ? "" : $app['secret_key']; ?>">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= __('Template') ?></td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <select class="form-control" name="layout" id="layout">
														<?php foreach (getLayouts() as $layout): ?>
                                                            <option value="<?= $layout ?>"<?= (isset($app['layout']) && $app['layout'] == $layout) ? " selected " : ""; ?>><?= $layout ?></option>
														<?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= __('Number of elements per page') ?></td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <input class="form-control" type="text" name="pagination"
                                                           value="<?= (!(isset($app['pagination']) && $app['pagination'])) ? "" : $app['pagination']; ?>">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= __('Number of messages in admin chat') ?></td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <input class="form-control" type="text"
                                                           name="count_message_admin_chat"
                                                           value="<?= (!(isset($app['count_message_admin_chat']) && $app['count_message_admin_chat'])) ? "" : $app['count_message_admin_chat']; ?>">
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th><?= __('Key') ?></th>
                                            <th><?= __('Value') ?></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="tokens" role="tabpanel" aria-labelledby="tab-tokens">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th><?= __('Key') ?></th>
                                            <th><?= __('Value') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><?= __('Tinify key') ?></td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <input class="form-control" type="text" name="tinify_key"
                                                           value="<?= (!(isset($app['tinify_key']) && $app['tinify_key'])) ? "" : $app['tinify_key']; ?>">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= __('Pushover Token') ?></td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <input class="form-control" type="text" name="pushover_token"
                                                           value="<?= (!(isset($app['pushover_token']) && $app['pushover_token'])) ? "" : $app['pushover_token']; ?>">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= __('Pushover User Token') ?></td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <input class="form-control" type="text" name="pushover_group_token"
                                                           value="<?= (!(isset($app['pushover_group_token']) && $app['pushover_group_token'])) ? "" : $app['pushover_group_token']; ?>">
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th><?= __('Key') ?></th>
                                            <th><?= __('Value') ?></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="information" role="tabpanel"
                                 aria-labelledby="tab-information">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th><?= __('Key') ?></th>
                                            <th><?= __('Value') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <?= __('Email administrator') ?>
                                                <br/>
                                                <span class="add-desc"><?= __('If the domain of mail is different from the main domain of the site,<br/> then you need to configure SMTP authorization for this mail service') ?></span>
                                            </td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <input class="form-control" type="email" name="admin_email"
                                                           value="<?= (!(isset($app['admin_email']) && $app['admin_email'])) ? "" : $app['admin_email']; ?>">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= __('Address') ?></td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <input class="form-control" type="text" name="address"
                                                           value="<?= (!(isset($app['address']) && $app['address'])) ? "" : $app['address']; ?>">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= __('Phone') ?></td>
                                            <td>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <label class="input-group-text">
                                                            <img src="img/flags/<?= $domain ?>.png" alt="Flag country"
																 class="phone_flag_country"
																 id="phone_country_code_flag">
                                                        </label>
                                                    </div>

                                                    <select class="custom-select"
															id="phone_country_code"
															name="phone_country_code">
														<?php foreach ($countries as $country): ?>
															<option
																value="<?= $country->domain ?>"<?= ($app['phone_country_code'] == $country->domain) ? " selected" : ""; ?>>
																<?= $country->code_phone ?> (<?= $country->name ?>)
                                                            </option>
														<?php endforeach; ?>
                                                    </select>

                                                    <input class="form-control" id="phoneAdmin" type="text" name="phone"
                                                           value="<?= (!(isset($app['phone']) && $app['phone'])) ? "" : $app['phone']; ?>"
                                                           data-inputmask="'mask': '(999) 999-99-99'" data-mask="">

                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th><?= __('Key') ?></th>
                                            <th><?= __('Value') ?></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="smtp" role="tabpanel" aria-labelledby="tab-smtp">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th><?= __('Key') ?></th>
                                            <th><?= __('Value') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><?= __('Activate SMTP service') ?></td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <input type="checkbox"
                                                           name="smtp_active"<?= (isset($app['smtp_active']) && $app['smtp_active']) ? " checked" : ""; ?> />
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= __('SMTP Login') ?></td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <input class="form-control" type="text" name="smtp_login"
                                                           value="<?= (!(isset($app['smtp_login']) && $app['smtp_login'])) ? "" : $app['smtp_login']; ?>">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= __('SMTP Password') ?></td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <input class="form-control" type="text" name="smtp_password"
                                                           value="<?= (!(isset($app['smtp_password']) && $app['smtp_password'])) ? "" : $app['smtp_password']; ?>">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= __('SMTP Server') ?></td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <input class="form-control" type="text" name="smtp_server"
                                                           value="<?= (!(isset($app['smtp_server']) && $app['smtp_server'])) ? "" : $app['smtp_server']; ?>">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= __('SMTP Security') ?></td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <input class="form-control" type="text" name="smtp_security"
                                                           value="<?= (!(isset($app['smtp_security']) && $app['smtp_security'])) ? "" : $app['smtp_security']; ?>">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td><?= __('SMTP Port') ?></td>
                                            <td>
                                                <div class="form-group has-feedback">
                                                    <input class="form-control" type="text" name="smtp_port"
                                                           value="<?= (!(isset($app['smtp_port']) && $app['smtp_port'])) ? "" : $app['smtp_port']; ?>">
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th><?= __('Key') ?></th>
                                            <th><?= __('Value') ?></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="box-footer clearfix no-border text-right">
													{input_csrf}
													<button class="btn btn-success pull-right">
                                                        <i class="fa fa-save"></i> <?= __('Save') ?>
													</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
