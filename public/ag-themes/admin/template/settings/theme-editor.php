<?php

$isTechwork = isset($app['techwork']);

$get = (!empty($_GET['file']) && isset($files[$_GET['file']])) ? $_GET['file'] : "style.css";

$fp = fopen($files[$get]['path'], "r");
$content = fread($fp, filesize($files[$get]['path']));
fclose($fp);

?><!-- Main content -->
<style>
    .CodeMirror {
        width: 100%;
        min-height: 60vh;
        height: calc(100vh - 300px);
        border: 1px solid #ddd;
        box-sizing: border-box;
    }
</style>

<script>
	const filepath = "<?=$ajax['filepath']?>",
		get = "<?=$get?>",
		theme = "<?=(isset($app['dark_mode'])) ? "dracula" : "eclipse";?>",
		extension = "<?=getExtension($get)?>"
</script>

<section class="content">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-lg-12 col-xl-9">
							<h2 id="theme-files-label"><?= __('Selected file') ?>: <i><?= $get ?></i></h2>
							<div style="border: 1px solid black; padding: 3px; background-color: #F8F8F8">
								<textarea id="code" cols="120" rows="30"></textarea>
							</div>
						</div>
						<div class="col">
							<h2 id="theme-files-label"><?= __('Available files') ?>: <i><?= $app['layout'] ?></i></h2>

							<ul role="tree" aria-labelledby="theme-files-label" id="file-list">
								<?php foreach ($files as $filename => $file):
									if ((!is_array($file) && !$file) || (is_array($file) && !file_exists($file['path']))) {
										continue;
									}
									if (!str_contains($filename, "separator")) : ?>
										<li <?= ($get === $filename) ? 'class="current-file"' : ''; ?>>
											<a role="treeitem"<?= ($get === $filename) ? ' class="notice notice-info"' : ''; ?>
											   href="<?= ADMIN ?>/settings/theme-editor?file=<?= $filename ?>">
                                            <span>
                                                <?= $filename ?>
                                            </span>
												<?php if (isset($file['description'])): ?>
													<br>
													<span class="nonessential"><?= $file['description'] ?></span>
												<?php endif; ?>
											</a>
										</li>
									<?php else: ?>
										<hr>
									<?php endif; ?>
								<?php endforeach; ?>
							</ul>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="float-right">
								<button id="themeEditorSaveButton"
										class="btn btn-info<?= ($isTechwork) ? ' disabled' : ''; ?>">
									<?= __('Save') ?>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
