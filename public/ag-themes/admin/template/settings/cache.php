<?php ?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline card-outline-tabs">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
							<tr>
								<th><?= __('Key') ?></th>
								<th><?= __('Description') ?></th>
								<th><?= __('Time end') ?></th>
								<th><?= __('Size') ?></th>
								<th><?= __('Action') ?></th>
							</tr>
                            </thead>
                            <tbody>
                            <?php foreach ($cacheList as $cache): ?>
								<tr>
									<td><?= $cache['name'] ?></td>
									<td><?= $cache['description'] ?></td>
									<td><?= ($cache['end_time'] > 0) ? getTimestamp($cache['end_time']) : "" ?></td>
									<td><?= $cache['size'] ?></td>
									<td class="text-center">
			                            <?php if ($cache['dataKey'] != 'tmp-images' && !empty($cache['cache'])): ?>
											<span class="btn btn-elegant" style="cursor: pointer" data-toggle="modal"
												  data-target="#modal-<?= removeChars($cache['dataKey'], ['.']) ?>"><i
                                                        class="fa fa-eye"></i></span>
			                            <?php endif; ?>
										<a class="delete btn btn-danger"
										   href="<?= ADMIN ?>/settings/cache-delete?key=<?= $cache['dataKey'] ?>&csrf={csrf}">
											<i class="fa fa-fw fa-times text-white"></i>
										</a>
									</td>
								</tr>
                            <?php endforeach; ?>
                            </tbody>
                            <tfoot>
							<tr>
								<th><?= __('Key') ?></th>
								<th><?= __('Description') ?></th>
								<th><?= __('Time end') ?></th>
								<th><?= __('Size') ?></th>
								<th><?= __('Action') ?></th>
							</tr>
                            </tfoot>
                        </table>
                    </div>
                    <p class="text-center">
						<a class="delete btn btn-danger"
						   href="<?= ADMIN; ?>/settings/cache-delete?key=all&csrf={csrf}"><i
								class="fa fa-fw fa-times text-white"></i> <?= __('Delete all') ?></a>
                        <br>
                        <i>( <?= $allSize ?> )</i>
                    </p>
                </div>
            </div>
        </div>
    </div>

	<?php foreach ($cacheList as $cache): ?>
        <div class="modal fade" id="modal-<?= removeChars($cache['dataKey'], ['.']) ?>" style="display: none;">
            <div class="modal-dialog modal-xl modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Cache: <?= $cache['dataKey'] ?></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <pre><?= htmlspecialchars(print_r($cache['cache'], true)); ?></pre>
                    </div>
                    <div class="modal-footer">
                        <div class="pull-left">
                            <a class="delete btn btn-danger"
							   href="<?= ADMIN; ?>/settings/cache-delete?key=<?= $cache['dataKey'] ?>">
                                <i class="fa fa-fw fa-times text-white"></i> <?= __('Delete') ?>
                            </a>
                        </div>
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
	<?php endforeach; ?>

</section>
<!-- /.content -->
