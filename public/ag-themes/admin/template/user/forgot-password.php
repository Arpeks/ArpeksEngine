<p class="login-box-msg"><?= __('New password') ?></p>
<hr>
<div class="text-center">
	<b><?= $newPassword ?></b>
</div>
<hr>
<p class="mb-1">
	<a href="<?= ADMIN ?>/user/login-admin" class="btn btn-success btn-block btn-flat"><?= __('Login') ?></a>
</p>
