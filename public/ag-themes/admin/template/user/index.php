<?php

use app\models\User;

?><!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover dataTable"
                               data-page-length='<?= $app['pagination'] ?>'>
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Email</th>
                                <th><?= __('Username') ?></th>
                                <th><?= __('Role') ?></th>
                                <th><?= __('Registration date') ?></th>
                                <th><?= __('Registration IP') ?></th>
                                <th><?= __('Last IP') ?></th>
                                <th><?= __('Last action time') ?></th>
                                <th>Online</th>
                                <th><?= __('Actions') ?></th>
                            </tr>
                            </thead>
                            <tbody>
							<?php foreach ($users as $user):
								/** @var User $user */

								$color = ($roles[$user->role_id]->name != "banned") ? "red" : "green";
								$colorRestore = ($color == "green") ? "red" : "green";
								$class = ($roles[$user->role_id]->name != "banned") ? "trash" : "trash-restore";

								if ($class == "trash") {
									$trashTitle = __("Ban user");
								} else {
									$trashTitle = __("Unban user");
								}

								$isEditInformationAccess = (User::checkPermission("admin_edit_information") || $user->id == $_SESSION['user']['id']);
								$isBanSystemAccess = (User::checkPermission("admin_ban_system") && $user->id != $_SESSION['user']['id'] && $roles[$user->role_id]->name != "root");

								?>
                                <tr class="bg-<?= $colorRestore; ?>-light">
                                    <td><?= $user->id; ?></td>
                                    <td><?= $user->email; ?></td>
                                    <td><?= $user->name; ?></td>
                                    <td>
										<?php
										echo $roles[$user->role_id]->name;
										?>
                                    </td>
                                    <td><?= date("d.m.Y H:i:s", strtotime($user->reg_date)); ?></td>
                                    <td><?= $user->reg_ip; ?></td>
                                    <td><?= $user->last_ip; ?></td>
                                    <td><?= date("d.m.Y H:i:s", strtotime($user->last_action)); ?></td>
                                    <td class="text-center"><i
                                                class="fa fa-circle text-<?= ($user->online) ? "success" : "danger" ?>"></i>
                                    </td>
                                    <td>
										<?php if ($isEditInformationAccess) : ?>
                                            <a href="<?= ADMIN; ?>/profile?id=<?= $user->id; ?>" data-toggle="tooltip"
                                               data-placement="auto"
                                               data-original-title="<?= __('Edit profile') ?>">
                                                <i class="fa fa-fw fa-cogs"></i>
                                            </a>
                                            <a href="<?= ADMIN; ?>/profile/session?id=<?= $user->id; ?>"
                                               data-placement="auto"
                                               data-toggle="tooltip" data-original-title="<?= __('Session') ?>">
                                                <i class="fa-fw fas fa-feather"></i>
                                            </a>
                                            <a href="<?= ADMIN; ?>/profile/audit?id=<?= $user->id; ?>"
                                               data-placement="auto"
                                               data-toggle="tooltip" data-original-title="<?= __('Audit') ?>">
                                                <i class="fa fa-fw fa-eye"></i>
                                            </a>
										<?php endif; ?>

										<?php if ($isBanSystemAccess && $isEditInformationAccess): ?>
                                            |
										<?php endif; ?>

										<?php if ($isBanSystemAccess): ?>
                                            <a href="<?= ADMIN; ?>/user/delete?id=<?= $user->id; ?>"
                                               data-placement="auto"
                                               data-toggle="tooltip" title="<?= $trashTitle ?>"
                                               class="delete"><i class="fa fa-<?= $class ?>-alt text-<?= $color ?>"></i></a>
										<?php endif; ?>
                                    </td>
                                </tr>
							<?php endforeach; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Email</th>
                                <th><?= __('Username') ?></th>
                                <th><?= __('Role') ?></th>
                                <th><?= __('Registration date') ?></th>
                                <th><?= __('Registration IP') ?></th>
                                <th><?= __('Last IP') ?></th>
                                <th><?= __('Last action time') ?></th>
                                <th>Online</th>
                                <th><?= __('Actions') ?></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->
