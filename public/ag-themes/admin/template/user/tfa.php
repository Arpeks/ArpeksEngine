<div id="loaderTFA"></div>

<p class="login-box-msg"><?= __('Enter code') ?></p>

<form id="fLogin">
	<div class="mb-3">
		<input autofocus type="text" maxlength="1" class="tfaCode">
		<input type="text" maxlength="1" class="tfaCode">
		<input type="text" maxlength="1" class="tfaCode">
		<input type="text" maxlength="1" class="tfaCode">
		<input type="text" maxlength="1" class="tfaCode">
		<input type="text" maxlength="1" class="tfaCode">
	</div>
	<div class="row">
		<div class="col-5">
			<a href="<?= ADMIN ?>/user/logout" class="btn btn-danger btn-block btn-flat"><?= __('Logout') ?></a>
		</div>
		<div class="col-5 offset-2">
			<input type="hidden" name="csrf" value="{csrf}">
			<a class="btn btn-primary btn-block btn-flat" id="sendTFA"><?= __('Enter') ?></a>
		</div>
		<!-- /.col -->
	</div>
</form>


<script type="module" src="js/tfa.js"></script>
