<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <form method="post" action="/admin/user/signup" role="form" data-toggle="validator">
                    <div class="card-body">
                        <div class="form-group has-feedback">
                            <label for="email">Email*</label>
                            <input class="form-control" name="email" id="email" type="email"
								   value="<?= $_SESSION['form_data']['email'] ?? '' ?>"
                                   required>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="password"><?= __('Password') ?>*</label>
                            <input class="form-control" name="password" id="password" type="password" data-minlength="6"
                                   data-error="<?= __('The password must be at least 6 characters long.') ?>" required>
                            <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="name"><?= __('Name') ?>*</label>
                            <input type="text" class="form-control" name="name" id="name" required>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="name"><?= __('Surname') ?>*</label>
                            <input type="text" class="form-control" name="surname" id="surname" required>
                        </div>
                        <div class="form-group">
                            <label><?= __('Role') ?></label>
                            <select class="form-control" name="role_id">
								<?php foreach ($roles as $role): ?>
                                    <option value="<?= $role->getID() ?>"><?= $role->name ?></option>
								<? endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="card-footer">
                        <input type="hidden" name="secret_key" value="<?= SECRET_KEY ?>"/>
                        <button type="submit" class="btn btn-primary"><?= __('Add user') ?></button>
                    </div>
                </form>
				<?php if (isset($_SESSION['form_data'])) unset($_SESSION['form_data']); ?>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
