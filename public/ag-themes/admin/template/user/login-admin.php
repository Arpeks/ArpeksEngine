<p class="login-box-msg"><?= __('LogIn in') ?><br><b>AG</b>Panel</p>

<form action="<?= ADMIN ?>/user/login-admin" method="post" id="fLogin">
	<div class="input-group mb-3">
		<input name="login" autofocus type="text" class="form-control" placeholder="Email or Login">
		<div class="input-group-append">
			<div class="input-group-text">
				<span class="fas fa-envelope"></span>
			</div>
		</div>
	</div>
	<div class="input-group mb-3">
        <input name="password" type="password" class="form-control" placeholder="Password" id="password">
		<div class="input-group-append">
			<div class="input-group-text">
				<span class="fas fa-lock"></span>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-4 offset-8">
			<input type="hidden" name="csrf" value="{csrf}">
			<input type="hidden" name="recaptcha_response" id="recaptchaResponse">
			<button type="submit" class="btn btn-primary btn-block btn-flat"><?= __('Login') ?></button>
		</div>
		<!-- /.col -->
	</div>
	<?php
	doAction("login_admin_auth_button", ['query' => 'user/login-admin']);
	?>
</form>

<?php if (isActiveSMTP()): ?>
	<div class="text-center m-1">
		<p>- <?= __('OR') ?> -</p>
	</div>

	<p class="mb-1">
		<a href="<?= ADMIN ?>/user/forgot-user"
		   class="btn btn-success btn-block btn-flat"><?= __('Forgot password') ?></a>
	</p>
<?php endif; ?>
