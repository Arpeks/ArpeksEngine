<p class="login-box-msg"><?= __('Forgot your password? Then enter your email from your account and you will be able to proceed to the password reset procedure') ?></p>

<form action="<?= ADMIN ?>/user/forgot-user" method="post" id="fLogin">
	<div class="input-group mb-3">
        <input name="email" autofocus type="email" id="email" class="form-control" placeholder="Email"/>
		<div class="input-group-append">
            <label class="input-group-text" for="email">
				<span class="fas fa-envelope"></span>
            </label>
		</div>
	</div>
	<div class="row">
		<div class="col-5 offset-7">
			<input type="hidden" name="csrf" value="{csrf}">
            <input type="submit" class="btn btn-primary btn-block btn-flat" value="<?= __('Enter') ?>"/>
		</div>
		<!-- /.col -->
	</div>
</form>

<div class="text-center m-1">
	<p>- <?= __('OR') ?> -</p>
</div>

<p class="mb-1">
	<a href="<?= ADMIN ?>/user/login-admin" class="btn btn-success btn-block btn-flat"><?= __('Login') ?></a>
</p>
