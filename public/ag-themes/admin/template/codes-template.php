<ul class="list-group codes-list">

    <li class="list-group-item">
        <span class="font-weight-bold">[images]url1,url2,url3,...[/images]</span>
        <hr>
        <span>Выводит изображения со стороннего ресурса в виде галереи</span>
    </li>

    <li class="list-group-item">
        <span class="font-weight-bold">[hr]</span>
        <hr>
        <span>Аналог тега <span class="font-weight-bold">hr</span></span>
    </li>

</ul>