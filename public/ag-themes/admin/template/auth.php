<!DOCTYPE html>
<html lang="<?= core\Language::getLanguage(true) ?>">
<head>
    <meta charset="utf-8">
    <base href="<?= PATH ?>/ag-themes/admin/">
    {meta}
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= PATH_ADMINLTE ?>/plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?= PATH_ADMINLTE ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- sweetalert2 -->
	<link rel="stylesheet" href="css/sweetalert2.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= PATH_ADMINLTE ?>/dist/css/adminlte.min.css">

    <link rel="stylesheet" href="css/my.css?v=<?= time() ?>">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

	<link rel="shortcut icon" href="{logo_path}" type="image/png">

    <script>
        const path = '<?= PATH ?>',
							csrf = '{csrf}',
							adminpath = '<?= ADMIN ?>';
    </script>

    <script src="<?= PATH_ADMINLTE ?>/plugins/jquery/jquery.min.js"></script>
</head>

<body class="login-page">

<div class="login-box">

    <div class="card card-outline card-primary">
        <div class="card-header text-center">
            <a href="<?= PATH ?>" class="h1"><b>AG</b>Panel</a>
        </div>

        <div class="card-body">

			<?= $content ?>

        </div>

    </div>
</div>

<!-- Bootstrap 4 -->
<script src="<?= PATH_ADMINLTE ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SweetAlert 2 -->
<script src="js/libs/sweetalert2.js"></script>
<!-- Functions -->
<script type="module" src="js/functions.js"></script>
<?php if (ENV->RECAPTCHA_SECRET_KEY !== "") : ?>
	<!-- reCAPTCHA v3 -->
	<script src="https://www.google.com/recaptcha/api.js?render={recaptcha_site_key}" async></script>
	<script type="module">
		const loginForm = document.getElementById('fLogin')
		loginForm.addEventListener('submit', event => {
			event.preventDefault()

            if (document.getElementById('password') === null) {
                loginForm.submit()
            } else {
                grecaptcha.ready(() => {
                    grecaptcha.execute('{recaptcha_site_key}', {action: 'login'}).then((token) => {
                        const recaptchaResponse = document.getElementById('recaptchaResponse')
                        recaptchaResponse.value = token
                        loginForm.submit()
                    })
                })
            }



		})
	</script>
<?php endif; ?>

<?php if (isset($_SESSION['error'])): ?>
	<script defer type="module">
		import { notifySweetAlert } from './js/functions.js'

		notifySweetAlert('<?= $_SESSION['error'] ?>', 'error', 10)
    </script><?php unset($_SESSION['error']); ?>
<?php endif; ?>
<?php if (isset($_SESSION['success'])): ?>
	<script defer type="module">
		import { notifySweetAlert } from './js/functions.js'
			notifySweetAlert('<?= $_SESSION['success'] ?>', 'success');
    </script><?php unset($_SESSION['success']); ?>
<?php endif; ?>

</body>
</html>
