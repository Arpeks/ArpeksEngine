<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">

                    <div class="page-content page-console">
                        <div class="console mobile-full-width">
                            <div class="out" id="console"></div>
                            <div class="in">
                                <span class="in-start"><i class="fa fa-angle-right"></i></span>
								<input id="c-input" type="text" />
                            </div>
                        </div>


                        <div class="text-right" style="margin-top: 10px">
                            <span class="btn btn-danger" onclick="clearDataConsole()"><i
									class="fas fa-fw fa-times text-white"></i> Очистить</span>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->
<link rel="stylesheet" href="css/log.css">
<script src="js/libs/ansi_up.js"></script>
