<?php /** @var core\base\EngineErrors $e */ ?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<base href="<?= PATH ?>/ag-themes/admin/">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Error: {error_code} | {error_name} | {site_name}</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	<link rel="stylesheet" href="css/errors.css?v=<?= time() ?>">

	<link rel="shortcut icon" href="{logo_path}" type="image/png">

</head>

<body>


<main class="wrapper">

	<section class="error-name">
		<div class="logo"><img src="{logo_path}" alt="Логотип"></div>
		<h1>{error_name} - {error_code}</h1>
		<div class="logo"><img src="{logo_path}" alt="Логотип"></div>
	</section>


	<section class="error-content">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="left">File:</div>
					<div class="right">
                        {error_file}
                        [
                        <a href="phpstorm://open?{error_file}:{error_line}">Открыть в phpStorm</a>
                        ]
                    </div>
				</div>
				<div class="row">
					<div class="left">Line:</div>
					<div class="right">{error_line}</div>
				</div>
				<div class="row">
					<div class="left">Message:</div>
					<div class="right">{error_message}</div>
				</div>
			</div>
			<?php if ($e->getBacktrace()): ?>
				<div class="card-footer">
					<div class="row">
						<h3>Backtrace:</h3>
					</div>
					<?php foreach ($e->getBacktrace() as $backtrace): ?>
						<div class="backtrace__block">
							<?php foreach ($backtrace as $k => $v): ?>
								<div class="row">
									<div class="left"><?= ucwords($k) ?>:</div>
									<div class="right">
                                        <?php if($k === "file"): ?>
                                            <?= $v ?>
                                            [
                                            <a href="phpstorm://open?<?=$backtrace['file']?>:<?=$backtrace['line']?>">Открыть в phpStorm</a>
                                            ]
                                        <?php else: ?>
                                            <?= htmlspecialchars(print_r($v, true)) ?>
                                        <?php endif; ?>
                                    </div>
								</div>
							<?php endforeach; ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</section>
</main>

<footer>
	{copyright}
</footer>

</body>
</html>
