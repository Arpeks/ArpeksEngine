<?php

use core\MenuGenerator;

?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered dataTable" data-page-length='<?= $app['pagination'] ?>'>
                            <thead>
                            <tr>
                                <th><?= __('Key') ?></th>
                                <th><?= __('Version') ?></th>
                                <th><?= __('Description') ?></th>
                                <th><?= __('Author') ?></th>
                                <th><?= __('Actions') ?></th>
                            </tr>
                            </thead>
                            <tbody>
							<?php foreach ($pluginsInfo as $plugin => $info): ?>
                                <tr <?php if (!$info['deactivated'] && !isset($info['isDownload'])) {
									echo 'class="bg-success"';
								} else if (isset($info['isDownload'])) {
									echo 'class="bg-warning"';
								} ?>>
                                    <td><?= $info['name']; ?></td>
                                    <td><?= $info['version']; ?></td>
                                    <td><?= $info['description']; ?></td>
                                    <td><?= $info['author']; ?></td>
                                    <td>
										<?php if (!$info['deactivated'] && !isset($info['isDownload'])) { ?>
                                            <a href="<?= ADMIN; ?>/plugins/disable?name=<?= $plugin ?>"><i
                                                        class="fa fa-fw fa-times text-red"></i></a>
										<?php } else if ($info['deactivated'] && !isset($info['isDownload'])) { ?>
                                            <a href="<?= ADMIN; ?>/plugins/enable?name=<?= $plugin ?>"><i
                                                        class="fa fa-fw fa-check text-green"></i></a>
										<?php } ?>

										<?php if (MenuGenerator::isPluginSettingsMenu($info['name-2'])): ?>
                                            <a href="<?= ADMIN; ?>/plugins/manager/<?= $info['url'] ?>"><i
                                                        class="fa fa-fw fa-eye text-white"></i></a>
										<?php endif; ?>

										<?php if (isset($info['isUpdate']) && $info['isUpdate'] && !isset($info['isDownload'])): ?>
                                            <a href="<?= ADMIN; ?>/plugins/update?name=<?= $plugin ?>"><i
                                                        class="fa fa-fw fa-sync-alt text-indigo"></i></a>
										<?php endif; ?>

										<?php if (isset($info['isDownload']) && $info['isDownload']): ?>
                                            <a href="<?= ADMIN; ?>/plugins/download?name=<?= $plugin ?>"><i
                                                        class="fa fa-fw fa-download text-black"></i></a>
										<?php else: ?>
                                            <a href="<?= ADMIN; ?>/plugins/delete?name=<?= $plugin ?>"><i
                                                        class="fa fa-trash-alt text-red"></i></a>
										<?php endif; ?>
                                    </td>
                                </tr>
							<?php endforeach; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th><?= __('Key') ?></th>
                                <th><?= __('Version') ?></th>
                                <th><?= __('Description') ?></th>
                                <th><?= __('Author') ?></th>
                                <th><?= __('Actions') ?></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->