<script>
	const extension = "<?=getExtension($_GET['path'] ?? '')?>",
		theme = "<?=(isset($app['dark_mode'])) ? "dracula" : "eclipse";?>"
</script>
<style>
    .CodeMirror {
        width: 100%;
        min-height: 60vh;
        height: calc(100vh - 300px);
        border: 1px solid #ddd;
        box-sizing: border-box;
        background-color: #eee;
    }
</style>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="50px" class="text-center"><?= __('Type', 'explorer') ?></th>
                                <th class="text-center"><?= __('Key', 'explorer') ?></th>
                                <th width="150px" class="text-center"><?= __('Size', 'explorer') ?></th>
                            </tr>
                            </thead>

                            <tbody>
							<?php if (!empty($_GET['path'])): ?>
                                <tr style="cursor:pointer"
									onclick="document.location.href = '<?php echo ADMIN . "/module/explorer?path=$explorer[back]" ?>'">
                                    <td></td>
                                    <td>
										<a href="<?php echo ADMIN . "/module/explorer?path=$explorer[back]" ?>">...</a>
                                    </td>
                                    <td></td>
                                </tr>
							<?php endif; ?>
							<?php if (!empty($files)) : foreach ($files as $item): ?>
                                <tr style="cursor:pointer"
									onclick="document.location.href = '<?php echo ADMIN . "/module/explorer?path=$item[path]" ?>'">
                                    <td class="text-center">
										<i class="fa fa-<?php echo (is_dir("$path/$item[file]")) ? 'folder' : 'file'; ?>"></i>
                                    </td>
                                    <td>
										<a href="<?php echo ADMIN . "/module/explorer?path=$item[path]" ?>">
		                                    <?= $item['file']; ?>
                                        </a>
                                    </td>
                                    <td class="text-center">
	                                    <?= $item['size']; ?>
                                    </td>
                                </tr>
							<?php endforeach; endif; ?>
                            </tbody>
                        </table>
                    </div>
					<?php if (is_file($path)) {
						if (getimagesize($path)) {
							$path = str_replace(ROOT, "", $path);
							echo "<p class='text-center bg-gray-light'>";
							echo "<img style='width:30%; padding:5px;' src='" . PATH . mb_substr($path, 7) . "' alt='" . basename($path) . "' /><br />";
							echo "<span>" . basename($path) . "</span>";
							echo "</p>";
						} else { ?>
                            <textarea id="code" cols="120" rows="30"><?= file_get_contents($path) ?></textarea>
						<?php }
					} ?>

                </div>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->
