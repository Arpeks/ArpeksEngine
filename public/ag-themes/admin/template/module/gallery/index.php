<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover dataTable"
                               data-page-length='<?= $app['pagination'] ?>'>
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th><?= __('Key', 'gallery') ?></th>
                                <th><?= __('Description', 'gallery') ?></th>
                                <th>IMG</th>
                                <th><?= __('Actions', 'gallery') ?></th>
                            </tr>
                            </thead>
                            <tbody>
							<?php foreach ($gallery as $item): ?>
                                <tr>
                                    <td><?= $item->id; ?></td>
                                    <td><?= $item->title; ?></td>
                                    <td><?= substring($item->description); ?></td>
                                    <td>
                                        <img src="<?= PATH ?>/ag-themes/<?= $app['layout'] ?>/img/gallery/<?= $item->img ?>"
                                             alt="image-gallery-<?= $item->id; ?>" height="100px"/></td>
                                    <td>
                                        <a href="<?= ADMIN; ?>/module/gallery/edit?id=<?= $item->id; ?>"><i
                                                    class="fa fa-fw fa-eye"></i></a>
                                        <a href="<?= ADMIN; ?>/module/gallery/delete?id=<?= $item->id; ?>"
                                           class="delete"><i
                                                    class="fa fa-trash-alt text-red"></i></a>
                                    </td>
                                </tr>
							<?php endforeach; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th><?= __('Key', 'gallery') ?></th>
                                <th><?= __('Description', 'gallery') ?></th>
                                <th>IMG</th>
                                <th><?= __('Actions', 'gallery') ?></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->