<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <form action="<?= ADMIN; ?>/module/gallery/edit" method="post" data-toggle="validator">
                    <div class="card-body">
                        <div class="form-group has-feedback">
                            <label for="name"><?= __('Title', 'gallery') ?></label>
                            <input type="text" class="form-control" name="title" id="name" required
                                   value="<?= h($gallery->title); ?>">
                        </div>

                        <div class="form-group has-feedback">
                            <label for="description"><?= __('Description', 'gallery') ?></label>
                            <textarea name="description" id="description"
                                      class="form-control"><?= $gallery->description ?></textarea>
                        </div>

                        <div class="form-group">
                            <div class="card card-danger card-solid file-upload">
                                <div class="card-header">
                                    <h3 class="box-title"><?= __('Image', 'gallery') ?></h3>
                                </div>
                                <div class="card-body">
                                    <div class="single">
                                        <img src="<?= PATH ?>/ag-themes/<?= $app['layout'] ?>/img/gallery/<?= $gallery->img ?>"
                                             alt="" style="max-height: 150px;">
                                    </div>
                                    <div id="single" data-name="single" data-url="/module/gallery/add-image"
                                         class="btn btn-success mt-2">Choose file
                                    </div>
                                    <p><small><?= __('Recommended size', 'gallery') ?>: 1280x720</small></p>
                                </div>
                                <div class="overlay" style="display: none">
                                    <i class="fas fa-sync-alt fa-3x fa-spin"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <input type="hidden" name="id" value="<?= $gallery->id; ?>">
                        <input type="hidden" name="secret_key" value="<?= SECRET_KEY ?>"/>
                        <button type="submit" class="btn btn-primary"><?= __('Save', 'gallery') ?></button>
                        <a class="btn btn-danger delete"
                           href="<?= ADMIN ?>/module/gallery/delete?id=<?= $gallery->id; ?>"><?= __('Remove', 'gallery') ?></a>
                    </div>
                </form>
            </div>

        </div>
    </div>

</section>
<!-- /.content -->