<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered dataTable" data-page-length='<?= $app['pagination'] ?>'>
                            <thead>
                            <tr>
                                <th><?= __('Key') ?></th>
                                <th><?= __('Description') ?></th>
                                <th><?= __('Author') ?></th>
                                <th><?= __('Actions') ?></th>
                            </tr>
                            </thead>
                            <tbody>
							<?php foreach ($moduleInfo as $module => $info): ?>
                                <tr <?php if ($info['active']) {
									echo 'class="bg-success"';
								} ?>>
                                    <td><?= $module; ?></td>
                                    <td><?= $info['description']; ?></td>
                                    <td><?= $info['author']; ?></td>
                                    <td>
										<?php if ($info['active']) { ?>
                                            <a href="<?= ADMIN; ?>/module/<?= mb_strtolower($module) ?>/disable?name=<?= $module ?>"><i
                                                        class="fa fa-fw fa-times text-red"></i></a>
										<?php } else { ?>
                                            <a href="<?= ADMIN; ?>/module/<?= mb_strtolower($module) ?>/enable?name=<?= $module ?>"><i
                                                        class="fa fa-fw fa-check text-green"></i></a>
										<?php } ?>
                                        |
                                        <a href="<?= ADMIN; ?>/module/<?= mb_strtolower($module) ?>/remove?name=<?= $module ?>"
                                           data-placement="auto"
                                           data-toggle="tooltip" title="Удалить модуль"
                                           class="delete"><i class="fa fa-trash-alt text-red"></i></a>
                                    </td>
                                </tr>
							<?php endforeach; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th><?= __('Key') ?></th>
                                <th><?= __('Description') ?></th>
                                <th><?= __('Author') ?></th>
                                <th><?= __('Actions') ?></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->