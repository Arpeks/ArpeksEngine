<!-- Main content -->
<section class="content">
    <div class="row">
		<?php $mailWidget->render(); ?>
        <div class="col-md-9">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title"><?= __('Read message', 'mailbox') ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="card-body no-padding">
                    <div class="mailbox-read-info">
                        <h3><b><?= $message->subject ?></b></h3>
                        <h5><?php if ($message->is_to == '1'): echo __('To', 'mailbox'); else: echo __('From', 'mailbox'); endif; ?>
                            : <a
                                    href="<?= ADMIN ?>/module/mailbox/send?email=<?= $message->email ?>"><?= $message->email ?></a>
                            <span class="mailbox-read-time float-right"><?= $message->time ?></span></h5>
                    </div>
                    <!-- /.mailbox-read-info -->
                    <div class="mailbox-read-message">
						<?= $message->message ?>
                    </div>
                    <!-- /.mailbox-read-message -->
                </div>
                <!-- /.box-body -->
                <div class="card-footer">
                    <form action="<?= ADMIN ?>/module/mailbox/delete" method="post">
                        <input type="hidden" value="<?= $message->id ?>" name="id">
                        <input type="hidden" name="secret_key" value="<?= SECRET_KEY ?>"/>
                        <button type="submut" name="delete" class="btn btn-danger float-right" class="delete"><i
                                    class="fa fa-trash-alt"></i> <?= __('Delete', 'mailbox') ?>
                        </button>
                    </form>
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /. box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->