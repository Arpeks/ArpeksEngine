<!-- Main content -->
<section class="content">
    <div class="row">
		<?php $mailWidget->render(); ?>
        <div class="col-md-9">
            <div class="card card-primary card-outline">
                <form action="<?= ADMIN ?>/module/mailbox/send" method="post">
                    <div class="card-header">
                        <h3 class="card-title"><?= __('Send a new message', 'mailbox') ?></h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="form-group">
                            <input class="form-control" name="to" placeholder="To:"
                                   value="<?= (isset($_GET['email'])) ? $_GET['email'] : '' ?>">
                        </div>
                        <div class="form-group">
                            <input class="form-control" name="subject" placeholder="Subject:"
                                   value="<?= (isset($_GET['subject'])) ? $_GET['subject'] : '' ?>">
                        </div>
                        <div class="form-group">
                            <textarea id="compose-textarea" name="message" class="form-control"></textarea>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="float-right">
                            <input type="hidden" name="secret_key" value="<?= SECRET_KEY ?>"/>
                            <button type="submit" class="btn btn-primary"><i
                                        class="far fa-envelope"></i> <?= __('Send', 'mailbox') ?></button>
                            <button type="reset" class="btn btn-default"><i
                                        class="fas fa-times"></i> <?= __('Discard', 'mailbox') ?></button>
                        </div>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->