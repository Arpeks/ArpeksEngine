<!-- Main content -->
<section class="content">
    <div class="row">
		<?php $mailWidget->render(); ?>
        <div class="col-md-9">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title"><?= __('List of messages', 'mailbox') ?></h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <form action="<?= ADMIN ?>/module/mailbox/delete-checkboxes" method="post">
                    <input type="hidden" name="secret_key" value="<?= SECRET_KEY ?>"/>
                    <input type="hidden" name="id" value="-2"/>

                    <div class="card-body p-0">
                        <div class="table-responsive mailbox-messages mt-3">
                            <table class="table table-hover table-striped dataTable" role="grid"
                                   data-page-length='<?= $app['pagination'] ?>'>
                                <thead>
                                <tr role="row">
                                    <th rowspan="1" colspan="1" class="text-center"></th>
                                    <th rowspan="1" colspan="1" class="text-center"><?= __('To', 'mailbox') ?></th>
                                    <th rowspan="1" colspan="1"
                                        class="text-center"><?= __('Subject', 'mailbox') ?></th>
                                    <th rowspan="1" colspan="1" class="text-center"><?= __('Time', 'mailbox') ?></th>
                                </tr>
                                </thead>
                                <tbody>
								<?php if (!empty($messages)) : foreach ($messages as $message) :
									$message->time = changeDateFormat($message->time);

									if (!$message->is_read) {
										$message->subject = "<b>" . $message->subject . "</b>";
										$message->name = "<b>" . $message->name . "</b>";
									}
									?>
                                    <tr>
                                        <td>
                                            <div class="icheck-primary">
                                                <input type="checkbox" id="check<?= $message->id ?>"
                                                       name="<?= $message->id ?>">
                                                <label for="check<?= $message->id ?>"></label>
                                            </div>
                                        </td>
                                        <td class="mailbox-name text-center"><a
                                                    href="<?= ADMIN ?>/module/mailbox/read?id=<?= $message->id ?>"><?= $message->name ?></a>
                                        </td>
                                        <td class="mailbox-subject text-center"><?= $message->subject ?></td>
                                        <td class="mailbox-date text-center"><?= $message->time ?></td>
                                    </tr>
								<?php endforeach; endif; ?>
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.mail-box-messages -->
                    </div>

                    <!-- /.box-body -->
                    <div class="card-footer p-0">
                        <div class="mailbox-controls">
                            <button type="button" class="btn btn-default btn-sm checkbox-toggle"
                            "><i class="far fa-square"></i></button>
                            <button type="submit" name="delete" class="btn btn-danger btn-sm delete"><i
                                        class="fa fa-trash-alt"></i></button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /. box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
