<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col">
			<form action="<?= ADMIN; ?>/backup/export" method="post" data-toggle="validator">
				<div class="card card-primary">
					<div class="card-body">

						<div class="container">
							<div class="row">
								<div class="col">
									<h3><?= __('Task') ?></h3>
									<hr>
								</div>
							</div>

							<div class="row">
								<div class="col-3">
					<?= __('Export') ?>...
								</div>
								<div class="col-9">
									<div class="form-check has-feedback">
										<label class="form-check-label"><input type="checkbox" checked
																													 class="form-check-input"
																													 name="backup_settings_xml" /> <?= __('XML settings') ?>
										</label><br>
										<label class="form-check-label"><input type="checkbox" checked
																													 class="form-check-input"
																													 name="backup_database" /> <?= __('Database backup') ?>
										</label><br>
										<label class="form-check-label"><input type="checkbox" checked
																													 class="form-check-input"
																													 name="backup_files" /> <?= __('Files backup') ?>
										</label><br>
										<span class="add-desc">
                                            <?= __('The file backup only copies the files in the /app/ folder, /public/ag-themes/ and the /composer.json file') ?>
                                        </span>
									</div>
								</div>
							</div>

							<div class="row mt-4">
								<div class="col">
									<h3><?= __('Options') ?></h3>
									<hr>
								</div>
							</div>

							<div class="row">
								<div class="col-3">
									<label class="form-check-label"
												 for="backup_archive_name"><?= __('Archive name') ?></label>
								</div>
								<div class="col-9">
									<div class="form-group has-feedback">
										<input type="text" value="<?= $archiveName_pattern ?>"
													 name="backup_archive_name" id="backup_archive_name"
													 class="form-control" />
										<p>
						<?= __('Result') ?>: <code
												id="archiveName"><?= changeDateFormat(getTimestamp(), $archiveName_pattern) ?></code><code
												id="archiveHash">-<?= $archiveHash ?>.</code><code
												id="archiveExt"><?= $archiveExt ?></code>
										</p>
										<p>
											<strong><?= __('Available variables') ?>:</strong>
										</p>
										<table class="table">
											<tr class="text-center">
												<th><?= __('Key') ?></th>
												<th><?= __('Russian') ?></th>
												<th><?= __('English') ?></th>
											</tr>
											<tr>
												<td class="text-center"><code>d</code></td>
												<td>Две цифры дня месяца с ведущим нулём</td>
												<td>Two digits of the day of the month with a leading zero</td>
											</tr>
											<tr>
												<td class="text-center"><code>j</code></td>
												<td>День месяца без ведущего нуля</td>
												<td>Day of the month without a leading zero</td>
											</tr>
											<tr>
												<td class="text-center"><code>m</code></td>
												<td>День месяца с ведущим нулём</td>
												<td>Day of month with leading zero</td>
											</tr>
											<tr>
												<td class="text-center"><code>n</code></td>
												<td>Представление месяца (без ведущего нуля)</td>
												<td>Month representation (no leading zero)</td>
											</tr>
											<tr>
												<td class="text-center"><code>Y</code></td>
												<td>Четыре цифры года</td>
												<td>Four digits of the year</td>
											</tr>
											<tr>
												<td class="text-center"><code>y</code></td>
												<td>Две цифры года</td>
												<td>Two digits of the year</td>
											</tr>
											<tr>
												<td class="text-center"><code>a</code></td>
												<td>Строчные буквы am и pm</td>
												<td>Lowercase letters am and pm</td>
											</tr>
											<tr>
												<td class="text-center"><code>A</code></td>
												<td>Заглавные буквы AM и PM</td>
												<td>Uppercase letters AM and PM</td>
											</tr>
											<tr>
												<td class="text-center"><code>B</code></td>
												<td colspan="2" class="text-center">Swatch Internet Time</td>
											</tr>
											<tr>
												<td class="text-center"><code>g</code></td>
												<td>12-часовой формат часов без ведущего нуля</td>
												<td>12-hour clock format without a leading zero</td>
											</tr>
											<tr>
												<td class="text-center"><code>G</code></td>
												<td>24-часовой формат часов без ведущего нуля</td>
												<td>24-hour clock format without a leading zero</td>
											</tr>
											<tr>
												<td class="text-center"><code>h</code></td>
												<td>12-часовой формат часов с ведущим нулём</td>
												<td>12-hour clock format with master zero</td>
											</tr>
											<tr>
												<td class="text-center"><code>H</code></td>
												<td>24-часовой формат часов с ведущим нулём</td>
												<td>24-hour clock format with master zero</td>
											</tr>
											<tr>
												<td class="text-center"><code>i</code></td>
												<td>Две цифры минут</td>
												<td>Two digit minutes</td>
											</tr>
											<tr>
												<td class="text-center"><code>s</code></td>
												<td>Две цифры секунд</td>
												<td>Two digit seconds</td>
											</tr>
										</table>

									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-3">
					<?= __('File format') ?>
								</div>
								<div class="col-9">
									<div class="form-check has-feedback">
										<label class="form-check-label"><input type="radio" value="zip"
																													 class="form-check-input"
																													 name="backup_archive_ext"<?= ($archiveExt == "zip") ? " checked" : ""; ?> />
											Zip</label><br>
					  <?php if (class_exists("Archive_Tar")): ?>
												<label class="form-check-label"><input type="radio" value="tar"
																															 class="form-check-input"
																															 name="backup_archive_ext"<?= ($archiveExt == "tar") ? " checked" : ""; ?> />
													Tar</label><br>
												<label class="form-check-label"><input type="radio" value="tar.gz"
																															 class="form-check-input"
																															 name="backup_archive_ext"<?= ($archiveExt == "tar.gz") ? " checked" : ""; ?> />
													Tar GZip</label>
					  <?php endif; ?>
									</div>
								</div>
							</div>

						</div>

					</div>
					<div class="card-footer">
						<div class="box-footer">
							{input_csrf}
							<input type="hidden" name="secret_key" value="<?= SECRET_KEY ?>" />
							<button type="submit"
											class="btn btn-primary float-right"><?= __('Create backup') ?></button>
						</div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<!-- /.content -->
