<!-- Main content -->
<section class="content">

	<div class="row">
		<!-- /.col -->
		<div class="col">
			<div class="card card-primary">
				<div class="card-body">
					<form action="<?= ADMIN; ?>/backup/import" method="post" data-toggle="validator">
						<div class="box-body">

							<div class="form-group">
								<div class="card card-danger card-solid file-upload">
									<div class="card-header">
										<h3 class="card-title"><?= __('Import backup') ?></h3>
									</div>
									<div class="card-body">
										<div class="importBackup"></div>
										<div id="importBackup" data-name="importBackup" data-url="/backup/import-file"
												 class="btn btn-success mt-2"><?= __('Select file') ?>
										</div>
										<p><small><?= __('Select backup file to import') ?></small></p>
									</div>
									<div class="overlay" style="display: none">
										<i class="fas fa-sync-alt fa-3x fa-spin"></i>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 col-sm-12 mx-auto input-group">
										<select id="importBackupArchive"
														class="form-control">
											<option value="null" id="nullFileImport"></option>
						<?php foreach ($archives as $archive): ?>
													<option value="<?= $archive ?>"><?= $archive ?></option>
						<?php endforeach; ?>
										</select>
										<div id="importBackupArchiveButton" data-name="importBackupArchive"
												 data-url="/backup/import-archive"
												 class="btn btn-primary ml-2"><?= __('Select archive') ?>
										</div>
									</div>
								</div>


							</div>

							<div class="table-responsive">
								<table class="table table-bordered table-hover">
									<thead>
									<tr>
										<th><?= __('Key') ?></th>
										<th><?= __('Value') ?></th>
									</tr>
									</thead>
									<tbody id="formImportBackup">
									<tr>
										<td></td>
										<td></td>
									</tr>
									</tbody>
									<tfoot>
									<tr>
										<th><?= __('Key') ?></th>
										<th><?= __('Value') ?></th>
									</tr>
									</tfoot>
								</table>
							</div>

						</div>
						<div class="box-footer">
							<input type="hidden" name="secret_key" value="<?= SECRET_KEY ?>" />
							<input type="hidden" name="archive" value="" />
							{input_csrf}
							<button type="submit" id="backup_restore" class="btn btn-primary disabled" disabled>
				  <?= __('Restore backup') ?>
							</button>
						</div>
					</form>

				</div>
			</div>
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->

</section>
<!-- /.content -->
