<?php

use app\models\User;use core\{App, Language, MenuGenerator};

$profileImageUrl = $_SESSION['user']['img'];
if (!file_exists(WWW_THEME . "/admin/img/users/" . $profileImageUrl)) {
	$profileImageUrl = "default.png";
}
$profileImageUrl = "img/users/" . $profileImageUrl;

$logoPath = PATH . "/ag-themes/admin/img/logo.png";
if (file_exists(WWW_THEME . "/" . App::$app->get('layout') . "/img/logo.png")) {
	$logoPath = PATH . "/ag-themes/" . App::$app->get('layout') . "/img/logo.png";
}

/* @var MenuGenerator $menuReg */
$menuReg = MenuGenerator::getMenu();

echo isActiveSMTP();

?><!DOCTYPE html>
<html lang="<?= Language::getLanguage(true) ?>">
<head>
    <base href="<?= PATH ?>/ag-themes/admin/">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    {meta}
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="shortcut icon" href="<?= $logoPath ?>" type="image/png">

	{styles}

	<script src="js/libs/jquery/jquery-3.7.1.min.js"></script>

    <script>
		const path = '<?=PATH?>',
			adminpath = '<?=rtrim(ADMIN, "/")?>',
			csrf = '{csrf}',
			user_id = <?=$_SESSION['user']['id'] ?? -1?>;
    </script>
</head>

<body class="hold-transition sidebar-mini layout-navbar-fixed layout-footer layout-fixed<?php if (isset($app['dark_mode'])) {
	echo " dark-mode";
} ?>">

<div class="preloader" id="preloader">
    <div class="preloader__loader">
        <div class="spinner-grow text-danger" role="status" style="width: 4rem; height: 4rem;">
            <span class="sr-only"><?= __('Loading') ?>...</span>
        </div>
    </div>
</div>

<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item">
                <span class="page-title">{site_title}</span>
            </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item mr-3">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label class="input-group-text">
                            <img src="img/flags/<?= User::getLanguage()->domain ?>.png" alt="Flag country"
                                 class="flag_country" id="flag_country">
                        </label>
                    </div>
                    <select name="language" id="select_language"
                            class="custom-select form-control">
						<?php foreach ($langList as $lang): ?>
							<option
								value="<?= $lang->domain ?>"<?= ($lang->language == Language::getLanguage(false)) ? " selected" : ""; ?>><?= $lang->name ?></option>
						<?php endforeach; ?>
                    </select>
                </div>
            </li>
	        <?php if ($isUpdate): ?>
                <li class="nav-item">
                    <span data-toggle="modal" data-target="#modal-core-update" class="nav-link c-pointer"><i
                                class="fas fa-cloud-download-alt text-primary"></i></span>
                </li>
			<?php endif; ?>
            <li class="nav-item">
                <span class="nav-link c-pointer" data-widget="fullscreen">
                    <i class="fas fa-expand-arrows-alt"></i>
                </span>
            </li>
            <li class="nav-item">
                <span data-toggle="modal" data-target="#modal-bug-report" class="nav-link c-pointer">
                    <i class="fa fa-bug text-red"></i>
                </span>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= ADMIN ?>/user/logout">
                    <i class="fas fa-sign-out-alt"></i>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="<?= ADMIN ?>" class="brand-link">
            <img src="<?= $logoPath ?>" class="brand-image img-circle" style="opacity: .8" alt="logo"/>
            <span class="brand-text font-weight-light">Arpeks Engine</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="/ag-themes/admin/<?= $profileImageUrl ?>" class="img-circle"
                         alt="User Image">
                </div>
                <div class="info">
                    <a href="<?= ADMIN ?>/profile"><?= $session['name'] ?></a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column nav-flat" data-widget="treeview" role="menu"
                    data-accordion="false">
					<?= generateAdminMenu($menu->getAll()); ?>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header"></section>

        <div class="container-fluid">
