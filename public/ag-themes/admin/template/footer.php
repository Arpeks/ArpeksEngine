<?php

use core\Cache;

?>
</div>
</div>
<!-- /.content-wrapper -->
<div class="clear"></div>
<footer class="main-footer text-center" style="background-color: #212529">
	{copyright}
</footer>
</div>
<!-- ./wrapper -->

<div class="modal fade" id="modal-bug-report" style="display: none;">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Bug Report</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span></button>
			</div>
			<div class="modal-body">
				<form id="formBugReport" data-toggle="validator" novalidate="true">
					<div class="form-group has-feedback">
						<label for="b-message"
									 class="text-center"><?= __('Tell us about the error you found and under what conditions you received it.') ?></label>
						<textarea name="message" id="b-message" class="form-control" required></textarea>
						<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
					</div>

					<div class="box-footer justify-content-between">
						{input_csrf}
						<button type="submit" id="bugReportButton" class="btn btn-danger" style="float:right;">
				<?= __('Send') ?>
						</button>
					</div>
				</form>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<?php if (!isActiveSMTP()): ?>
	<div class="modal fade" id="modal-error-SMTP-deactivated" style="display: none;">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">SMTP Deactivated</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span></button>
				</div>
				<div class="modal-body">


					<p class="text-center"><?= __('SMTP service is disabled or configured incorrectly.') ?></p>
					<p class="text-center"><?= __('Send the archive with logs manually to our e-mail address:') ?>
						<a href="mailto:my@arpeks.ru" target="_blank"
							 rel="nofollow">my@arpeks.ru</a></p>
					<span class="glyphicon form-control-feedback" aria-hidden="true"></span>

				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
<?php endif; ?>

<?php if (isset($isUpdate) && $isUpdate): $check_cms = Cache::get('cms-check-update'); ?>
	<div class="modal fade" id="modal-core-update" style="display: none;">
		<div class="modal-dialog modal-xl modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title"><i class="fas fa-cloud-download-alt"></i> <?= __('Update CMS') ?></h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span></button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-6">
							<h4>Локальная информация</h4>
							<ul>
								<li>Название: <strong><?= $check_cms['cms_info']['AE_CMS_NAME'] ?></strong></li>
								<li>Номер сборки: <strong><?= $local_version ?></strong></li>
								<li>Год: <strong><?= $check_cms['cms_info']['AE_CMS_YEAR'] ?></strong></li>
							</ul>
						</div>
						<div class="col-6">
							<h4>Информация на сервере</h4>
							<ul>
								<li>Название: <strong><?= $check_cms['cms_info']['AE_CMS_NAME'] ?></strong></li>
								<li>Номер сборки: <strong><?= $server_version ?></strong></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<div class="container-fluid">

						<div class="row">
							<div class="col-12">
								<div class="text-center">
									<form action="<?= ADMIN ?>/ajax/core-update" method="post" id="updateCoreForm">
										{input_csrf}
										<button id="updateCore_btn" type="submit" class="btn btn-primary pull-right"><i
												class="fas fa-download"></i> Обновить
										</button>
									</form>
								</div>
							</div>
						</div>

						<div class="row d-none" id="updateCore_main">
							<div class="col-12">
								<hr>
								<div class="text-center h3" id="updateCore_text">0%</div>
								<div class="progress" style="height: 15px;">
									<div id="updateCore_progress"
											 class="progress-bar progress-bar-striped progress-bar-animated bg-success"
											 role="progressbar" aria-valuemin="0" aria-valuemax="100"
											 style="width: 0%"></div>
								</div>
								<pre class="mt-3" style="height: 190px; border: 1px solid #000;"
										 id="updateCore_logs"></pre>
							</div>
						</div>

					</div>
				</div>

			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
<?php endif; ?>

{scripts}

<script>
	$.widget.bridge('uibutton', $.ui.button)
</script>

<?php if (isset($_SESSION['success'])): ?>
	<script type="module">
		import { notifySweetAlert } from './js/functions.js'

		notifySweetAlert('<?=$_SESSION['success']?>', 'success')
	</script>
	<?php unset($_SESSION['success']); ?>
<?php endif; ?>

<?php if (isset($_SESSION['error'])): ?>
	<script type="module">
		import { notifySweetAlert } from './js/functions.js'

		notifySweetAlert('<?=$_SESSION['error']?>', 'error')
	</script>
	<?php unset($_SESSION['error']);
endif; ?>

</body>
</html>
