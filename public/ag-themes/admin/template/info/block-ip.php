<?php ?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="card card-primary card-outline card-outline-tabs">
                <div class="card-body table-responsive">
                    <table class="table table-bordered table-hover dataTable" role="grid"
                           data-page-length='<?= $app['pagination'] ?>'>
                        <thead>
                        <tr role="row">
                            <th rowspan="1" colspan="1" class="text-center"><?= __('Time') ?></th>
                            <th rowspan="1" colspan="1" class="text-center">IP</th>
                        </tr>
                        </thead>
                        <tbody>
						<?php foreach ($errors as $id => $error): ?>
                            <tr role="row" style="cursor: pointer" data-toggle="modal" data-target="#modal-<?= $id ?>">
                                <td style="vertical-align: middle;text-align: center;"><?= $error['time'] ?></td>
                                <td style="vertical-align: middle;text-align: center;"><a target='_blank'
                                                                                          href='https://cleantalk.org/ru/blacklists/<?= $error['ip'] ?>'><?= $error['ip'] ?></a>
                                </td>
                            </tr>
						<?php endforeach; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th rowspan="1" colspan="1" class="text-center"><?= __('Time') ?></th>
                            <th rowspan="1" colspan="1" class="text-center">IP</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <p class="text-right">
                        <span class="btn btn-danger c-pointer" id="cacheClear"
                              data-success="<?= __('Cleared success') ?>"
                              data-error="<?= __('Cleared error') ?>"
                              data-url="<?= ADMIN; ?>/ajax/cache-clear" data-key="cms_error_log_ip">
                            <i class="fa fa-fw fa-times text-white"></i> <?= __('Clear') ?>
                        </span>
                    </p>
                </div>
            </div>

        </div>
    </div>

	<?php foreach ($errors as $id => $error): unset($error['text']); ?>
        <div class="modal fade" id="modal-<?= $id ?>" style="display: none;">
            <div class="modal-dialog modal-xl modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Error: <?= $id + 1 ?></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
						<?php debug($error, false); ?>
                    </div>
                </div>
            </div>
        </div>
	<?php endforeach; ?>

</section>
<!-- /.content -->
