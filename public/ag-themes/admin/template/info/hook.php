<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline card-outline-tabs">
                <div class="card-header p-0 border-bottom-0">
                    <ul class="nav nav-tabs" id="hook-filter" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hook-tab" data-toggle="pill" href="#hook" role="tab"
                               aria-controls="hook" aria-selected="true">Хуки</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="filter-tab" data-toggle="pill" href="#filter" role="tab"
                               aria-controls="filter" aria-selected="false">Фильтры</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    <div class="tab-content table-responsive" id="hook-filter-tabContent">
                        <div class="tab-pane fade active show" id="hook" role="tabpanel" aria-labelledby="hook-tab">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="hook" class="table table-bordered table-hover dataTable" role="grid"
                                           data-page-length='<?= $app['pagination'] ?>'>
                                        <thead>
                                        <tr role="row">
                                            <th rowspan="1" colspan="1"
                                                style="vertical-align: middle;text-align: center;"><?= __('Key') ?></th>
                                            <th rowspan="1" colspan="1"
                                                style="vertical-align: middle;text-align: center;"><?= __('Description') ?></th>
                                            <th rowspan="1" colspan="1"
                                                style="vertical-align: middle;text-align: center;"><?= __('Params') ?></th>
                                            <th rowspan="1" colspan="1"
                                                style="vertical-align: middle;text-align: center;"><?= __('File') ?></th>
                                            <th rowspan="1" colspan="1"
                                                style="vertical-align: middle;text-align: center;"><?= __('Engine version') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
										<?php foreach ($hookList['hook'] as $hookName => $hookParam): $params = print_r($hookParam['params'], true); ?>
                                            <tr role="row" class="bg-red-light">
                                                <td><?= $hookName ?></td>
                                                <td><?= $hookParam['description'] ?></td>
                                                <td>
                                                    <pre><?= $params ?></pre>
                                                </td>
                                                <td><?= $hookParam['file'] ?></td>
                                                <td><?= $hookParam['engine_version'] ?></td>
                                            </tr>
										<?php endforeach; ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th rowspan="1" colspan="1"
                                                style="vertical-align: middle;text-align: center;"><?= __('Key') ?></th>
                                            <th rowspan="1" colspan="1"
                                                style="vertical-align: middle;text-align: center;"><?= __('Description') ?></th>
                                            <th rowspan="1" colspan="1"
                                                style="vertical-align: middle;text-align: center;"><?= __('Params') ?></th>
                                            <th rowspan="1" colspan="1"
                                                style="vertical-align: middle;text-align: center;"><?= __('File') ?></th>
                                            <th rowspan="1" colspan="1"
                                                style="vertical-align: middle;text-align: center;"><?= __('Engine version') ?></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="filter" role="tabpanel" aria-labelledby="filter-tab">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="filter" class="table table-bordered table-hover dataTable" role="grid"
                                           data-page-length='<?= $app['pagination'] ?>'>
                                        <thead>
                                        <tr role="row">
                                            <th rowspan="1" colspan="1"
                                                style="vertical-align: middle;text-align: center;"><?= __('Key') ?></th>
                                            <th rowspan="1" colspan="1"
                                                style="vertical-align: middle;text-align: center;"><?= __('Description') ?></th>
                                            <th rowspan="1" colspan="1"
                                                style="vertical-align: middle;text-align: center;"><?= __('Params') ?></th>
                                            <th rowspan="1" colspan="1"
                                                style="vertical-align: middle;text-align: center;"><?= __('File') ?></th>
                                            <th rowspan="1" colspan="1"
                                                style="vertical-align: middle;text-align: center;"><?= __('Engine version') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
										<?php foreach ($hookList['filter'] as $hookName => $hookParam): $params = print_r($hookParam['params'], true); ?>
                                            <tr role="row" class="bg-red-light">
                                                <td><?= $hookName ?></td>
                                                <td><?= $hookParam['description'] ?></td>
                                                <td>
                                                    <pre><?= $params ?></pre>
                                                </td>
                                                <td><?= $hookParam['file'] ?></td>
                                                <td><?= $hookParam['engine_version'] ?></td>
                                            </tr>
										<?php endforeach; ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th rowspan="1" colspan="1"
                                                style="vertical-align: middle;text-align: center;"><?= __('Key') ?></th>
                                            <th rowspan="1" colspan="1"
                                                style="vertical-align: middle;text-align: center;"><?= __('Description') ?></th>
                                            <th rowspan="1" colspan="1"
                                                style="vertical-align: middle;text-align: center;"><?= __('Params') ?></th>
                                            <th rowspan="1" colspan="1"
                                                style="vertical-align: middle;text-align: center;"><?= __('File') ?></th>
                                            <th rowspan="1" colspan="1"
                                                style="vertical-align: middle;text-align: center;"><?= __('Engine version') ?></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->