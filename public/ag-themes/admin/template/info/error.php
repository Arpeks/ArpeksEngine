<?php

use app\controllers\admin\AppController;

$isExplorer = AppController::isModuleManagerActive("Explorer");

if ($isExplorer) {
	$url = ADMIN . "/module/explorer?path=";
}

?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="card card-primary card-outline card-outline-tabs">
                <div class="card-body table-responsive">
                    <table class="table table-bordered table-hover dataTable" role="grid"
                           data-page-length='<?= $app['pagination'] ?>'>
                        <thead>
                        <tr role="row">
                            <th rowspan="1" colspan="1" class="text-center"><?= __('Time') ?></th>
                            <th rowspan="1" colspan="1" class="text-center"><?= __('Code') ?></th>
                            <th rowspan="1" colspan="1" class="text-center"><?= __('Name') ?></th>
                            <th rowspan="1" colspan="1" class="text-center">IP</th>
                            <th rowspan="1" colspan="1" class="text-center"><?= __('Text') ?></th>
                            <th rowspan="1" colspan="1" class="text-center"><?= __('File') ?></th>
                        </tr>
                        </thead>
                        <tbody>
						<?php foreach ($errors as $id => $error):
							$cnt = count($errors) - $id;

							if ($isExplorer) {
								$path = "";
								$p = false;
								foreach (preg_split("#[\\\/]#", $error['file']) as $v) {
									if ($p || in_array($v, ['core', 'tmp', 'public', 'config', 'app', 'vendor'])) {
										$path .= $v . '/';
										$p = true;
									}
								}
								$path = trim($path, '\\/');
							}

							?>
                            <tr style="cursor: pointer" data-toggle="modal" data-target="#modal-<?= $cnt ?>">
                                <td style="vertical-align: middle;text-align: center;"><?= $error['time'] ?></td>
                                <td style="vertical-align: middle;text-align: center;"><?= $error['code'] ?></td>
                                <td style="vertical-align: middle;text-align: center;"><?= $error['error_name'] ?></td>
                                <td style="vertical-align: middle;text-align: center;"><?= $error['ip'] ?></td>
                                <td><?= $error['text'] ?></td>
                                <td style="vertical-align: middle;text-align: center;"><?php if ($isExplorer) { ?><a
                                        href="<?= $url . $path ?>" target="_blank"><?= $path ?></a><?php } else {
										echo $error['file'];
									} ?> (<?= $error['line'] ?>)
                                </td>
                            </tr>
						<?php endforeach; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th rowspan="1" colspan="1" class="text-center"><?= __('Time') ?></th>
                            <th rowspan="1" colspan="1" class="text-center"><?= __('Code') ?></th>
                            <th rowspan="1" colspan="1" class="text-center"><?= __('Name') ?></th>
                            <th rowspan="1" colspan="1" class="text-center">IP</th>
                            <th rowspan="1" colspan="1" class="text-center"><?= __('Text') ?></th>
                            <th rowspan="1" colspan="1" class="text-center"><?= __('File') ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <p class="text-right">
                        <span class="btn btn-danger c-pointer" id="cacheClear"
                              data-success="<?= __('Cleared success') ?>"
                              data-error="<?= __('Cleared error') ?>"
                              data-url="<?= ADMIN; ?>/ajax/cache-clear" data-key="cms_error_log">
                            <i class="fa fa-fw fa-times text-white"></i> <?= __('Clear') ?>
                        </span>
                    </p>
                </div>
            </div>

        </div>
    </div>


	<?php foreach ($errors as $id => $error): $cnt = count($errors) - $id; ?>
        <div class="modal fade" id="modal-<?= $cnt ?>" style="display: none;">
            <div class="modal-dialog modal-xl modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Error: <?= $cnt ?></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
						<?php debug($error, false); ?>
                    </div>
                </div>
            </div>
        </div>
	<?php endforeach; ?>

</section>
<!-- /.content -->
