<style>
    .timeline ul {
        list-style: none;
        padding-left: 5px;
    }

    .timeline ul li:before {
        content: "-";
        position: relative;
        left: -5px;
    }
</style>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- The timeline -->
            <div class="timeline">
				<?php foreach ($historyList as $history): ?>
                    <!-- timeline item -->
                    <div>
						<?php
						$color = createColor($history['build_num']);
						?>
                        <span class="fas"
                              style="background-color: #<?= $color; ?>; color: #<?=getTextColor($color)?>;"><?= $history['build_num'] ?></span>
                        <div class="timeline-item">
                            <h3 class="timeline-header"><i class="fas fa-clock"></i> <?= $history['date'] ?></h3>
                            <div class="timeline-body"><?= $history['description'] ?></div>
                        </div>
                    </div>
                    <!-- END timeline item -->
				<?php endforeach; ?>
                <div>
                    <i class="fas fa-clock bg-gray"></i>
                    <div class="timeline-item">
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->
