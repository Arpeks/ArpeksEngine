import { notifySweetAlert, sendRequest } from '../functions.js'

const textThemeEditorSave = () => {
	sendRequest(
		adminpath + '/settings/theme-editor',
		{
			text: document.querySelector('.CodeMirror').CodeMirror.getValue(),
			file: localStorage['codeMirrorFile'],
			csrf
		},
		res => {
			let tmp = JSON.parse(res)
			notifySweetAlert(tmp.response, tmp.type)
		}
	)
}

const themeEditorButton = document.getElementById('themeEditorSaveButton')
if (themeEditorButton !== null) {
	CodeMirror.modeURL = '../admin/vendor/almasaeed2010/adminlte/plugins/codemirror/mode/%N/%N.js'
	const editor = CodeMirror.fromTextArea(document.getElementById('code'), {
			continuousScanning: 500,
			lineWrapping: true,
			lineNumbers: true,
			lineWiseCopyCut: true,
			autoCloseBrackets: true,
			autoCloseTags: true,
			matchBrackets: true,
			continueComments: 'Enter',
			styleActiveLine: true,
			inputStyle: 'textarea',
			theme: theme,
			matchTags: {
				bothTags: true
			}
		}),
		getMode = CodeMirror.findModeByExtension(extension)

	editor.setOption('mode', getMode.mode)
	CodeMirror.autoLoadMode(editor, getMode.mode)

	setTimeout(() => {
		sendRequest(adminpath + '/ajax/get-content-theme', { file: filepath, csrf }, res => {
			const text = JSON.parse(res)[0]
			editor.setValue(text)
			localStorage['codeMirrorFile'] = get
		})
	}, 500)

	themeEditorButton.addEventListener('click', () => textThemeEditorSave())

	document.addEventListener('keydown', e => {
		if ((e.ctrlKey || e.metaKey) && e.key === 's') {
			e.preventDefault()

			if (
				document.activeElement ===
				document.getElementById('code').parentNode.children[1].children[0].children[0]
			) {
				textThemeEditorSave()
			}
		}
	})
}

const phoneCountyCode = document.getElementById('phone_country_code')
if (phoneCountyCode !== null) {
	phoneCountyCode.addEventListener('change', event => {
		const value = event.currentTarget.value
		document.getElementById('phone_country_code_flag').setAttribute('src', 'img/flags/' + value + '.png')
	})
}
