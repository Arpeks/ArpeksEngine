import { fadeOut, isNumeric, notifySweetAlert, sendRequest } from '../functions.js'

let ajaxConfirm = true
const chatMessage = document.getElementById('chat-message')

document.addEventListener('DOMContentLoaded', () => {
	sendRequest(adminpath + '/ajax/get-info', {}, res => {
		const json = JSON.parse(res)

		for (const key in json) {
			if (json.hasOwnProperty(key)) {
				const elementsWithClassK = document.querySelectorAll('.count-' + key + ' h3, .menu-count-' + key)
				elementsWithClassK.forEach(function (element) {
					element.textContent = json[key]
				})
			}
		}
	})

	const table = document.getElementById('table-checksys')
	const cells = table.querySelectorAll('tbody tr td:nth-child(2)')

	cells.forEach(cell => {
		let text = cell.innerHTML.trim()
		text = text.replace(/On/g, "<span class='text-green'>On</span>")
		text = text.replace(/Off/g, "<span class='text-red'>Off</span>")
		text = text.replace(/Undefined/g, "<span class='text-red'>Undefined</span>")
		text = text.replace(/Enabled/g, "<span class='text-green'>Enabled</span>")
		text = text.replace(/Disabled/g, "<span class='text-red'>Disabled</span>")
		cell.innerHTML = text
	})
})

localStorage['admin-chat'] = 0
let counter = 0
let adminChatUpdate
setInterval(
	(adminChatUpdate = () => {
		const obj = new URLSearchParams({
			last_id: localStorage['admin-chat'],
			csrf
		})

		sendRequest(adminpath + '/ajax/get-chat', obj.toString(), res => {
			const json = JSON.parse(res)

			for (let key in json) {
				if (!isNumeric(key)) continue

				let tmp = json[key],
					isOnline = tmp.user.online === '1' ? 'on' : 'off'

				if (counter <= key) {
					document.querySelectorAll('#chat-box img.user-img-check').forEach(element => {
						element.classList.remove('offline', 'online')
						element.classList.add(isOnline + 'line')
					})
				}

				if (tmp.isNew) {
					counter++
					const isCurrentUser = parseInt(tmp.user_id) === user_id
					const alignmentClass = isCurrentUser ? 'float-left' : 'float-right'

					const tmpHtml = `
<div class="direct-chat-msg${isCurrentUser ? '' : ' right'}">
  <div class="direct-chat-infos clearfix">
	<span class="direct-chat-name ${alignmentClass}">${tmp.user.name}</span>
	<span class="direct-chat-timestamp ${alignmentClass}"> (${tmp.time})</span>
  </div>
  <img class="direct-chat-img ${isOnline}line user-img-check" src="img/users/${tmp.user.img}" alt="message user image">
  <div class="direct-chat-text"><span>${tmp.message}</span></div>
</div>
`
					document.getElementById('chat-box').insertAdjacentHTML('beforeend', tmpHtml)
					const block = document.getElementById('chat-box')
					block.scrollTop = block.scrollHeight
				}
			}
			localStorage['admin-chat'] = counter

			let lines = document.querySelectorAll('#chat-box > *')
			if (lines.length > json.count_message) {
				for (let i = 0; i < lines.length - json.count_message; i++) lines[i].remove()
			}
		})
	}),
	5000
)
adminChatUpdate()

chatMessage.addEventListener('keyup', event => {
	if (
		event.currentTarget.value.trim() !== '' &&
		event.currentTarget === document.activeElement &&
		event.key === 'Enter'
	) {
		sendMessage()
	}
})

function sendMessage() {
	const message = chatMessage.value,
		user_id = chatMessage.dataset.id

	sendRequest(
		adminpath + '/ajax/send-chat',
		{
			message,
			user_id,
			csrf
		},
		function () {
			chatMessage.value = ''
			chatMessage.focus()
		}
	)
}

const updateListener = () => {
	document.querySelectorAll('.setTodoCheck').forEach(th => {
		const id = th.dataset.id,
			is_close = th.checked

		th.addEventListener('click', () => {
			sendRequest(adminpath + '/ajax/todo-set', { id, is_close, csrf }, () => {})
		})
	})

	document.querySelectorAll('.tools .fa-edit').forEach(th => {
		const id = th.dataset.id,
			taskTodoText = document.getElementById('editTaskTodoText')

		th.addEventListener('click', () => {
			taskTodoText.value = document.getElementById('textTodo' + id).textContent
			taskTodoText.dataset.id = id
			document.querySelector('#taskEditModalTitle .idTaskEdit').textContent = id
		})
	})

	document.querySelectorAll('.tools .fa-trash').forEach(th => {
		const id = th.dataset.id

		th.addEventListener('click', () => {
			const tmp = th.parentElement.parentElement.parentElement.parentElement
			sendRequest(adminpath + '/ajax/todo-delete', { id, csrf }, () => {
				tmp.classList.add('done')
				fadeOut(tmp, 1000)
				setTimeout(function () {
					tmp.style.display = 'none'
				}, 750)
			})
		})
	})
}

document.getElementById('noticeTextButton').addEventListener('click', () => {
	textNoticeSave()
})

const textNoticeSave = () => {
	const textarea = document.getElementById('noticeText')

	sendRequest(
		adminpath + '/ajax/send-notice',
		{
			notice: textarea.value,
			csrf
		},
		res => {
			const json = JSON.parse(res)
			if (json.success) {
				notifySweetAlert('Запись сохранена', 'success')
			} else {
				notifySweetAlert('Ошибка при сохранении записи', 'error')
			}
		}
	)
}

document.getElementById('chat-send').addEventListener('click', () => {
	sendMessage()
})

document.getElementById('addTaskButton').addEventListener('click', () => {
	const task = document.getElementById('taskTodoText').value,
		modal = document.getElementById('addTodoModal')

	if (task !== '' && ajaxConfirm) {
		ajaxConfirm = false

		sendRequest(adminpath + '/ajax/todo-add', { task, csrf }, res => {
			if (!isNumeric(res)) {
				notifySweetAlert(res.error, 'error')
				ajaxConfirm = true
				return false
			}

			const id = res,
				html = `
	<li>
    <div class="row">
      <div class="col-1">
        <div class="icheck-primary d-inline ml-1">
          <input type="checkbox" class="setTodoCheck" data-id="${id}" name="todo${id}" id="todoCheck${id}" />
          <label for="todoCheck${id}"></label>
        </div>
      </div>
      <div class="col-8">
        <span id="textTodo${id}" class="text">${task}</span>
      </div>
      <div class="col-1">
        <small class="badge badge-danger"><i class="far fa-clock"></i> 0 min</small>
      </div>
      <div class="col-2">
        <div class="tools">
          <i class="fas fa-edit" data-toggle="modal" data-target="#editTodoModal" data-id="${id}"></i>
          <i class="fas fa-trash fa-delete-todo" data-id="${id}"></i>
        </div>
      </div>
    </div>
  </li>`

			document.getElementById('taskTodoText').value = ''
			document.querySelector('.todo-list').insertAdjacentHTML('beforeend', html)
			document.getElementById('card-todo').scrollTop = document.getElementById('card-todo').scrollHeight

			$(modal).modal('hide')
			ajaxConfirm = true
			updateListener()
		})
	}
	return false
})

document.getElementById('editTaskButton').addEventListener('click', () => {
	const tmp = document.getElementById('editTaskTodoText'),
		id = tmp.dataset.id,
		task = tmp.value

	sendRequest(
		adminpath + '/ajax/todo-edit',
		{
			csrf,
			id,
			task
		},
		res => {
			ajaxConfirm = false
			res = JSON.parse(res)

			if (typeof res.text !== 'undefined') {
				document.getElementById('textTodo' + id).textContent = res.text
				$('#editTodoModal').modal('hide')
				ajaxConfirm = true
				return true
			}

			if (!isNumeric(res)) {
				notifySweetAlert(res.error, 'error')
				ajaxConfirm = true
				return false
			}

			if (typeof res.error === 'undefined') {
				notifySweetAlert(res.error, 'error')
				ajaxConfirm = true
				return false
			}
		}
	)

	return false
})

document.addEventListener('keydown', e => {
	if ((e.ctrlKey || e.metaKey) && e.key === 's') {
		e.preventDefault()

		if (document.activeElement === document.getElementById('noticeText')) textNoticeSave()
	}
})

document.getElementById('restartScan').addEventListener('click', event => {
	const takeSnapshotButton = document.getElementById('takeSnapshot')
	const restartScanButton = document.getElementById('restartScan')

	takeSnapshotButton.classList.add('disabled')
	takeSnapshotButton.setAttribute('disabled', 'true')

	restartScanButton.classList.add('disabled')
	restartScanButton.setAttribute('disabled', 'true')

	Scan(event.currentTarget)
})

document.getElementById('scanButton').addEventListener('click', event => {
	Scan(event.currentTarget)
})

document.getElementById('takeSnapshot').addEventListener('click', event => {
	TakeSnapshot(event.currentTarget)
})

const TakeSnapshot = th => {
	const url = th.dataset.url,
		text = th.dataset.text,
		success = th.dataset.success,
		key = th.dataset.id

	Sweetalert2.fire({
		title: 'Confirm',
		text: text,
		icon: 'question',
		showCancelButton: true,
		cancelButtonText: 'No',
		confirmButtonText: 'Yes'
	}).then(result => {
		if (result.value) {
			const takeSnapshot = document.getElementById('takeSnapshot')
			takeSnapshot.classList.add('disabled')
			takeSnapshot.setAttribute('disabled', 'true')

			sendRequest(url, { csrf }, res => {
				const json = JSON.parse(res)

				if (json.isSuccess) {
					if ($.fn.dataTable.isDataTable('#dataTableScan')) {
						let scanDataTable = $('#dataTableScan').DataTable()
						scanDataTable.clear()
					} else {
						let scanDataTable = $('#dataTableScan').DataTable({
							ordering: false,
							paging: false,
							autoWidth: true,
							stripeClasses: false,
							searching: false,
							info: false,
							scrollCollapse: true,
							scrollY: '430px'
						})
						if (typeof dataTableLanguage !== 'undefined') scanDataTable.language = dataTableLanguage
						scanDataTable.clear()
					}

					const restartScan = document.getElementById('restartScan')
					restartScan.classList.add('disabled')
					restartScan.setAttribute('disabled', 'true')

					notifySweetAlert(success, 'success')
					$(key).modal('hide')
				} else {
					notifySweetAlert(json.error, 'error')
				}
			})
		}
	})
}

const Scan = th => {
	const url = th.dataset.url,
		key = th.dataset.id

	$(key).modal('show')
	document.getElementById('scan_start').style.display = 'block'

	sendRequest(url, { csrf }, res => {
		const json = JSON.parse(res)

		let antiDT = $('#dataTableScan').DataTable({
			ordering: false,
			paging: false,
			autoWidth: true,
			stripeClasses: false,
			searching: false,
			info: false,
			scrollCollapse: true,
			scrollY: '430px',
			retrieve: true
		})

		if (typeof dataTableLanguage !== 'undefined') antiDT.language = dataTableLanguage

		antiDT.clear().draw(false)
		json.forEach(data => {
			antiDT.row.add([data.filename, data.fileSize, data.fileDate, data.type]).draw(false)
		})

		const takeSnapshot = document.getElementById('takeSnapshot')
		const restartScan = document.getElementById('restartScan')

		const takeSnapshotButton = document.getElementById('takeSnapshot')
		if (json.length > 0) {
			takeSnapshotButton.classList.remove('disabled')
			takeSnapshotButton.disabled = false
		} else {
			takeSnapshotButton.classList.add('disabled')
			takeSnapshotButton.disabled = true
		}

		if (json.length > 0) {
			takeSnapshot.classList.remove('disabled')
			takeSnapshot.removeAttribute('disabled')
		} else {
			takeSnapshot.classList.add('disabled')
			takeSnapshot.setAttribute('disabled', 'true')
		}

		restartScan.classList.remove('disabled')
		restartScan.removeAttribute('disabled')
		document.getElementById('scan_start').style.display = 'none'
	})
}

updateListener()
