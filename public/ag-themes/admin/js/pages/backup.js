import { notifySweetAlert, sendRequest } from '../functions.js'

const backupChangeName = () => {
	let name = document.getElementById('backup_archive_name').value,
		ext = document.querySelector('input[name="backup_archive_ext"]:checked').value

	sendRequest(adminpath + '/ajax/backup-archive', { name, ext, csrf }, res => {
		const json = JSON.parse(res)

		document.getElementById('archiveName').textContent = json.name
		document.getElementById('archiveHash').textContent = '-' + json.hash + '.'
		document.getElementById('archiveExt').textContent = json.ext
	})
}

const backupArchiveName = document.getElementById('backup_archive_name')
if (backupArchiveName !== null) {
	backupArchiveName.addEventListener('focusout', () => {
		backupChangeName()
	})
	backupArchiveName.addEventListener('input', () => {
		backupChangeName()
	})
}

const importBackupArchive = document.getElementById('importBackupArchiveButton')
if (importBackupArchive !== null) {
	importBackupArchive.addEventListener('click', event => {
		const name = event.currentTarget.dataset.name,
			selected = document.querySelector('#' + name + ' option:checked')

		sendRequest(
			adminpath + event.currentTarget.dataset.url,
			{ name: selected.value, upload: true, csrf },
			response => {
				setTimeout(() => {
					response = JSON.parse(response)
					const listBackup = response.backup

					let html = ''
					for (let item in listBackup) {
						html += `
    <tr>
        <td>
            <label class="font-weight-normal" for="${listBackup[item].name}">
                ${listBackup[item].text}
            </label>
        </td>
        <td>
            <div class="form-check has-feedback">
                <input type="checkbox" id="${listBackup[item].name}" name="${listBackup[item].name}" checked />
            </div>
        </td>
    </tr>`
					}

					document.getElementById('formImportBackup').innerHTML = html
					document.querySelector('input[name="archive"]').value = response.filePath

					const backUp = document.getElementById('backup_restore')
					backUp.removeAttribute('disabled')
					backUp.classList.remove('disabled')
					document.querySelector('.importBackup').textContent = ''
				}, 1000)
			},
			err => {
				if (!(err && /^(zip|tar|gz)$/i.test(err))) {
					notifySweetAlert('Ошибка! Неверный формат файла', 'error')
					return false
				}
			}
		)
	})
}
