CodeMirror.modeURL = '../admin/vendor/almasaeed2010/adminlte/plugins/codemirror/mode/%N/%N.js'

const code = document.getElementById('code')
if (typeof code !== 'undefined' && code !== null) {
	const editor = CodeMirror.fromTextArea(code, {
			lineWrapping: true,
			lineNumbers: true,
			readOnly: true,
			styleActiveLine: true,
			theme: theme
		}),
		getMode = CodeMirror.findModeByExtension(extension)

	editor.setOption('mode', getMode.mode)
	CodeMirror.autoLoadMode(editor, getMode.mode)
}
