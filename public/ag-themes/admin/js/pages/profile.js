import { modalClose, modalShow, notifySweetAlert, sendRequest } from '../functions.js'
import { dataTables } from '../main.js'

dataTables.on('click', 'tbody tr', function () {
	const id = this.children[0].textContent

	sendRequest(adminpath + '/ajax/get-audit', { id, csrf }, response => {
		modalShow()
		document.getElementById('modal-title').textContent = 'Audit: ' + id
		document.getElementById('modal-body').innerHTML = '<pre>' + response + '</pre>'
	})
})

document.querySelectorAll('.clearCache').forEach(element => {
	element.addEventListener('click', e => {
		const url = e.currentTarget.dataset.url,
			session = e.currentTarget.dataset.session,
			txtSuccess = e.currentTarget.dataset.success,
			txtError = e.currentTarget.dataset.error

		Sweetalert2.fire({
			title: 'Вы уверены?',
			text: 'Данное действие завершит сессию пользователя!',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Завершить'
		}).then(result => {
			if (result.value) {
				sendRequest(url, { session, csrf }, res => {
					const json = JSON.parse(res)
					if (json.isCancel) {
						const sessionElement = document.getElementById('session-' + session)
						if (sessionElement !== null) {
							sessionElement.classList.add('bg-red-light')
							sessionElement.classList.remove('bg-green-light')
						}
						if (e.currentTarget instanceof Element) {
							e.currentTarget.remove()
						}

						notifySweetAlert(txtSuccess, 'success')
					} else {
						notifySweetAlert(txtError, 'error')
					}
					if (json.isExit) {
						window.location.reload()
					}
				})
			}
		})

		return false
	})
})
