import { notifySweetAlert, sendRequest } from '../functions.js'
import { dataTables } from '../main.js'

const cacheClearButton = document.getElementById('cacheClear')
if (cacheClearButton !== null) {
	cacheClearButton.addEventListener('click', event => {
		const th = event.currentTarget,
			url = th.dataset.url,
			key = th.dataset.key,
			id = th.dataset.id,
			txtSuccess = th.dataset.success,
			txtError = th.dataset.error

		Sweetalert2.fire({
			title: 'Вы уверены?',
			text: 'Данное действие очистит весь лог ошибок!',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Очистить'
		}).then(result => {
			if (result.value)
				sendRequest(url, { key, csrf }, res => {
					if (id === undefined) {
						const json = JSON.parse(res)
						if (json.isClear) {
							dataTables.table().clear().draw()
							notifySweetAlert(txtSuccess, 'success')
						} else {
							notifySweetAlert(txtError, 'error')
						}
					} else {
						document.getElementById(id).innerText = '0 byte'
					}
				})
		})
	})
}
