import { sendRequest } from './functions.js'

let cmdhistory = [],
	histcurrent = 0,
	histlatest = ''

const ansi_up = new AnsiUp(),
	jConsole = $('#console'),
	jInputConsole = $('#c-input')

function addConsoleLine(line) {
	line = line.replace(/\u001b[\(\)][AB012]/g, '')
	line = ansi_up.ansi_to_html(line)
	jConsole.append('<div class="line">' + line + '</div>')

	let lines = $('#console > *')
	if (lines.length > 1000) {
		for (var i = 0; i < lines.length - 1000; i++) {
			lines[i].remove()
		}
	}
	jConsole.scrollTop(600000)
}

function clearDataConsole() {
	sendRequest(adminpath + '/terminal/clear-session', { csrf }, function () {
		localStorage['cmdhistory'] = ''
		cmdhistory = []
		clearConsole()
	})
}

function clearConsole() {
	jConsole.html('')
	jInputConsole.focus()
}

function getLog() {
	sendRequest(adminpath + '/terminal/ajax', { cmdhistory, csrf }, function (data) {
		const lines = JSON.parse(data)
		if (lines[0] !== 'NO' && lines['update'] !== 'NO') {
			for (const line in lines) {
				addConsoleLine(lines[line])
			}
		}
		$.removeCookie('ARPEKS_SEC', { path: '/' })
	})
}

jInputConsole.keyup(function (e) {
	let tmp = ''

	if (e.keyCode === 13) {
		let val = jInputConsole.val()
		if (val.substring(0, 1) === '/') {
			val = val.substring(1, val.length)
		}

		if (val.trim() !== '') {
			cmdhistory.push({
				command: val,
				time: Math.floor(Date.now() / 1000)
			})
			localStorage['cmdhistory'] = JSON.stringify(cmdhistory)
		}
		histcurrent = 0

		jInputConsole.val('')
		setTimeout(getLog, 500)
	} else if (e.keyCode === 38) {
		if (cmdhistory.length > 0) {
			if (histcurrent === 0) {
				histlatest = jInputConsole.val()
			}
			if (histcurrent < cmdhistory.length) {
				histcurrent++
			}

			tmp = cmdhistory[cmdhistory.length - histcurrent]
			jInputConsole.val(tmp.command)
		}
	} else if (e.keyCode === 40) {
		if (histcurrent > 0) {
			histcurrent--
		}
		if (histcurrent === 0 && cmdhistory.length > 0) {
			jInputConsole.val(histlatest)
		} else {
			tmp = cmdhistory[cmdhistory.length - histcurrent]
			jInputConsole.val(tmp.command)
		}
	}
})

jConsole.click(function () {
	if (window.getSelection().toString().length === 0) {
		jInputConsole.focus()
	}
})

$(document).ready(function () {
	jInputConsole.focus()
	if (localStorage['cmdhistory'] !== undefined && localStorage['cmdhistory'] !== '')
		cmdhistory = JSON.parse(localStorage['cmdhistory'])

	sendRequest(adminpath + '/terminal/clear-session', { csrf })

	let timerIdInfo = setTimeout(function getTerminalLog() {
		getLog()
		timerIdInfo = setTimeout(getTerminalLog, 6000)
	}, 500)
})
