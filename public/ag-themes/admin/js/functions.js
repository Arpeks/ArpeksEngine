export const sendRequest = async (url, data, callback, error) => {
	try {
		const response = await fetch(url, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			body: new URLSearchParams(data).toString()
		})

		if (!response.ok) {
			throw new Error(`HTTP error! status: ${response.status} - ${response.statusText}`)
		}

		const responseText = await response.text()
		if (typeof callback === 'function') {
			callback(responseText)
		}
	} catch (err) {
		if (typeof error === 'function') {
			error(err.message)
		}
	} finally {
		console.debug({
			url: {
				typeof: typeof url,
				value: url
			},
			data: {
				typeof: typeof data,
				value: data
			},
			callback: {
				typeof: typeof callback,
				value: callback
			},
			error: {
				typeof: typeof error,
				value: error
			}
		})
	}
}

export const notifySweetAlert = (text = 'No text', type = 'success', seconds = 5) => {
	Sweetalert2.mixin({
		toast: true,
		position: 'top',
		showConfirmButton: false,
		timerProgressBar: true,
		timer: seconds * 1000,
		didOpen: toast => {
			toast.addEventListener('mouseenter', Sweetalert2.stopTimer)
			toast.addEventListener('mouseleave', Sweetalert2.resumeTimer)
		}
	})
		.fire({
			icon: type,
			title: text
		})
		.catch(console.error)
}

export const isNumeric = n => !isNaN(parseFloat(n)) && isFinite(n)

export const fadeOut = (element, duration, callback) => {
	let opacity = 1
	const step = 16 / duration // 16 ms ~ 60 frames per second

	const fade = () => {
		opacity -= step
		if (opacity <= 0) {
			opacity = 0
			element.style.opacity = opacity
			if (callback) callback()
		} else {
			element.style.opacity = opacity
			requestAnimationFrame(fade)
		}
	}

	requestAnimationFrame(fade)
}

/**
 * @param {string} formID
 * @param {'string'|'object'} returnType
 * @return {{}|string}
 */
export const serializeForm = (formID = '', returnType = 'object') => {
	const formData = new FormData(document.getElementById(formID))
	const obj = Object.fromEntries(formData.entries())

	return returnType === 'string' ? new URLSearchParams(obj).toString() : obj
}

export const modalClose = () => {
	const modal = document.getElementById('modal')
	if (modal) {
		modal.classList.remove('show')
		;['modal_close', 'modal-backdrop'].forEach(id => {
			document.getElementById(id)?.removeEventListener('click', modalClose)
		})
	}
}

export const modalShow = () => {
	const modal = document.getElementById('modal')
	if (modal) {
		modal.classList.add('show')
		;['modal_close', 'modal-backdrop'].forEach(id => {
			document.getElementById(id)?.addEventListener('click', modalClose)
		})
	}
}

/**
 *
 * @param {Date} date
 * @param {string} format
 * @return {string}
 */
export const getDateFormat = (date = new Date(), format = 'dd.MM.yyyy HH:mm:ss') => {
	const options = {
		day: '2-digit',
		month: '2-digit',
		year: 'numeric',
		hour: '2-digit',
		minute: '2-digit',
		second: '2-digit'
	}
	return new Intl.DateTimeFormat('default', options)
		.formatToParts(date)
		.reduce((acc, part) => acc.replace(part.type, part.value), format)
}

export const toUrlEncoded = obj => new URLSearchParams(obj).toString()

export const fetchPost = async (url, data) => {
	const response = await fetch(url, {
		method: 'POST',
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
		body: toUrlEncoded(data)
	})

	if (!response.ok) {
		throw new Error(`Network error: ${response.statusText}`)
	}

	return response.json()
}

export const smoothScroll = (targetPosition, duration) => {
	const startPosition = window.scrollY || document.documentElement.scrollTop
	const distance = targetPosition - startPosition
	let startTime = null

	const ease = (t, b, c, d) => {
		t /= d / 2
		if (t < 1) return (c / 2) * t * t + b
		t--
		return (-c / 2) * (t * (t - 2) - 1) + b
	}

	const animation = currentTime => {
		if (!startTime) startTime = currentTime
		const timeElapsed = currentTime - startTime
		const run = ease(timeElapsed, startPosition, distance, duration)
		window.scrollTo(0, run)
		if (timeElapsed < duration) requestAnimationFrame(animation)
	}

	requestAnimationFrame(animation)
}

export const sleep = ms => new Promise(resolve => setTimeout(resolve, isNumeric(ms) ? ms : 1000))

export const getRandomInt = (min, max) => {
	min = Math.ceil(min)
	max = Math.floor(max)
	return Math.floor(Math.random() * (max - min + 1)) + min
}
