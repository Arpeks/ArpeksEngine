import { notifySweetAlert, sendRequest } from './functions.js'

const tfaCode = document.getElementsByClassName('tfaCode')

for (let i = 0; i < tfaCode.length; i++) {
	const currentInput = tfaCode[i]

	currentInput.addEventListener('input', e => {
		if (!/^[0-9]*$/.test(e.target.value)) {
			e.target.value = ''
		}

		if (currentInput.value.length === 1) {
			const nextInput = currentInput.nextElementSibling
			if (nextInput != null) {
				nextInput.focus()
			} else {
				currentInput.select()
			}
		}
		checkAllFieldsFilled().then(r => {})
	})

	currentInput.addEventListener('keydown', e => {
		if (e.keyCode === 8 && currentInput.value.length === 0) {
			const prevInput = currentInput.previousElementSibling
			if (prevInput != null) {
				prevInput.focus()
			}
		}
		checkAllFieldsFilled().then(r => {})
	})

	currentInput.addEventListener('paste', e => {
		if (!/^[0-9]*$/.test(e.target.value)) {
			e.target.value = ''
		}

		const clipboardData = e.clipboardData || window.clipboardData
		const pastedData = clipboardData.getData('text')
		const inputs = document.getElementsByClassName('tfaCode')
		let currentInputIndex = Array.from(inputs).findIndex(input => input === e.target)
		let remainingLength = pastedData.length
		const ConstCharactersToAdd = pastedData.length

		for (let i = currentInputIndex; i < inputs.length; i++) {
			let input = inputs[i]
			let charactersToAdd = Math.min(remainingLength, 1)
			input.value = pastedData.substring(
				pastedData.length - remainingLength,
				pastedData.length - remainingLength + charactersToAdd
			)
			remainingLength -= charactersToAdd
			if (remainingLength <= 0) {
				break
			}
		}
		inputs[ConstCharactersToAdd - 1].focus()
	})
}

const checkAllFieldsFilled = async () => {
	let allFilled = true
	let text = ''

	for (let i = 0; i < tfaCode.length; i++) {
		if (tfaCode[i].value === '') {
			allFilled = false
		} else {
			text += tfaCode[i].value
		}
	}

	if (allFilled) {
		toggleFieldsTFA()

		sendRequest(
			adminpath + '/user/tfa',
			{
				code: text,
				csrf
			},
			res => {
				const ret = JSON.parse(res)
				if (ret[0] === 'OK') {
					window.location.href = adminpath
				} else {
					toggleFieldsTFA(true)
					for (let i = 0; i < tfaCode.length; i++) {
						tfaCode[i].value = ''
					}
					tfaCode[0].focus()
					notifySweetAlert(ret[1], 'error', 5)
				}
			}
		)
	}
}

const toggleFieldsTFA = (visible = false) => {
	const black = document.getElementById('loaderTFA')

	for (let i = 0; i < tfaCode.length; i++) {
		if (visible) tfaCode[i].removeAttribute('disabled')
		else tfaCode[i].setAttribute('disabled', 'disabled')
	}

	if (visible) {
		black.style.display = 'none'
	} else {
		black.style.display = 'block'
	}
}

document.getElementById('sendTFA').addEventListener('click', e => {
	e.preventDefault()
	checkAllFieldsFilled().then(r => {})
})
