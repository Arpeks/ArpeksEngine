import { fadeOut, fetchPost, getDateFormat, notifySweetAlert, sendRequest, serializeForm } from './functions.js'

export const dataTables = new DataTable('.dataTable', {
	lengthChange: false,
	processing: true,
	stateSave: true,
	stateDuration: 30,
	searching: true,
	ordering: false,
	retrieve: true
})

if (typeof dataTableLanguage !== 'undefined') dataTables.language = dataTableLanguage

document.querySelectorAll('a.delete').forEach(val => {
	val.addEventListener('click', e => {
		e.preventDefault()

		Sweetalert2.fire({
			title: 'Вы уверены?',
			text: 'Данное действие нельзя будет отменить!',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Да, я уверен!'
		}).then(result => {
			if (result.value) document.location.replace(val.href)
		})
		return false
	})
})

window.addEventListener('DOMContentLoaded', event => {
	const preloader = document.getElementById('preloader'),
		loader = preloader.querySelector('.preloader__loader')

	fadeOut(preloader, 400, () => {
		preloader.style.display = 'none'
	})

	$('.timestamp').inputmask('datetime')
	$('[data-mask]').inputmask()
	$('#compose-textarea').summernote({
		height: 300,
		tabSize: 2,
		tooltip: false
	})

	$('[data-toggle="tooltip"]').tooltip({
		delay: 100
	})

	document.querySelectorAll('.nav-sidebar a, .sidebar-menu a').forEach(element => {
		const location = window.location.protocol + '//' + window.location.host + window.location.pathname,
			link = element.href

		if (link === location) {
			element.classList.add('active')
			element.closest('.treeview')?.classList.add('menu-open')
			element.closest('.treeview')?.children[0].classList.add('active')
		}
	})

	const buttonSingle = $('#single')
	const importBackup = $('#importBackup')

	if (importBackup.length) {
		new AjaxUpload(importBackup, {
			action: adminpath + importBackup.data('url') + '?upload=1',
			data: { name: importBackup.data('name') },
			name: importBackup.data('name'),
			onSubmit: function (file, ext) {
				if (!(ext && /^(zip|tar|gz)$/i.test(ext))) {
					notifySweetAlert('Ошибка! Неверный формат файла', 'error')
					return false
				}
				importBackup.closest('.file-upload').find('.overlay').css({ display: 'flex' })
			},
			onComplete: function (file, response) {
				setTimeout(function () {
					importBackup.closest('.file-upload').find('.overlay').css({ display: 'none' })
					response = JSON.parse(response)
					document.querySelector('.' + importBackup.dataset.name).innerHTML = response.filename
					const listBackup = response.backup
					let html = ''

					for (let item in listBackup) {
						html +=
							'<tr><td><label class="font-weight-normal" for="' +
							listBackup[item].name +
							'">' +
							listBackup[item].text +
							'</label></td><td><div class="form-check has-feedback"><input type="checkbox" id="' +
							listBackup[item].name +
							'" name="' +
							listBackup[item].name +
							'" checked /></div></td></tr>'
					}

					document.getElementById('formImportBackup').innerHTML = html
					document.querySelector("input[name='archive']").value = response.filePath
					document.getElementById('backup_restore').classList.remove('disabled')
					document.getElementById('backup_restore').removeAttribute('disabled')
					document.querySelector('#importBackupArchive option:selected').removeAttribute('selected')
					document.getElementById('nullFileImport').setAttribute('selected', true)
				}, 1000)
			}
		})
	}

	if (buttonSingle.length) {
		new AjaxUpload(buttonSingle, {
			action: adminpath + buttonSingle.data('url') + '?upload=1',
			data: { name: buttonSingle.data('name') },
			name: buttonSingle.data('name'),
			onSubmit: function (file, ext) {
				if (!(ext && /^(jpg|png|jpeg|gif)$/i.test(ext))) {
					notifySweetAlert('Ошибка! Неверный формат файла', 'error')
					return false
				}
				buttonSingle.closest('.file-upload').find('.overlay').css({ display: 'flex' })
			},
			onComplete: function (file, response) {
				setTimeout(function () {
					buttonSingle.closest('.file-upload').find('.overlay').css({ display: 'none' })

					response = JSON.parse(response)
					$('.' + buttonSingle.data('name')).html(
						'<img alt="Изображение" src="/tmp/' + response.file + '.cache" style="max-height: 150px">'
					)
				}, 1000)
			}
		})
	}
})

document.getElementById('formBugReport').addEventListener('submit', event => {
	event.preventDefault()
	const formData = $('#formBugReport').serialize()
	$('#modal-bug-report').modal('hide')

	sendRequest(adminpath + '/ajax/bug-report', formData, res => {
		if (res === '') {
			$('#modal-error-SMTP-deactivated').modal('show')
			window.location.href = path + '/smtp-deactivated.php'
		} else {
			const json = JSON.parse(res)
			if (json.isSuccess) {
				notifySweetAlert(json.success, 'success')
			} else {
				if (json.smtp === false && json.csrf !== false) {
					$('#modal-error-SMTP-deactivated').modal('show')
					window.location.href = path + '/smtp-deactivated.php'
				} else {
					notifySweetAlert(json.error, 'error')
				}
			}
		}

		const message = document.getElementById('b-message')
		message.value = ''
		message.classList.remove('is-valid', 'is-invalid')
	})
})

document.getElementById('select_language').addEventListener('change', event => {
	const value = event.currentTarget.value
	document.getElementById('flag_country').setAttribute('src', 'img/flags/' + value + '.png')

	sendRequest(adminpath + '/ajax/change-language', { newLang: value, csrf }, res => {
		const ret = JSON.parse(res)[0]
		if (ret === 'OK') {
			location.reload()
		}
	})
})

const updateCore = document.getElementById('updateCore_btn')
if (updateCore !== null) {
	const updateProgress = value => {
		const percentageText = document.getElementById('updateCore_text'),
			progress = document.getElementById('updateCore_progress')
		if (value > 100) value = 100
		percentageText.textContent = value + '%'
		progress.style.width = value + '%'
	}

	const addLogs = text => {
		const logs = document.getElementById('updateCore_logs'),
			txt = '[' + getDateFormat() + '] ' + text

		if (logs.textContent !== '') logs.textContent = logs.textContent + '\n' + txt
		else logs.textContent = txt
		console.log(txt)
	}

	const formUpdate = document.getElementById('updateCoreForm')
	formUpdate.addEventListener('submit', async event => {
		event.preventDefault()

		const url = formUpdate.getAttribute('action'),
			percentage = Math.round(100 / 7),
			serialize = serializeForm('updateCoreForm', 'object')

		serialize.csrf = undefined

		let data = {},
			step = 0,
			listFiles = {}

		document.getElementById('updateCore_logs').textContent = ''

		data.csrf = serialize.csrf
		updateProgress(percentage * step)

		document.getElementById('updateCore_btn').disabled = true
		document.getElementById('updateCore_main').classList.remove('d-none')

		addLogs('Начало обновления')

		try {
			const c1 = await fetchPost(url, { action: 'downloadUpdate', csrf })
			updateProgress(++step * percentage)
			addLogs('Архив с обновлением загружен')

			const c2 = await fetchPost(url, { action: 'getFileListUpdate', csrf })
			updateProgress(++step * percentage)
			addLogs('Список изменений получен')
			listFiles = JSON.parse(c2).data

			console.log(listFiles)

			const c3 = await fetchPost(url, { action: 'techwork', enable: true, csrf })
			updateProgress(++step * percentage)
			addLogs('Активация режима технического обслуживания')

			// const c4 = await fetchPost(url, { action: 'fileDelete', listFiles: listFiles.delete, csrf });
			// updateProgress(++step * percentage);
			// addLogs('Установка обновления (Этап 1/2)');
			//
			// const c5 = await fetchPost(url, { action: 'fileExtract', listFiles: listFiles.files, csrf });
			// updateProgress(++step * percentage);
			// addLogs('Установка обновления (Этап 2/2)');

			const c6 = await fetchPost(url, { action: 'techwork', enable: false, csrf })
			updateProgress(++step * percentage)
			addLogs('Отключение режима технического обслуживания')

			const c7 = await fetchPost(url, { action: 'deleteArchive', csrf })
			updateProgress(100)
			addLogs('Завершение обновления')

			setTimeout(function () {
				location.reload()
			}, 5000)
		} catch (error) {
			console.error('Error during the update process:', error)
			addLogs('Ошибка во время процесса обновления на этапе: ' + step)
		}
	})
}
