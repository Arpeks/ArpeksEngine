/**
 * @typedef {Object} Response
 * @property {boolean} isSuccess - Флаг успешного выполнения запроса
 * @property {boolean} isCancel - Флаг завершения запроса
 * @property {boolean} isExit - Флаг выхода
 * @property {string} error - Сообщение об ошибке, если она возникла при выполнении запроса
 * @property {boolean} isNew - [Chat] Указывает на новое сообщение в чате
 * @property {integer} count_message - [Chat] Кол-во сообщений в чате
 * @property {string} fileSize - [Scan] Размер в человеко понятном формате
 * @property {string} fileDate - [Scan] Дата изменения файла
 * @property {string} filePath - [Backup] Путь к файлу
 * @property {string} backup - [Backup] Список доступных бекапов
 */
