<?php

/**
 * @author Arpeks
 * @version 235
 */

require_once __DIR__ . "/vendor/autoload.php";

use core\base\EngineException;
use JetBrains\PhpStorm\Pure;

/**
 * Загружает файл и возвращает результат загрузки
 * @param string $filename_old Название существующего файла
 * @param string $filename_new Название файла после загрузки. Хранит название файла из сессии
 * @param string $absolutePathFolder Абсолютный путь к папке, куда будет загружен файл
 * @return string
 */
function uploadImage(string $filename_old, string $filename_new, string $absolutePathFolder): string
{
	$filePath = rtrim($absolutePathFolder, "/") . "/" . $filename_old;

	if (file_exists($filePath))
		@unlink($filePath);

	$webp = getPath($filePath) . DIRECTORY_SEPARATOR . getFileName($filePath) . ".webp";
	if (file_exists($webp))
		@unlink($webp);
	unset($webp);

	if (!file_exists($absolutePathFolder)) mkdir($absolutePathFolder);

	if (!empty($filename_new)) {
		$filePath = rtrim($absolutePathFolder, "/") . "/" . $filename_new;
		$filename_old = $filename_new;
	}

	if (copy(WWW . '/tmp/' . $filename_old . ".cache", $filePath)) {
		@unlink(WWW . '/tmp/' . $filename_old . ".cache");
	}

	return $filePath;
}

/**
 * Возвращает HTML-код тела письма
 * @param array $params Параметры передаваемые в тело письма
 * @return string HTML-код тела письма
 * @throws EngineException Вызывается, если файл отсутствует
 * @since 232
 */
function getBodyMail(array $params = array()): string
{
	extract($params);

	if (file_exists(WWW_THEME . "/" . LAYOUT . "/template/mail.php")) {
		ob_start();
		require_once WWW_THEME . "/" . LAYOUT . "/template/mail.php";
		return ob_get_clean();
	}
	throw new EngineException("File `mail.php` not found", EngineException::FILE_NOT_FOUND);
}

/**
 * Функция получения шаблонов сайта для выборки
 * @return array Массив всех доступных шаблонов
 */
function getLayouts(): array
{
	$return = [];
	foreach (scandir(WWW_THEME . "/") as $i => $file) {
		if ($file[0] == '.') continue;
		if ($file == 'admin') continue;
		$return[] = $file;
	}
	return $return;
}

/**
 * Генерирует список файлов выбранного шаблона
 * @param string $pathTemplate Путь к шаблону
 * @param array $files Ссылка на изначальный список файлов
 * @param string $indexName Нужен для рекурсивного вывода пути к вложенным файлам.<br>Если файл не вложен в папку, то оставьте пустым
 * @return array Массив списка файлов
 * @since 228 Theme Editor Update
 */
function generateThemeListFiles(string $pathTemplate, array &$files, string $indexName = ""): array
{
	foreach (scandir($pathTemplate) as $file) {
		if ($file[0] == '.') continue;
		if (is_dir($pathTemplate . "/" . $file)) {
			generateThemeListFiles($pathTemplate . "/" . $file, $files, $indexName . "$file/");
			continue;
		}
		$files[trim($indexName . $file, "/")]['path'] = $pathTemplate . "/" . $file;
	}
	return $files;
}

/**
 * Генерирует древовидное меню на основе переданного массива
 * @param array $menuReg Массив данных меню
 * @return string HTML-код меню
 * @since 227 UI AdminPanel Update
 */
function generateAdminMenu(array $menuReg): string
{
	$html = "";
	$search = array(
		'ae_delimiter_modules',
		'ae_delimiter_plugins'
	);

	foreach ($menuReg as $item) {
		if (in_array($item['name'], $search)) {
			$html .= "<li class='nav-header' style='border-bottom: 1px solid #4f5962'>{$item['url']}</li>";
			continue;
		}

		$html .= '<li class="nav-item';
		if (!empty($item['is_tree']))
			$html .= ' treeview';
		$html .= '">';

		$html .= "<a href='" . $item['url'] . "'  class='nav-link'";
		if (!empty($item['blank']))
			$html .= ' target="_blank"';
		$html .= ">";


		if (isset($item['icon']) && $item['icon']) {
			$html .= '<i class="nav-icon ' . $item['icon'] . '"></i>';
		}

		$html .= '<p>' . $item['name'];
		if (!empty($item['is_tree']) || !empty($item['is_append'])) {
			$html .= '<i class="fas fa-angle-left right"></i>';
			if ($item['is_append'] && !empty($item['append'])) {
				foreach ($item['append'] as $append)
					$html .= '<span class="badge badge-info right ' . $append['class'] . '" style="background-color: ' . $append['color'] . '">' . $append['text'] . '</span>';
			}
		}
		$html .= '</p>';
		$html .= '</a>';

		if (!empty($item['is_tree'])) {
			$html .= '<ul class="nav nav-treeview">';
			foreach ($item['tree'] as $tree) {
				$html .= generateAdminMenu([$tree]);
			}
			$html .= '</ul>';
		}
		$html .= '</li>';
	}

	return $html;
}

/**
 * Сравнивает значения двух массивов по ключу 'price'
 * @param array $a Первый массив для сравнения
 * @param array $b Второй массив для сравнения
 * @param string $field Ключ для сравнения
 * @return int Возвращает целое, которое меньше, равно или больше нуля, если первый аргумент является соответственно меньшим, равным или большим, чем второй.
 */
#[Pure] function usortByField(array $a, array $b, string $field = 'price'): int
{
	if (compareArrayByIndex($a, $b, $field, AE_COMPARE_LESS))
		return 1;
	return -1;
}

/**
 * Преобразует массив в XML
 * @param array $data Данные дня преобразования в XML
 * @param SimpleXMLElement $xml_data Ссылка на объект SimpleXMLElement
 * @since 232
 */
function array2xml(array $data, SimpleXMLElement &$xml_data): void
{
	foreach ($data as $key => $value) {
		if (is_array($value)) {
			if (is_numeric($key)) {
				$key = 'item' . $key;
			}
			$subNode = $xml_data->addChild($key);
			array2xml($value, $subNode);
		} else {
			$xml_data->addChild("$key", htmlspecialchars("$value"));
		}
	}
}

/**
 * Возвращает строку с прямыму слешами
 * @param string $str Строка с обратными слешками
 * @return string
 */
function replaceBack2Slash(string $str): string
{
	return str_replace("\\", "/", $str);
}
