import { sendRequest, smoothScroll } from '../../admin/js/functions.js'

window.addEventListener('DOMContentLoaded', event => {
	document.querySelectorAll('input').forEach(key => {
		key.setAttribute('autocomplete', 'off')
	})

	const navBars = document.getElementsByClassName('site_navbar')
	for (let i = 0; i < navBars.length; i++) {
		navBars[i].addEventListener('click', event => {
			const id = navBars[i].dataset.href

			// TODO: Перевести на CommonJS
			const targetPosition =
				document.getElementById(id).offsetTop - document.getElementById('navbar').offsetHeight
			smoothScroll(targetPosition, 2000)
		})
	}
})

// +=========================================+
// |                FUNCTIONS                |
// +=========================================+
document.getElementById('f-btn').addEventListener('click', event => {
	event.preventDefault()

	const name = document.getElementById('f-name').value,
		email = document.getElementById('f-envelope').value,
		message = document.getElementById('f-pencil').value,
		subject = document.getElementById('f-tag').value,
		csrf = document.querySelector('#contact_form input[name=csrf]').value,
		jsAlert = document.getElementById('js-alert')

	if (name === '' || message === '' || subject === '' || email === '') {
		jsAlert.parentElement.classList.add('show', 'alert-danger')
		jsAlert.parentElement.classList.remove('alert-success')
		jsAlert.innerHTML = 'Вы не заполнили все обязательные поля формы'
	} else
		sendRequest(path + '/main/send-contact', { name, email, subject, message, csrf }, res => {
			const json = JSON.parse(res)
			if (json.status === 'ok') {
				jsAlert.parentElement.classList.add('show', 'alert-success')
				jsAlert.parentElement.classList.remove('alert-danger')
				jsAlert.innerHTML = 'Письмо успешно отправлено'
				document.getElementById('f-name').value = ''
				document.getElementById('f-envelope').value = ''
				document.getElementById('f-tag').value = ''
				document.getElementById('f-pencil').value = ''
			} else {
				jsAlert.parentElement.classList.add('show', 'alert-danger')
				jsAlert.parentElement.classList.remove('alert-success')
				jsAlert.innerHTML = json.description
			}
		})

	setTimeout(() => {
		document.getElementById('js-alert').parentElement.classList.remove('show')
	}, 2000)
})
