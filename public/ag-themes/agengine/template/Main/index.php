<!--Main layout-->
<main class="mt-5">
    <div class="container" id="mainContent">

        <!--Section: Best Features-->
        <section id="best-features" class="text-center">

            <!-- Heading -->
            <h2 class="mb-5 font-weight-bold">Лучшие качества</h2>

            <!--Grid row-->
            <div class="row d-flex justify-content-center mb-4">

                <!--Grid column-->
                <div class="col-md-8">

                    <!-- Description -->
                    <p class="grey-text">
                        Далеко-далеко за словесными горами в стране, гласных и согласных живут рыбные тексты.
                        Которое журчит то, толку власти, не взгляд продолжил выйти щеке все моей, вершину пунктуация
                        пустился решила сих. Алфавит, дорогу, предупреждал.
                    </p>

                </div>
                <!--Grid column-->

            </div>
            <!--Grid row-->

            <!--Grid row-->
            <div class="row">

                <!--Grid column-->
                <div class="col-md-4 mb-5">
                    <i class="fa fa-camera-retro fa-4x orange-text"></i>
                    <h4 class="my-4 font-weight-bold">Опыт</h4>
                    <p class="grey-text">
                        Составитель, знаках журчит до, вскоре меня одна пор запятых ты выйти, единственное имеет жизни
                        но дал буквоград решила текстами
                        ipsum подзаголовок переулка напоивший семь домах дороге продолжил они страну.
                        Взобравшись.
                    </p>
                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-md-4 mb-1">
                    <i class="fa fa-heart fa-4x orange-text"></i>
                    <h4 class="my-4 font-weight-bold">Счастье</h4>
                    <p class="grey-text">
                        Эта агенство большого бросил одна всемогущая, гор они имени приставка собрал даль своих там
                        выйти встретил живет.
                        Залетают ты, мир текст себя, меня инициал буквоград, использовало семантика алфавит текстами
                        переписали.
                    </p>
                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-md-4 mb-1">
                    <i class="fa fa-bicycle fa-4x orange-text"></i>
                    <h4 class="my-4 font-weight-bold">Приключение</h4>
                    <p class="grey-text">
                        Пустился вопрос, семь напоивший безопасную, силуэт, свой до ручеек грамматики по всей
                        составитель это домах.
                        Заманивший домах текста напоивший, запятых переписали наш оксмокс, гор продолжил переписывается
                        не семантика это океана то!
                    </p>
                </div>
                <!--Grid column-->

            </div>
            <!--Grid row-->

        </section>
        <!--Section: Best Features-->

        <!--Section: gallery-->
		<?php if (!empty($gallery)): ?>
            <hr class="my-5">
            <section id="gallery" class="text-center">
                <h2 class="mb-5 font-weight-bold">Галерея</h2>
                <div class="row">
					<?php $i = 3;
					foreach ($gallery as $object) : ?>

                        <div class="col-lg-4 col-md-<?php if ($i == 3): echo "12";
							$i = 0;
						else: echo "6"; endif; ?> mb-4">
                            <div class="view overlay hoverable">
                                <img src="img/gallery/<?= $object->img; ?>" alt="<?= $object->title; ?>"
                                     class="img-fluid">
                                <div class="mask flex-center"></div>
                            </div>
                            <h4 class="my-4 font-weight-bold"><?= $object->title; ?></h4>
                            <p class="grey-text"><?= $object->description; ?></p>
                        </div>
						<?php $i++; endforeach; ?>
                </div>
            </section>
		<?php endif; ?>

        <!--Section: gallery-->

        <hr class="my-5">

        <!--Section: Contact-->
        <section id="contact">

            <!-- Heading -->
            <h2 class="mb-5 font-weight-bold text-center">Контакты</h2>

            <!--Grid row-->
            <div class="row">

                <!--Grid column-->
                <div class="col-lg-5 col-md-12">
                    <h4 class="text-center">Отправить сообщение</h4>
                    <form class="p-5" id="contact_form">
                        <div class="md-form form-sm">
                            <i class="fa fa-user prefix grey-text"></i>
                            <input required type="text" id="f-name" name="name" class="form-control form-control-sm"/>
                            <label for="f-name">Ваше имя*</label>
                        </div>
                        <div class="md-form form-sm">
                            <i class="fa fa-envelope prefix grey-text"></i>
                            <input required type="text" id="f-envelope" name="envelope"
                                   class="form-control form-control-sm"/>
                            <label for="f-envelope">Ваша почта*</label>
                        </div>
                        <div class="md-form form-sm">
                            <i class="fa fa-tag prefix grey-text"></i>
                            <input required type="text" id="f-tag" name="tag" class="form-control form-control-sm"/>
                            <label for="f-tag">Тема*</label>
                        </div>
                        <div class="md-form form-sm textarea-label">
                            <i class="fa fa-pencil-alt prefix grey-text"></i>
                            <textarea id="f-pencil" name="pencil" class="form-control form-control-sm p-1"
                                      rows="3"></textarea>
                            <label for="f-pencil" class="pl-1">Ваше сообщение*</label>
                        </div>
                        <div class="text-center mt-4">
                            {input_csrf}
                            <button type="button" class="btn btn-primary waves-effect waves-light" id="f-btn">Отправить
                                <i class="fas fa-paper-plane ml-1"></i></button>
                        </div>
                    </form>
                </div>

                <!--Grid column-->

                <!--Grid column-->
                <div class="col-lg-7 col-md-12">
					<?php if (!empty($app['phone'])) : ?>
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <p><i class="fa fa-phone fa-1x mr-2 grey-text"></i><a href="tel:{phone_full_url}">{phone_full}</a>
                                </p>
                            </div>
                        </div>
					<?php endif; ?>

					<?php if (!empty($app['admin_email'])) : ?>
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <p><i class="fa fa-envelope fa-1x mr-2 grey-text"></i><a
                                            href="mailto:<?= $app['admin_email'] ?>"><?= $app['admin_email'] ?></a></p>
                            </div>
                        </div>
					<?php endif; ?>

					<?php if (!empty($app['address'])) : ?>
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <p><i class="fa fa-map fa-1x mr-2 grey-text"></i><?= $app['address'] ?></p>
                            </div>
                        </div>
					<?php endif; ?>

                    <div class="row">
                        <!--Map-->
                        <div class="z-depth-1-half map-container mb-5 col-lg-12 col-md-12">
                            <iframe src="https://yandex.ru/map-widget/v1/-/CWbeAUyc" width="100%" height="400"
                                    frameborder="0"></iframe>
                        </div>
                    </div>

                </div>
                <!--Grid column-->

            </div>
            <!--Grid row-->

        </section>
        <!--Section: Contact-->

    </div>
</main>
<!--Main layout-->
