<div style="background-color: #F4F4F4; width: 100%">
    <div style="color:#4f4f4f;font:13px/1.4em 'verdana' , 'arial'; width:50%; margin: auto; padding: 30px 0;">

        <div style="border:1px solid #d0d6e8;border-collapse:collapse;">
            <div style="background-color: #5C7DC4; color: #ffffff; font: 1.25em 'verdana' , 'arial', 'sans-serif'; padding: 20px;">
				<?= $app['site_name'] ?>
            </div>
            <div style="background-color: #ffffff; width: 100%;">
                <div style="border-collapse:collapse;color:#4f4f4f;font:1em 'verdana' , 'arial'; padding: 10px;">
					<?= $emailContent ?>
                </div>
            </div>
            <div style="background-color: #455d91; color: #ffffff; font: 0.8em 'verdana' , 'arial', 'sans-serif'; padding: 15px; text-align: center">
				<?= getCopyright() ?>
            </div>
        </div>

    </div>
</div>