<?php init(); ?><!DOCTYPE html>
<html lang="<?= USER_LANGUAGE ?>">

<head>
    <base href="{base_path_layout}">
    {meta}
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta http-equiv="x-ua-compatible" content="ie=edge"/>

    <link href="css/fa-all.min.css" rel="stylesheet"/>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/mdb.min.css" rel="stylesheet"/>
    <link href="css/style.css" rel="stylesheet"/>
</head>

<body>

<!--Main Navigation-->
<header id="home">
    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar" id="navbar">

        <div class="container">

            <!-- Navbar brand -->
            <a class="navbar-brand" href="/">{site_title}</a>

            <!-- Collapse button -->
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarToggler"
                    aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Collapsible content -->
            <div class="collapse navbar-collapse" id="navbarToggler">
                <!-- Links -->

                <div class="col-xl-8 col-md-12">

                    <ul class="navbar-nav mr-auto smooth-scroll">
                        <li class="nav-item">
                            <a data-href="home" class="nav-link waves-effect waves-light site_navbar">Домой</a>
                        </li>
                        <li class="nav-item">
                            <a data-href="best-features" class="nav-link waves-effect waves-light site_navbar">Лучшие
                                качества</a>
                        </li>
						<?php if (!empty($gallery)): ?>
                            <li class="nav-item">
                                <a data-href="gallery"
                                   class="nav-link waves-effect waves-light site_navbar">Галерея</a>
                            </li>
						<?php endif; ?>
                        <li class="nav-item">
                            <a data-href="contact" class="nav-link waves-effect waves-light site_navbar">Контакты</a>
                        </li>
                    </ul>
                    <!-- Links -->
                </div>

                <div class="col-xl-4 col-md-12">

                    <hr class="d-xl-none white"/>

                    <div class="row">
                        <div class="col-8">
                            <!-- Social Icon  -->
                            <ul class="navbar-nav nav-flex-icons float-left">
								<?php $socials->render(); ?>
                            </ul>
                        </div>
                        <div class="col-4">
                            <ul class="navbar-nav btn-group float-right">
                                <li>
                                    <a href="/<?= ADMIN_URI ?>" target="_blank" class="text-white">AdminPanel</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    </hr>


                </div>
                <!-- Collapsible content -->

            </div>

    </nav>
    <!--/.Navbar-->

    <!--Mask-->
    <div id="intro" class="view">

        <div class="mask rgba-black-strong">

            <div class="container-fluid d-flex align-items-center justify-content-center h-100">

                <div class="row d-flex justify-content-center text-center">

                    <div class="col-md-10">

                        <!-- Heading -->
                        <h2 class="display-4 font-weight-bold white-text pt-5 mb-2">{site_name}</h2>

                        <!-- Divider -->
                        <hr class="hr-light">

                        <!-- Description -->
                        <h4 class="white-text my-4">{site_description}</h4>
                        <a class="site_navbar" data-href="mainContent">
                            <button type="button" class="btn btn-outline-white">Далее <i class="ml-2 fas fa-book"
                                                                                         style="font-style: italic"></i></i>
                            </button>
                        </a>
                    </div>

                </div>

            </div>

        </div>

    </div>
    <!--/.Mask-->

</header>
<!--Main Navigation-->
