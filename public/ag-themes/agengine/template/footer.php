<?php

$email = explode("@", $app['admin_email']);
try {
	$punycode = $email[0] . "@" . idn_to_utf8($email[1]);
} catch (Error $e) {
	$punycode = $app['admin_email'];
}

?>

<div class="container">
    <div class="row">
        <div class="mr-2 fix-bottom alert fade" role="alert">
            <span id="js-alert"></span>
        </div>
    </div>
</div>



<!-- Footer -->
<footer class="page-footer font-small unique-color-dark pt-0">
    <div class="primary-color">
        <div class="container">
            <div class="row py-4 d-flex align-items-center">
                <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
                    <h6 class="mb-0 white-text">Мы в социальных сетях</h6>
                </div>
                <div class="col-md-6 xol-lg-7 text-center text-md-right">
					<?php $socials->render(); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-5 mb-4 text-center text-md-left">
        <div class="row mt-3">
            <div class="col-md-6 col-lg-6 col-xl-6 mx-auto mb-4">
                <h6 class="text-uppercase font-weight-bold"><strong>Ссылки</strong></h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px">
                <p><a href="<?= ADMIN ?>" target="_blank" rel="nofollow">Администрирование</a></p>
                <p><a href="https://gitlab.com/Arpeks/ArpeksEngine" target="_blank" rel="nofollow">GitLab</a></p>
                <p><a href="https://mdbootstrap.com" target="_blank" rel="nofollow">Material Design for Bootstrap</a>
                </p>
            </div>

            <div class="col-md-6 col-lg-6 col-xl-6">
                <h6 class="text-uppercase font-weight-bold"><strong>Контакты</strong></h6>
                <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px">
                <p><i class="fa fa-home fa-1x mr-2 grey-text"></i>{address}</p>
                <p><i class="fa fa-envelope fa-1x mr-2 grey-text"></i><a
                            href="mailto:{admin_email}"><?= $punycode ?></a></p>
                <p><i class="fa fa-phone fa-1x mr-2 grey-text"></i><a href="tel:{phone_full_url}">{phone_full}</a></p>

            </div>
        </div>
    </div>

    <div class="container mt-5 mb-4 text-center text-md-left">
        <hr class="white accent-2 mb-4 mt-0 d-inline-block mx-auto w-100">
        <div class="row mt-3">
            <div class='copyright mb-3 col-md-12 text-center'>
                {copyright}
            </div>
        </div>
    </div>
</footer>
<!-- Footer -->

<script>
    const path = '<?= PATH ?>',
			csrf = '{csrf}',
			user_id = <?=$_SESSION['user']['id'] ?? -1?>;
</script>


<script src="js/common.js" defer type="module"></script>
</body>
</html>
