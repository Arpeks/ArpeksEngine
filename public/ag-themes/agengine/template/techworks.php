<!DOCTYPE html>
<html lang="ru">
<head>
    <base href="{base_path_layout}"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="refresh" content="120">
    <title>Сайт находится на обслуживании</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/fa-all.min.css">
    <style>
        body {
            background: url(img/bg-techwork.jpg) center fixed no-repeat;
            background-size: cover;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            font-family: Arial, serif;
            color: white;
        }

        a {
            color: #eee;
        }

        a:hover, a:focus, a:active {
            color: #ddd;
            text-decoration: none;
        }

        hr {
            background-color: white;
            height: 2px;
        }

        div.row:first-child {
            margin-top: 160px;
        }
    </style>
</head>
<body>

<div class="container">

    <div class="row">
        <div class="col-12">
            <div class="h1 text-center">Сайт на обслуживании, зайдите позже!</div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="h5 mt-5">В настоящее время на сайте ведутся технические работы. Зайдите, пожалуйста, позже или
                свяжитесь с нами одним из следующих способов:
            </div>
        </div>
    </div>

    <div class="row h2 mt-5">
        <div class="col-12">
            <i class="fas fa-phone"></i>
            <a href="tel:{phone_full_url}">{phone_full}</a>
        </div>
    </div>

    <div class="row h2">
        <div class="col-12">
            <i class="far fa-envelope"></i>
            <a href="mailto:{admin_email}">{admin_email}</a>
        </div>
    </div>

    <div class="row fixed-bottom">
        <div class="col-12">
            <hr>
            <div class="text-center mb-4 h5">
                {copyright}
            </div>
        </div>
    </div>

</div>

</body>
</html>